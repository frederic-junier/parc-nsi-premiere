site_name: "Première NSI Lycée du Parc"
site_url: https://frederic-junier.gitlab.io/parc-nsi-premiere/
repo_url: https://gitlab.com/frederic-junier/parc-nsi-premiere
edit_uri: tree/main/docs/
site_description: "Site  de  première  NSI  du    Lycée  du  Parc  à Lyon "

#use_directory_urls: true
#voir https://www.mkdocs.org/user-guide/configuration/#docs_dir

nav:
  - Progression: index.md
  - Langages:
      - Chapitre 1 constructions de base d'un langage de programmation: chapitre1.md
      - Chapitre 3 fonctions, spécification et mise au point, portée d'une variable: chapitre3.md
  - Types de bases:
      - Chapitre 8 représentation des entiers: chapitre8.md
      - Chapitre 12 codage des caractères: chapitre12.md
      - Chapitre 14 les flottants: chapitre16.md
  - Types construits:
      - Chapitre 4  tableaux à 1 dimension:
          - Index: chapitre4.md
          - Cours: chapitre6/Cours/tableaux-cours-git.md
      - Chapitre 5 tableaux à 2 dimensions:
          - Index: chapitre5.md
      - Chapitre 15 p-uplets:
          - Index: chapitre15.md
          - Cours: chapitre15/Cours/puplets-cours-git.md
      - Chapitre 18 dictionnaires:
          - Index: chapitre18.md
          - Cours: chapitre18/Cours/dictionnaires-cours-git.md
      - Chapitre 19 traitement de données en tables:
          - Index: chapitre19.md
          - Cours: chapitre19/Cours/cours-tables-indexation-git.md
          - TP Fusion: chapitre19/TP-Fusion/tp-fusion-git.md
          - TP Recherche et tri: chapitre19/TP-Recherche-Tri/tp-recherche-tri-git.md
  - IHM:
      - Chapitre ?: chapitrequickpi.md
  - Architecture:
      - Chapitre 9 système d'exploitation et ligne de commandes:
          - Index: chapitre9.md
          - Cours: chapitre9/cours-systeme/systeme-cours-git.md
          - Memento Shell: chapitre9/memento-shell/memento-shell-git.md
          - TP Terminus: chapitre9/terminus/terminus.md
          - TP2: chapitre9/TP2/NSI-TP2-systeme-git.md
      - Chapitre 13 portes logiques et circuits combinatoires:
          - Index: chapitre13.md
          - Cours: chapitre13/cours-circuits-logiques-git2.md
      - Chapitre 17 architecture de Von Neumann:
          - Index: chapitre17.md
  - Réseau / Web:
      - Chapitre 2 HTML/CSS: chapitre2.md
      - Chapitre 21  HTTP et formulaires:
          - Index: chapitre21.md
          - Cours: chapitre21/http-git.md
          - Correction: chapitre21/correction-git.md
      - Chapitre non traité  Interactions côté serveur, PHP et Flask: chapitre230.md
      - Chapitre 23  Interactions dans le navigateur, Javascript:
          - Index: chapitre23.md
          - Cours: chapitre23/javascript-git2.md
      - Chapitre 25 Réseau TCP/IP:
          - Index: chapitre25.md
          - Cours: chapitre25/reseau-cours-git.md
  - Algorithmique:
      - Chapitre 10  recherches séquentielle et dichotomique: chapitre10.md
      - Chapitre 11  algorithmes de tri: chapitre11.md
      - Chapitre 14  complexité: chapitre14.md
      - Chapitre 20  correction: chapitre20.md
      - Chapitre 22  algorithme KNN: chapitre22.md
      - Chapitre 24  algorithmes gloutons: chapitre24.md
  - Séances: seances.md
  - Automatismes:
      - QCM: divers/qcm.md
      - Automatismes 2021/2022: automatismes/automatismes-2021-2022.md
      - Thèmes:
          - Variables:
              - Permutation de variables: automatismes/variables/exercice1_variables.md
          - Tests:
              - if / else: automatismes/tests/exercice1_tests.md
              - if / elif / .. / else: automatismes/tests/exercice2_tests.md
              - Test avec opérateur logique 1: automatismes/tests/exercice3_tests.md
              - Test avec opérateur logique 2: automatismes/tests/exercice4_tests.md
          - Boucle for:
              - Range épisode 1: automatismes/bouclefor/exercice1_range.md
              - Range épisode 2: automatismes/bouclefor/exercice2_range.md
              - Range épisode 3: automatismes/bouclefor/exercice3_range.md
              - Range épisode 4: automatismes/bouclefor/exercice4_range.md
          - Boucle while:
              - Test de boucle avec une comparaison 1: automatismes/bouclewhile/exercice1_while.md
              - Test de boucle avec une comparaison 2: automatismes/bouclewhile/exercice2_while.md
          - Fonctions:
              - Aire d'un disque: automatismes/fonctions/exo1_fonctions_disque.md
              - Valeur absolue: automatismes/fonctions/exo3_fonctions_valeur_absolue.md
              - Conversion en secondes: automatismes/fonctions/exo2_fonctions_secondes.md
              - Somme des entiers successifs: automatismes/fonctions/exo4_fonctions_somme_for.md
              - Produit des entiers successifs: automatismes/fonctions/exo5_fonctions_produit_for.md
          - Tableaux:
              - Somme d'un tableau, Vincent Bouillot: automatismes/tableaux/exo1_tableaux_somme_bouillot.md
          - Dictionnaires:
              - Scrabble: automatismes/dictionnaire/exercice_scrabble.md
          - Gloutons:
              - Mise en boîtes: automatismes/gloutons/exercice1_gloutons.md
          - Chaines:
              - Comparaison: automatismes/chaines/exercice1_chaines.md
          - VincentBouillot:
              - Test1: automatismes/N1/110-maximum_nombres/sujet.md
              - Test2: automatismes/N1/140-multiplication/sujet.md

  - Projets: projets.md
  - Divers:
      - Ressources: divers/ressources.md
      - Outils: divers/outils.md
      - Capytale: divers/capytale.md
      - Bac:
          - EDS 2021:
              - Amérique du Nord:
                  - Sujet: divers/bac/2021/BACNSI2021AmNo.pdf
                  - Fichier élève: divers/bac/2021/amerique_du_nord_2021_eleve.py
                  - Éléments de correction: divers/bac/2021/NSI_Amerique_du_Nord_2021.ipynb
              - Candidats libres:
                  - Sujet: divers/bac/2021/Metropole_Specialite_NSI_Candidats_libres.pdf
                  - Fichier élève: divers/bac/2021/Metropole_Specialite_NSI_Candidats_libres_Eleves.ipynb
                  - Éléments de correction: divers/bac/2021/Metropole_Specialite_NSI_Candidats_libres.ipynb
          - Bac 2022:
              - Asie Mai 2022 sujet 2: divers/bac/2022/asie/asie_sujet2.md

      - Maths:
          - Énigmes: divers/maths/enigmes.md
          - Graphes: divers/notebooks/Graphes.ipynb
          - Résidus quadratiques: divers/notebooks/Residu_Quadratique.ipynb
#  - Sandbox : sandbox.md

theme:
  name: material
  font: false
  language: fr

  palette: # Palettes de couleurs jour/nuit
    - media: "(prefers-color-scheme: dark)"
      scheme: slate
      primary: blue
      accent: blue
      toggle:
        icon: material/weather-night
        name: Passer au mode jour
    - media: "(prefers-color-scheme: light)"
      scheme: default
      primary: indigo
      accent: indigo
      toggle:
        icon: material/weather-sunny
        name: Passer au mode nuit
  custom_dir: my_theme_customizations/
  font: false
  language: fr
  logo: assets/sierpingif.gif
  favicon: assets/sierpingif.gif
  features:
    - navigation.instant
    - navigation.tabs
    # - navigation.expand
    - navigation.top
    - toc.integrate
    - header.autohide
    - navigation.tabs.sticky
    - navigation.sections

markdown_extensions:
  - meta
  - abbr
  - def_list # Les listes de définition.
  - attr_list # Un peu de CSS et des attributs HTML.
  - admonition # Blocs colorés  !!! info "ma remarque"
  - pymdownx.details #   qui peuvent se plier/déplier.
  - footnotes # Notes[^1] de bas de page.  [^1]: ma note.
  - pymdownx.caret # Passage ^^souligné^^ ou en ^exposant^.
  - pymdownx.mark # Passage ==surligné==.
  - pymdownx.tilde # Passage ~~barré~~ ou en ~indice~.
  - pymdownx.snippets # Inclusion de fichiers externe.
  - pymdownx.highlight: # Coloration syntaxique du code
      auto_title: true
      auto_title_map:
        "Python": "🐍 Script Python"
        "Python Console Session": "🐍 Console Python"
        "Text Only": "📋 Texte"
        "E-mail": "📥 Entrée"
        "Text Output": "📤 Sortie"

  - pymdownx.tasklist: # Cases à cocher  - [ ]  et - [x]
      custom_checkbox: false #   avec cases d'origine
      clickable_checkbox: true #   et cliquables.
  - pymdownx.inlinehilite # pour  `#!python  <python en ligne>`
  - pymdownx.superfences: # Imbrication de blocs.
      preserve_tabs: true
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_code_format
  - pymdownx.keys # Touches du clavier.  ++ctrl+d++
  - pymdownx.tabbed:
      alternate_style: true
  - pymdownx.betterem:    
    smart_enable: all
#   - pymdownx.emoji: # Émojis  :boom:
#       emoji_index: !!python/name:materialx.emoji.twemoji
#       emoji_generator: !!python/name:materialx.emoji.to_svg
  - pymdownx.arithmatex:
      generic: true
      smart_dollar: false
  - toc:
      permalink: ⚓︎
      toc_depth: 3
  - mkdocs_graphviz #https://gitlab.com/rodrigo.schwencke/mkdocs-graphviz
  - md_in_html #pour écrire du markdown dans du code HTML voir https://mooc-forums.inria.fr/moocnsi/t/mkdocs-astuce-pour-centrer-une-image-en-markdown-pur/2831/2

plugins:
  - awesome-pages:
      collapse_single_pages: false
      strict: false
  - search
  - macros:
      verbose: True
      include_dir: include # voir https://mkdocs-macros-plugin.readthedocs.io/en/latest/advanced/#changing-the-directory-of-the-includes
  #- mermaid2
  #- mkdocs-jupyter:
  #    kernel_name: python3
  #    execute: False
  #    include_source: True

extra:
  social:
    - icon: fontawesome/solid/paper-plane
      link: mailto:admin@frederic-junier.org
      name: Mail

    - icon: fontawesome/brands/gitlab
      link: https://gitlab.com/frederic-junier/parc-nsi
      name: Dépôt git

    - icon: fontawesome/solid/school
      link: https://lyceeduparc.fr/ldp/
      name: Lycée du Parc

  raw_url: https://gitlab.com/frederic-junier/parc-nsi-premiere/-/raw/
  io_url: https://gitlab.com/frederic-junier/parc-nsi-premiere

copyright: Sous licence CC BY-NC-SA 4.0

extra_javascript:
  - javascripts/config.js
  #- javascripts/pyodid.js
  # - xtra/javascripts/interpreter.js
  #- javascripts/repl.js
  - https://polyfill.io/v3/polyfill.min.js?features=es6
  - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js
  #- https://cdn.jsdelivr.net/pyodide/v0.17.0/full/pyodide.js    #chargé dans l'extension du template html overrides/main.html

extra_css:
  - xtra/stylesheets/ajustements.css # ajustements
