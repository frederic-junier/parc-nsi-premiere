---
title:  Chapitre 4  tableaux
layout: parc
---




Cours de Frédéric Junier.



# Cours

* [Cours en version pdf](chapitre6/Cours/tableaux-cours-.pdf)

* [Cours en version markdown](chapitre6/Cours/tableaux-cours-git.md)

* [Exercices du cours en version notebook (Capytale)](https://capytale2.ac-paris.fr/web/c/a3d0-4422844)

* [Brouillon des exemples du cours](chapitre6/Cours/brouillon-prof-cours-tableaux.py)

    
# TP

* [TP en version pdf](chapitre6/TP/TP-tableaux-.pdf)

* [TP en version markdown](chapitre6/TP/TP-tableaux-git.md)
  
* [Matériel du TP à télécharger](chapitre6/TP/eleves.zip)

* [Corrigé du TP en version pdf](chapitre6/TP/corriges/TP-Tableaux-Corrige.pdf)
  
* [Corrigé du TP en version html](chapitre6/TP/corriges/TP-Tableaux-Corrige.html)
  
* [Corrigé  du TP en version notebook (Capytale)](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/ba0b-101079)
