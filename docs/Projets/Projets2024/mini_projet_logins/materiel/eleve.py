#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep  3 21:01:05 2023

@author: fjunier
"""
# imports des modules
import random
from nsi_ui import *


# Fonctions
def element_aleatoire(tab):
    """
    Prend en paramètre un tableau de type list en Python
    Renvoie un élément aléatoire dans ce tableau à l'aide
    de la fonction random.randint du module random
    """
    # à compléter


def mdp_aleatoire():
    """
    Fonction sans paramètre

    -------
    Renvoie un mot de passe aléatoire de 10 caractères
    contenant au moins 1 chiffre entre 0 et 10
              au moins 1 caractère alphabétique minuscule
              au moins 1 caractère alphabétique majuscule
              au moins un caractère spécial parmi '?!$#'
    """
    # tableau des 10 chiffres ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    chiffres = [str(k) for k in range(0, 10)]
    # tableau des 26 caractères minuscules ['a', 'b', ..., 'z']
    car_min = [chr(k) for k in range(ord("a"), ord("z") + 1)]


def nettoyer_diacritique(mot):
    """
    Renvoie le mot de type str sans accent ni cédille
    Précondition mot doit être en minuscules
    """
    # Précondition
    assert mot == mot.lower()
    # Dictionnaire de translation sans accents
    dicoTrans = {k: "e" for k in range(232, 236)}
    # dicoTrans.update({ ord(chr(k).upper())  : 'E' for k in range(232, 236)})
    dicoTrans.update({k: "i" for k in range(236, 240)})
    # dicoTrans.update({ ord(chr(k).upper())  : 'I' for k in range(236, 240)})
    dicoTrans.update({k: "a" for k in range(224, 230)})
    # dicoTrans.update({ ord(chr(k).upper())  : 'A' for k in range(224, 230)})
    dicoTrans.update({k: "o" for k in range(242, 247)})
    # dicoTrans.update({ ord(chr(k).upper())  : 'O' for k in range(242, 247)})
    dicoTrans.update({k: "u" for k in range(249, 253)})
    # dicoTrans.update({ ord(chr(k).upper())  : 'U' for k in range(249, 253)})
    dicoTrans.update({k: "y" for k in range(253, 256)})
    # dicoTrans.update({ ord(chr(k).upper())  : 'Y' for k in range(253, 256)})
    dicoTrans.update({231: "c"})  # c avec cédille
    # dicoTrans.update({ ord(chr(231).upper()) : 'C'})
    # dictionnaire de translation
    dicoTrans = str.maketrans(dicoTrans)
    # dictionnaire de translation
    dicoTrans = str.maketrans(dicoTrans)
    return mot.translate(dicoTrans)


def generer_login(prenom, nom):
    """
    Génère un login de type prenom.nom
    à partir des valeurs de  prenom et de nom convertis en minuscules
    Les accents et cédilles de prenom et nom sont nettoyés par nettoyer_diacritique
    """
    # à compléter


def test_generer_login():
    """
    Jeu de tests unitaires pour la fonction générer login
    """
    assert generer_login("Frédéric", "Junier") == "frederic.junier"
    assert generer_login("François", "Mitterrand") == "francois.mitterrand"
    assert generer_login("Noël", "Mamère") == "noel.mamere"
    print("Tests réussis pour generer_login")


def csv_vers_table(chemin):
    """
    chemin est un chemin vers un fichier CSV
    Ouvre ce fichier en mode lecture
    Renvoie un tableau de type list en Python
    dont chaque élément est un tableau listant les différents champs
    d'une ligne du fichier séparés par une virgule dans le fichier
    Il doit y en voir trois : numéro,prénom,nom

    Exemple de tableau renvoyé :

        [['1', 'Timothée', 'Gaillard'], ['2', 'Zacharie', 'Lefort']]
    """
    # consulter cette documentation https://realpython.com/read-write-files-python/
    table = []  # initialisation d'un tableau vide
    reader = open(chemin, mode="r")  # ouverture du fichier en mode lecture
    for ligne in reader:  # itération sur les lignes du fichier
        table.append(ligne.rstrip().split(","))  # que fait cette instruction ?
    reader.close()  # fermeture du fichier
    return table  # on renvoie le tableau


def generer_fichier_identifiants(table, chemin):
    """
    table est un tableau d'utilsiateurs renvoyé par csv_vers_table
    chemin est le chemin d'un fichier CSV dans lequel on va écrire des lignes
    de la forme 'prenom,nom,login,mot de passe' telles que :

    Timothée,Gaillard,timothee.gaillard,zS1ZVUal2u
    Zacharie,Lefort,zacharie.lefort,q5jE03ryad

    La fonction ne renvoie rien
    """
    # consulter cette documentation https://realpython.com/read-write-files-python/
    # à compléter


def interface_textuelle():
    """
    Génère dans la console une Interface Homme Machine Textuelle qui permet
    de générer un fichier d'identifiants à partir d'un fichier d'utlisateurs.
    Une boucle permet de réaliser cette tâche tant qu'on le souhaite

    Exemple :

    Voulez-vous générer de nouveaux identifiants (o/n) ?
    o
    Chemin vers le fichier CSV source avec lignes de la forme 'numéro,prénom,nom' ?
    nom_prenom.csv
    Chemin vers le fichier CSV de sortie avec lignes de la forme 'numéro,prénom,nom,login,mdp' ?
    identifiants.csv
    Voulez-vous générer de nouveaux identifiants (o/n) ?
    o
    Chemin vers le fichier CSV source avec lignes de la forme 'numéro,prénom,nom' ?
    nom_prenom.csv
    Chemin vers le fichier CSV de sortie avec lignes de la forme 'numéro,prénom,nom,login,mdp' ?
    identifiants2.csv
    Voulez-vous générer de nouveaux identifiants (o/n) ?
    n
    Fin du programme
    """
    # à compléter


def interface_graphique():

    """
    Interface graphique basée sur le module nsi_ui, équivalente à l'interface textuelle avec  :

            deux champs de saisie (entry) source et sortie
            un bouton d'action  (button) pour lancer la génération du fichier d'identifiants

    Documentation dans le manuel pages 52 et 53

    """
    # à compléter


# Programme principal
# interface_textuelle()
#interface_graphique()
