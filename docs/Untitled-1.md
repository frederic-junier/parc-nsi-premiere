---
title:  Opérateurs booléens
---


!!! note "Type booléen et opérateur booléen"

    Le type **booléen** ne prend que deux valeurs `True` et `False`.

    Une fonction qui ne prend que des paramètres booléens et qui renvoie un booléen, est appelée **opérateur booléen**.

    Si la fonction prend $n$ paramètres et que chaque paramètre peut prendre deux valeurs cela donne $2^{n}$ listes distinctes de valeurs de paramètres.

    Un **opérateur booléen** est donc entièrement déterminé par un tableau où on fait apparaître une colonne par paramètre et une colonne pour la valeur renvoyée. Un tel tableau est appelé **table de vérité**.


* `non` prend en argument le booléen `a` et renvoie `False` si `a` vaut `True`, `True` s'il vaut `False` ;
* `et` prend en argument les booléens `a` et `b` et ne renvoie `True` que s'ils valent l'un et l'autre `True` ; 
* `ou` prend en argument les booléens `a` et `b` et renvoie `True` dès que l'un ou l'autre vaut `True` (ou les deux) ; 
* `ou_exclusif` prend en argument les booléens `a` et `b` et renvoie `True` si les deux arguments ont des valeurs différentes ; 
* `non_ou` prend en argument les booléens `a` et `b` et ne renvoie `True` que s'ils valent l'un et l'autre `False`. ; 
* `non_et` prend en argument les booléens `a` et `b` et ne renvoie `False` que s'ils valent l'un et l'autre `True`. 

## Exemples 

```python
>>> non(True)
False
>>> et(True, False)
False
>>> ou(True, False)
True
>>> ou_exclusif(True, True)
False
>>> non_ou(False, True)
False
>>> non_et(False, True)
True
```


## Tables de vérité
   
Les valeurs prises par un opérateur booléen peuvent être résumées dans une **table de vérité**.
    
### La table de vérité de l'opérateur `non` 

|   `a`   | `non(a)` |
| :-----: | :------: |
| `False` |  `True`  |
| `True`  | `False`  |


### La table de vérité de l'opérateur `et`
    
|   `a`   |   `b`   | `et(a, b)` |
| :-----: | :-----: | :--------: |
| `False` | `False` |  `False`   |
| `False` | `True`  |  `False`   |
| `True`  | `False` |  `False`   |
| `True`  | `True`  |  `True`   |

### La table de vérité de l'opérateur `ou` 



|   `a`   |   `b`   | `ou(a, b)` |
| :-----: | :-----: | :--------: |
| `False` | `False` |  `False`   |
| `False` | `True`  |   `True`   |
| `True`  | `False` |   `True`   |
| `True`  | `True`  |   `True`   |

### La table de vérité de l'opérateur `ou exclusif` 

|   `a`   |   `b`   | `ou_exclusif(a, b)` |
| :-----: | :-----: | :-----------------: |
| `False` | `False` |       `False`       |
| `False` | `True`  |       `True`        |
| `True`  | `False` |       `True`        |
| `True`  | `True`  |       `False`       |
    

### La table de vérité de l'opérateur `non_ou` 


|   `a`   |   `b`   | `non_ou(a, b)` |
| :-----: | :-----: | :-----------------: |
| `False` | `False` |       `True`       |
| `False` | `True`  |       `False`        |
| `True`  | `False` |       `False`        |
| `True`  | `True`  |       `False`       |

### La table de vérité de l'opérateur `non_et` 


|   `a`   |   `b`   | `non_et(a, b)` |
| :-----: | :-----: | :--------: |
| `False` | `False` |  `True`   |
| `False` | `True`  |  `True`   |
| `True`  | `False` |  `True`   |
| `True`  | `True`  |  `False`   |