---
title:  Chapitre 12  codage des caractères
layout: parc
---


Cours de Frédéric Junier.


## Cours 

* [Cours version pdf](chapitre12/cours/Cours-NSI-Codage-Caracteres-2025.pdf)
* [Exercicdes  du cours avec corrections sur Capytale ](https://capytale2.ac-paris.fr/web/c/9aac-5273384)


![Emojis, caractères de points de codes entre U+1F600 et U+1F64F](chapitre12/cours/images/emojis.png)