%====================================================================
\documentclass[sujet]{../myTP_SC_2}
\DeclareUnicodeCharacter{2212}{-}
\begin{document}
\TP{5}
\title{Algorithmes gloutons\\
\vskip 2mm
\small D'après des documents eduscol et un travail de Nicolas Mesnier}
\maketitle


\paragraph{Objectifs :}
\begin{itemize}
\item Comprendre la philosophie des algorithmes gloutons.
\item Comprendre que l'algorithme glouton choisi fournit parfois la solution optimale, MAIS parfois même pas une solution exacte.
\end{itemize}

\paragraph{Rappel:}
\begin{itemize}
\item Enregistrez dès le début du TP votre travail avec
un nom de la forme \texttt{VOTRENOM\_TPX.py} (évitez les accents et
caractères spéciaux).
\end{itemize}


\section{Généralités}

\textit{Optimiser} un problème, c’est déterminer les conditions dans lesquelles ce problème présente \textit{une caractéristique spéciﬁque}. Par exemple, déterminer le minimum ou le maximum d’une fonction est un problème d’optimisation. 
On peut également citer la répartition optimale de tâches suivant des critères précis, le problème du rendu de monnaie, le problème du sac à dos, la recherche d’un plus court chemin dans un graphe... 
De nombreuses techniques informatiques sont susceptibles d’apporter une solution exacte ou approchée à ces problèmes. 
Certaines de ces techniques, comme l’énumération exhaustive de toutes les solutions, ont un coût machine qui les rend souvent peu pertinentes au regard de contraintes extérieures imposées (temps de réponse de la solution imposé, moyens machines limités).\\

Les \textbf{algorithmes gloutons} constituent une alternative dont le résultat n’est pas toujours optimal. Plus précisément, ces algorithmes déterminent une solution optimale en effectuant successivement des choix \textbf{locaux}, jamais remis en cause. Au cours de la construction de la solution, l’algorithme résout une partie du problème puis se focalise ensuite sur \textbf{LE} sous-problème restant à
résoudre. 
Une différence essentielle avec la \textit{programmation dynamique} est que celle-ci peut remettre en cause des solutions déjà établies. Au lieu de se focaliser sur un seul sous-problème, elle explore les solutions de \textbf{TOUS} les sous-problèmes pour les combiner ﬁnalement de manière optimale.\\

Le principal avantage des \textbf{algorithmes gloutons} est leur facilité de mise en œuvre. En outre, dans certaines situations dites \textbf{canoniques}, il arrive qu’ils renvoient non pas \textbf{UN} optimum mais \textbf{L'}optimum d’un problème.\\ 

Le but de ce TP est de présenter de telles situation, en montrant les avantages mais aussi les limites de la technique.

\section{Rendu de monnaie}

Un achat dit en espèces se traduit par un échange de pièces et de billets. Dans la suite ce cet exposé, les pièces désignent indifféremment les véritables pièces que les billets. 
Supposons qu’un achat induise un rendu de 49 euros. Quelles pièces peuvent être rendues ? La réponse, bien qu’évidente, n’est \textbf{pas unique}. Quatre pièces de 10 euros, 1 pièce de 5 euros et deux pièces de 2
euros conviennent. Mais quarante-neuf pièces de 1 euros conviennent également ! \\

Si la question est de rendre la monnaie avec un minimum de pièces, \textbf{le problème change de nature}. La réponse est la première solution proposée. Toutefois, comment parvient-on à un tel résultat ? Quels choix ont été faits qui optimisent le nombre de pièces rendus ? C’est \textbf{le problème du
rendu de monnaie} dont la solution dépend du système de monnaie utilisé.

Dans le système monétaire français, les pièces prennent les valeurs 0.01, 0.02, 0.05, 0.10, 0.20, 0.50, 1, 2, 5, 10, 20, 50, 100, 200, 500 euros. 
Rendre 49 euros avec un minimum de pièces est un \textbf{problème d’optimisation}. En pratique, sans s’en rendre compte généralement, tout individu met en œuvre un algorithme glouton.
Il choisit d’abord la plus grandeur valeur de monnaie, parmi 1, 2, 5, 10, contenue dans 49 euros. En l’occurrence, quatre fois un pièce de 10 euros. La somme de 9 euros restant à rendre, il choisit une pièce de 5 euros, puis deux pièces de 2 euros. 
Cette stratégie gagnante pour la somme de 49 euros l’est-elle pour n’importe quelle somme à rendre ? \textbf{On peut montrer que la réponse est positive pour le système monétaire français}. Pour cette raison, \textbf{un tel système de monnaie est qualiﬁé de canonique}.

\begin{Iexo}
Considérons une monnaie qui a pour liste de valeurs $ \lbrack c_0,...,c_{n-1} \rbrack $ 
avec $ 0 < c_0 <...< c_{n−1}$.
% Par exemple dans le système monétaire européen : $C = \lbrack 0.01, 0.02, 0.05, 0.10, 0.20, 0.50, 1, 2, 5, %10, 20, 50, 100, 200, 500 \rbrack$.
\begin{enumerate}
\item 
Pour simpliﬁer, nous nous intéressons seulement aux valeurs entières, dans un premier temps. Implémenter une fonction \verb+rendu_monnaie_1(s,P)+ qui étant donné une liste de valeurs $P$ et une somme $s$ à rendre, applique la stratégie gloutonne décrite plus haut et renvoie la liste des valeurs rendues dans l’ordre par le commerçant.
\item Tester la fonction écrite pour différents exemples. 

\begin{verbatim}
def rendu_monnaie_1(s,P):
    ....

>>> rendu_monnaie_1(49,EUR)
[20, 20, 5, 2, 2]
>>> rendu_monnaie_1(76,EUR)
[50, 20, 5, 1]

\end{verbatim}

\item Adapter, si nécessaire, la fonction pour une liste complète de valeurs, ne se limitant plus aux valeurs entières (on se limitera à des valeurs décimales multiples du centime).
\item  Commenter les éventuels problèmes rencontrés.

\begin{verbatim}

>>> rendu_monnaie_3(13.45,EURC)
[10, 2, 1, 0.2, 0.2, 0.05]
>>> rendu_monnaie_3(25.58,EURC)
[20, 5, 0.5, 0.05, 0.02, 0.01]

\end{verbatim}

\item Tester la fonction \verb+rendu_monnaie_1(s,P)+ pour les valeurs $P = [1,3,6,12,24,30]$ et $s = 49$. Conclure.
\item Effectuer un dernier test pour les valeurs $P = [2,3]$ et $s = 7$. Conclure
\end{enumerate}
\end{Iexo}

\fbox{
\begin{minipage}{1\textwidth}
Dans un problème, la stratégie gloutonne consiste à faire le choix le plus efficace \textbf{localement} (c’est-à-dire \textit{à chaque étape}). Parfois, la stratégie conduit bien à \textit{la solution optimale}. \textbf{MAIS} ce n'est pas toujours le cas. Il arrive que cela ne conduise même pas à la solution recherchée...
\end{minipage}
}


\section{Le problème du sac à dos}

\subsection{Le problème type}
On dispose d’un sac pouvant supporter un poids maximal donné et de divers objets ayant chacun une valeur et un poids. Il s’agit de choisir les objets à emporter dans le sac aﬁn d’obtenir la valeur totale la plus grande tout en respectant la contrainte du poids maximal. C’est un \textit{problème d’optimisation avec contrainte}.

Ce problème peut se résoudre par \textbf{force brute}, c’est-à-dire en testant tous les cas possibles. Mais ce type de résolution présente un problème d’efﬁcacité. Son coût en fonction du nombre d’objets disponibles croît de manière exponentielle.

Nous pouvons envisager \textbf{une stratégie gloutonne}. Le principe d’un algorithme glouton est de faire le meilleur choix pour prendre le premier objet, puis le meilleur choix pour prendre le deuxième, et ainsi de suite. Que faut-il entendre par meilleur choix ? Est-ce prendre l’objet qui a la plus grande valeur, l’objet qui a le plus petit poids, l’objet qui a le rapport valeur/poids le plus grand ? Cela reste à déﬁnir.

\subsection{Le problème à résoudre}
Soit un sac à dos, dont le contenu ne doit pas dépasser une masse de 20 kg. Le propriétaire du sac dispose de plusieurs  objets , caractérisés chacun par une masse et une valeur.\\

Quels objets emporter dans le sac à dos pour maximiser la valeur totale, sans dépasser le poids maximum ?\\ 

Les tableaux 1 et 2 ci-dessous présentent les objets disponibles avec les valeurs en euros et les masses en kg pour chacun des deux sacs à dos.\\

\begin{table}[!ht]
    \center
	\begin{tabular}{|c|c|c|}
	\hline 
	Objets & Valeur & Masse \\ 
	\hline \hline 
 	1 & 2.0 & 10.0 \\ 
	\hline 
	2 & 4.0 & 14.0 \\ 
	\hline 
	3 & 4.0 & 7.0 \\ 
	\hline 
	4 & 5.0 & 3.0 \\ 
	\hline 
	\end{tabular} 
	\caption{Sac à dos n°1, masse maximale autorisée : 20 kg}
\end{table}

\begin{table}[!ht]
    \center
	\begin{tabular}{|c|c|c|}
	\hline 
	Objets & Valeur & Masse \\ 
	\hline \hline
 	1 & 2.0 & 10.0 \\ 
	\hline 
	2 & 4.0 & 11.0 \\ 
	\hline 
	3 & 8.0 & 7.0 \\ 
	\hline 
	4 & 5.0 & 5.0 \\ 
	\hline 
	5 & 8.0 & 6.0 \\ 
	\hline 
	6 & 10.0 & 15.0 \\ 
	\hline
	7 & 11.0 & 8.0 \\ 
	\hline  
	\end{tabular} 
	\caption{Sac à dos n°2, masse maximale autorisée : 40 kg}
\end{table}

\begin{comment}

\begin{table}[!ht]
    \center
    \begin{tabular}[b]{|l|c|c|c|}
    \hline
    & \multicolumn{3}{|c|}{Année} \\
    \hline
    Produit & 1999 & 2000 & 2001 \\
    \hline \hline
    Livre & 15 & 10 & 7 \\
    CD & 10 & 17 & 22 \\
    \hline
    \end{tabular}
    \caption{Vente pour les 1999 2001}
\end{table}

\end{comment}

\subsection{Représentation des données}

\noindent Deux méthodes de représentation des données sont proposées ci-dessous :

\begin{enumerate}

\item Utilisation d’une liste de p-uplets

Un objet est représenté par un triplet contenant son numéro de type \verb+int+, sa valeur de type \verb+float+ et sa masse de type \verb+float+. Les triplets obtenus sont les éléments d’une liste.

\item Utilisation d’un dictionnaire

L’ensemble des objets est représenté par un dictionnaire dont les clé sont les numéros des objets de type \verb+int+. Pour chaque clé (chaque objet), la valeur correspondante est un dictionnaire dont les clés sont les chaînes \verb+"Valeur"+ et \verb+"Masse"+ de type \verb+str+.
Les valeurs correspondantes sont la valeur de type \verb+float+ et la masse de type \verb+float+.

\end{enumerate}

\noindent Dans la suite du problème, \textbf{la représentation choisie sera une liste de p-uplets}.

\subsection{Force brute}

\noindent Le principe est simple : il faut tester tous les cas possibles ! 

\noindent La mise en œuvre l’est moins. Comment obtenir tous les cas sans les
répéter et sans en oublier un ? Cette question pose la difﬁculté principale.\\

\noindent\textit{\textbf{Indication :} une méthode est d’associer le chiffre 1 à un objet s’il est choisi et le chiffre 0 sinon. Nous obtenons ainsi un nombre entier écrit en binaire avec 4 chiffres. Le nombre 1011 signiﬁe que nous avons choisi les objets 1, 3 et 4. Le nombre 1111 signiﬁe que nous avons choisi tous les objets. A chaque nombre correspond exactement une possibilité pour construire une partie de l’ensemble des 4 objets.}

\noindent \textit{Il apparaît alors que le nombre total de cas est $2^4$ puisqu’avec 4 chiffres (0 ou 1), nous pouvons écrire exactement $2^4$ nombres.}

\begin{Iexo}
\begin{Ienumerate}

\item En utilisant l'indication précédente, écrire une fonction \verb+ens_des_parties+ qui prend en paramètre un ensemble d’objets et renvoie une liste dont chaque élément est une partie de l’ensemble.


\textbf{Indication :} la fonction \verb+bin(n)+ permet de convertir l'entier \verb+n+ en binaire. Attention : une chaîne de caractère commençant par \verb+'0b'+ est renvoyée. Une coupe de la chaîne pourra être effectuée... 

\item Implémenter une fonction \verb+force_brute(objets, masse_max)+ (et les sous-fonctions utiles) qui, étant donné un ensemble de fichiers, applique la stratégie de la \textit{force brute} et renvoie la liste des triplets associés aux objets à emporter dans le sac à dos pour maximiser la valeur de son contenu, ainsi que la valeur du contenu.


\textbf{Indication :} Les sous-fonctions \verb+valeur_totale(liste)+ et \verb+masse_totale(liste)+, qui renvoient respectivement la valeur et la masse totale d'une liste d'objets, pourront être implémentées.

\item Afficher la liste des objets à emporter ainsi que la valeur du contenu du sac à dos optimal pour les deux exemples proposés.

\end{Ienumerate}
\end{Iexo}



\subsection{Stratégie gloutonne}

\begin{Iexo}

\begin{Ienumerate}
\item Implémenter une fonction \verb+glouton(objets, masse_max, choix)+ (et les sous-fonctions utiles), qui prend en paramètres une liste de d'objets, une masse maximale \verb+masse_max+ (celle que peut contenir le sac à dos) et le type de choix utilisé (par valeur, par masse, ou par le rapport valeur/masse) pour la stratégie gloutonne et qui renvoie la liste des objets à emporter pour maximiser le critère choisi.

\textbf{Indication :} on pourra construire une nouvelle liste en triant la liste passée en paramètre suivant le type de choix utilisé avec la fonction \verb+sorted+. L’instruction \verb+help(sorted)+ écrite dans l’interpréteur permet d’obtenir des informations sur la fonction
\verb+sorted+\footnote{pour des exemples, consultez \url{https://pythonexamples.org/python-sorted/}}. 

Les sous-fonctions \verb+masse(objet)+, \verb+valeur(objet)+ et \verb+rapport(objet)+, qui renvoient respectivement la masse, la valeur et le rapport valeur/masse d'un objet représenté par un triplet, pourront être implémentées.

\item Comparer les résultats obtenus pour chaque choix avec ceux obtenus à l'aide de la stratégie de la \textit{force brute}. Conclure.

\end{Ienumerate}
\end{Iexo}

\section{Comment placer des pylônes ? \textit{(s'il reste du temps)}}

L'objet de cette partie est de déterminer où placer des pylônes pour relier le point le plus à gauche d'un paysage unidimensionnel au point le plus à droite par une ligne à haute tension, en fonction de critères de coût. 

Nous ferons la simplification suivante: les fils sont sans poids et tendus, ils relient donc en ligne droites les sommets de deux pylônes consécutifs. 

Les normes de sécurité imposent que les fils soient en tout point à une distance d'au moins $\Delta$ (mesurée verticalement) au-dessus du sol. Les pylônes sont tous de hauteur identique $\ell \geq \Delta.$ 
Voici par exemple une proposition valide de placement de pylônes pour le paysage ci-dessous, avec $\Delta=1$ et $\ell=3.$

\begin{center}
\includegraphics[width=\linewidth]{pylones1}
\end{center}

\noindent Le paysage est défini comme étant une ligne brisée reliant $n+1$ points $P_i$ de coordonnées $(i,h_i)$, avec $i \in \llbracket 0 , n \rrbracket$ et $h_i \in \R$. Par convention, $h_0=0$. Informatiquement, un paysage est donc représenté par une liste~:
$$\verb!L_h! \;=\; [h_0,h_1,\ldots,h_{n}]$$

Pour relier le point le plus à gauche au point le plus à droite par une ligne à haute tension, il faut choisir à quels points parmi les $P_i$ planter les pylônes intermédiaires. On considère désormais les points $S_i$, de coordonnées $(i,h_i+\ell)$, c'est-à-dire les sommets des éventuels pylônes.\\

Dans la suite, les variables \verb!L_h!, $\ell$ et $\Delta$ seront suposées globales: on pourra donc accéder à leur valeur en entrant (respectivement) \verb!L_h!, \verb!l! et \verb!delta! à n'importe quel moment.\\

La législation impose que le fil reste à une distance supérieure à $\Delta$ (mesurée verticalement) au-dessus du sol.
\begin{enumerate}
\item Pour traduire mathématiquement le fait qu'un fil tiré entre $S_i$ et $S_j$ (avec $i<j$) respecte la législation, on peut, par exemple, s'intéresser aux pentes des droites qui relient les sommets.

Fixons $i<j$. Une façon de procéder est de considérer les nombres
$$
\alpha_{i,k}=\dfrac{(h_k+\Delta)-(h_i+\ell)}{k-i} \quad \textmd{pour} \; k \in \llbracket i+1 , j-1 \rrbracket  \qquad \textmd{et} \qquad \beta_{i,j}=\dfrac{(h_j+\ell)-(h_i+\ell)}{j-i} 
$$
qui sont les pentes des fils tirés depuis le pylône en $P_i$ jusqu'à une hauteur $\Delta$ au-dessus de $P_k$ d'une part, et jusqu'à une hauteur $\ell$ au-dessus de $P_j$ d'autre part (voir la figure ci-dessous).

\begin{center}
% \epsfig{figure=./graphe1.eps,width=\linewidth}
\includegraphics[width=0.7\linewidth]{pylones2}
\end{center}

Dans ces conditions, le fil tiré d'un pylône en $P_i$ à un pylône en $P_j$ respecte la législation si et seulement si:
$$
\boxed{ \beta_{i,j}\geq \underset{i<k<j}{\max} \alpha_{i,k} }
$$

\item La fonction \verb!respecte_legislation!, fournie dans le fichier \verb+cadeau.py+, prend en entrée deux entiers $i<j$, et renvoie \verb!True! si un fil tiré entre $S_i$ et $S_j$ respecte la législation, et \verb!False! sinon.\\

On peut donc commencer par coder les deux fonctions suivantes~:
\vspace{0.3cm}

\begin{minipage}{.5\linewidth}
\begin{verbatim}
def alpha(i,k):
    return((L_h[k]+delta-L_h[i]-l)/(k-i))

def beta(i,j):
    return((L_h[j]+l-L_h[i]-l)/(j-i))
\end{verbatim}        
\end{minipage}\hfill

\vspace{0.3cm}

puis~:

\vspace{0.3cm}

\begin{minipage}{.5\linewidth}
\begin{verbatim}
def respecte_legislation(i,j):
    beta=beta(i,j)
    for k in range(i+1,j):
        if beta<alpha(i,k):
            return False
    return True
\end{verbatim}        
\end{minipage}

\vspace{0.3cm}

\end{enumerate}

\noindent Considérons la stratégie suivante de placement de pylônes, dite \textit{stratégie gloutonne vers l'avant}. Le premier pylône est placé en~$0$. Pour calculer l'emplacement du pylône suivant, on part du dernier pylône planté, et on avance vers la droite avec le fil tendu tant que la législation est respectée (et que le dernier point d'abscisse $n$ n'est pas atteint). Un nouveau pylône est alors planté, et on recommence jusqu'à ce que le point d'abscisse $n$ soit atteint.

\begin{Iexo}[\textit{Stratégie gloutonne vers l'avant}]

\'Ecrire une fonction \verb!strategie_glouton_avant()! qui calcule la disposition des pylônes selon la stratégie décrite ci-dessus: celle-ci renverra la liste des indices $i$ où un pylône sera placé.\\
\noindent Préciser, après une brève justification, la complexité en fonction de $n$ de cet algorithme.\\
\noindent \textit{\noindent Indication~: on pourra considérer que quels que soient $j<i$, l'exécution de} \verb!respecte_legislation(j,i)! \textit{demande un nombre d'opérations élémentaires inférieur à $C \times n$, où $C$ est une constante raisonnable.}
\end{Iexo}

\noindent Une seconde stratégie, dite \textit{gloutonne au plus loin}, consiste à planter le prochain pylône le plus à droite possible (en testant jusqu'à $n$) de la position courante, le premier pylône étant toujours planté en $0$.

\begin{Iexo}[\textit{Stratégie gloutonne au plus plus loin}]

\'Ecrire une fonction \verb!strategie_glouton_plus_loin()! qui calcule la disposition des pylônes selon cette seconde stratégie.\\
\noindent Préciser, après une brève justification, la complexité en fonction de $n$ de cet algorithme.

\end{Iexo}

\fin

\end{document}
