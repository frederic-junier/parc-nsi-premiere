#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
TP 11
"""

# %% Quelques outils

# %% Exercice 2

# %%
def copie_tab(t):
    """
    Renvoie une copie superficielle d'un tableau d'entiers

    Paramètre:
        t : tableau d'entiers
        
    Retour:
        tableau d'entiers
    """
    # à compléter

#%%
def rendu_monnaie_glouton1(systeme_monetaire, reste):
    """
    Prend en paramètre un sysyeme_monetaire et un reste à rendre
    Renvoie une solution gloutonne au problème du rendu de monnaie ou None	

    Parametres :
        systeme_monetaire : tableau d'entiers
        reste : entier
        Précondition : reste >= 0

    Retour:
        un tableau d'entiers s 
        (liste de valeurs de pièces, éventuellement vide)
        Postcondition : reste == 0 ou pas de solution
	"""
    # à compléter en traduisant directement l'algo générique
    s = []
    c = copie_tab(systeme_monetaire) # copie superficielle    
    while reste != 0 and  c != []:        
        x = c[len(c)-1] # plus grande pièce restante
        # à compléter
    if reste == 0:
        return s
    else:
        return []
    

def rendu_monnaie_glouton2(systeme_monetaire, reste):
    """
    Prend en paramètre un sysyeme_monetaire et un reste à rendre
    Renvoie une solution gloutonne au problème du rendu de monnaie ou None	

    Parametres :
        systeme_monetaire : tableau d'entiers
        reste : entier
        Précondition : reste >= 0

    Retour:
        un tableau d'entiers s 
        (liste de valeurs de pièces, éventuellement vide)
        Postcondition : reste == 0 ou pas de solution
	"""
    # à compléter en parcourant la liste des candidats avec un indice    
    s = []
    n = len(systeme_monetaire) # nombre de pièces du système
    i = n - 1  # indice plus grande pièce
    # à compléter

def test_rendu_monnaie_glouton(fonction_rendu):
    """
    Tests unitaires pour une fonction 
    de signature rendu_monnaie_glouton(systeme_monetaire, reste)
    """ 
    systeme_euro = [1, 2, 5, 10, 20, 50, 100, 200, 500]    
    assert fonction_rendu(systeme_euro, 0)  == []
    assert fonction_rendu(systeme_euro, 76) == [50, 20, 5, 1]
    systeme_non_canonique = [1, 3, 6, 12, 24, 30]
    assert fonction_rendu(systeme_non_canonique, 0)  == []
    assert fonction_rendu(systeme_non_canonique, 49)  == [30, 12, 6, 1]
    assert fonction_rendu(systeme_non_canonique, 76) == [30, 30, 12, 3, 1]
    autre_systeme = [2, 3, 4]
    assert fonction_rendu(autre_systeme, 0)  == []
    assert fonction_rendu(autre_systeme, 15)  == [4, 4, 4, 3]
    assert fonction_rendu(autre_systeme, 1) == []
    print("Tests unitaires réussis")
    
    
# tests unitaires, à décommenter
#test_rendu_monnaie_glouton(rendu_monnaie_glouton1)
#test_rendu_monnaie_glouton(rendu_monnaie_glouton2)

#%% Exercice 4

#%%
def compatible(demande1, demande2):
    """
    Déterminer si deux demandes sont compatibles

    Parameters
    ----------
    demande1 : tableau d'entiers [debut1, fin1]
    demande2 : tableau d'entiers [debut2, fin2]

    Returns : booléen
    -------
    """
    debut1, fin1 = demande1
    debut2, fin2 = demande2
    return (fin1 <= debut2) or (fin2 <= debut1)


def test_compatible():
    """Tests unitaires pour la fonction compatible"""
    assert compatible([0, 2], [2, 4]) == True
    assert compatible([0, 2], [1, 4]) == False
    assert compatible([2, 4], [5, 7]) == True
    assert compatible([0, 4], [1, 2]) == False
    print("Tests réussis pour test_compatible")


# à décommenter
# test_compatible()

#%%
def planning_glouton(liste_demande):
    """
    On  examine les séances dans l'ordre de la liste
    et on sélectionne la séance examinée
    si elle est compatible avec tous les choix précédents

    Parametres:
        liste_demande (tableau de tableaux d'entiers [[debut, fin],...]):
            liste de demandes

    Returns:
        tableau de tableaux d'entiers [[debut, fin],...]
        sélectionne dans liste_demande
    """
    selection = []
    for i in range(len(liste_demande)):
        demande = liste_demande[i]
        # à compléter
    return selection


def test_planning_glouton():
    """
    Tests unitaires pour planning_glouton
    """
    assert planning_glouton([[0, 3], [1, 2], [2, 3]]) == [[0, 3]]
    assert planning_glouton([[1, 2], [0, 3], [2, 3]]) == [[1, 2], [2, 3]]
    assert planning_glouton([[2, 3], [0, 1]]) == [[2, 3], [0, 1]]
    # à compléter avec d'autres tests
    print("Test unitaires réussis")


# à décommenter
# test_planning_glouton()


#%%
def planning_glouton_dernier_choix(liste_demande):
    """
    On  examine les séances dans l'ordre de la liste
    et on sélectionne la séance examinée
    si elle est compatible avec le dernier choix effectué.

    Parametres:
        liste_demande (tableau de tableaux d'entiers [[debut, fin],...]):
            liste de demandes

    Returns:
        tableau de tableaux d'entiers [[debut, fin],...]
        sélectionne dans liste_demande
    """
    selection = []
    for k in range(len(liste_demande)):
        demande = liste_demande[k]
        debut = demande[0]
        fin = demande[1]
        # à compléter
    return selection


def test_planning_glouton_dernier_choix():
    """
    Tests unitaires pour planning_glouton
    """
    assert planning_glouton_dernier_choix([[0, 3], [1, 2], [2, 3]]) == [[0, 3]]
    assert planning_glouton_dernier_choix([[1, 2], [0, 3], [2, 3]]) == [
        [1, 2],
        [2, 3],
    ]
    # à compléter avec d'autres tests
    print("Test unitaires réussis")


# à décommenter
# test_planning_glouton()

#%%


def clef_tri_fin(demande):
    """
    Clef de tri d'un tableau de tableau d'entiers
    [[debut, fin],...]     par fin croissante
    Inverse l'ordre lexicographique de lecture des
    éléments du tableau [debut, fin] passé en paramètre

    Parametre:
        tableau de de deux entiers
    Returns:
        tuple d'entiers
    """
    return [demande[1], demande[0]]


def test_clef_tri_fin():
    """
    Tests unitaires pour clef_tri_fin
    """
    assert sorted([[0, 3], [2, 3], [1, 2]], key=clef_tri_fin) == [
        [1, 2],
        [0, 3],
        [2, 3],
    ]
    assert sorted([[2, 3], [1, 3], [0, 5]], key=clef_tri_fin) == [
        [1, 3],
        [2, 3],
        [0, 5],
    ]
    print("Tests unitaires réussis")


#%% Graphiques
import matplotlib.pyplot as plt
import random


def echantillon(binf, bsup, n):
    """
    Renvoie un échantillon aléatoire de demandes
    sous la forme d'un tableau de tableaux [debut, fin]
    avec debut < fin

    Parametres:
        binf (int) : borne inférieure des dates de début
        bsup (int): borne supérieure des dates de fin
        n (int): taille de l'échantillon

    Returns:
        tableau de tableaux d'entiers : [[debut, fin],...]
    """
    assert binf < bsup  # précondition
    t = []
    for _ in range(n):
        duree = 0  # fin - debut
        while duree == 0:  # fin - debut > 0
            duree = random.randint(0, bsup - binf)
        debut = random.randint(binf, bsup - duree)
        t.append([debut, debut + duree])
    return t


def graphique(echantillon, solution=None):
    """

    Parametres:
        echantillon : tableau de tableaux d'entiers : [[debut, fin],...]
                      représente un échantillon aléatoire de demandes
        solution : extrait d'echantillon sélectionné dans le planning
    """
    fig, ax = plt.subplots(figsize=(10, 5))
    ax.grid(True)
    ax.set_yticks([])
    ax.set_xlabel("Temps")
    ax.set_title("Demandes de réservation")
    n = len(echantillon)
    for k in range(n):
        debut, fin = echantillon[k]
        if (solution is not None) and (echantillon[k] in solution):
            plt.plot([debut, fin], [k + 1, k + 1], linewidth=2, marker="o")
        else:
            plt.plot(
                [debut, fin],
                [k + 1, k + 1],
                linewidth=2,
                linestyle="dashed",
                marker="o",
            )
    plt.show()


#%% Comparaison des différents algos gloutons


def comparaison_choix(nb_essais, binf, bsup, nb_demandes):
    """
    Pour nb_essais essais, représente graphiquement
    le nombre de demandes satisfaites
    pour les algos gloutons avec d'abord un tri par début croissant,
    un tri par fin  croissante ou sans tri


    Parametres:
        nb_essais (int): nombre d'essais
        binf (int): borne inférieure des dates de debut
        bsup (int): borne supérieure des dates de fin
        nb_demandes (int): nombre de demandes par essai
    """
    fig, ax = plt.subplots(figsize=(10, 5))
    t1 = []
    t = []
    tdc = []
    tfc = []
    for _ in range(nb_essais):
        e = echantillon(binf, bsup, nb_demandes)
        p1 = planning_glouton(e)
        p = planning_glouton_dernier_choix(e)
        pdc = planning_glouton_dernier_choix(sorted(e))
        pfc = planning_glouton_dernier_choix(sorted(e, key=clef_tri_fin))
        t1.append(len(p1))
        t.append(len(p))
        tdc.append(len(pdc))
        tfc.append(len(pfc))
    ax.set_title(
        f"Comparaison d'heuristiques : binf = {binf} | bsup = {bsup} | demandes = {nb_demandes}"
    )
    ax.plot(t1, label="sans tri", marker="s")
    ax.plot(t, label="sans tri dernier choix", marker="o")
    ax.plot(tdc, label="tri début croissant", marker="+")
    ax.plot(tfc, label="tri fin croissante", marker="x")
    ax.legend(loc="best")
    plt.show()

# à décommenter
#comparaison_choix(100, 0, 100, 50)

#%% Exercice 5

import csv # import du module

def csv_vers_table(chemin):
    """
    Extrait le contenu d'un fichier csv
    dans un table

    Paramètre:
        chemin (str): chemin vers le fichier csv
    
    Retour:
        tableau de dictionnaires 
        dont les clefs sont les attributs de la table 
        (titres des colonnes en première ligne du fichier csv)        
    """
    f = open(chemin, mode='r', encoding='utf8', newline='')
    # création d'un objet reader
    reader = csv.DictReader(f, delimiter=',') 
    table = [dict(enregistrement) for enregistrement in reader]
    f.close()
    return table

#%%
def incrementation_binaire(tableau_bits):
    """
    Renvoie la représentation binaire
    du successeur de l'entier représenté
    en binaire par tableau_bits
    (sur le même nombre de bits)

    Parametre:
        tableau_bits : tableau de bits (0 ou 1)

    Returns:
        tableau de bits (O ou 1) de même taille que tableau_bits
    """
    # copie superficielle de tableau_bits
    copie = tableau_bits[:]
    # à compléter

def test_incrementation_binaire():
    """
    Tests unitaires pour incrementation_binaire
    """
    assert incrementation_binaire([0, 0]) == [0, 1]
    assert incrementation_binaire([0, 1]) == [1, 0]
    assert incrementation_binaire([1, 0]) == [1, 1]
    assert incrementation_binaire([0, 1, 1]) == [1, 0, 0]
    print("Tests réussis")

def liste_parties(n):
    """
    Renvoie la liste des parties 
    d'un ensemble à n éléments codées en binaire
    
    Parametre:
        n (int) : taille de l'ensemble
    
    Retour:
        Tableau de tableaux de bits de taille n    
    """    
    dernier = [0] * n
    liste = [dernier]
    for k in range(2 ** n - 1):
        "à compléter"
    return liste

def test_liste_parties():
    """
    Tests unitaires pour liste_parties
    """
    assert liste_parties(1) == [[0], [1]]
    assert liste_parties(2) == [[0, 0], [0, 1], [1, 0], [1, 1]]
    assert liste_parties(3) == [[0, 0, 0], [0, 0, 1], [0, 1, 0],
                                [0, 1, 1], [1, 0, 0], [1, 0, 1],
                                [1, 1, 0], [1, 1, 1]]
    print("Tests réussis")

def somme_tab(t):
    """
    Renvoie la somme des éléments d'un tableau de nombres

    Parameters:
        t : tableau de nombres
    
    Returns:
        un nombre
    """
    s = 0
    # à compléter

def sac_force_brute(table, masse_max):
    """
    Renvoie une valeur maximale
    et une sélection d'objets la réalisant
    pour une contrainte de masse masse_max
    et une table d'objets  


    Parameters:
        table : tableau de dictionnaires 
                d'attributs ('objet', 'valeur', 'masse')
        masse_max (int): masse maximale du sac

    Returns:
        tuple :
            valeur_max (int)
            selection_max (tableau de dictionnaires)        
    """
    n = len(table)
    parties = liste_parties(n)
    selection_max =  [objet for objet in table if float(objet['masse']) <= masse_max][0]
    valeur_max =  float(selection_max['valeur'])    
    for p in parties:
        selection = [table[k] for k in range(n) if p[k] == 1]
        # à compléter    
    return valeur_max, selection_max

def test_sac_force_brute():
    """
    Tests unitaires
    pour sac_force_brute
    """
    sac1 = csv_vers_table('sac1.csv')
    assert sac_force_brute(sac1, 20) == (11.0,
 [{'objet': '1', 'valeur': '2', 'masse': '10'},
  {'objet': '3', 'valeur': '4', 'masse': '7'},
  {'objet': '4', 'valeur': '5', 'masse': '3'}])
    sac2 = csv_vers_table('sac2.csv')
    assert sac_force_brute(sac2, 40) == (37.0,
 [{'objet': '3', 'valeur': '8', 'masse': '7'},
  {'objet': '5', 'valeur': '8', 'masse': '6'},
  {'objet': '6', 'valeur': '10', 'masse': '15'},
  {'objet': '7', 'valeur': '11', 'masse': '8'}])
    print("Tests réussis")
        
#%%

def sac_glouton(table, masse_max):
    """
    Renvoie une valeur totale de sac
    et une sélection d'objets remplissant le sac
    Sélection gloutonne des objets dans l'ordre 
    de parcours de table en choisissant 
    l'objet courant s'il peut s'ajouter au sac

    Parameters:
        table : tableau de dictionnaires 
                d'attributs ('objet', 'valeur', 'masse')
        masse_max (int): masse maximale du sac

    Returns:
        tuple :
            valeur_max (int)
            selection_max (tableau de dictionnaires)        
    """
    i = 0
    total_masse = 0
    total_valeur = 0
    n = len(table)
    selection = []
    # à compléter
    return total_valeur, selection

#%%
def clef_tri_masse_croissant(objet):
    return float(objet['masse'])

def clef_tri_valeur_decroissant(objet):
    return -float(objet['valeur'])

def clef_tri_ratio_decroissant(objet):
    return  -float(objet['valeur']) / float(objet['masse'])

#%%
def test_sac_glouton():
    """
    tests unitaires pour sac_glouton
    """
    # à compléter
    print("Tests réussis")




