---
layout: parc
title: Séances
---

??? done "Matériel et méthode de travail"

    * Matériel :
        * Un classeur avec des feuilles pour écrire et des pochettes transparentes pour ranger les cours
        * Mettre le site <https://frederic-junier.gitlab.io/parc-nsi-premiere/> dans les favoris de son navigateur, navigation responsive adaptée aux smartphone : toutes les ressources (cours, corrigés) sont publiés et disponibles sur ce site.des liens vers des QCM externes et des exercices pour travailler les automatismes.
        * Une clef USB de 8 Go minimum, cet [article](https://www.boulanger.com/c/cle-usb) n'est pas cher.
        * Le manuel Hachette NSI version papier fourni par la région de référence `978-2-01-786630-5`, accessible en ligne sur <https://mesmanuels.fr/acces-libre/3813624>

    * Méthode de travail :
        * D'une séance à l'autre : relire le cours, faire les exercices
        * Pendant la séance : alternance de temps d'activités et de synthèse, travail sur des projets    à rendre
        * Évaluations :
            * Rendu de mini-projet ou de projet plus conséquent (pendant les vacances) : travail en classe et à la maison en binôme, évaluations écrites ou orales
            * Formatives sous forme d'interrogations courtes (format QCM) ou d'exposés oral (histoire de l'informatique, synthèse de cours)
            * Sommatives sous forme de devoir d'une heure ou de TP noté
            * Autres (exposés, création d'un tuto video)

??? bug "Septembre"

    ??? done "Séance 1 : mardi 5/09/2023"

        * 🎯 Chapitre 1: constructions de bases en langage Python

            1. Rejoindre le groupe __ParcPremiereNSI__ sur <http://www.france-ioi.org>  (se créer un compte d'abord) et commencer le chapitre 1 (affichage) du niveau 1.
            2. [TP 1](../chapitre1/TP1/1NSI-Chap1-Variables-TP1-.pdf) : instructions; expressions, variables, erreurs. Sur Capytale < <https://capytale2.ac-paris.fr/web/c/a5e6-644695/mcer>

        * ✍️ Travail à faire 

            1. Rejoindre le groupe __ParcPremiereNSI__ sur <http://www.france-ioi.org>  (se créer un compte d'abord) et finir le chapitre 1 (affichage) du niveau 1.
            2. Faire les exos 5 et 6 du TP1 sur Capytale <https://capytale2.ac-paris.fr/web/c/a5e6-644695/mcer>


        ![map](ressources/map.jpg){:align="center"}


    ??? done "Séance 2 : mercredi 6/09/2023"

        * 🎯 Chapitre 1: constructions de bases en langage Python

            1.  [TP 1](../chapitre1/TP1/1NSI-Chap1-Variables-TP1-.pdf) : correction des exos 3 et 4 dans  Capytale  <https://capytale2.ac-paris.fr/web/c/a5e6-644695/mcer>.
            2.  Le point sur les constructions de base d'un langage de programmation [Section 1 : Bases d'un langage de programmation : instructions, littéraux, expressions](../chapitre1/cours/Chap1-Bases-Programmation-2021.pdf). Les thèmes suivants sont abordés :
    
                * ➡️ _Bases d'un langage de programmation : instructions, littéraux, expressions_, exemple d'un programme avec entrée/sortie (calcul d'âge à partir de la date de naissance)
                * ➡️ _Instructions conditionnelles_ :  Exercice 5 p. 42, QCM question 6 p. 39
                * ➡️ _Boucles bornées_ 
                * ➡️ _Boucles non bornées_ 
    
            3. On avance sur le  [TP2](../chapitre1/TP2/TP2.pdf) en s'aidant de la synthèse de cours. Faire les exercices dans ce carnet [Capytale](https://capytale2.ac-paris.fr/web/c/63ce-1776650)

                Ressources pour le [TP2](../chapitre1/TP2/TP2.pdf)  :
                Liens vers les corrigés :

                * [corrigé pdf](../chapitre1/TP2/correction/Correction_TP2.pdf)
                * [corrigé .py](https://gitlab.com/frederic-junier/parc-nsi/-/raw/master/docs/chapitre1/TP2/correction/Correction_TP2.py)


        * ✍️ Travail à faire 
            1.    Sur [France IOI](http://www.france-ioi.org)  : avancer le chapitre 4 jusqu'au premier exercice de validation _La Grande Braderie_. 
            2.   Finir les exercices du TP2 jusqu'à l'exercice 9 : [Capytale](https://capytale2.ac-paris.fr/web/c/63ce-1776650).


    ??? done "Séance 3 : mercredi 13/09/2023"


        * 🎯 Chapitre 1: constructions de bases en langage Python
    
            1.  Le point sur les constructions de base d'un langage de programmation [Section 1 : Bases d'un langage de programmation : instructions, littéraux, expressions](../chapitre1/cours/Chap1-Bases-Programmation-2021.pdf). Les thèmes suivants sont abordés :
                * ➡️ _Bases d'un langage de programmation : instructions, littéraux, expressions_, exemple d'un programme avec entrée/sortie (calcul d'âge à partir de la date de naissance)
                * ➡️ _Instructions conditionnelles_ : 
                * ➡️ _Boucles bornées_ 
                * ➡️ _Boucles non bornées_ 
    
            2. On avance sur le  [TP2](../chapitre1/TP2/TP2.pdf) en s'aidant de la synthèse de cours. Faire les exercices dans ce carnet [Capytale](https://capytale2.ac-paris.fr/web/c/63ce-1776650)

                Ressources pour le [TP2](../chapitre1/TP2/TP2.pdf)  :
                Liens vers les corrigés :

                * [corrigé pdf](../chapitre1/TP2/correction/Correction_TP2.pdf)
                * [corrigé .py](https://gitlab.com/frederic-junier/parc-nsi/-/raw/master/docs/chapitre1/TP2/correction/Correction_TP2.py)

        * ✍️ Travail à faire 
            * A faire pour  mardi 19/09 :

                1.  Sur [France IOI](http://www.france-ioi.org)  : avancer le chapitre 4 jusqu'au premier exercice de validation _La Grande Braderie_. 
                2.  Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](../Culture/expos%C3%A9s.md) ou de proposer un sujet à faire valider par l'enseignant.
                3.  Finir les exercices d'entraînement du [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/a5da-1816952).

            * Pour  Mercredi 20/09 

                Interrogation écrite sur les constructions de base d'une programme : variables, expressions, affectations, structures conditionnelles, boucles bornées ou non, fonctions.

                En plus des exercices faits en classe et du polycopié de cours , un [très bon cours de Franck Chambon](https://e-nsi.forge.aeif.fr/init_python/)



    ??? done "Séance 4 : mardi 19/09/2023"

        * 🎯 Chapitre 1: constructions de bases en langage Python

            1. On passe en revue toutes les constructions de base en Python (variables, conditionnelles, boucles, fonctions voir [cours](../chapitre1/cours/Chap1-Bases-Programmation-2021.pdf)) dans ces deux carnets Capytale : 

                * Correction : [Carnet Capytale, exercices du site e-nsi](https://capytale2.ac-paris.fr/web/c/a5da-1816952)
                * [16 exercices du jour](https://capytale2.ac-paris.fr/web/c/4db4-1856946)

        * ✍️ Travail à faire 
            * Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](../Culture/expos%C3%A9s.md) ou de proposer un sujet à faire valider par l'enseignant.
            * Interrogation écrite sur les constructions de base d'une programme : variables, expressions, affectations, structures conditionnelles, boucles bornées ou non, fonctions. En plus des exercices faits en classe et du polycopié de cours , un [très bon cours de Franck Chambon](https://e-nsi.forge.aeif.fr/init_python/)



                
    ??? done "Séance 5 : mercredi 20/09/2023"

        * 💯 Interrogation sur les constructions de base en langage Python

        * 🎯 Chapitre 1: constructions de bases en langage Python

            1. On passe en revue toutes les constructions de base en Python (variables, conditionnelles, boucles, fonctions voir [cours](../chapitre1/cours/Chap1-Bases-Programmation-2021.pdf)) dans ces deux carnets Capytale : 

                * [16 exercices du jour](https://capytale2.ac-paris.fr/web/c/4db4-1856946)
                * [Exercices supplémentaires de France IOI](https://capytale2.ac-paris.fr/web/c/9cd0-1866888)

        * ✍️ Travail à faire 
            * Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](../Culture/expos%C3%A9s.md) ou de proposer un sujet à faire valider par l'enseignant.
            * Traiter les six premiers exercices du carnet Capytale [Exercices supplémentaires de France IOI](https://capytale2.ac-paris.fr/web/c/9cd0-1866888)

    
    ??? done "Séance 6 : mardi 26/09/2023"

        * 💯 Retour de l'interrogation sur les constructions de base en langage Python. [Lien de téléchargement](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/folder/view.php?id=2065)
        * Correction des six premiers exercices du carnet [Exercices supplémentaires de France IOI](https://capytale2.ac-paris.fr/web/c/9cd0-1866888)
        * 🎯 Chapitre 2 : HTML-CSS
            * Pour commencer un [QCM Moodle](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/quiz/view.php?id=1127).
            * [Synthèse de cours](../chapitre2/memo/MemoHTML-CSS-2020.pdf)  ➡️ pages 1 à 3
            * Télécharger le [le mini-site portable](https://0690026d.moodle.ent.auvergnerhonealpes.fr/pluginfile.php/7970/mod_resource/content/1/HTML-CSS.zip)
            * Activités proposées dans [le mini-site portable](https://0690026d.moodle.ent.auvergnerhonealpes.fr/pluginfile.php/7970/mod_resource/content/1/HTML-CSS.zip)  :
                * Dans `site/premiere_page_html/html_balises.html` faire les exercices 2, 3, 4, 5 et 6 : lire les consignes sur le mini-site et traiter les exercices dans Capytale :
        
                    * [exercice 2](https://capytale2.ac-paris.fr/web/c/2c4a-61119/mcer)
                    * [exercice 3](https://capytale2.ac-paris.fr/web/c/0f0c-61116/mcer)
                    * [exercice 4](https://capytale2.ac-paris.fr/web/c/6caa-61110/mcer)
                    * [exercice 5](https://capytale2.ac-paris.fr/web/c/6226-61112/mcer)
                    * [exercice 6](https://capytale2.ac-paris.fr/web/c/29bf-61114/mcer)
                *  Dans `site/premiere_feuille_css/css_selecteurs.html` lire le cours sur les sélecteurs CSS par identifiant ou par classe, puis faire :
    
                    *  [exercice 2](https://capytale2.ac-paris.fr/web/code/9d3b-61121)
    
        * ✍️ Travail à faire 
            * Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](../Culture/expos%C3%A9s.md) ou de proposer un sujet à faire valider par l'enseignant.
            * Choisir un binôme et un mini-projet :
                * les [sujets](./Projets/Projets2024/mini_projets/NSI-MiniProjets1-2024-Sujets.pdf)
                * s'inscrire dans ce [tableau](https://nuage03.apps.education.fr/index.php/s/Xi84bbydYN5a86f)





    ??? done "Séance 7 : mercredi 27/09/2023"

        * 🎯 Chapitre 2 : HTML-CSS
            * [Synthèse de cours](../chapitre2/memo/MemoHTML-CSS-2020.pdf)  ➡️ pages 1 à 3
            * Télécharger le [le mini-site portable](https://0690026d.moodle.ent.auvergnerhonealpes.fr/pluginfile.php/7970/mod_resource/content/1/HTML-CSS.zip)
            * Activités proposées dans [le mini-site portable](https://0690026d.moodle.ent.auvergnerhonealpes.fr/pluginfile.php/7970/mod_resource/content/1/HTML-CSS.zip)  :
                * Dans `site/premiere_page_html/html_balises.html` faire les exercices 2, 3, 4, 5 et 6 : lire les consignes sur le mini-site et traiter les exercices dans Capytale :
        
                    * [exercice 2](https://capytale2.ac-paris.fr/web/c/2c4a-61119/mcer)
                    * [exercice 3](https://capytale2.ac-paris.fr/web/c/0f0c-61116/mcer)
                    * [exercice 4](https://capytale2.ac-paris.fr/web/c/6caa-61110/mcer)
                    * [exercice 5](https://capytale2.ac-paris.fr/web/c/6226-61112/mcer)
                    * [exercice 6](https://capytale2.ac-paris.fr/web/c/29bf-61114/mcer)
                *  Dans `site/premiere_feuille_css/css_selecteurs.html` lire le cours sur les sélecteurs CSS par identifiant ou par classe, puis faire :

                    *  [exercice 2](https://capytale2.ac-paris.fr/web/code/9d3b-61121)

        * Mini-Projet :
            *  Choisir un binôme et un mini-projet puis commencer à travailler :
                * les [sujets](./Projets/Projets2024/mini_projets/NSI-MiniProjets1-2024-Sujets.pdf)
                * s'inscrire dans ce [tableau](https://nuage03.apps.education.fr/index.php/s/Xi84bbydYN5a86f)
                * Voici un lien vers un carnet avec une [boîte à outils](https://capytale2.ac-paris.fr/web/c/70dc-1958589) avec des compléments sur les chaînes de caractères, les tableaux, les dictionnaires, des types de données *conteneurs* qui peuvent être utiles pour certains sujets.

        * ✍️ Travail à faire 
            * Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](../Culture/expos%C3%A9s.md) ou de proposer un sujet à faire valider par l'enseignant.
            * Avancer son mini-projet
            * Traiter les exercices 9, 10 et 11 du carnet Capytale <https://capytale2.ac-paris.fr/web/c/9cd0-1866888>

    
??? bug "Octobre"

    ??? done "Séance 7 : mardi 03/10/2023"

        * 🗣️ Un exposé oral de 5 minutes. Voir [fiche d'évaluation](./Culture/grille_evaluation_oral.md)
        * Correction des exercices 9, 10 et 11 du carnet Capytale <https://capytale2.ac-paris.fr/web/c/9cd0-1866888>

        * Mini-Projet :
            *  Au travail ! Date limite de rendu : le jeudi 12/10 à 23h00 au plus tard, déposez votre programme nommé `SujetXX-NomEleve1-NomEleve2.py` avec `XX` le numéro du sujet dans cet espace de dépôt sur Moodle : <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=2079>. L'oral (5 minutes maximum par binôme) aura lieu le mercredi 18/10.
                * les [sujets](./Projets/Projets2024/mini_projets/NSI-MiniProjets1-2024-Sujets.pdf)
                * s'inscrire dans ce [tableau](https://nuage03.apps.education.fr/index.php/s/Xi84bbydYN5a86f)
                * Voici un lien vers un carnet avec une [boîte à outils](https://capytale2.ac-paris.fr/web/c/70dc-1958589) avec des compléments sur les chaînes de caractères, les tableaux, les dictionnaires, des types de données *conteneurs* qui peuvent être utiles pour certains sujets.

        
        
        * 🎯  Chapitre 3 : fonctions, spécification et tests

            * [Cours](./chapitre3.md)
            * Vous faites les exercices dans le [carnet Capytale](https://capytale2.ac-paris.fr/web/c/23a7-84924/mcer). Aujourd'hui :
                * Définition d'une fonction :exercices 1, 2 et 3
                * Utilisation d'une bibliothèque : exercice 4 
           

        * ✍️ Travail à faire 
            * Avancer son mini-projet
            * Traiter les exercices 1 et 2  du carnet Capytale <https://capytale2.ac-paris.fr/web/c/18b9-1972969>


    ??? done "Séance 8 : mercredi 04/10/2023"

        * Correction des  exercices 1 et 2  du carnet Capytale <https://capytale2.ac-paris.fr/web/c/18b9-1972969>

        * Mini-Projet :
            *  Au travail ! Date limite de rendu : le jeudi 12/10 à 23h00 au plus tard, déposez votre programme nommé `SujetXX-NomEleve1-NomEleve2.py` avec `XX` le numéro du sujet dans cet espace de dépôt sur Moodle : <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=2079>. L'oral (5 minutes maximum par binôme) aura lieu le mercredi 18/10.
                * les [sujets](./Projets/Projets2024/mini_projets/NSI-MiniProjets1-2024-Sujets.pdf)
                * s'inscrire dans ce [tableau](https://nuage03.apps.education.fr/index.php/s/Xi84bbydYN5a86f)
                * Voici un lien vers un carnet avec une [boîte à outils](https://capytale2.ac-paris.fr/web/c/70dc-1958589) avec des compléments sur les chaînes de caractères, les tableaux, les dictionnaires, des types de données *conteneurs* qui peuvent être utiles pour certains sujets.

        
        
        * 🎯  Chapitre 3 : fonctions, spécification et tests

            * [Cours](../chapitre3.md)
            * [Activité Capytale ➡️ cours fonctions](https://capytale2.ac-paris.fr/web/c/23a7-84924/mcer)
            * Vous faites les exercices dans le [carnet Capytale](https://capytale2.ac-paris.fr/web/c/23a7-84924/mcer). Aujourd'hui :
                * Définition d'une fonction :exercices 1, 2 et 3
                * Utilisation d'une bibliothèque : exercice 4 


        * ✍️ Travail à faire 
            * Date limite de dépôt du mini-projet : le jeudi 12/10 à 23h00 au plus tard, déposez votre programme nommé `SujetXX-NomEleve1-NomEleve2.py` avec `XX` le numéro du sujet dans cet espace de dépôt sur Moodle : <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=2079>
            * Traiter l'exercice 2 de ce carnet Capytale <https://capytale2.ac-paris.fr/web/c/993d-1987227>
            * DS sur le chapitre 1 Constructions de base en programmation avec  en plus  fonctions (chapitre 3). Voir [ici](https://0690026d.moodle.ent.auvergnerhonealpes.fr/course/view.php?id=2&section=24) pour le DS 2 2022/2023
        

    ??? done "Séance 8 : mardi 10/10/2023"

        * 🗣️ Un exposé oral de 5 minutes. Voir [fiche d'évaluation](./Culture/grille_evaluation_oral.md)
        * 🏊🏽‍♀️ Correction de l'exercice 2 de ce carnet Capytale <https://capytale2.ac-paris.fr/web/c/993d-1987227>     
        * 🎯  Chapitre 3 : fonctions, spécification et tests

            * [Cours](./chapitre3.md)
            * Vous faites les exercices dans le [carnet Capytale](https://capytale2.ac-paris.fr/web/c/23a7-84924/mcer). Aujourd'hui on termine ce chapitre.
            *  Des exercices supplémentaires dans ce [carnet](https://capytale2.ac-paris.fr/web/c/aeaf-2040132)


        * ✍️ Travail à faire 
            * Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](https://frederic-junier.gitlab.io/parc-nsi-premiere/Culture/expos%C3%A9s.md) ou de proposer un sujet à faire valider par l'enseignant
            * Date limite de dépôt du mini-projet : le jeudi 12/10 à 23h00 au plus tard, déposez votre programme nommé `SujetXX-NomEleve1-NomEleve2.py` avec `XX` le numéro du sujet dans cet espace de dépôt sur Moodle : <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=2079>
            * DS sur le chapitre 1 Constructions de base en programmation avec  en plus  fonctions (chapitre 3). Voir [ici](https://0690026d.moodle.ent.auvergnerhonealpes.fr/course/view.php?id=2&section=24) pour les sujets des années précédentes.


    ??? done "Séance 9 : mercredi 11/10/2023"

       
        * 💯 DS n°2
        * 🎯  Chapitre 3 : fonctions, spécification et tests

            * [Cours](./chapitre3.md)
            * Vous faites les exercices dans le [carnet Capytale](https://capytale2.ac-paris.fr/web/c/23a7-84924/mcer). Aujourd'hui on termine ce chapitre.
            *  Des exercices supplémentaires dans ce [carnet](https://capytale2.ac-paris.fr/web/c/aeaf-2040132)

        * 🎯  Chapitre 4 : tableaux à une dimension

            Les élèves à l'aise avancent en autonomie les autres sont guidés par l'enseignant.

            * [Cours](../chapitre4.md) 
            * [Exemples du cours en version notebook (Capytale)](https://capytale2.ac-paris.fr/web/c/68ac-101047)

        * ✍️ Travail à faire 
            * Finir les exercices de ce [carnet](https://capytale2.ac-paris.fr/web/c/aeaf-2040132)
            * Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](https://frederic-junier.gitlab.io/parc-nsi-premiere/Culture/expos%C3%A9s.md) ou de proposer un sujet à faire valider par l'enseignant
            * Date limite de dépôt du mini-projet : le jeudi 12/10 à 23h00 au plus tard, déposez votre programme nommé `SujetXX-NomEleve1-NomEleve2.py` avec `XX` le numéro du sujet dans cet espace de dépôt sur Moodle : <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=2079>


    ??? done "Séance 10 : mardi 17/10/2023"

        * 🗣️ Un exposé oral de 5 minutes. 
        * 💯 Retour du DS n°2, correction de l'exercice 5
        * 🎯  Chapitre 3 : fonctions, spécification et tests
            *  Exercice sur les opérateurs booléens : correction de l'exercice 3 de ce [carnet](https://capytale2.ac-paris.fr/web/c/993d-1987227) + table de vérité de `not(a or not(b))`.
            *  Des exercices supplémentaires dans ce [carnet](https://capytale2.ac-paris.fr/web/c/aeaf-2040132) :  correction des exercices 4, 5 et 6
            *  Encore des exercices supplémentaires, avec un [carnet spécial shadok](https://capytale2.ac-paris.fr/web/c/2e20-2104997)

        * 🎯  Chapitre 4 : tableaux à une dimension

            Les élèves à l'aise avancent en autonomie les autres sont guidés par l'enseignant.

            * [Cours](../chapitre4.md) 
            * [Exemples du cours en version notebook (Capytale)](https://capytale2.ac-paris.fr/web/c/68ac-101047)

        * ✍️ Travail à faire 
            * Petite interrogation sur opérateurs booléens, test et boucles.
            * Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](https://frederic-junier.gitlab.io/parc-nsi-premiere/Culture/expos%C3%A9s.md) ou de proposer un sujet à faire valider par l'enseignant


    ??? done "Séance 11 : mercredi 18/10/2023"

        * 🗣️ Un exposé oral de 5 minutes. 
        * 💯 DS n°3 sur opérateurs booléens et boucles
        * 🎯  Chapitre 4 : tableaux à une dimension

            Les élèves à l'aise avancent en autonomie les autres sont guidés par l'enseignant.

            * [Cours](./chapitre6/Cours/tableaux-cours-git.md) 
            * [Exemples du cours en version notebook (Capytale)](https://capytale2.ac-paris.fr/web/c/68ac-101047)

        * ✍️ Travail à faire 
            * Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](https://frederic-junier.gitlab.io/parc-nsi-premiere/Culture/expos%C3%A9s.md) ou de proposer un sujet à faire valider par l'enseignant
            * Pour le mardi 7/11 faire sur Capytale ce DM  <https://capytale2.ac-paris.fr/web/c/7d59-2118004>



??? bug "Novembre"

    ??? done "Séance 12 : mardi 7/11/2023"

        * 💯 Retour du DS n°3 sur opérateurs booléens et boucles avec son corrigé
        * 🎯  Chapitre 4 : tableaux à une dimension

            * Les élèves font d'abord les exercices de ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/c1d1-2193695) en s'aidant du [Cours](./chapitre6/Cours/tableaux-cours-git.md). Le carnet peut être téléchargé dans une  [archive zip](./chapitre6/exercices_tableaux.zip)
            * Ensuite les élèves traitent les [Exercices  du cours en version notebook (Capytale)](https://capytale2.ac-paris.fr/web/c/68ac-101047) : 1, 2, 3, 4, 5 et 8
            * Les élèves qui sont en avance traitent les exercices avec le tag boucle sur le site [/e-nsi.forge.aeif.fr/pratique](https://e-nsi.forge.aeif.fr/pratique/tags/#1-boucle)
         * ✍️ Travail à faire  :
             * Préparer pour demain, l'intervention orale sur le mini projet (2 ou 3 minutes par personne maximum), les critères de notation sont :
                 * la qualité du contenu sur 2 points
                 * la fluidité du discours, le ton et l'assurance de la voix, la variété des inflexions sur 1 point
                 * la posture (corps, regard) sur 1 point


    ??? done "Séance 13 : mercredi 8/11/2023"

          * 🗣️  Évaluation orale du mini-projet
          * 🎯  Chapitre 4 : tableaux à une dimension

              * Les élèves font d'abord les exercices de ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/c1d1-2193695) en s'aidant du [Cours](./chapitre6/Cours/tableaux-cours-git.md). Le carnet peut être téléchargé dans une  [archive zip](./chapitre6/exercices_tableaux.zip)
              * Ensuite les élèves traitent les [Exercices  du cours en version notebook (Capytale)](https://capytale2.ac-paris.fr/web/c/68ac-101047) : 1, 2, 3, 4, 5 et 8
              * Les élèves qui sont en avance traitent les exercices avec le tag boucle sur le site [/e-nsi.forge.aeif.fr/pratique](https://e-nsi.forge.aeif.fr/pratique/tags/#1-boucle)
          * ✍️ Travail à faire  :
              * [DM sur Capytale](https://capytale2.ac-paris.fr/web/c/7d59-2118004) pour mardi
              * Facultatif : [carnet avec d'autres défis sur les tableaux](https://capytale2.ac-paris.fr/web/c/0918-2208003)


    
    ??? done "Séance 14 : mercredi 15/11/2023"

          * 🎯  Chapitre 4 : tableaux à une dimension

              * Les élèves traitent  les exercices 7 à 14 de ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/c1d1-2193695) en s'aidant du [Cours](./chapitre6/Cours/tableaux-cours-git.md).
              * Les élèves qui sont en avance traitent les défis dans ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/fb96-2274861)
          * 🎯 Chapitre 5 : tableaux à deux dimensions / Traitement d'images

              * [Cours / TP en version pdf](../chapitre7/NSI-Images-Tableaux2d--2023V1.pdf)  
              * [Cours / TP en version notebook sur Capytale (avec corrigés)](https://capytale2.ac-paris.fr/web/c/7ad3-163223)
  
          * ✍️ Travail à faire  :
              * Finir les exercices de ce [carnet](https://capytale2.ac-paris.fr/web/c/fb96-2274861) sur les tableaux à une dimension


    ??? done "Séance 15 : mardi 21/11/2023"
          
          * 🏊  Entraînement : puzzle de Parsons, remettre le code dans l'ordre :
              *  [Recherche de maximum](www.codepuzzle.io/PZ72N)
              *  [Recherche linéaire d'un élément](www.codepuzzle.io/PTFZN)
          * 🗣️  Exposé 
          * 🎯 Chapitre 5 : tableaux à deux dimensions / Traitement d'images

              * [Cours / TP en version pdf](../chapitre7/NSI-Images-Tableaux2d--2023V1.pdf)  
              * [Cours / TP en version notebook sur Capytale (avec corrigés)](https://capytale2.ac-paris.fr/web/c/7ad3-163223)

          * 🎯  Chapitre 4 : tableaux à une dimension

              * Correction des exercices de  ce [carnet](https://capytale2.ac-paris.fr/web/c/fb96-2274861) sur les tableaux à une dimension

  
          * ✍️ Travail à faire  :
              *  Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](../Culture/expos%C3%A9s.md) ou de proposer un sujet à faire valider par l'enseignant
              *  Pour le mercredi 28/11/2023 :
                 *  DS (1h) sur la programmation Python et les tableaux à une dimension
              *  Pour mercredi 22/11 :
                 *  Traiter les exercices dans ce 
           
    
    ??? done "Séance 16 : mercredi 22/11/2023"
          
          * 🏊  Entraînement :
              *  correction des exercices sur les tableaux à deux dimensions dans ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/fe19-2326379)
              *  Réponse aux questions sur les huit premiers exercices de ce [carnet](https://capytale2.ac-paris.fr/web/c/fb96-2274861) sur les tableaux à une dimension, qu'il faut impérativement maîtriser pour le devoir
          *  Élèves espagnols : [futurecoder.io](https://fr.futurecoder.io/), traduction en espagnole disponible
          *  Élèves en avance : [mini projet Stéganographie sur Capytale](https://capytale2.ac-paris.fr/web/c/69bf-1019976)
          *  🗣️  Exposé oral
          * 🎯 Chapitre 5 : tableaux à deux dimensions / Traitement d'images

              * [Cours / TP en version pdf](../chapitre7/NSI-Images-Tableaux2d--2023V1.pdf)  
              * [Cours / TP en version notebook sur Capytale (avec corrigés)](https://capytale2.ac-paris.fr/web/c/7ad3-163223)

  
          * ✍️ Travail à faire  :
              *  Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](./Culture/exposés.md) ou de proposer un sujet à faire valider par l'enseignant
              *  Pour le mercredi 28/11/2023 :
                 *  DS (1h) sur la programmation Python et les tableaux à une dimension
                 *  Revoir  :
                    * tous les exercices de ce  [carnet Capytale](https://capytale2.ac-paris.fr/web/c/c1d1-2193695) sur les compétences de base
                    * les exercices 1 à 8 de ce  [carnet Capytale](https://capytale2.ac-paris.fr/web/c/fb96-2274861)
                    * le [Cours / TP en version notebook sur Capytale (avec corrigés)](https://capytale2.ac-paris.fr/web/c/7ad3-163223)
  

    ??? done "Séance 17 : mardi 28/11/2023"

          * 🏊 [QCM sur les tableaux à 1 dimension](https://genumsi.inria.fr/qcm.php?h=45405b2f41716ea7ea773a4e3d3f7177) et [Corrigé](https://genumsi.inria.fr/qcm-corrige.php?cle=Mjg7MTE1OzE0NTsxNDY7Mjc1OzI3ODsyNzk7MzI0OzQ2Njs0NzA7MTM2MTsxMzc0OzEzOTE7MTM5MjsxMzkzOzEzOTQ7MTM5NTsxMzk2OzE0Mjc=)
          
          * 🎯 Chapitre 5 : tableaux à deux dimensions / Traitement d'images

              * [Cours / TP en version pdf](../chapitre7/NSI-Images-Tableaux2d--2023V1.pdf)  
              * [Cours / TP en version notebook sur Capytale (avec corrigés)](https://capytale2.ac-paris.fr/web/c/7ad3-163223)

          *  Élèves espagnols : [futurecoder.io](https://fr.futurecoder.io/), traduction en espagnole disponible
          *  Élèves en avance : [mini projet Stéganographie sur Capytale](https://capytale2.ac-paris.fr/web/c/69bf-1019976)
          *  🎯 Chapitre 6 : représentation des entiers

             * [Cours / TP en version pdf](../chapitre8/Chapitre6-ReprésentationEntiers-2021V1.pdf)
             * [Cours / TP en notebook Python sur Capytale](https://capytale2.ac-paris.fr/web/c/2076-172112)
             * [Corrigé du cours / TP en version pdf](../chapitre8/Cours_Representation_Entiers_Correction.pdf)
             * [Corrigé des exercices de calcul sans Python](https://frederic-junier.gitlab.io/parc-nsi/chapitre8/Chapitre6-Repr%C3%A9sentationEntiers-2021V1.pdf)

  
          * ✍️ Travail à faire  :
              *  Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](./Culture/exposés.md) ou de proposer un sujet à faire valider par l'enseignant
              *  Pour le mercredi 28/11/2023 :
                 *  DS (1h) sur la programmation Python et les tableaux à une dimension
                 *  Revoir  :
                    * tous les exercices de ce  [carnet Capytale](https://capytale2.ac-paris.fr/web/c/c1d1-2193695) sur les compétences de base
                    * les exercices 1 à 8 de ce  [carnet Capytale](https://capytale2.ac-paris.fr/web/c/fb96-2274861)
                    * le [Cours / TP en version notebook sur Capytale (avec corrigés)](https://capytale2.ac-paris.fr/web/c/7ad3-163223)


    ??? done "Séance 18 : mercredi  29/11/2023"

          
          * 💯 DS n°4  sur les tableaux à une dimension
          * 🗣️  Exposé oral
          *  Élèves espagnols : [futurecoder.io](https://fr.futurecoder.io/), traduction en espagnole disponible + Parcours PIX avec le code de campagne NDAGER535
          *  🎯 Chapitre 6 : représentation des entiers

             * [Cours / TP en version pdf](../chapitre8/Chapitre6-ReprésentationEntiers-2021V1.pdf)
             * [Cours / TP en notebook Python sur Capytale](https://capytale2.ac-paris.fr/web/c/2076-172112)
             * [Corrigé du cours / TP en version pdf](../chapitre8/Cours_Representation_Entiers_Correction.pdf)
             * [Corrigé des exercices de calcul sans Python](https://frederic-junier.gitlab.io/parc-nsi/chapitre8/Chapitre6-Repr%C3%A9sentationEntiers-2021V1.pdf)
             * Objectif du jour : exercice 2 (liste des chiffres d'un décimal), exercice 3 (compter en binaire), exercice 4 (conversion gloutonne ou par divisions successives d'un décimal en binaire)

  
          * ✍️ Travail à faire  :
              *  Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](./Culture/exposés.md) ou de proposer un sujet à faire valider par l'enseignant
              *  Pour le mardi 5/12/2023 :
                 *  Faire les exercices de [ce carnet Capytale](https://capytale2.ac-paris.fr/web/c/ca0c-2410438)

??? bug "Décembre"

    ??? done "Séance 19 : mardi 05/12/2023"

          
          * 💯 Retour du DS n°4  sur les tableaux à une dimension
          * 🗣️  Exposé oral
          * 🏊 Correction des exercices de [ce carnet Capytale](https://capytale2.ac-paris.fr/web/c/ca0c-2410438)  sur la représentation des entiers et les changements de base
          *  🎯 Chapitre 6 : représentation des entiers

             * [Cours / TP en version pdf](../chapitre8/Chapitre6-ReprésentationEntiers-2021V1.pdf)
             * [Cours / TP en notebook Python sur Capytale](https://capytale2.ac-paris.fr/web/c/2076-172112)
             * [Corrigé du cours / TP en version pdf](../chapitre8/Cours_Representation_Entiers_Correction.pdf)
             * [Corrigé des exercices de calcul sans Python](https://frederic-junier.gitlab.io/parc-nsi/chapitre8/Chapitre6-Repr%C3%A9sentationEntiers-2021V1.pdf)
             * Objectifs du jour : 
                 * exercice 3 (compter en binaire)
                 * exercice 4 (conversion gloutonne ou par divisions successives d'un décimal en binaire)
                 * exercice 5 (additionneur binaire)
                 * représentation en base 16 : exercice 6
                 * représentation des entiers relatifs : complément à deux, exercices 7 et 8

          * [Carnet Capytale avec exercices sur les tableaux à deux dimensions](https://capytale2.ac-paris.fr/web/c/1c4f-2465363) : exercices 1 à 3
          * ✍️ Travail à faire  :
              *  Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](./Culture/exposés.md) ou de proposer un sujet à faire valider par l'enseignant
              *  Pour le mercredi 6/12/2023 :
                 *  Finir les exercices 1 à 3 de [ce carnet Capytale sur les tableaux à deux dimensions](https://capytale2.ac-paris.fr/web/c/1c4f-2465363)



    ??? done "Séance 20 : mercredi 06/12/2023"

          
        * 💯 Retour du DS n°4  sur les tableaux à une dimension
        * 🗣️  Exposé oral
        * 🏊 Correction des exercices de [ce carnet Capytale sur les tableaux à deux dimensions](https://capytale2.ac-paris.fr/web/c/1c4f-2465363) : exercices 1 et 2
        *  🎯 Chapitre 6 : représentation des entiers

             * [Cours / TP en version pdf](../chapitre8/Chapitre6-ReprésentationEntiers-2021V1.pdf)
             * [Cours / TP en notebook Python sur Capytale](https://capytale2.ac-paris.fr/web/c/2076-172112)
             * [Corrigé du cours / TP en version pdf](../chapitre8/Cours_Representation_Entiers_Correction.pdf)
             * [Corrigé des exercices de calcul sans Python](https://frederic-junier.gitlab.io/parc-nsi/chapitre8/Chapitre6-Repr%C3%A9sentationEntiers-2021V1.pdf)
             * Objectifs du jour : 
                 * représentation en base 16 : exercice 6
                 * représentation des entiers relatifs : complément à deux, exercices 7 et 8
        *  🎯 Chapitre 7 : recherche séquentielle et dichotomique
             * [Cours version pdf](../chapitre10/cours/NSI-Recherches-2021.pdf)
             * [Exercices du cours, carnet Notebook Capytale](https://capytale2.ac-paris.fr/web/c/8c8a-224776)
        * [Carnet Capytale avec exercices sur les tableaux à deux dimensions](https://capytale2.ac-paris.fr/web/c/1c4f-2465363) : exercices 3 à 4
        * ✍️ Travail à faire  :
             *  Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](./Culture/exposés.md) ou de proposer un sujet à faire valider par l'enseignant
             *  Pour le mardi 12/12/2023 :
                *  Finir les exercices 3 et 4 de [ce carnet Capytale sur les tableaux à deux dimensions](https://capytale2.ac-paris.fr/web/c/1c4f-2465363)
                *  Faire les exercices de ce [carnet Capytale sur la recherche séquentielle](https://capytale2.ac-paris.fr/web/c/ef3e-2480184)


    ??? done "Séance 21 : mardi 12/12/2023"

       
        * 🏊 Correction des exercices de ce [carnet Capytale sur la recherche séquentielle](https://capytale2.ac-paris.fr/web/c/ef3e-2480184)
        * 🏊 Correction des exercices de [ce carnet Capytale sur les tableaux à deux dimensions](https://capytale2.ac-paris.fr/web/c/1c4f-2465363)
        *  🎯 Chapitre 7 : recherche séquentielle et dichotomique
             * [Cours version pdf](../chapitre10/cours/NSI-Recherches-2021.pdf)
             * [Exercices du cours, carnet Notebook Capytale](https://capytale2.ac-paris.fr/web/c/8c8a-224776)
        * 🚧 Constitution des groupes pour le [mini-projet Noel 2023](https://capytale2.ac-paris.fr/web/c/c0cf-2381199)  : 
            * code à rendre le mercredi 17/01 et présentation orale le 24/01
        * ✍️ Travail à faire  :
             *  Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](./Culture/exposés.md) ou de proposer un sujet à faire valider par l'enseignant
             *  Pour le mercredi 13/12/2023 :
                *  Faire  les exercices  de [ce carnet Capytale](https://capytale2.ac-paris.fr/web/c/3065-2536221)
             * Pour le mercredi 19/12/2023 : DS 5, réviser les chapitres Tableaux à 1 ou 2 dimensions et représentations des entiers.  Pour réviser => [anciens sujets de DS sur Moodle](https://0690026d.moodle.ent.auvergnerhonealpes.fr/course/view.php?id=2)




    ??? done "Séance 22 : mercredi 13/12/2023"

          
        * 🗣️  Exposé oral
        * 🏊 Correction des exercices de [ce carnet Capytale](https://capytale2.ac-paris.fr/web/c/3065-2536221)
        *  🎯 Chapitre 7 : recherche séquentielle et dichotomique
             * [Cours version pdf](../chapitre10/cours/NSI-Recherches-2021.pdf)
             * [Exercices du cours, carnet Notebook Capytale](https://capytale2.ac-paris.fr/web/c/8c8a-224776)
             * [Mémo Dichotomie](./chapitre10/cours/Dichotomie.md) et [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/75cf-2549106)
             * [Carnet Capytale avec exercices sur la recherche dichotomique](https://capytale2.ac-paris.fr/web/c/ad55-2549759)
        * 🚧 Travail sur le [mini-projet Noel 2024](https://capytale2.ac-paris.fr/web/c/c0cf-2381199)  : 
            * code à rendre le mercredi 17/01 et présentation orale le 24/01
        * ✍️ Travail à faire  :
             *  Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](./Culture/exposés.md) ou de proposer un sujet à faire valider par 
             * Pour le mercredi 19/12/2023 : DS 5, réviser les chapitres Tableaux à 1 ou 2 dimensions et représentations des entiers.  Pour réviser => [anciens sujets de DS sur Moodle](https://0690026d.moodle.ent.auvergnerhonealpes.fr/course/view.php?id=2)


    ??? done "Séance 23 : mardi 19/12/2023"

          
        * 🗣️  Exposé oral
        *  🎯 Chapitre 8 : Système d'exploitation et ligne de commandes
            * Découverte de la ligne de commandes
               * [TP Terminus (version originale de Charles Poulmaire)](./chapitre9/terminus/terminus.md)
               * Memento shell
                   * [Memento shell version pdf](./chapitre9/memento-shell/memento-shell-.pdf)
                   * [Memento shell version markdown](./chapitre9/memento-shell/memento-shell-git.md)
                   * [Matériel  pour les exemples du memento](./chapitre9/memento-shell/sandbox.zip)
            * [Cours Système d'exploitation](https://www.frederic-junier.org/NSI/premiere/chapitre9/) 
            * Répondre par écrit aux [Questions suivantes](https://frederic-junier.org/PremiereNSI2023/rituels/20231219/rituel.html)

             
        * 🚧 Travail sur le [mini-projet Noel 2024](https://capytale2.ac-paris.fr/web/c/c0cf-2381199)  : 
            * code à rendre le mercredi 17/01 et présentation orale le 24/01
        * ✍️ Travail à faire  :
             *  Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](./Culture/exposés.md) ou de proposer un sujet à faire valider par 
             * Pour le mercredi 19/12/2023 : DS 5, réviser les chapitres Tableaux à 1 ou 2 dimensions et représentations des entiers.  Pour réviser => [anciens sujets de DS sur Moodle](https://0690026d.moodle.ent.auvergnerhonealpes.fr/course/view.php?id=2)



    ??? done "Séance 24 : mercredi 20/12/2023"

                   
        * 💯  DS 5 sur tableaux à 1 ou 2 dimensions et représentations des entiers
        *  🎯 Chapitre 8 : Système d'exploitation et ligne de commandes
            * Répondre par écrit aux [Questions suivantes](https://frederic-junier.org/PremiereNSI2023/rituels/20231219/rituel.html)

             
        * 🚧 Travail sur le [mini-projet Noel 2024](https://capytale2.ac-paris.fr/web/c/c0cf-2381199)  : 
            * code à rendre le mercredi 17/01 et présentation orale le 24/01
        * ✍️ Travail à faire  :
            *  Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](./Culture/exposés.md) ou de proposer un sujet à faire valider par 
            * Pour le mercredi 17/01/2024 : envoyer par la messagerie ENT une archive zip avec le code du projet et tous les fichiers ressources nécessaires sous le nom `titre_projet_eleve1_eleve2.zip`.  
            * Pour le mercredi 24/01/2024 déposer une capsule video du projet sur la [nouvelle plateforme Moodle Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=2601) :
                * Description du cahier des charges
                * Répartition des tâches
                * Description de la solution algorithmique proposée
                * Commentaire d'un extrait de code
                * Démonstration d'un jeu de tests ou d'un exemple d'exécution

??? bug "Janvier"


    ??? done "Séance 25 : mardi 9/01/2024"

                   
        * 💯  Retour du DS 5 sur tableaux à 1 ou 2 dimensions et représentations des entiers avec corrigé
        * [Rituel du jour sur Capytale](https://capytale2.ac-paris.fr/web/c/90db-2678532) : quatre exercices sur les recherches séquentielle et dichotomique, les tableaux à 2 dimensions et la conversion d'un décimal en binaire
        *  🎯 Chapitre 8 : Système d'exploitation et ligne de commandes
            * Répondre par écrit aux [Questions suivantes](https://frederic-junier.org/PremiereNSI2023/rituels/20231219/rituel.html)
        * 🎯 Chapitre 9 : Codage des caractères :

            * On traite les exercices 1 à 4 de manipulation de chaînes de caractères.
            * On présente les notions de table de codage et d'encodage et on traite les exercices 5 et 6 (QCM).
            * [Cours version pdf](../chapitre12/cours/NSI-CodageCaracteres-2020V1.pdf)
            * [Exercices  du cours avec corrections sur Capytale ](https://capytale2.ac-paris.fr/web/c/9192-288733)

              1. Table de codage ASCII  : exercices 2, 3 et 4 
              2. Tables de codages ISO-8859 !: exercice 5
              3. Tables de codages ISO-8859 : exercices 6 et 7
   
            ![Emojis, caractères de points de codes entre U+1F600 et U+1F64F](../chapitre12/cours/images/emojis.png)

             
        * 🚧 Travail sur le [mini-projet Noel 2024](https://capytale2.ac-paris.fr/web/c/c0cf-2381199)  : 
            * code à rendre le mercredi 17/01 et présentation orale le 24/01
        * ✍️ Travail à faire  :
            *  Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](./Culture/exposés.md) ou de proposer un sujet à faire valider par 
            * Pour le mercredi 17/01/2024 : envoyer par la messagerie ENT une archive zip avec le code du projet et tous les fichiers ressources nécessaires sous le nom `titre_projet_eleve1_eleve2.zip`.  
            * Pour le mercredi 24/01/2024 : Interrogation sur les recherches séquentielle, dichotomique, les systèmes d'exploitation et la ligne de commande
            * Pour le mercredi 31/01/2024 déposer une capsule video du projet sur la [nouvelle plateforme Moodle Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=2601) :
                * Description du cahier des charges
                * Répartition des tâches
                * Description de la solution algorithmique proposée
                * Commentaire d'un extrait de code
                * Démonstration d'un jeu de tests ou d'un exemple d'exécution


    ??? done "Séance 26 : mercredi 10/01/2024"

                    
        * 💯  Retour du DS 5 sur tableaux à 1 ou 2 dimensions et représentations des entiers avec corrigé
        * 🗣️  Exposé oral
        * [Rituel du jour sur Capytale](https://capytale2.ac-paris.fr/web/c/b2d2-2693455) : exercices sur les listes/tableaux
        *  🎯 Chapitre 8 : Système d'exploitation et ligne de commandes
            * Répondre par écrit aux [Questions suivantes](https://frederic-junier.org/PremiereNSI2023/rituels/20231219/rituel.html)
        * 🎯 Chapitre 9 : Codage des caractères :

            * On traite les exercices 1 à 4 de manipulation de chaînes de caractères et les QCM des exercices 5 et 6
            * On présente les notions de table de codage et d'encodage et on traite les exercices 5 et 6 (QCM).
            * [Cours version pdf](../chapitre12/cours/NSI-CodageCaracteres-2020V1.pdf)
            * [Exercices  du cours avec corrections sur Capytale ](https://capytale2.ac-paris.fr/web/c/9192-288733)

            1. Table de codage ASCII  : exercices 2, 3 et 4 
            2. Tables de codages ISO-8859 !: exercice 5
            3. Tables de codages ISO-8859 : exercices 6 

            ![Emojis, caractères de points de codes entre U+1F600 et U+1F64F](../chapitre12/cours/images/emojis.png)

            
        * 🚧 Travail sur le [mini-projet Noel 2024](https://capytale2.ac-paris.fr/web/c/c0cf-2381199)  : 
            * code à rendre le mercredi 17/01 et présentation orale le 24/01
        * ✍️ Travail à faire  :
            *  Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](./Culture/exposés.md) ou de proposer un sujet à faire valider par 
            * Pour le mercredi 17/01/2024 : envoyer par la messagerie ENT une archive zip avec le code du projet et tous les fichiers ressources nécessaires sous le nom `titre_projet_eleve1_eleve2.zip`.  
            * Pour le mercredi 24/01/2024 : Interrogation sur les recherches séquentielle, dichotomique, les systèmes d'exploitation et la ligne de commande
            * Pour le mercredi 31/01/2024 déposer une capsule video du projet sur la [nouvelle plateforme Moodle Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=2601) :
                * Description du cahier des charges
                * Répartition des tâches
                * Description de la solution algorithmique proposée
                * Commentaire d'un extrait de code
                * Démonstration d'un jeu de tests ou d'un exemple d'exécution
  

    ??? done "Séance 27 : mardi 16/01/2024"

        * Correction des exercices de ce [ce carnet](https://capytale2.ac-paris.fr/web/c/b2d2-2693455) : exercices sur les listes/tableaux
        * 🎯 Chapitre 9 : Codage des caractères :

            * On traite les exercices 1 à 4 de manipulation de chaînes de caractères et les QCM des exercices 5 et 6
            * On présente les notions de table de codage et d'encodage et on traite les exercices 5 et 6 (QCM).
            * [Cours version pdf](../chapitre12/cours/NSI-CodageCaracteres-2020V1.pdf)
            * [Exercices  du cours avec corrections sur Capytale ](https://capytale2.ac-paris.fr/web/c/9192-288733)

            1. Table de codage ASCII  : exercices 2, 3 et 4 
            2. Tables de codages ISO-8859 !: exercice 5
            3. Tables de codages ISO-8859 : exercices 6 

            ![Emojis, caractères de points de codes entre U+1F600 et U+1F64F](../chapitre12/cours/images/emojis.png)
        * 🏊 Exercices supplémentaires sur les chaînes de caractères dans ce [carnet](https://capytale2.ac-paris.fr/web/c/7e19-2746023)
        * 🎯 Chapitre 10 : Algorithmes de tri
            * [Cours/TP version pdf](../chapitre11/1NSI-Cours-Tris-2021V1.pdf)
            * [Cours/TP sur Capytale](https://capytale2.ac-paris.fr/web/c/967c-255137)
        

        * ✍️ Travail à faire  :
            * Pour le mercredi 17/01 : finir les exercices de ce [carnet](https://capytale2.ac-paris.fr/web/c/7e19-2746023)
            *  Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](./Culture/exposés.md) ou de proposer un sujet à faire valider par 
            * Pour le mercredi 17/01/2024 : envoyer par la messagerie ENT une archive zip avec le code du projet et tous les fichiers ressources nécessaires sous le nom `titre_projet_eleve1_eleve2.zip`.  
            * Pour le mercredi 24/01/2024 : Interrogation sur les recherches séquentielle, dichotomique, les systèmes d'exploitation et la ligne de commande
            * Pour le mercredi 31/01/2024 déposer une capsule video du projet sur la [nouvelle plateforme Moodle Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=2601) :
                * Description du cahier des charges
                * Répartition des tâches
                * Description de la solution algorithmique proposée
                * Commentaire d'un extrait de code
                * Démonstration d'un jeu de tests ou d'un exemple d'exécution



    ??? done "Séance 28 : mercredi 17/01/2024"

          * Rituels :  [exercices de ce carnet](https://capytale2.ac-paris.fr/web/c/4bcb-2759581) : parcours de tableaux, recherche séquentielle et dichotomique
          * 🎯 Chapitre 9 : Codage des caractères :

              * On traite les exercices 1 à 4 de manipulation de chaînes de caractères et les QCM des exercices 5 et 6
              * On présente les notions de table de codage et d'encodage et on traite les exercices 5 et 6 (QCM).
              * [Cours version pdf](../chapitre12/cours/NSI-CodageCaracteres-2020V1.pdf)
              * [Exercices  du cours avec corrections sur Capytale ](https://capytale2.ac-paris.fr/web/c/9192-288733)

              1. Table de codage ASCII  : exercices 2, 3 et 4 
        

              ![Emojis, caractères de points de codes entre U+1F600 et U+1F64F](../chapitre12/cours/images/emojis.png)
          * 🏊 Exercices supplémentaires sur les chaînes de caractères dans ce [carnet](https://capytale2.ac-paris.fr/web/c/7e19-2746023)
          * 🎯 Chapitre 10 : Algorithmes de tri
              * [Cours/TP version pdf](../chapitre11/1NSI-Cours-Tris-2021V1.pdf)
              * [Cours/TP sur Capytale](https://capytale2.ac-paris.fr/web/c/967c-255137)
          

          * ✍️ Travail à faire  :
              *  Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](./Culture/exposés.md) ou de proposer un sujet à faire valider par  
              * Pour le mercredi 24/01/2024 : Interrogation sur les recherches séquentielle, dichotomique, les systèmes d'exploitation et la ligne de commande
              * Pour le mercredi 31/01/2024 déposer une capsule video du projet sur la [nouvelle plateforme Moodle Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=2601) :
                  * Description du cahier des charges
                  * Répartition des tâches
                  * Description de la solution algorithmique proposée
                  * Commentaire d'un extrait de code
                  * Démonstration d'un jeu de tests ou d'un exemple d'exécution



    ??? done "Séance 29 : mardi  23/01/2024"
          * 🆕  : Actualités : une vidéo d'illustration de la recherche dichotomique (Nicolas Réveret) :<https://nreveret.forge.aeif.fr/preuves_visuelles/recherche_dichot/media/videos/dicho/480p15/DichotomieSucces.mp4>
          *  🏊 Corrections des exercices 1 et 2 de ce  [carnet](https://capytale2.ac-paris.fr/web/code/4bcb-2759581) : exercices divers sur listes et chaines de caractères                  
          * 🎯 Chapitre 10 : Algorithmes de tri : aujourd'hui le tri par sélection
              * [Cours/TP version pdf](../chapitre11/1NSI-Cours-Tris-2021V1.pdf)
              * [Cours/TP sur Capytale](https://capytale2.ac-paris.fr/web/c/967c-255137)
              * 🏊 Exercices supplémentaires sur les tris dans ce [carnet](https://capytale2.ac-paris.fr/web/c/e6d5-2801465)  
          

          * ✍️ Travail à faire  :
              *  Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](./Culture/exposés.md) ou de proposer un sujet à faire valider par  
              * Pour le mercredi 24/01/2024 : Interrogation sur les recherches séquentielle, dichotomique, les systèmes d'exploitation et la ligne de commande
              * Pour le mercredi 31/01/2024 déposer une capsule video du projet sur la [nouvelle plateforme Moodle Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=2601) :
                  * Description du cahier des charges
                  * Répartition des tâches
                  * Description de la solution algorithmique proposée
                  * Commentaire d'un extrait de code
                  * Démonstration d'un jeu de tests ou d'un exemple d'exécution


    ??? done "Séance 30 : mercredi  24/01/2024"
          * 💯 DS 6 sur pacours de tableaux à une ou deux dimensions, les recherches séquentielle et dichotomique et la ligne de commande
          * 🗣️  Exposé oral
          * 🎯 Chapitre 10 : Algorithmes de tri : aujourd'hui la complexité du  tri par sélection et le tri par insertion
              * [Cours/TP version pdf](../chapitre11/1NSI-Cours-Tris-2021V1.pdf)
              * [Cours/TP sur Capytale](https://capytale2.ac-paris.fr/web/c/967c-255137)
              * 🏊 Exercices supplémentaires sur les tris dans ce [carnet](https://capytale2.ac-paris.fr/web/c/e6d5-2801465)
              * 🏊  Encore d'autres exercices sur les tris et les tableaux/listes dans ce [carnet](https://capytale2.ac-paris.fr/web/c/e4c5-2818520)
          

          * ✍️ Travail à faire  :
              *  Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](./Culture/exposés.md) ou de proposer un sujet à faire valider par  l'enseignant
              *  Pour le mardi 30 Janvier finir les exercices de ce [carnet](https://capytale2.ac-paris.fr/web/c/e6d5-2801465) ou de ce [carnet](https://capytale2.ac-paris.fr/web/c/e4c5-2818520) pour les plus avancés
              * Pour le mercredi 31/01/2024 déposer une capsule video du projet sur la [nouvelle plateforme Moodle Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=2601) :
                  * Description du cahier des charges
                  * Répartition des tâches
                  * Description de la solution algorithmique proposée
                  * Commentaire d'un extrait de code
                  * Démonstration d'un jeu de tests ou d'un exemple d'exécution

    ??? done "Séance 31 : mardi  30/01/2024"
          * 💯 Retour du DS 6 sur parcours de tableaux à une ou deux dimensions, les recherches séquentielle et dichotomique et la ligne de commande. [Dépôt Moodle ave énoncés et corrigés de DS](https://0690026d.moodle.ent.auvergnerhonealpes.fr/course/view.php?id=2)
          * 🗣️  Exposé oral
          * 🎯 Chapitre 10 : Algorithmes de tri : aujourd'hui le tri par insertion : algorithme, implémentation et complexité
              * [Cours/TP version pdf](../chapitre11/1NSI-Cours-Tris-2021V1.pdf)
              * [Cours/TP sur Capytale](https://capytale2.ac-paris.fr/web/c/967c-255137)
              * 🏊 Exercices supplémentaires sur les tris dans ce [carnet](https://capytale2.ac-paris.fr/web/c/e6d5-2801465)
              * 🏊  Encore d'autres exercices sur les tris et les tableaux/listes dans ce [carnet](https://capytale2.ac-paris.fr/web/c/e4c5-2818520)
          * 🎯 Chapitre 11 : tuples 
            * [Cours](../chapitre15/Cours/puplets-cours-reduit-.pdf) 
            * TP : [énoncé](../chapitre15/TP/NSI-Puplets-TP-2020V1.pdf) et [carnet Capytale avec exercices et corrigés](https://capytale2.ac-paris.fr/web/c/8361-1269409)
          * 🧮  Projets pour s'occuper :
            * [Projet Snake](https://www.frederic-junier.org/NSI/premiere/Projets/Projets2024/snake/snake-pyxel/) => à rendre à la rentrée des vacances d'hiver
            * [Stéganographie](https://capytale2.ac-paris.fr/web/c/69bf-1019976)
            * [Projet P5](https://capytale2.ac-paris.fr/web/c/3990-1277333)

          

          * ✍️ Travail à faire  :
              *  Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](./Culture/exposés.md) ou de proposer un sujet à faire valider par  l'enseignant
              *  Pour le mercredi 7 février : interrogation écrite sur le codage des caractères et les tris. [DS des années précédentes pour préparer](https://nuage03.apps.education.fr/index.php/s/8fTkKsNdm533BsS)
              * Pour le mercredi 31/01/2024 déposer une capsule video du projet sur la [nouvelle plateforme Moodle Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=2601) :
                  * Description du cahier des charges
                  * Répartition des tâches
                  * Description de la solution algorithmique proposée
                  * Commentaire d'un extrait de code
                  * Démonstration d'un jeu de tests ou d'un exemple d'exécution


??? bug "Février"


    ??? done "Séance 32 : mardi  06/02/2024"
          * 🙋  Présentation de la spécialité NSI. 
          * 🏊 Pour préparer l'interrogation de demain, carnet avec des exercices différenciés selon deux niveaux : [carnet](https://capytale2.ac-paris.fr/web/c/c894-2926615)
          * 🎯 Chapitre 10 : Algorithmes de tri : aujourd'hui le tri par insertion : complexité
              * [Cours/TP version pdf](../chapitre11/1NSI-Cours-Tris-2021V1.pdf)
              * [Cours/TP sur Capytale](https://capytale2.ac-paris.fr/web/c/967c-255137)
          * 🎯 Chapitre 11 : tuples 
              * [Cours](../chapitre15/Cours/puplets-cours-reduit-.pdf) 
              * TP : [énoncé](../chapitre15/TP/NSI-Puplets-TP-2020V1.pdf) et [carnet Capytale avec exercices et corrigés](https://capytale2.ac-paris.fr/web/c/8361-1269409)
          * 🧮  Projets pour s'occuper :
              * [Projet Snake](https://www.frederic-junier.org/NSI/premiere/Projets/Projets2024/snake/snake-pyxel/) => à rendre à la rentrée des vacances d'hiver
              * [Stéganographie](https://capytale2.ac-paris.fr/web/c/69bf-1019976)
              * [Projet P5](https://capytale2.ac-paris.fr/web/c/3990-1277333)
          * ✍️ Travail à faire  :
              *  Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](./Culture/exposés.md) ou de proposer un sujet à faire valider par  l'enseignant
              *  Pour le mercredi 7 février : interrogation écrite sur le codage des caractères et les tris. [DS des années précédentes pour préparer](https://nuage03.apps.education.fr/index.php/s/8fTkKsNdm533BsS)
             
    ??? done "Séance 33 : mercredi  07/02/2024"
          * 💯 DS 7 sur Codage des caractères et algorithmes de tri
          * 🎯 Chapitre 11 : tuples 
              * [Cours](../chapitre15/Cours/puplets-cours-reduit-.pdf) 
              * TP : [énoncé](../chapitre15/TP/NSI-Puplets-TP-2020V1.pdf) et [carnet Capytale avec exercices et corrigés](https://capytale2.ac-paris.fr/web/c/8361-1269409)
          * 🧮  Projets pour s'occuper :
              * [Projet Snake](https://www.frederic-junier.org/NSI/premiere/Projets/Projets2024/snake/snake-pyxel/) => à rendre à la rentrée des vacances d'hiver
              * [Stéganographie](https://capytale2.ac-paris.fr/web/c/69bf-1019976)
              * [Projet P5](https://capytale2.ac-paris.fr/web/c/3990-1277333)
          * ✍️ Travail à faire  :
              *  Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](./Culture/exposés.md) ou de proposer un sujet à faire valider par  l'enseignant
              *  Pour le mardi 13/02 : [Projet Snake](https://www.frederic-junier.org/NSI/premiere/Projets/Projets2024/snake/snake-pyxel/)


    ??? done "Séance 34 : mardi  13/02/2024"
          *  💯 Retour du DS 7 sur Codage des caractères et algorithmes de tri => [Espace Moodle avec DS et corrigés](https://0690026d.moodle.ent.auvergnerhonealpes.fr/course/view.php?id=2)
          * 🎯 Chapitre 11 : tuples 
              * [Cours](../chapitre15/Cours/puplets-cours-reduit-.pdf) 
              * TP : [énoncé](../chapitre15/TP/NSI-Puplets-TP-2020V1.pdf) et [carnet Capytale avec exercices et corrigés](https://capytale2.ac-paris.fr/web/c/8361-1269409) => Faire l'exercice 2 uniquement
          * 🧮  Projet à rendre à la rentrée des vacances de Février :
              * [Projet Snake](https://www.frederic-junier.org/NSI/premiere/Projets/Projets2024/snake/snake-pyxel/) => à rendre à la rentrée des vacances d'hiver
          * 🎯 Chapitre 12 : architecture de Von Neumann:
              * [Vidéo d'introduction : Space War](https://nuage03.apps.education.fr/index.php/s/MxsjqBJBxkAztr6)
              * [Lien vers le chapitre 17](../chapitre17.md)
              * [Cours version pdf](../chapitre17/NSI-ArchitectureVonNeumann-Cours2022.pdf)
              * __Premier temps :__ histoire de l'informatique et visionnage de quelques séquences de la video <https://www.lumni.fr/video/une-histoire-de-l-architecture-des-ordinateurs> avec application aux exercices 1 et 2
              * __Second temps (cours) :__ présentation de l'architecture de Von Neummann, de la hiérarchie des mémoires, des différents niveaux de langage entre l'homme et la machine :  faire l'exercice 3 mais pas les 4 et 5. ([exercice 5 pour les curieux](./chapitre17/ressources/exercice5.zip)) 
              * __Troisième temps (pratique) :__  Manipulation de l'[Emulateur Aqua](http://www.peterhigginson.co.uk/AQA/)
                  * Ressources : [Programmes en langage d'assemblage Aqua](./chapitre17/ressources/programmes_assembleur_eleves.zip)
                  * [Correction video de l'exercice 7](https://nuage03.apps.education.fr/index.php/s/PZmdbwns2wW2AJ8)
                  * [Correction video de l'exercice 10](https://nuage03.apps.education.fr/index.php/s/zwPMMFXXm6gzbCt)
          * ✍️ Travail à faire  :
              *  Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](./Culture/exposés.md) ou de proposer un sujet à faire valider par  l'enseignant
              *  Pour le mardi 05/03 : [Projet Snake](https://www.frederic-junier.org/NSI/premiere/Projets/Projets2024/snake/snake-pyxel/) à terminer sur Capytale.
  


    ??? done "Séance 35 : mercredi  14/02/2024"
              * [Projet Snake](https://www.frederic-junier.org/NSI/premiere/Projets/Projets2024/snake/snake-pyxel/) => à rendre à la rentrée des vacances d'hiver
          * 🎯 Chapitre 12 : architecture de Von Neumann:
              * [Vidéo d'introduction : Space War](https://nuage03.apps.education.fr/index.php/s/MxsjqBJBxkAztr6)
              * [Lien vers le chapitre 17](../chapitre17.md)
              * [Cours version pdf](../chapitre17/NSI-ArchitectureVonNeumann-Cours2022.pdf)
              * __Premier temps :__ histoire de l'informatique et visionnage de quelques séquences de la video <https://www.lumni.fr/video/une-histoire-de-l-architecture-des-ordinateurs> avec application aux exercices 1 et 2
              * __Second temps (cours) :__ présentation de l'architecture de Von Neummann, de la hiérarchie des mémoires, des différents niveaux de langage entre l'homme et la machine :  faire l'exercice 3 mais pas les 4 et 5. ([exercice 5 pour les curieux](./chapitre17/ressources/exercice5.zip)) 
              * __Troisième temps (pratique) :__  Manipulation de l'[Emulateur Aqua](http://www.peterhigginson.co.uk/AQA/)
                  * Ressources : [Programmes en langage d'assemblage Aqua](./chapitre17/ressources/programmes_assembleur_eleves.zip)
                  * [Correction video de l'exercice 7](https://nuage03.apps.education.fr/index.php/s/PZmdbwns2wW2AJ8)
                  * [Correction video de l'exercice 10](https://nuage03.apps.education.fr/index.php/s/zwPMMFXXm6gzbCt)
          * ✍️ Travail à faire  :
              *  Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/KpsGWnw9H5fMCqW) : possibilité de choisir un des sujets proposés [ici](./Culture/exposés.md) ou de proposer un sujet à faire valider par  l'enseignant
              *  Pour le mardi 05/03 : [Projet Snake](https://www.frederic-junier.org/NSI/premiere/Projets/Projets2024/snake/snake-pyxel/) à terminer sur Capytale.



??? bug "Mars"

    ??? done "Séance 36 : mardi  05/03/2024"

        * 🏊 Expressions booléennes et tables de vérité : automatismes 11, 12 et 13 sur <https://frederic-junier.org/NSI/premiere/automatismes/automatismes-2021-2022/>
        * 🎯 Chapitre 13 : dictionnaires
            * [Cours](../chapitre18/Cours/dictionnaires-cours-git.md)
            * [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/8394-398180) avec exercices 2, 3 et 4 du cours + exercices sur le site  <https://e-nsi.gitlab.io/nsi-pratique/N1/antecedents/sujet/> :

    ??? done "Séance 37 : mercredi 06/03/2024"

        * 🎯 Chapitre 13 : dictionnaires
            * [Cours](../chapitre18/Cours/dictionnaires-cours-git.md)
            * [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/8394-398180) avec exercices 2, 3 et 4 du cours + exercices sur le site  <https://e-nsi.gitlab.io/nsi-pratique/N1/antecedents/sujet/>.
            * [Exercices supplémentaires sur les dictionnaires](https://capytale2.ac-paris.fr/web/c/58bf-3114330)
        * 🎯 Chapitre 14 traitement de données en tables
            * Suivre le polycopié de cours et traiter les exemples depuis le carnet Capytale.
            * [Cours version pdf](../chapitre19/Cours/cours-tables-indexation-.pdf)
            * [Cours version markdown](../chapitre19/Cours/cours-tables-indexation-git.md)
            * [Carnet  Capytale avec les exercices de code](https://capytale2.ac-paris.fr/web/c/ae9d-1603777)
        * ✍️ Travail à faire pour mercredi prochain :
            * [Exercices supplémentaires sur les dictionnaires](https://capytale2.ac-paris.fr/web/c/58bf-3114330)
        
    ??? done "Séance 38 : mercredi 12/03/2024"
        
        * 🏊   Rituel : [carnet Capytale 3 sur les dictionnaires](https://capytale2.ac-paris.fr/web/c/9f9a-3163563)
        * 🎯 Chapitre 13 : dictionnaires
            * [Cours](../chapitre18/Cours/dictionnaires-cours-git.md)
            * [Exercices supplémentaires sur les dictionnaires](https://capytale2.ac-paris.fr/web/c/58bf-3114330)
        * 🎯 Chapitre 14 Complexité :
            * [Cours version pdf](../chapitre14/cours_complexite_2022.pdf)
            * [Carnet Capytale avec corrigés des exercices](https://capytale2.ac-paris.fr/web/c/09bd-1434270)
        * ✍️ Travail à faire pour mercredi prochain :
            *  Interrogation sur les dictionnaires, réviser :
               *  [Carnet Capytale avec exercices du cours](https://capytale2.ac-paris.fr/web/c/8394-398180)
               *  [Carnet Capytale 2 avec Exercices supplémentaires sur les dictionnaires](https://capytale2.ac-paris.fr/web/c/58bf-3114330)
               *  [carnet Capytale 3 sur les dictionnaires](https://capytale2.ac-paris.fr/web/c/9f9a-3163563)
  
       
    ??? done "Séance 39 : mardi 19/03/2024"
        
        * 💯 DS sur les dictionnaires
        * 🎯 Chapitre 14 Complexité :
            * [Cours version pdf](../chapitre14/cours_complexite_2022.pdf)
            * [Carnet Capytale avec corrigés des exercices](https://capytale2.ac-paris.fr/web/c/09bd-1434270)
        * ✍️ Travail à faire pour mercredi 20/03 :
          * Faire les deux exercices de ce [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/4a32-3220264)


    ??? done "Séance 40 : mercredi 20/03/2024"
        
        *  🏊 Correction de deux exercices de ce [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/4a32-3220264)
        * 🎯 Chapitre 15 Alorithmes gloutons :
            * [Cours/TP](./Algorithmes_gloutons/algorithmes_gloutons/algorithmes_gloutons.md)
            * [Synthèse de cours](./Algorithmes_gloutons/Synthèse/synthese_gloutons.md)
        * ✍️ Travail à faire pour mardi 26/03 :
          * [2 Exercices dans Carnet Capytale](https://capytale2.ac-paris.fr/web/c/8483-3232133)
          * Petite interrogation de rattrapage sur les dictionnaires :réviser le QCM, la question 1) de l'exercice 2 et la question 1) b) de l'exercice 3.


    ??? done "Séance 41 : mercredi 26/03/2024"
        
        * 💯 DS de rattrapage sur  les dictionnaires
        * 🎯 Chapitre 14 Complexité :
            * [Cours version pdf](../chapitre14/cours_complexite_2022.pdf)
            * [Carnet Capytale avec corrigés des exercices](https://capytale2.ac-paris.fr/web/c/09bd-1434270)
        * 🎯 Chapitre 15 Algorithmes gloutons :
            * [Cours/TP](./Algorithmes_gloutons/algorithmes_gloutons/algorithmes_gloutons.md)
            * [Synthèse de cours](./Algorithmes_gloutons/Synthèse/synthese_gloutons.md)
        * ✍️ Travail à faire pour mardi 02/04 :
            * Travail personnel sur le chapitre Algorithmes gloutons :
                * Faire l'exercice sur le rendu de monnaie du [Cours/TP](./Algorithmes_gloutons/algorithmes_gloutons/algorithmes_gloutons.md). [Lien vers carnet Capytale](https://capytale2.ac-paris.fr/web/c/8483-3232133)
                * Traiter les deux exercices de ce [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/4a32-3220264)

??? bug "Avril"

    ??? done "Séance 42 : mardi 02/04/2024"
        * 💯 Retour du DS de rattrapage sur  les dictionnaires
        * 🎯 Chapitre 15 Algorithmes gloutons :
            * [Cours/TP](./Algorithmes_gloutons/algorithmes_gloutons/algorithmes_gloutons.md)
            * [Synthèse de cours](./Algorithmes_gloutons/Synthèse/synthese_gloutons.md) 
            * Correction des deux exercices de ce [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/4a32-3220264)
        *  🎯 Chapitre 16 traitement de données en tables
            * Cours : suivre le polycopié de cours et traiter les exemples depuis le carnet Capytale.
                * [Cours version pdf](../chapitre19/Cours/cours-tables-indexation-.pdf)
                * [Cours version markdown](../chapitre19/Cours/cours-tables-indexation-git.md)
                * [Carnet  Capytale avec les exercices de code](https://capytale2.ac-paris.fr/web/c/ae9d-1603777)
            * TP Recherche/Tri dans une table :
                * [Énoncé du TP](../chapitre19/TP-Recherche-Tri/tp-recherche-tri-git.md)
                * [Carnet  Capytale avec les exercices](https://capytale2.ac-paris.fr/web/c/0bb5-484953)

    ??? done "Séance 43 : mercredi 03/04/2024"
        * 🎯 Chapitre 15 Algorithmes gloutons :
            * [Correction de l'exercice 4 sur l'ordonnancement glouton](https://capytale2.ac-paris.fr/web/c/2649-3232097)
        *  🎯 Chapitre 16 traitement de données en tables
            * Cours : 
                * [Cours version pdf](../chapitre19/Cours/cours-tables-indexation-.pdf)
                * [Cours version markdown](https://frederic-junier.org/NSI/premiere/chapitre19/Cours/cours-tables-indexation-git/)
            * TP Recherche/Tri dans une table :
                * [Énoncé du TP](https://frederic-junier.org/NSI/premiere/chapitre19/TP-Recherche-Tri/tp-recherche-tri-.pdf)
                * [Carnet  Capytale avec les exercices](https://capytale2.ac-paris.fr/web/c/0bb5-484953)


    ??? done "Séance 44 : mardi 09/04/2024"
        * 💯 DS sur les thèmes Dictionnaire, Complexité et Algorithmes gloutons
        *  🎯 Chapitre 16 traitement de données en tables
            *  TP Recherche/Tri dans une table :
                * [Énoncé du TP](https://frederic-junier.org/NSI/premiere/chapitre19/TP-Recherche-Tri/tp-recherche-tri-.pdf)
                * [Carnet  Capytale avec les exercices](https://capytale2.ac-paris.fr/web/c/0bb5-484953)
            * TP Fusion de tables :
                 *  [TP version pdf](../chapitre19/TP-Fusion/tp-fusion-.pdf)
                 * [TP version markdown](./chapitre19/TP-Fusion/tp-fusion-git.md)
                 * [TP sur Capytale](https://capytale2.ac-paris.fr/web/c/50a5-11200)
        * Présentation du projet final :
            * [Page des projets](./projets.md)


    ??? done "Séance 45 : mercredi 10/04/2024"
        *  🎯 Chapitre 16 traitement de données en tables
            * TP Fusion de tables :
                *  [TP version pdf](../chapitre19/TP-Fusion/tp-fusion-.pdf)
                * [TP version markdown](./chapitre19/TP-Fusion/tp-fusion-git.md)
                * [TP sur Capytale](https://capytale2.ac-paris.fr/web/c/50a5-11200)
        * Présentation du projet final :
            * [Page des projets](./projets.md) => [exemple d'interface graphique avec Tkinter](./Projets/Tkinter/inversion_chaine.py)
            * [Tableau à compléter avec son choix de sujet](https://nuage03.apps.education.fr/index.php/s/q32GMKisA8rgo7Z)


    ??? done "Séance 46 : mardi 30/04/2024"
        *  Date de la Nuit du Code : le mercredi 29/05 de 12 h 30 à 19 h en salle 717
        *  🎯 Chapitre 17 Flottants :
            * [Cours](https://nuage03.apps.education.fr/index.php/s/W7PBwWd8L3ZJ59G)
            * [Carnet Capytale avec les corrigés](https://capytale2.ac-paris.fr/web/c/78a9-386046) => Attention le carnet contient d'autres exercices donc la numérotation ne correspond pas.
        * Travail sur le  projet final :
            * [Page des projets](./projets.md) => [exemple d'interface graphique avec Tkinter](./Projets/Tkinter/inversion_chaine.py)
            * [Tableau à compléter avec son choix de sujet](https://nuage03.apps.education.fr/index.php/s/q32GMKisA8rgo7Z)
            * Point étape à partir d'une première version de la [fiche de suivi](https://frederic-junier.org/NSI/premiere/Projets/Projets2024/projet_final/Fiche_Suivi_Projet_Final_2024.pdf)
        * Date de rendu du projet final : le jeudi 23/05 (capsule vidéo + code  voir [document de cadrage](https://frederic-junier.org/NSI/premiere/Projets/Projets2024/projet_final/NSI_Presentation_Projet2024.pdf))

??? bug "Mai"

    ??? done "Séance 47 : mardi 07/05/2024"
        *  Date de la Nuit du Code : le mercredi 29/05 de 12 h 30 à 19 h en salle 717
           *  [Un squelette de code niveau première](https://www.pyxelstudio.net/qwsyjx6b)
           *  [Une activité de préparation niveau terminale](https://fjunier.forge.apps.education.fr/tnsi/Projets/P5_pacman/projet_pacman/)
        *  🎯 Chapitre 18  Correction :
            * [Cours](./chapitre20/1NSI-Cours-Correction-2024V1.pdf)
        * Travail sur le  projet final :
            * [Page des projets](./projets.md) => [exemple d'interface graphique avec Tkinter](./Projets/Tkinter/inversion_chaine.py)
            * [Tableau à compléter avec son choix de sujet](https://nuage03.apps.education.fr/index.php/s/q32GMKisA8rgo7Z)
            * Point étape à partir d'une première version de la [fiche de suivi](https://frederic-junier.org/NSI/premiere/Projets/Projets2024/projet_final/Fiche_Suivi_Projet_Final_2024.pdf)
        * Date de rendu du projet final : le dimanche 26/05 (capsule vidéo + code  voir [document de cadrage](https://frederic-junier.org/NSI/premiere/Projets/Projets2024/projet_final/NSI_Presentation_Projet2024.pdf))
        * Mardi 21/05 : DS sur les chapitres Dictionnaires / Traitement de données en tables / Flottants / Correction


    ??? done "Séance 48 : mardi 14/05/2024"
        *  Date de la Nuit du Code : le mercredi 29/05 de 12 h 30 à 19 h en salle 717
           *  [Un squelette de code niveau première](https://www.pyxelstudio.net/qwsyjx6b)
           *  [Une activité de préparation niveau terminale](https://fjunier.forge.apps.education.fr/tnsi/Projets/P5_pacman/projet_pacman/)
        *  [Préparation du DS11](https://nuage03.apps.education.fr/index.php/s/oig9xqWoaGo5cNQ)
        *  🎯 Chapitre 18  Terminaison et correction :
            * [Cours](./chapitre20/1NSI-Cours-Correction-2024V1.pdf) => correction d'algorithme
        * Travail sur le  projet final :
            * [Page des projets](./projets.md) => [exemple d'interface graphique avec Tkinter](./Projets/Tkinter/inversion_chaine.py)
            * [Tableau à compléter avec son choix de sujet](https://nuage03.apps.education.fr/index.php/s/q32GMKisA8rgo7Z)
            * Point étape à partir d'une première version de la [fiche de suivi](https://frederic-junier.org/NSI/premiere/Projets/Projets2024/projet_final/Fiche_Suivi_Projet_Final_2024.pdf)
        * Date de rendu du projet final : le dimanche 26/05 (capsule vidéo + code  voir [document de cadrage](https://frederic-junier.org/NSI/premiere/Projets/Projets2024/projet_final/NSI_Presentation_Projet2024.pdf))
        * Mardi 21/05 : DS 11 sur les chapitres Dictionnaires / Traitement de données en tables / Flottants / Correction
  

    ??? done "Séance 49 : mercredi 15/05/2024"
        *  Date de la Nuit du Code : le mercredi 29/05 de 12 h 30 à 19 h en salle 717
           *  [Un squelette de code niveau première](https://www.pyxelstudio.net/qwsyjx6b)
           *  [Une activité de préparation niveau terminale](https://fjunier.forge.apps.education.fr/tnsi/Projets/P5_pacman/projet_pacman/)
        *  🎯 Chapitre 18  Terminaison et correction :
            * [Cours](./chapitre20/1NSI-Cours-Correction-2024V1.pdf) => correction d'algorithme
        *  🎯 Chapitre 19  Protocole HTTP :
              *  Travail en autonomie, les élèves s'approprient tout seul le cours à partir des différents exemples et exercices. Il vérifient leurs réponses à l'aide de la correction. Travail dans le navigateur Firefox avec les outils de développement.
                 - [Lien vers le chapitre 19 avec le cours](../chapitre21.md)
                 - **Premier point étape (10 minutes de travail individuel)** après l'exercice 3
                 - **Second point étape (15 minutes de travail individuel)** après l'exemple 1 => **Point de cours 2** sur les _Méthodes de passage des paramètres : GET ou POST_ puis **Exercice 4** en collectif.
                 - **Second point étape (30 minutes de travail individuel)** après le **Point de cours 3** sur les _éléments de formulaire HTML_ et l'exercice 5 => Synthèse en commun.
        * Travail sur le  projet final :
            * [Page des projets](./projets.md) => [exemple d'interface graphique avec Tkinter](./Projets/Tkinter/inversion_chaine.py)
            * [Tableau à compléter avec son choix de sujet](https://nuage03.apps.education.fr/index.php/s/q32GMKisA8rgo7Z)
            * Point étape à partir d'une première version de la [fiche de suivi](https://frederic-junier.org/NSI/premiere/Projets/Projets2024/projet_final/Fiche_Suivi_Projet_Final_2024.pdf)
        * Date de rendu du projet final : le dimanche 26/05 (capsule vidéo + code  voir [document de cadrage](https://frederic-junier.org/NSI/premiere/Projets/Projets2024/projet_final/NSI_Presentation_Projet2024.pdf))
        * Mercredi 22/05 : DS 11 sur les chapitres Dictionnaires / Traitement de données en tables / Flottants / Terminaison


    ??? done "Séance 50 : mardi 21/05/2024"
        *  Date de la Nuit du Code : le mercredi 29/05 de 12 h 30 à 19 h en salle 717
           *  [Un squelette de code niveau première](https://www.pyxelstudio.net/qwsyjx6b)
           *  [Une activité de préparation niveau terminale](https://fjunier.forge.apps.education.fr/tnsi/Projets/P5_pacman/projet_pacman/)
        *  🎯 Chapitre 19  Protocole HTTP :
              *  Travail en autonomie, les élèves s'approprient tout seul le cours à partir des différents exemples et exercices. Il vérifient leurs réponses à l'aide de la correction. Travail dans le navigateur Firefox avec les outils de développement.
                 - [Lien vers le chapitre 19 avec le cours](./chapitre21.md)
                 - **Premier point étape (10 minutes de travail individuel)** après l'exercice 3
                 - **Second point étape (15 minutes de travail individuel)** après l'exemple 1 => **Point de cours 2** sur les _Méthodes de passage des paramètres : GET ou POST_ puis **Exercice 4** en collectif.
                 - **Second point étape (30 minutes de travail individuel)** après le **Point de cours 3** sur les _éléments de formulaire HTML_ et l'exercice 5 => Synthèse en commun.
        * Travail sur le  projet final :
            * [Page des projets](./projets.md) => [exemple d'interface graphique avec Tkinter](./Projets/Tkinter/inversion_chaine.py)
            * [Tableau à compléter avec son choix de sujet](https://nuage03.apps.education.fr/index.php/s/q32GMKisA8rgo7Z)
            * Point étape à partir d'une première version de la [fiche de suivi](https://frederic-junier.org/NSI/premiere/Projets/Projets2024/projet_final/Fiche_Suivi_Projet_Final_2024.pdf)
        * Date de rendu du projet final : le dimanche 26/05 (capsule vidéo + code  voir [document de cadrage](https://frederic-junier.org/NSI/premiere/Projets/Projets2024/projet_final/NSI_Presentation_Projet2024.pdf))
        * Mercredi 22/05 : DS 11 sur les chapitres Dictionnaires / Traitement de données en tables / Flottants / Terminaison


    ??? done "Séance 51 : mercredi 22/05/2024"
        *  Date de la Nuit du Code : le mercredi 29/05 de 12 h 30 à 19 h en salle 717
              *  [Un squelette de code niveau première](https://www.pyxelstudio.net/qwsyjx6b)
              *  [Une activité de préparation niveau terminale](https://fjunier.forge.apps.education.fr/tnsi/Projets/P5_pacman/projet_pacman/)
        *  💯 DS 11
        *  🎯 Chapitre 19  Protocole HTTP :
              *  [Lien vers le chapitre 19 avec le cours](./chapitre21/http-git.md)
              *  [Lien vers la correction](./chapitre21/correction-git.md)
              *  Travail en autonomie, les élèves s'approprient tout seul le cours à partir des différents exemples et exercices. Il vérifient leurs réponses à l'aide de la correction. Travail dans le navigateur Firefox avec les outils de développement.
                 - Retour sur l'exercice 2 et le protocole HTTP  
                 - *Exemple1* et **Point de cours 2** sur les _Méthodes de passage des paramètres : GET ou POST_ puis **Exercices 3 et  4** en collectif.
                 - **Second point étape (30 minutes de travail individuel)** après le **Point de cours 3** sur les _éléments de formulaire HTML_ et l'exercice 5 => Synthèse en commun.
        * Travail sur le  projet final :
            * [Page des projets](./projets.md) => [exemple d'interface graphique avec Tkinter](./Projets/Tkinter/inversion_chaine.py)
            * [Tableau à compléter avec son choix de sujet](https://nuage03.apps.education.fr/index.php/s/q32GMKisA8rgo7Z)
            * Point étape à partir d'une première version de la [fiche de suivi](https://frederic-junier.org/NSI/premiere/Projets/Projets2024/projet_final/Fiche_Suivi_Projet_Final_2024.pdf)
        * Date de rendu du projet final : le dimanche 26/05 (capsule vidéo + code  voir [document de cadrage](https://frederic-junier.org/NSI/premiere/Projets/Projets2024/projet_final/NSI_Presentation_Projet2024.pdf)



    ??? done "Séance 52 : mardi 29/05/2024"
        *  Date de la Nuit du Code : le mercredi 29/05 de 12 h 30 à 19 h en salle 717
              *  [Un squelette de code niveau première](https://www.pyxelstudio.net/qwsyjx6b)
              *  [Une activité de préparation niveau terminale](https://fjunier.forge.apps.education.fr/tnsi/Projets/P5_pacman/projet_pacman/)
        *  💯 DS 11 : retour
        *  🎯 Chapitre 20  Interaction dans une page WEb côté client avec Javascript :
              *  [Lien vers le chapitre 20 avec le cours](./chapitre23/javascript-git2.md)
              *  _Temps 1 (30 minutes):_ Découverte de Javascript : [Activité Capytale](https://capytale2.ac-paris.fr/web/c/0cab-3661615)  de code **0cab-3661615** copiée depuis la bibliothèque de Capytale
              *  _Temps 2 (10  minutes) Cours :_ synthèse et Javascript en console dns le navigateur : Point de cours 1  et 2 
              *  _Temps 3 (20 minutes) :_ Javascript et interaction dans une page web, exercice 2
              *  _Temps 3 (15 minutes) Cours :_  Ecrire des gestionnaires d'événements en Javascript : exemples 1 et 2 et Point de cours 3
              *  _Travaux Pratiques (45 minutes) :_ exercices 3 et 4

    ??? done "Séance 53 : mercredi 30/05/2024"
        *  🎯 Chapitre 20  Interaction dans une page WEb côté client avec Javascript :
              *  [Lien vers le chapitre 20 avec le cours](./chapitre23/javascript-git2.md)
              *  _Temps 3 (15 minutes) Cours :_  Ecrire des gestionnaires d'événements en Javascript : exemples 1 et 2 et Point de cours 3
              *  _Travaux Pratiques (95 minutes) :_ 
                 *  exercice 3  [sur Capytale](https://capytale2.ac-paris.fr/web/c/e8f2-3670833)  code `e8f2-3670833`
                 *  exercice 4  [sur Capytale](https://capytale2.ac-paris.fr/web/c/5283-3671002)  code  `5283-3671002`


    ??? done "Séance 54 : mardi 04/06/2024"
        *  🎯 Chapitre 21   Architecture d'un réseau et pile de protocoles TCP/IP :
              * [Lien vers le chapitre 21 avec le cours](./chapitre25/reseau-cours-git.md)  =>  activités de simulation avec Filius, synthèse de cours et vérification par QCM
        *  🎯 Chapitre 20  Interaction dans une page Web côté client avec Javascript :
              *  [Lien vers le chapitre 20 avec le cours](./chapitre23/javascript-git2.md)
              *  _Temps 3 (15 minutes) Cours :_  Ecrire des gestionnaires d'événements en Javascript : exemples 1 et 2 et Point de cours 3
              *  _Travaux Pratiques  :_ 
                 *  exercice 3  [sur Capytale](https://capytale2.ac-paris.fr/web/c/e8f2-3670833)  code `e8f2-3670833`
                 *  exercice 4  [sur Capytale](https://capytale2.ac-paris.fr/web/c/5283-3671002)  code  `5283-3671002`

    ??? done "Séance 55 : mercredi 05/06/2024"
        *  🎯 Projet P5 dans [Capytale](https://capytale2.ac-paris.fr/web/c/3990-1277333) code `3990-1277333`
        *  🎯 Méli-Mélo de problèmes de programmation dans [Capytale](https://capytale2.ac-paris.fr/web/c/8f9e-3709308) code `8f9e-3709308``
        

        
