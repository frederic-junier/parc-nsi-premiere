---
title: Séance du 02/03/2022
---

# Séance du 02/03/2022


## Chapitre tuples 

On termine le chapitre avec les exercices 2 et 3 dans Capytale.

* [Cours version pdf](../chapitre15/Cours/puplets-cours-.pdf)
* [Cours version markdown](../chapitre15/Cours/puplets-cours-git.md)
* [Carnet Capytale avec exercices et corrigés](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/5745-345853)

## Préparation du DS 7

Jeudi 10/03, évaluation de 30 minutes (DS 7) sur les thèmes suivants :

*  [Circuits logiques et fonctions booléennes](../chapitre13/cours-circuits-logiques-.pdf) ➡️  en QCM
*  [Complexité](../chapitre14/cours_complexite_2022.pdf) ➡️  en QCM + un exercice
*  Complexité et parcours de tableaux ➡️ un exercice
*  _Parcours de tableaux à une ou deux dimensions_  ➡️  un exercice parmi les  suivants, proposés sur le site <https://e-nsi.gitlab.io/nsi-pratique/N1/aplatir/sujet/>  
    * [aplatir](https://e-nsi.gitlab.io/nsi-pratique/N1/aplatir/sujet)  🖥️  aujourd'hui en classe 
    * [dénivelé positif](https://e-nsi.gitlab.io/nsi-pratique/N1/denivele_positif/sujet/) 🖥️  aujourd'hui en classe 
    * [dernière occurrence](https://e-nsi.gitlab.io/nsi-pratique/N1/derniere_occurence/sujet/)
    * [histogramme](https://e-nsi.gitlab.io/nsi-pratique/N1/histo/sujet/) 🖥️  à chercher pour demain
    * [indice du minimum](https://e-nsi.gitlab.io/nsi-pratique/N1/ind_min/sujet/)
    * [liste des différences](https://e-nsi.gitlab.io/nsi-pratique/N1/liste_differences/sujet/) 🖥️  à chercher pour demain
    * [maximum](https://e-nsi.gitlab.io/nsi-pratique/N1/maximum_nombres/sujet/)
    * [mise en boîtes](https://e-nsi.gitlab.io/nsi-pratique/N1/mise_en_boites/sujet/)  🖥️  à chercher pour demain
    * [occurrences du minimum](https://e-nsi.gitlab.io/nsi-pratique/N1/occurrences_du_mini/sujet/) 🖥️  aujourd'hui en classe 


## Chapitre Dictionnaires

* Introduction de la structure de données dictionnaire à travers un [mini TP autour du jeu de Scrabble](../chapitre18/scrabble.md).

## Travail sur le projet sutom


* Date de remise sur le Moodle :  le samedi 19 mars 2022, [projet sutom](../projets.md)


## Travail pour demain :

Chercher les exercices :

* [histogramme](https://e-nsi.gitlab.io/nsi-pratique/N1/histo/sujet/) 🖥️  à chercher pour demain
* [liste des différences](https://e-nsi.gitlab.io/nsi-pratique/N1/liste_differences/sujet/) 🖥️  à chercher pour demain
* [mise en boîtes](https://e-nsi.gitlab.io/nsi-pratique/N1/mise_en_boites/sujet/)  🖥️  à chercher pour demain
