---
title: Séance du 13/04/2022
---

# Séance du 13/04/2022




## Devoir commun

* [Entraînement sur les exercices avec juge en ligne sur Moodle](https://0690026d.moodle.ent.auvergnerhonealpes.fr/course/view.php?id=2&sectionid=3)

* DS et corrigés donnés cette année : <https://nuage-lyon.beta.education.fr/s/sGHxPqMizZj9Txd>
