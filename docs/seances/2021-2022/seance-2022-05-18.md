---
title: Séance du 18/05/2022
---

# Séance du 18/05/2022

## Interactions homme / machine dans un navigateur Web avec Javascript

- Le [cours](../chapitre23/javascript-git2.md) :
  - Présentation du langage [Javascript][Javascript] et de la programmation événementielle côté client : points de cours 1 et 2 puis exercices 1 et 2.
  - Approfondissement et rédaction de scripts et gestionnaires d’événements : exemples 1 et 2 puis point de cours 3.

## Travail sur le projet de fin d'année

- [Tutoriel sur le module pyxel](../Projets/Pyxel/decouverte_pyxel.md)
- [Document de cadrage du projet final](../Projets/2021-2022/ProjetFinal/Cadrage/NSI_Presentation_Projet2022.pdf)
- [Inscription avec le mot de passe donné par l'enseignant](https://nuage-lyon.beta.education.fr/s/Fyf7EQgrpHSfCFP)
