---
title: Séance du 08/09/2022
---

# Séance du 08/09/2022

## Consignes et méthode de travail :

* Matériel :
    * Un classeur avec des feuilles pour écrire et des pochettes transparentes pour ranger les cours
    * Mettre le site <https://parc-nsi.github.io/premiere/> dans les favoris de son navigateur, navigation responsive adaptée aux smartphone : toutes les ressources (cours, corrigés) sont publiés et disponibles sur ce site. Trois rubriques à retenir :
        * La [progression](https://parc-nsi.github.io/premiere/) avec les liens vers les chapitres
        * La liste des [séances](https://parc-nsi.github.io/premiere/seances/) détaillées qui sont ensuite copiées dans Pronote.
        * La rubrique [Automatismes](https://parc-nsi.github.io/premiere/automatismes/) avec des liens vers des QCM externes et des exercices pour travailler les automatismes.
    * Une clef USB de 8 Go minimum, cet [article](https://www.boulanger.com/ref/872118) n'est pas cher.
    * Le manuel Hachette NSI version papier fourni par la région de référence `978-2-01-786630-5`, accessible en ligne sur <https://mesmanuels.fr/acces-libre/3813624>
    * __Installer la distribution portable [Edupyter](https://www.edupyter.net/) sur sa clef USB et apporter sa clef à chaque cours.__

* Méthode de travail :
    * D'une séance à l'autre : relire le cours, faire les exercices
    * Pendant la séance : alternance de temps d'activités et de synthèse, travail sur des projets    à rendre
    * Évaluations :
        * Rendu de mini-projet ou de projet plus conséquent (pendant les vacances) : travail en classe et à la maison en binôme, évaluations écrites ou orales
        * Formatives sous forme d'interrogations courtes (format QCM) ou d'exposés oral (histoire de l'informatique, synthèse de cours)
        * Sommatives sous forme de devoir d'une heure ou de TP noté
        * Autres (exposés, création d'un tuto video)


#
## Chapitre 1: constructions de bases en langage Python


1. [France IOI](http://www.france-ioi.org)  :
    *  Niveau 1, chapitre 1, _-_Séquence d'instructions_: Correction de l'exercice [Recette secrète](http://www.france-ioi.org/algo/task.php?idChapter=642&idTask=1874)
    *    Niveau 1 Chapitre 2 (Répétitions): jusqu'à l'exercice 7 (Jeu de dames)
    

2.  [TP 1](../chapitre1/TP1/1NSI-Chap1-Variables-TP1-.pdf) : correction des exos 5 et 6 et présentation de Capytale < <https://capytale2.ac-paris.fr/web/c/a5e6-644695/mcer>
3. On avance sur le  [TP2](../chapitre1/TP2/TP2.pdf) en s'aidant de la synthèse de cours : [Section 1 : Bases d'un langage de programmation : instructions, littéraux, expressions](../chapitre1/cours/Chap1-Bases-Programmation-2021.pdf). Les thèmes suivants sont abordés :
    * ➡️ _Bases d'un langage de programmation : instructions, littéraux, expressions_, exemple d'un programme avec entrée/sortie (calcul d'âge à partir de la date de naissance)
     * ➡️ _Instructions conditionnelles_ :  Exercice 5 p. 42, QCM question 6 p. 39
    * ➡️ _Boucles bornées_ 
    * ➡️ _Boucles non bornées_


    Ressources pour le [TP2](../chapitre1/TP2/TP2.pdf)  :
    Liens vers les corrigés :

    * [corrigé pdf](../chapitre1/TP2/correction/Correction_TP2.pdf)
    * [corrigé .ipynb](../chapitre1/TP2/correction/Correction_TP2.ipynb)
    * [corrigé .py](https://gitlab.com/frederic-junier/parc-nsi/-/raw/master/docs/chapitre1/TP2/correction/Correction_TP2.py)





## A faire pour mardi 13/09 :

1. Sur <http://www.france-ioi.org>  faire jusuq'à l'exercice 4 le  __niveau 1  chapitre 3(calculs)__  et jusqu'à l'exercice 5 le __niveau 1  chapitre 4(lecture de l'entrée)__   du parcours général 
2. Refaire les exercices du [TP2](../chapitre1/TP2/TP2.pdf)   corrigés en classe. 
3. Réfléchir à  un sujet de mini-projet sur [MiniProjet1](../Projets/MiniProjets2022-2023/NSI-MiniProjets1-2022-2023-Sujets.pdf), s'inscrire par binôme sur <https://nuage03.apps.education.fr/index.php/s/WYT8ED4GL8NDAZ3>. A rendre pour le vendredi 23/09 dans la zone de dépôt sur Moodle <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=964>

