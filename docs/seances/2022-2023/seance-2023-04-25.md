---
title: Séance du 25/04/2023
---

# Séance du 25/04/2023



## Chapitre Correction (Fin partie correction)

* [Cours version pdf](../chapitre20/1NSI-Cours-Correction-2023.pdf)
* [Correction du cours sur la correction (version 2021-2022)](../chapitre20/corrige/1NSI-Cours-Correction-2023-correction.pdf)
* On traite en classe :
  * la terminaison pour l'exercice 4 calcul du nombre de chiffres en base 2 d'un entier
  * la correction pour le calcul de puissance par multiplications successives
  * la correction pour le calcul de quotient et reste d'une division euclidienne


## Chapitre traitement de données en tables


###  Cours 

Suivre le polycopié de cours et traiter les exemples depuis un IDE Python à partir de l'archive.

* [Cours version pdf](../chapitre19/Cours/cours-tables-indexation-.pdf)
* [Cours version markdown](../chapitre19/Cours/cours-tables-indexation-git.md)
* [Archive avec les exemples du cours](../chapitre19/Cours/exemples_cours_tables.zip)



## Travail sur le projet de fin d'année

* [Tutoriel pyxel](../Projets/Pyxel/decouverte_pyxel.md)
* [Document](https://frederic-junier.gitlab.io/parc-nsi/Projets/Projets2023/ProjetFinal/Cadrage/ressources.zip)
* [Inscription pour le projet final](https://nuage03.apps.education.fr/index.php/s/nk7wGPA8S78BsqP)
* [Fiche de suivi (pdf modifiable)](../Projets/Projets2023/ProjetFinal/Cadrage/Fiche_Suivi_Projet_Final_2023.pdf) à remplir individuellement et déposer au plus tard le mardi 25 avril dans l'espace de dépôt Moodle <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=1841>
* Projet (code + capsule audio de présentation individuelle de 5 minutes) à déposer au plus tard le dimanche 21 mai  dans l'espace de dépôt Moodle <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=1843>



## Travail à faire

!!! task

    * Pour mardi 09/05, interrogation écrite courte sur les  chapitres Tables, Flottants  et Correction (uniquement la partie terminaison et les variants de boucles).
    * Projet Final : Déposez au plus tard le dimanche 21 mai dans <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=1843> :
        * une archive au format zip avec votre code et toutes les ressources nécessaires
        * une capsule audio ou video individuelle de 5 minutes présentant le projet avec un focus sur une de vos contributions au projet : plan structuré avec introduction et conclusion
