---
title: Séance du 19/01/2023
---

# Séance du 19/01/2023


## Codage des activités 

* 👌 : facile, maîtrise élémentaire du cours
* ✍️   : difficulté moyenne, bonne maîtrise  du cours
* 💪  :  difficile, niveau avancé



## AP NSI

Vérifiez sur Pronote si vous êtes inscrit dans le groupe d'AP qui se réunira de 13 à 14 en salle 715.

## DS n°6 

Recherche séquentielle et dichotomique, un peu de ligne de commande, tri par sélection.

## Algorithmes de Tri :

Aujourd'hui au programme :  retour sur le  _tri _par insertion_ (exercice 10 complexité) et implémentations dans d'autres langages (exercice 11)

* [Cours/TP version pdf](../chapitre11/1NSI-Cours-Tris-2021V1.pdf)
* [Matériel élève (squelette Python)](../chapitre11/materiel.zip)
* [Cours/TP sur Capytale](https://capytale2.ac-paris.fr/web/c/967c-255137)
* [Cours/TP correction sur Capytale](https://capytale2.ac-paris.fr/web/c/29c9-255106)
* [Cours/TP correction version pdf](../chapitre11/ressources/Cours_Tris_Premiere_NSI_2021_Correction.pdf)


Pour visualiser le déroulement d'un algorithme de tri : <http://fred.boissac.free.fr/AnimsJS/Dariush_Tris/index.html>


## Chapitre 10 : codage des caractères

On traite les exercices 1 à 4 de manipulation de chaînes de caractères.
On présente les notions de table de codage et d'encodage et on traite les exercices 5 et 6 (QCM).

* [Cours version pdf](../chapitre12/cours/NSI-CodageCaracteres-2020V1.pdf)
* [Exercices  du cours avec corrections sur Capytale ](https://capytale2.ac-paris.fr/web/c/9192-288733)


![Emojis, caractères de points de codes entre U+1F600 et U+1F64F](../chapitre12/cours/images/emojis.png)


## Travail à faire


* Pour mardi 23/01, faire les exercices 1 et 2 du chapitre Codage des caractères dans le carnet Capytale
