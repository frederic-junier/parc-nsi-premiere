---
title: Séance du 18/10/2022
---

# Séance du 18/10/2022


## Codage des activités 

* 👌 : facile, maîtrise élémentaire du cours
* ✍️   : difficulté moyenne, bonne maîtrise  du cours
* 💪  :  difficile, niveau avancé

## AP NSI

Vérifiez sur Pronote si vous êtes inscrit dans le groupe d'AP qui se réunira de 13 à 14 en salle 715.

## Retour du DS n°2 et exercices de remédiation

Faire les exercices suivants de la rubrique [Juge en ligne](https://0690026d.moodle.ent.auvergnerhonealpes.fr/course/view.php?id=2) du cours Moodle :

* 👌 [Plus grand](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=1336)
* 👌 [Produit nul](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=1337)
* ✍️ [Sommes des entiers successifs](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/url/view.php?id=1150)
* ✍️ [Seuil harmonique](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=1344)


## Chapitre 3 : fonctions, spécification et tests

* [Cours](../chapitre3.md)
* [Activité Capytale ➡️ cours fonctions](https://capytale2.ac-paris.fr/web/c/23a7-84924/mcer)
* 💪 Portée d'une variable : exercice 6 et 7


##  Chapitre 4 : tableaux à une dimension

Les élèves à l'aise avancent en autonomie les autres sont guidés par l'enseignant.

* [Cours](../chapitre4.md) 
* [Exemples du cours en version notebook (Capytale)](https://capytale2.ac-paris.fr/web/c/68ac-101047)

## Projet _Lapins crétins_

![alt](./ressources/lapin.png)

* On poursuit le projet : [matériel](../Projets/LapinsCretins/materiel.zip) :
  * Exercice 5 : index d'un élément dans une séquence


## Pour jeudi 20/10

!!! abstract "DS n°3"

    * Interrogation sur les structures de base en programmation : variables, structures conditionnelles, boucles, fonctions
    * Conversions entre bases dix et trois


## Pour mardi 25/10 :

!!! abstract "Projet Lapins crétins"

    * Déposer son premier rendu dans l'espace <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=1308>
    