---
title: Séance du 01/06/2023
---

# Séance du 01/06/2023

## Codage des activités

- 👌 : facile, maîtrise élémentaire du cours
- ✍️ : difficulté moyenne, bonne maîtrise du cours
- 💪 : difficile, niveau avancé

## Automatismes

- ✍️ [Tri par bulles](https://e-nsi.forge.aeif.fr/pratique/N2/500-tri_bulles/sujet/)
- 💪 [Tri de zéros et uns](https://e-nsi.forge.aeif.fr/pratique/N2/511-tableau_0_1/sujet/)

## Algorithme de classification par les k plus proches voisins

- TP avec [énoncé](https://frederic-junier.gitlab.io/parc-nsi-premiere/chapitre22/TP/knn-tp-git/) et [matériel à télécharger](https://frederic-junier.gitlab.io/parc-nsi-premiere/chapitre22/TP/materiel_tp.zip)

## Pour les volontaires

Passer la [certification Python sur Codin Game](https://www.codingame.com/take-the-test/Python3).


