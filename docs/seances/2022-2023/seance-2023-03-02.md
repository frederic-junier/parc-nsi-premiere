---
title: Séance du 02/03/2023
---

# Séance du 02/03/2023


## Codage des activités 

* 👌 : facile, maîtrise élémentaire du cours
* ✍️   : difficulté moyenne, bonne maîtrise  du cours
* 💪  :  difficile, niveau avancé



## AP NSI

Vérifiez sur Pronote si vous êtes inscrit dans le groupe d'AP qui se réunira de 13 à 14 en salle 715.

## DS 

Thèmes : expressions boolénnees, tables de vérité, tuples, listes/tableaux Python, tris

## Chapitre Dictionnaires

* [Cours](../chapitre18/Cours/dictionnaires-cours-git.md)
* [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/8394-398180) avec exercices 2, 3 et 4 du cours + exercices sur le site  <https://e-nsi.gitlab.io/nsi-pratique/N1/antecedents/sujet/> + 2 exercices du site [Prologin](https://prologin.org/train/2010)

  

