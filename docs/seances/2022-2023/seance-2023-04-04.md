---
title: Séance du 04/04/2023
---

# Séance du 04/04/2023

## Retour du DS 7 sur les dictionnaires

## Automatismes 

!!! question

    Convertir en base deux le décimal $4,1$.

## Chapitre Flottants

* [Cours version pdf](../chapitre16/1NSI-Cours-Flottants-2021V1.pdf)
* [Carnet Capytale  des exemples du cours, avec corrigés](https://capytale2.ac-paris.fr/web/c/78a9-386046)
* On reprend à partir du paragraphe 2 _Les flottants_, on introduit pour la représentation des réels :
  * la représentation en virgule fixe (exercice 4)
  * la représentation en virgule flottante (exercice 5)
  * la norme IEEE  754 (exercice 6)



## Chapitre Correction

* [Cours version pdf](../chapitre20/1NSI-Cours-Correction-2023.pdf)
* [Correction du cours sur la correction (version 2021-2022)](../chapitre20/corrige/1NSI-Cours-Correction-2023-correction.pdf)



## Présentation du module pyxel et du projet de fin d'année

* [Tutoriel](../Projets/Pyxel/decouverte_pyxel.md)
* [Document](https://frederic-junier.gitlab.io/parc-nsi/Projets/Projets2023/ProjetFinal/Cadrage/ressources.zip)



## Travail à faire

!!! task

    * Représenter en base deux puis au format  IEEE  754 sur 32 bits le décimal  $-12.4609375$.
    * Pour la prochaine séance, choisir impérativement un sujet pour le projet final, former un binôme, s'inscrire dans ce [fichier](https://nuage03.apps.education.fr/index.php/s/nk7wGPA8S78BsqP)
