---
author: F.Junier
title: 🏡 Présentation
---

# Mini projet Pyxel 1NSI

!!! info "Présentation"
    Ce mini-site présente 7 sujets de mini-projets pour des élèves de première NSI qui reprennent les propositions faites par Laurent Abbal  en décembre 2024 dans son [calendrier 24 jours de pyxel](https://24jdpp.forge.apps.education.fr/) de découverte du module [pyxel](https://github.com/kitao/pyxel) à travers des défis quotidiens. Ma contribution est minime, je me suis contenté de tester les sujets et de reformuler un sujet. 
    Les sujets sont numérotés de 1 à 7 par difficulté croissante.

    Avant de commencer un projet, il faut :

    * créer une activité pyxel dans Capytale (voir le [tutoriel video](https://cdn.ac-paris.fr/capytale/media/4_3_qualite28_PyxelStudio.mp4)) ; une alternative est de se créer un compte sur [pyxelstudio.net](https://www.pyxelstudio.net/) 
    * traiter le défi du jour 1 du  [calendrier 24 jours de pyxel de Laurent Abbal](https://24jdpp.forge.apps.education.fr/){:target="_blank"}  pour découvrir les bases du module [pyxel](https://github.com/kitao/pyxel).
  
    Les sujets sont déjà répartis : [tableau de répartition des sujets](https://nuage03.apps.education.fr/index.php/s/LeJH2HGcLPabAyw)

    Tâches à réaliser :

    * Réaliser un code répondant au cahier des charges, à rendre au plus tard le **lundi 17/02**
    * Présentation orale du projet le **lundi 10/03** : de $3 \times 4$ minutes maximum, qui se décomposera ainsi : 
        * Introduction avec présentation du cahier des charges (élève 1)
        * Organisation du travail dans le groupe (élève 2)
        * Premier focus sur une partie du code (élève 1)
        * Second focus sur une autre partie du code (élève 2)
        * Troisième focsur sur une autre partie du code (élève 3)
        * Conclusion : bilan sur les compétences acquises (élève 3)

  
!!! info "👏 Crédits"


    Les sujets de mini-projet sont ceux proposés par  Laurent Abbal  dans son calendrier de l'avent [pyxel](https://github.com/kitao/pyxel) sur <https://24jdpp.forge.apps.education.fr/>. Merci à lui pour son travail précieux et tout ce qu'il partage en ligne 🙏.






 

