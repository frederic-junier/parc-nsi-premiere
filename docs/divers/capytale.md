---
layout: parc
title:  Capytale
---

# Activités disponibles sur Capytale


* [TP1 correction (chapitre 1)](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/06a2-40819)
* [Exercices du manuel chapitre 1](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/6b4e-36532)
* [Défi Codin Game Asteroid](https://capytale2.ac-paris.fr) : code **e495-13464**
* [Exercices 4 et 5 du sujet candidats libres 2021](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/ef14-31369)

