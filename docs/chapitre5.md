---
title:  Chapitre 5  tableaux à deux dimensions, images
layout: parc
---




Cours de Frédéric Junier.


## Cours / TP

* [Cours / TP en version pdf](chapitre7/NSI-Images-Tableaux2d--2023V1.pdf)
  
* [Cours / TP en version notebook sur Capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/7ad3-163223)

* [Matériel élèves](chapitre7/Images-Tableaux2d-materiel.zip)

* [Corrigé du cours / TP  en version  notebook sur Capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/a643-164217)

* [Corrigé du cours / TP en version pdf](chapitre7/Images-Tableaux2d-correction/Images-Tableaux2d-Eleves-Partie1-Correction.pdf)   



    
## Tutoriel vidéo 

[Cours de la maison Lumni sur les tableaux à deux dimensions et les images](http://www.lumni.fr/video/notion-de-listes-en-informatique-et-application-aux-images-numeriques)
