---
title:  Sujets d'exposés sur la culture informatique
layout: parc
---

[Grille d'évaluation](grille_evaluation_oral.pdf)

Thème transversal du [programme](https://eduscol.education.fr/document/30007/download) : *Histoire de l'informatique*.

# Thème 1 : Représentation des données : types et valeurs de base

|Numéro|Sujet|Personne(s)|Ressources|
|:---:|:---:|:---:|:---:|
|1|Georges Boole : sa logique mathématique première pierre du langage des ordinateurs|Georges Boole|[Blog binaire : qui a inventé la logique avec des 0 et des 1](https://www.lemonde.fr/blog/binaire/2017/10/04/qui-a-invente-la-logique-avec-des-0-et-des-1/)|
|2|Compression des données : tout fichier est-il compressible ?|Shannon|[article interstice compression](https://interstices.info/idee-recue-tout-est-compressible/) et [article interstice théorie de l'information](https://interstices.info/theories-et-theorie-de-linformation/)|
|3|Le codage binaire, un codage universel ?||[Reflet numérique du monde, article interstices](https://interstices.info/tout-a-un-reflet-numerique/)|
|4|Mesurer la quantité d'information|Claude Shannon|[Vidéo de la chaine Blob](https://youtu.be/PLhrqPtWVt4)|


# Thème 2 : Représentation des données : types construits

|Numéro|Sujet|Personne(s)|Ressources|
|:---:|:---:|:---:|:---:|
|1|Représentation numérique d'une image : bitmap ou vectoriel ?||[Article interstices, nom de code binaire](https://interstices.info/nom-de-code-binaire/) et [vidéo MOOC NSI](https://www.youtube.com/watch?v=FBeB817M9Sg&list=PLKGPGznq6a-V5QJrPiZ5xGfBNAmcqM_8P&index=10)|
|2|Hachage : identification de fichier et dictionnaires||<https://interstices.info/le-hachage/>|

# Thème 3 : Traitement de données en tables


|Numéro|Sujet|Personne(s)|Ressources|
|:---:|:---:|:---:|:---:|
|1|Une histoire des bases de données|Edgar Codd|[Video de la chaine blob](https://youtu.be/iu8z5QtDQhY)|
|2|Le langage SQL||[article Wikipedia sur SQL](https://fr.wikipedia.org/wiki/Structured_Query_Language) et <https://learnsql.fr/blog/l-histoire-de-sql-comment-tout-a-commence/>|
|3Open data : définition et usages.||<https://interstices.info/lopen-data-louverture-des-donnees-pour-de-nouveaux-usages/>|



# Thème 4 : Interactions entre l'homme et la machine sur le Web

|Numéro|Sujet|Personne(s)|Ressources|
|:---:|:---:|:---:|:---:|
|1|Louis Pouzin : un français précurseur d'internet ?|Vinton Cerf, Louis Pouzin|[Documentaire sur France 3](https://france3-regions.francetvinfo.fr/grand-est/les-francais-qui-n-ont-pas-invente-internet-le-rendez-vous-manque-de-l-histoire-de-l-informatique-2733902.html) et [article Interstices](https://interstices.info/louis-pouzin-la-tete-dans-les-reseaux/)|
|2|De la documentation du CERN à Wikipedia, l'explosion du Web|Tim Berners-Lee, Jimmy Wales|[Les débuts du Web … sous l'œil du W3C](https://interstices.info/les-debuts-du-web-sous-loeil-du-w3c/) et <https://interstices.info/du-web-aux-wikis-une-histoire-des-outils-collaboratifs/>|




# Thème 5 : Architectures matérielles et systèmes d'exploitation

|Numéro|Sujet|Personne(s)|Ressources|
|:---:|:---:|:---:|:---:|
|1|Louis Pouzin : un français précurseur d'internet ?|Vinton Cerf, Louis Pouzin|[Documentaire sur France 3](https://france3-regions.francetvinfo.fr/grand-est/les-francais-qui-n-ont-pas-invente-internet-le-rendez-vous-manque-de-l-histoire-de-l-informatique-2733902.html) et [article Interstices](https://interstices.info/louis-pouzin-la-tete-dans-les-reseaux/)|
|2|Comment faire communiquer des ordinateurs hétérogènes : l'histoire des protocoles d'internet dans les années 1970|Vinton Cerf, Robert Kahn, Louis Pouzin|[article Interstices : des réseaux centralisés à Internet](https://interstices.info/dune-informatique-centralisee-aux-reseaux-generaux-le-tournant-des-annees-1970/)|
|3|La naissance du Web|Tim Berners-Lee|[article Interstices : les débuts du web](https://interstices.info/les-debuts-du-web-sous-loeil-du-w3c/)|
|4|De la machine de Turing aux premiers ordinateurs ? |Alan Turing, von Neumann|[article Interstices : Alan Turing du concept à la machine](https://interstices.info/alan-turing-du-concept-a-la-machine/)|
|5|Les machines mécaniques : de la Pascaline à la machine analytique de Babbage|Babbage|<https://interstices.info/linvention-de-la-mecanographie/> et <https://interstices.info/outils-machines-et-informatique/>|
|6|ENIAC, EDVAC, EDSAC, rapport Von Neumann, les premiers ordinateurs (1945 - 1955)|Von Neumann|<https://interstices.info/le-modele-darchitecture-de-von-neumann/> et <https://interstices.info/lordinateur-objet-du-siecle/> |
|7|De GM/NAA à Linux et Windows en passant par Unix et MS DOS, une histoire des systèmes d'exploitation|Dennis Ritchie, Ken Thompson|<https://interstices.info/la-naissance-des-systemes-dexploitation/>|
|8|De l'Intel 4004 aux System On Chip comme le Qualcom Snapdragon, en passant par la loi de Moore, une histoire de l'intégration toujours plus poussée des microprocesseurs|Shannon, Moore|[Cours Lumni](https://www.lumni.fr/video/une-histoire-de-l-architecture-des-ordinateurs)|
|9|Loi de Wirth et loi de Moore : évolution des performances du logiciel et du matériel |[article wikipedia sur la loi de Wirth](https://fr.wikipedia.org/wiki/Loi_de_Wirth) et  [article wikipedia sur la loi de WMoore](https://fr.wikipedia.org/wiki/Loi_de_Moore)|


# Thème 6 : Langage et programmation

|Numéro|Sujet|Personne(s)|Ressources|
|:---:|:---:|:---:|:---:|
|1|Qu'est-ce qu'un programme en  informatique ? ||[Article Interstices : Demandez le programme](https://interstices.info/demandez-le-programme/)|
|2|Qu'appelle-t-on paradigme en programamtion ? ||[Partie paradigme de l'article Interstices : Demandez le programme](https://interstices.info/demandez-le-programme/) et [Cours d'Olivier Lécluse](https://www.lecluse.fr/nsi/NSI_T/langages/paradigmes/)|
|3|Chasser les bugs dans un programme : pourquoi ? comment ? toujours possible ?|Edmund Clarke, Edsger Dijsktra|[Partie l'erreur est humainede cet article](https://interstices.info/demandez-le-programme/) et [Partie 1 de cet article Intertsices](https://interstices.info/les-jeux-a-la-rescousse-de-la-verification/)|
|4|Le langage C|Dennis Ritchie et Ken Thompson|[Article Wikipedia](https://fr.wikipedia.org/wiki/C_(langage))|
|5|Une petite histoire des langages de programmation|Ada Lovelace|[Conférence de Judicaël Courant](https://tube.ac-lyon.fr/w/2f7065e3-13c7-432c-80cc-94e769d38272) et [son diaporama](ressources/histoire.pdf)|
|6|Alan Turing : du calculable à l'indécidable|Turing|<https://interstices.info/alan-turing-du-calculable-a-lindecidable/> et [article Wikipedia sur le problème de l'arrêt](https://fr.wikipedia.org/wiki/Probl%C3%A8me_de_l%27arr%C3%AAt)|

# Thème 7 : Algorithmique

|Numéro|Sujet|Personne(s)|Ressources|
|:---:|:---:|:---:|:---:|
|1|Algorithme et machine de Turing, qu'est-ce que c'est ?|Alan Turing, Ada Lovelace|[article Interstices : algorithmes mode d'emploi](https://interstices.info/algorithmes-mode-demploi/) et [article Interstices : Alan Turing du concept à la machine](https://interstices.info/alan-turing-du-concept-a-la-machine/)|
|2|Edsger Dijsktra : son oeuvre,  ses bons mots.|Edsger Dijsktra|[article Wikipedia](https://fr.wikipedia.org/wiki/Edsger_Dijkstra) et [article Interstices : Le plus court chemin, partie Algo de Dijkstra](https://interstices.info/le-plus-court-chemin/)|
|3|Donald Knuth : son oeuvre,  ses bons mots.|Donald Knuth|[article Wikipedia](https://fr.wikipedia.org/wiki/Donald_Knuth)|
|4|Comment programmer une IA pour un jeu à 2 joueurs commes les échecs ?|Donald Knuth|[article Interstices](https://interstices.info/programmation-des-echecs-et-dautres-jeux/)|


# Thème 8 : questions sociétales


|Numéro|Sujet|Personne(s)|Ressources|
|:---:|:---:|:---:|:---:|
|1|Effet Mathilda|Hedy Lamarr|[article CNRS le Journal](https://lejournal.cnrs.fr/articles/hedy-lamarr-le-genie-scientifique-eclipse-par-la-beaute?utm_source=firefox-newtab-fr-fr)|
|2|Ada Lovelace pionnière, une informaticienne du XIXeme siècle|Ada Lovelace|[Podcast de France Culture](https://www.radiofrance.fr/franceculture/podcasts/la-methode-scientifique/ada-lovelace-la-grande-ordinatrice-5403084) et [Page wikipedia](https://fr.wikipedia.org/wiki/Ada_Lovelace)|
|3|Grace Hopper, à la naissance des langages de programmation|Grace Hopper|[Article du blob binaire](https://www.lemonde.fr/blog/binaire/2015/03/08/la-petulante-grace-hopper/) et [Page interstices sur la naissance des langages de programmation](https://interstices.info/naissance-des-langages-de-programmation/)|