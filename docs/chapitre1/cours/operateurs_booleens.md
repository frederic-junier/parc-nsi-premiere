---
title:  Opérateurs booléens
---

# Type booléen et opérateurs booléens

!!! note "Définition 1 : Type booléen"

    Le type **booléen** ne prend que deux valeurs `True` et `False`. Son nom est un hommage à [George Boole](https://fr.wikipedia.org/wiki/George_Boole) qui est le premier mathématicien à formaliser des calculs logiques au dix neuvième siècle

!!! note "Définition 2 : opérateur booléen"

    Une fonction qui ne prend que des paramètres booléens et qui renvoie un booléen, est appelée **opérateur booléen**.

    Si la fonction prend $n$ paramètres et que chaque paramètre peut prendre deux valeurs cela donne $2^{n}$ listes distinctes de valeurs de paramètres.

    Un **opérateur booléen** peut donc entièrement déterminé par un tableau où on fait apparaître une colonne par paramètre et une colonne pour la valeur renvoyée. Un tel tableau est appelé **table de vérité**. Si l'opérateur a $n$ paramètres, sa table de vérité aura $2^{n}$ lignes.

    !!! example "Exemple"

        Voici la table de vérité d'un opérateur booléen `f` avec deux paramètres, on a $2^{2}$ lignes.

        |   `a`   |   `b`   | `f(a, b)` |
        | :-----: | :-----: | :--------: |
        | `False` | `False` |  `True`   |
        | `False` | `True`  |  `True`   |
        | `True`  | `False` |  `True`   |
        | `True`  | `True`  |  `False`   |
    

!!! info "Propriété 1 : Opérateurs booléens de base"

    !!! note "Opérateur Non"

        C'est un opérateur booléen _unaire_  (un paramètre donc $2^{1}$ lignes).

        __Table de vérité :__


        |   `a`   | `non(a)` |
        | :-----: | :------: |
        | `False` |  `True`  |
        | `True`  | `False`  |

        __En Python :__


        ~~~python
        >>> not True
        False
        ~~~


    !!! note "Opérateur Ou"

        C'est un opérateur booléen _binaire_  (deux paramètres donc $2^{2}$ lignes).

        __Table de vérité :__


        |   `a`   |   `b`   | `ou(a, b)` |
        | :-----: | :-----: | :--------: |
        | `False` | `False` |  `False`   |
        | `False` | `True`  |   `True`   |
        | `True`  | `False` |   `True`   |
        | `True`  | `True`  |   `True`   |

        __En Python :__


        ~~~python
        >>> True or False
        True
        ~~~


    !!! note "Opérateur Et"

        C'est un opérateur booléen _binaire_  (deux paramètres donc $2^{2}$ lignes).

        __Table de vérité :__


        |   `a`   |   `b`   | `et(a, b)` |
        | :-----: | :-----: | :--------: |
        | `False` | `False` |  `False`   |
        | `False` | `True`  |  `False`   |
        | `True`  | `False` |  `False`   |
        | `True`  | `True`  |  `True`   |

        __En Python :__


        ~~~python
        >>> True and False
        False
        ~~~

## Exercice :

Carnet Capytale : <https://capytale2.ac-paris.fr/web/c/aa70-4126072>