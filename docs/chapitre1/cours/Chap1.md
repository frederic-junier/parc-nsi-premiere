 **Introduction**

Dans ce chapitre, on revient sur les constructions vues dans les
programmes de lycée qui constituent le noyau du langage du programmation
Python :

-   les littéraux, les types de base (`int`, `float`, `bool`) ;

-   la notion de variable, l'instruction d'affectation et la distinction
    entre expression et instruction ;

-   les instructions constituant les briques de base d'un langage de
    programmation :

    -   la boucle bornée `for` ;

    -   les instructions de branchement `if ... elif ... else` ;

    -   la boucle non bornée `while`.

-   les fonctions qui permettent de réutiliser et rendre plus lisible le
    code.

Bases d'un langage de programmation : instructions, littéraux, expressions
==========================================================================

Langage de programmation et instruction
---------------------------------------

 **Définition**

Un **langage de programmation** permet d'écrire des programmes qui sont
exécutés par un ordinateur.

[Python](https://fr.wikipedia.org/wiki/Python_(langage)) est un
**langage de programmation interprété** créé par Guido Van Rossum dans
les années 1980.

Le code [Python](https://fr.wikipedia.org/wiki/Python_(langage)) peut
directement être évalué par programme appelé interpréteur Python, sans
passer par l'étape de compilation en langage machine.

Ainsi on peut évaluer du code
[Python](https://fr.wikipedia.org/wiki/Python_(langage)) en **mode
interactif** dans une console : un prompt précédé d'une invite comme
`>>>` ou `In[1]` attend la saisie d'une expression
[Python](https://fr.wikipedia.org/wiki/Python_(langage)) bien formatée,
son évaluation est affichée directement en dessous avec éventuellement
affichage d'un message d'erreur.

Le **mode interactif** est pratique pour tester de petits bouts de code
mais on écrit un programme structuré dans le **mode programme** : le
texte du programme est saisi dans un éditeur de texte puis exécuté avec
la commande `python` ou le bouton `Exécuter` de l'environnement de
programmation utilisé. Contrairement au **mode interactif** on peut
enregistrer un programme dans une mémoire pérenne sous la forme d'un
fichier texte. Par convention on donne l'extension `.py` aux programmes
Python désignés souvent comme des **scripts**.

Une conférence donnée en novembre 2019 par *Judicaël Courant* au lycée
du Parc offre un très bon panorama de l'histoire des langages de
programmation :

<https://tube.ac-lyon.fr/videos/watch/2f7065e3-13c7-432c-80cc-94e769d38272>.

![image](ressources/interpreteur.png)

Source : *Judicaël Courant*

``` {style="compil"}
>>> 1 + 2
3
>>> "1" + "2"
'12'
>>> "1" + 2
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: can only concatenate str (not "int") to str
```

``` {style="compil"}
user@pc~$ python hello_world.py 
Hello world
```

/home/fjunier/Maths/python-logo.png, marge=4\]  **Programme**

Voici un exemple de programme Python qui récupère des données sur son
**entrée standard** (ici une saisie de l'utilisateur avec la fonction
`input` mais ce pourrait être un fichier externe) , les traite puis
renvoie des valeurs sur sa **sortie standard** (ici la console
utilisateur avec la fonction `print` mais ce pourrait être un fichier
externe) :

``` {style="rond"}
#Définition de fonctions
def f(x):
    return x ** 2 - 3

## Programme principal

#entrées
a = float(input('Borne inférieure ? '))
b = float(input('Borne supérieure ? '))
s = float(input("Seuil de l'encadrement ?"))

#traitement 
while b - a > s: #boucle non bornée
    m = (a+b)/2  #affectation
    if f(m) < 0: #conditionnelle, branchement
        a = m
    else:
        b = m

#sorties
print(a, "<= racine(3) <= ", b)
```

 **Définition**

Un programme Python est un texte structuré comme une séquence
d'**instructions**.

Un interpréteur Python exécute le programme sur un ordinateur en
mobilisant des ressources de calcul (processeur) et de mémoire.

L'exécution d'une instruction peut modifier **l'état courant du
programme**, par **effet de bord**.

Après l'exécution d'une instruction, l'interpréteur évalue par défaut
l'instruction sur la ligne suivante (les lignes vides ne sont pas prises
en compte) mais certaines instructions se traduisent par des sauts en
avant (branchement) ou en arrière (boucle) dans le texte du programme.

Les séquences de caractères précédées d'un dièse `#` ne sont pas
interprétées, ce sont des **commentaires**.

Environnements pour programmer en Python
----------------------------------------

 **Manuel :** *Environnement de programmation*

Lire les pages 3 à 5.

 **Méthode**

Pour programmer en Python, on peut :

-   Installer une distribution Python comprenant un interpréteur et un
    environnement de programmation :

    -   la plus simple est Idle disponible sur le site officiel
        [Python](https://docs.python.org/3/tutorial/datastructures.html)
        ;

    -   une distribution complète avec une interface simple et un
        débogueur très visuel : <https://thonny.org/> ;

    -   une distribution plus lourde mais plus complète avec tous les
        modules scientifiques est
        [Anaconda](https://www.anaconda.com/products/individual).

-   Utiliser un interpréteur intégré au navigateur Web :

    -   <http://pythontutor.com/visualize.html#mode=edit> est idéal pour
        visualiser l'exécution du code mais propose peu de
        modules/bibliothèques externes ;

    -   <https://console.basthon.fr/> est plus riche en
        modules/bibliothèques externes.

    -   les activités **Capytale** partagées par les professeurs dans
        l'ENT (Ressources numériques) sont un autre moyen d'exécuter du
        code Python dans le navigateur.

Littéraux et types de base
--------------------------

 **Manuel :** *Types de base*

-   Définitions : page 30

-   Exercice 1 : p. 42, QCM 4 p. 39

 **Définition**

Un **littéral** est un texte qui est interprété par Python pour créer un
**objet** en mémoire avec une valeur bien spécifiée.

Un **objet** Python est caractérisé par son **identité**, son **type**
et sa **valeur**.

L'identifiant d'un objet s'obtient avec la fonction `id` et son type
avec la fonction `type`.

``` {style="compil"}
>>> id(842)
140200201318000
>>> type(842)
<class 'int'>
```

 **Méthode**

Les objets de même type peuvent être combinés à l'aide d'opérateurs pour
créer d'autres objets de même type. Certains opérateurs sont
**polymorphes** et s'appliquent à des objets de types différents. Une
opération entre des objets de types différents provoque en général une
erreur sauf pour des cas particuliers comme les types numériques pour
lesquels il existe des règles de conversion implicite. Les quatre types
de base sont :

    Type         Domaine de valeurs         Opérateurs        Exemple de littéraux
  --------- ---------------------------- ----------------- --------------------------
    `int`          entiers signés         `+ - * // % **`          `0 -4 842`
   `float`   sous-ensemble des décimaux    `+ - * / **`         `0.0 -1.0 3.14`
   `bool`         valeurs logiques         `not and or`           `True False`
    `str`      chaînes de caractères            `+`         `’tb’ ” ’2.4’ ’Bonjour’`

On peut ajouter un type spécifique `None` sur lequel on ne définit pas
d'opérateur car tous les objets de valeur `None` sont identiques.

Dans le cas d'une combinaison de plusieurs opérateurs, des **règles de
précédence** (ou priorité) déterminent l'ordre dans lequel les
opérations sont effectuées. Les priorités usuelles des opérations
algébriques sont bien connues (attention l'exponentiation a la plus haut
priorité). On peut changer l'ordre de priorité en utilisant des
**parenthèses**. Pour les opérations booléennes, les opérateurs classés
par ordre décroissant de priorité sont `not`, `and` puis `or`.

*Il est fortement recommandé d'utiliser des parenthèses qui sont
l'opérateur de plus haute priorité, lorsqu'on n'est pas sûr des règle de
précédence ou pour s'en affranchir.*

 **Exemple** *Opérations sur les types de base*

``` {style="compil"}
>>> 11 + 3   #addition
14
>>> 11 * 3  #multiplication
33
>>> 11 ** 3 #exponentiation
1331
>>> 11 // 3  #quotient de la division euclidienne
3
>>> 11 % 3   #reste de la division euclidienne
2
>>> 11 / 3   #division décimale
3.6666666666666665
>>> not True  #négation booléenne
False
>>> True or False  #disjonction booléenne
True
>>> True and False  #conjonction booléenne
False
>>> '2' + 1    #impossible d'ajouter un 'str' et un 'int'
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: can only concatenate str (not "int") to str
>>> '2' + '1'   #concaténation de 'str'
'21'
>>> not True and False    #not prioritaire sur and
False
>>> not(True and False)  #dans le doute on met des parenthèses
True
```

Variable et expression
----------------------

 **Définition**

Dans un programme pour manipuler des objets Python, on a besoin de les
référencer par des noms.

Une **variable** est l'association entre un **nom** et un objet Python
qu'on désigne souvent comme **valeur** de la variable.

L'opérateur ` = ` réalise cette association. L'interpréteur Python
évalue d'abord le membre de droite pour créer l'objet puis l'associe au
membre de gauche contenant le **nom**.

Cette **instruction** s'appelle une **affectation de variable**. Comme
elle modifie **l'état courant** du programme on parle d'**effet de
bord**.

Voici les représentations de quelques séquences d'affectations :

![image](ressources/affectation1-crop.pdf)

------------------------------------------------------------------------

![image](ressources/affectation2-crop.pdf)

------------------------------------------------------------------------

![image](ressources/affectation3-crop.pdf)

 **Définition**

Une **expression** est une combinaison de littéraux et de variables dont
la valeur peut être évaluée par l'interpréteur en remplaçant les noms de
variables par leur valeur.

Par exemple si dans l'état courant du programme la valeur de `x` est 842
alors l'expression `x + 1` a pour valeur 843.

 **Remarque**

-   Les noms de variables en Python doivent obéir à certaines règles
    syntaxiques (ne pas commencer par un chiffre, ne peut pas contenir
    de tiret haut). Il est recommandé de les choisir en minuscules et
    d'utiliser le tiret bas comme séparateur si un nom est constitué de
    plusieurs mots comme `compteur_vie`.

    Pour plus de détails sur la syntaxe on pourra consulter la
    documentation
    <https://docs.python.org/3.9/reference/lexical_analysis.html> et
    pour les conventions de style la PEP 8
    <https://www.python.org/dev/peps/pep-0008>.

-   L'instruction `x = x + 1` permet d'incrémenter la valeur de la
    variable `x` (à condition qu'elle soit définie sinon cela provoque
    une erreur).

    Dans une affectation, l'opérateur ` = ` n'est pas un opérateur
    d'égalité et `x = x + 1` ne doit pas être interprété comme une
    égalité. Le `x` à droite de l'opérateur représente la valeur
    associée précédemment au nom `x` tandis que le `x` à gauche est un
    nom auquel sera associée la valeur de l'expression `x + 1`.

 **Manuel :** *Variables et expressions*

-   Définitions : pages 30 et 31

-   Exercices 3 et 4 p. 42, QCM questions 5 et 8 p. 39

 **Méthode**

Pour bien comprendre un programme on peut compléter un **tableau
d'état** : on exécute le programme (en fixant éventuellement une
instance d'entrée) comme le ferait l'interpréteur, en notant les valeurs
de toutes les variables pour chaque instruction qui une modifie l'état
de la mémoire par effet de bord.

Cet exemple classique permet de comprendre qu'il faut une variable de
stockage pour sauvegarder la valeur de `a` si on veut échanger les
valeurs des variables `a` et `b`

``` {.numberLines style="rond" numbers="left"}
a = 842
b = 843
a = b
b = a
```

   Instruction (numéro de ligne)    a     b
  ------------------------------- ----- -----
              ligne 1              842  
              ligne 2              842   843
              ligne 3              843   843
              ligne 4              843   843

Pour échanger les valeurs des variables `a` et `b` on peut utiliser l'un
des deux programme ci-dessous, celui de droite plus pythonique utilise
le déballage de `tuple`.

``` {.numberLines style="rond" numbers="left"}
a = 842
b = 843
c = a
a = b
b = c
```

``` {.numberLines style="rond" numbers="left"}
a = 842
b = 843
a, b = b, a
```

 **Méthode**

Étant donné deux variables de même type (celui de l'objet référencé), on
peut :

-   répondre à la question *Ont-elles la même valeur ?* en les comparant
    avec l'opérateur ` == `

-   répondre à la question *Référencent-elles le même objet ?* en les
    comparant avec l'opérateur ` is `

Deux variables peuvent avoir la même valeur sans référencer le même
objet.

Si `a` est définie, l'instruction `c = a` copie en `c` la référence de
`a`. Ainsi les noms `a` et `c` partagent une référence vers le même
objet.

En Python, la copie de variable se fait par *aliasing* ou *copie de
référence*.

``` {style="compil"}
>>> a = 842
>>> b = 843
>>> a == b
False
>>> a = a + 1
>>> a == b
True
>>> a is b
False
>>> c = a 
>>> a == a
True
>>> c is a
True
```

Instructions conditionnelles (ou de branchement)
================================================

 **Manuel :** *Conditionnelle*

-   Définition : page 33

-   Exercice 5 p. 42, QCM question 6 p. 39

 **Exemple**

Le programme ci-dessous prend en entrée le crédit et le débit d'un
compte bancaire, calcule le solde puis une pénalité de 10 % du solde
s'il est négatif. Le bloc du `if` n'est exécuté que si le solde est
négatif mais le message `"Affichage de votre solde :"` s'affiche dans
tous les cas.

``` {style="rond"}
recette = float(input('Recette ?'))
debit = float(input('Débit ?'))
solde = recette - debit
if solde < 0:
    agios = solde * 0.1
    solde = solde + agio
print("Affichage de votre solde :")
```

On peut le compléter en affichant un message différent selon le signe du
solde, le bloc du `else` s'exécute si et seulement si la condition du
`if` n'est pas vérifiée.

``` {style="rond"}
if solde < 0:
    print("Solde débiteur : ", solde, "dont agios : ", agios)
else:
    print("Solde créditeur : ", solde)
```

Souvent on a besoin de considérer plus de deux alternatives, comme dans
le programme ci-dessous qui affiche le tarif d'une entrée au musée en
fonction de l'âge : gratuit pour les moins de 6 ans, sinon 5 euros pour
les moins de 13 ans et sinon 8 euros.

``` {style="rond"}
age = int(input('age ?'))
if age < 6:
    tarif = 0
elif age < 13:
    tarif = 5
else:
    tarif = 10
```

 **Définition**

Dans l'exemple d'une résolution d'équation du second degré, des
instructions de test permettent de choisir entre plusieurs branches pour
continuer l'exécution du programme selon le signe du discriminant. La
syntaxe générale des instructions de branchement est :

``` {style="rond"}
if condition1:
    instruction       #
      ...             # bloc d'instructions 1
    instruction       #
elif condition2:
    instruction       #
      ...             # bloc d'instructions 2
    instruction       #
    instruction
elif ...
    .....
else :     
    instruction       #
      ...             # dernier bloc d'instructions 
    instruction       #
```

Seul le premier mot clé `if` est obligatoire, les autres `elif` et
`else` sont optionnels. Noter que `else` n'est pas suivi d'une
condition.

Les conditions sont des variables de type booléen ou des expressions qui
sont évaluées sous forme de booléen. Attention, dans cette situation,
Python évalue en booléen des types qui n'en sont pas a priori.

Si `condition1` a pour valeur `True` alors le bloc d'instructions 1 est
exécuté.

Si ce n'est pas le cas, `condition2` est évaluée. Si sa valeur est
`True` alors le bloc d'instructions 2 est exécuté.

Sinon on passe au `elif` suivant et ainsi de suite.

Si aucune des conditions présentes derrière un des mot-clé `elif` n'est
évaluée à `True`alors le dernier bloc est exécuté.

*Remarque : Un seul des blocs d'instructions peut être exécuté : le
premier possible ceci même si l'état courant rend plusieurs des
conditions valides (`True`).*

Les conditions sont souvent construites à l'aide des opérateurs de
comparaison et des opérateurs logiques pour créer des expressions
booléennes.

  ---------- -- -----------------------------------
                
                
  `x == y`      `x est égal à y`
  `x != y`      `x est différent de y`
  `x > y`       `x est strictement supérieur à y`
  `x < y`       `x est strictement inférieur à y`
  `x >= y`      `x est supérieur ou égal à y`
  `x <= y`      `x est inférieur ou égal à y`
  ---------- -- -----------------------------------

  ----------- -- ----------------------------------------
                 
                 
  `E and F`      `Vraie si E est Vraie ET F est Vraie`
  `E or F`       `Vraie si E est Vraie OU F est Vraie `
  `not E`        `Vraie si E est Fausse`
  ----------- -- ----------------------------------------

*On peut combiner des opérateurs arithmétiques, de comparaison et
logiques pour créer des expressions booléennes complexes. Il faut prêter
attention aux **règles de précédence**. Les parenthèses sont
prioritaires sur tous les autres opérateurs donc on peut les utiliser
quand on n'est pas certain des **règles de précédence** ou pour s'en
affranchir.*

``` {style="compil"}
>>> 3<4 and 5 == 2*2+1  #on fait confiance aux règles de précédence
True
>>> (3<4) and (5 == 2*2+1) #avec des  parenthèses, + sur et + lisible
True
```

 **Remarque**

Des tests de validation de données sont souvent utilisés. Dans l'exemple
du tarif de cinéma, on peut souhaiter interrompre le programme si l'âge
saisi est en dehors de la plage attendue ($[0;120]$ par exemple). On
peut utiliser alors une instruction `assert` selon la syntaxe :
`assert condition, "message"` qui reste silencieuse si la condition est
vérifiée mais interrompt le programme sinon.

``` {style="compil"}
>>> age = -10
>>> assert (age >= 0 and age <= 120), "valeur invalide"
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AssertionError: valeur invalide
```

`assert condition, "message"` correspond à peu près à :

``` {style="compil"}
if not condition:
    exit("message")  #on sort du programme
#ici le programme continue
```

Boucle bornée
=============

 **Manuel :** *Boucle bornée*

-   Définition : page 34

-   Exercices 7 et 8 p. 42, QCM question 10 p. 39

 **Définition** *Boucle `for`*

Lorsque l'on veut répéter un certain nombre de fois un ensemble
d'instructions on utilise la boucle inconditionnelle ou boucle `for` :

/home/fjunier/Maths/python-logo.png, marge=4\]  **Programme**

     >>> for k in range(4):
            print("bonjour")
        
    bonjour
    bonjour
    bonjour
    bonjour

/home/fjunier/Maths/python-logo.png, marge=4\]  **Programme**

     >>> for k in range(4):
            print(k)
        
    0
    1
    2
    3

Ici la fonction range renvoie un *itérateur* qui produit consécutivement
les valeurs entières de 0 à 3. La boucle `for` ne fait que parcourir les
valeurs de cet itérateur.

Plus généralement, la boucle `for` permet de parcourir tout objet
*itérable* :

/home/fjunier/Maths/python-logo.png, marge=4\]  **Programme**

    >>> for c in 'AB':
            print(c)
        
    A
    B

/home/fjunier/Maths/python-logo.png, marge=4\]  **Programme**

    >>> for x in [2, 6]:
            print(x**2)
        
    4
    36

La syntaxe générale d'une boucle inconditionnelle `for` est :

    for element in iterable:
        instruction    #
        instruction    # bloc d'instructions
        instruction    #

La fonction `range` possède trois arguments dont deux sont optionnels :

-   `range(n)` retourne un itérateur parcourant les entiers consécutifs
    entre 0 et `n` exclu.

-   `range(m,n)` retourne un itérateur parcourant les entiers
    consécutifs entre `m` compris et `n` exclu.

-   `range(m,n,s)` retourne un itérateur parcourant les entiers
    consécutifs entre `m` compris et `n` exclu avec un pas de `s`.

 **Remarque**

En Python, le **bloc** d'une boucle inconditionnelle est délimité par :

-   Un **marqueur de début de bloc**, le caractère ` : ` à la fin de
    l'instruction définissant la boucle ;

-   Une **indentation** (décalage en espaces par rapport à la marge de
    gauche) commune à toutes les lignes d'instruction appartenant au
    bloc de la boucle.

Les instructions exécutées après la boucle ont un niveau d'indentation
inférieur à celui du **bloc** de boucle.

*En Python, **l'indentation** n'est pas un simple élément de
présentation mais bien un élément de syntaxe, `IndentationError` est un
message d'erreur classique :*

``` {style="compil"}
(base) fjunier@fjunier:~$ cat test.py
s  = 0
for k in range(10):
s = s + k
(base) fjunier@fjunier:~$ python3 test.py
  File "test.py", line 3
    s = s + k
    ^
IndentationError: expected an indented block
```

 **Méthode**

Le bloc d'une boucle peut contenir une autre boucle, on parle alors de
**boucles imbriquées**.

-   Pour énumérer tous les couples $(i, j)$ avec
    $1 \leqslant i \leqslant 3$ et $1 \leqslant j \leqslant 3$, on peut
    écrire le programme `couple_avec_repetition.py` :

    ``` {style="rond"}
    for i in range(1, 4):
        for j in range(1, 4):
            print(i, j)
    ```

    ``` {style="compil"}
    1 1
    1 2
    1 3
    2 1
    2 2
    2 3
    3 1
    3 2
    3 3
    ```

-   Pour énumérer tous les couples $(i, j)$ avec
    $1 \leqslant i < j \leqslant 3$ , on peut écrire le programme\
    `couple_ordre_croissant.py` :

    ``` {style="rond"}
    for i in range(1,4):
        for j in range(i+1,4):
            print(i, j)
    ```

    ``` {style="compil"}
    fjunier@fjunier:~$ python3 couple_ordre_croissant.py 
    1 2
    1 3
    2 3
    ```

Boucle non bornée
=================

 **Manuel :** *Boucle non bornée*

-   Définition : page 34

-   Exercices 6 et 9 p. 42, QCM question 6 p. 39

 **Définition**

Dans une boucle `for`, le nombre d'itérations est connue à l'avance (au
plus tard lors de la première exécution de l'instruction). Lorsque l'on
ne sait pas à l'avance combien de fois la boucle devra être exécutée, on
utilise une boucle `while`.

La syntaxe est la suivante :

``` {style="rond"}
while condition:
    instruction    #
    instruction    #   bloc d'instructions
    instruction    #
```

La `condition` est une expression qui doit pouvoir être évaluée sous
forme de booléen. Tant que sa valeur est True, le bloc d'instructions
est exécuté.

Lors de l'utilisation de `while`, il faudra s'assurer que la condition
finisse par prendre la valeur `False` sans quoi la boucle ne se
terminera pas !!

*Remarque : pour faire une opération jusqu'à la vérification d'une
certaine condition il suffit d'ajouter l'opérateur logique `not()`.*

 **Exemple**

-   Un exemple classique est celui du calcul du PGCD de deux entiers `a`
    et `b` non tous nuls par la méthode d'Euclide.

    ``` {style="rond"}
    a = int(input('Entier a ?'))
    b = int(input('Entier b ?'))
    while b != 0:
        tmp = a
        a = b 
        b = tmp % b    #reste de la division euclidienne
    print("PGCD : ", a)
    ```

-   Les algorithmes de seuil, sont un classique de l'enseignement de
    l'algorithmique dans le secondaire. Par exemple si on considère
    qu'une population augmente de 2 % par an on peut déterminer le
    nombre d'années au bout duquel sa valeur initiale sera doublée avec
    le programme suivant :

    ``` {style="rond"}
    population = int(input("Population initiale ? "))
    double = 2 * population
    n = 0
    taux = 2 / 100
    while population < double:
        population = population * (1 + taux)
        n = n + 1
    print("Doublement en ", n, "années")    
    ```

 **Méthode**

-   On peut écrire volontairement une **boucle infinie**, par exemple
    pour afficher un chronomètre :

    ``` {style="rond"}
    import time

    compteur = 1
    while True:     
        print(compteur)
        compteur = compteur + 1
        time.sleep(1)
    ```

-   On peut exécuter involontairement une **boucle infinie**, par
    exemple si on saisit un entier impair dans le programme ci-dessous :

    ``` {style="rond"}
    import time
    compteur = int(input("Saisir un entier positif"))
    while compteur != 0:
        print(compteur)     
        compteur = compteur - 2
        time.sleep(1)
    ```

Fonctions
=========

Définir une fonction
--------------------

 **Manuel :** *Fonction*

-   Définition : page 35

-   Exercice 10 et 11 p. 42, QCM question 11 p. 39

 **Définition**

Lorsqu'on a besoin de réutiliser tout un bloc d'instructions, on peut
l'encapsuler dans une **fonction**. On étend ainsi le langage avec une
nouvelle instruction. Une fonction sert à factoriser et clarifier le
code, elle facilite la maintenance et le partage. C'est un outil de
**modularité**.

Pour déclarer une fonction, on définit son **en-tête** (ou
**signature**) avec son **nom** et des **paramètres formels** d'entrée.
Vient ensuite le bloc d'instructions, décalé d'une indentation et qui
constitue le **corps** de la fonction.

**Fonction avec `return`**

``` {style="rond"}
def mafonction(parametre1, parametre2): #signature
    bloc d'instructions (optionnel)
    return valeur
```

**Fonction sans `return`**

``` {style="rond"}
def mafonction(parametre1, parametre2): #signature
    bloc d'instructions (non vide)
```

Si le corps de la fonction contient au moins une instruction préfixée
par le mot clef `return` alors l'exécution d'un `return` termine
l'exécution du corps de la fonction et renvoie une valeur au programme
principal. Si le `return` est dans une structure de contrôle (boucle,
test), il est possible que le corps de la fonction ne soit pas
entièrement exécuté, on parle de **sortie prématurée**.

Une fonction sans `return` s'appelle une **procédure**, elle modifie
l'état du programme principal par **effet de bord**. En Python, une
procédure renvoie quand même la valeur spéciale `None` au programme
principal.

On exécute une fonction en substituant aux **paramètres formels** des
valeurs particulières appelées **paramètres effectifs**. On parle
d'**appel de fonction**, on peut l'utiliser comme une **expression** si
une valeur est renvoyée ou comme une **instruction** s'il s'agit d'une
procédure.

Par exemple une fonction `carre` qui prend en paramètre un nombre `x` et
qui renvoie son carré, s'écrira :

/home/fjunier/Maths/python-logo.png, marge=4\]  **Programme**

``` {style="rond"}
def carre(x):
    return x ** 2
```

Une fonction peut prendre plusieurs paramètres. Par exemple une fonction
`carre_distance_origine(x,y)` qui prend en paramètres deux nombres `x`
et `y` et qui renvoie le carré de la distance d'un point de coordonnées
`(x, y)` à l'origine d'un repère orthonormal, s'écrira :

/home/fjunier/Maths/python-logo.png, marge=4\]  **Programme**

``` {style="rond"}
def carre_distance_origine(x, y):
    return x ** 2 + y ** 2
```

Une fonction peut retourner un tuple de valeurs. Par exemple une
fonction `coord_vecteur` qui prend en paramètres quatre nombres
`xA, yA, xB, yB` et qui retourne les coordonnées du vecteur lié dont les
extrémités ont pour coordonnées `(xA, yA)` et `(xB, yB)`, s'écrira :

/home/fjunier/Maths/python-logo.png, marge=4\]  **Programme**

``` {style="rond"}
def coord_vecteur(xA, yA, xB, yB):
    return (xB - xA, yB - yA)
```

Voici un exemple de fonction sans paramètres d'entrée, ni valeur de
retour (il s'agit donc d'une procédure).

/home/fjunier/Maths/python-logo.png, marge=4\]  **Programme**

``` {style="rond"}
def message_defaite():
    print("Vous avez perdu, merci d'avoir participé") 
```

Attention, `return valeur` renvoie `valeur` qu'on peut capturer dans une
variable alors que `print(valeur)` affiche `valeur` sur la sortie
standard (l'écran par défaut) mais `valeur` ne peut alors être capturée
dans une variable. On donne ci-dessous un extrait de console Python, où
on a défini maladroitement une fonction cube avec un `print` à la place
d'un `return`. On ne récupère pas la valeur de retour souhaitée mais
`None` lorsqu'on appelle la fonction.

``` {style="compil"}
In [10]: def cube(x):
    ...:     print(x ** 3)
    ...:     

In [11]: cube(4)
64

In [12]: b = cube(5)
125

In [13]: b

In [14]: print(type(b))
<class 'NoneType'>

In [15]: print(b + 1)

TypeError: unsupported operand type(s) for +: 'NoneType' and 'int'
```

Utiliser des bibliothèques de fonctions
---------------------------------------

 **Méthode**

On a parfois besoin d'utiliser des fonctions de Python qui ne sont pas
chargées pas défaut. Ces fonctions sont stockées dans des programmes
Python appelées **modules** ou **bibliothèques**. Par exemple le module
`math` contient les fonctions mathématiques usuelles et le module
`random` contient plusieurs types de générateurs de nombres
pseudo-aléatoires.

Pour importer une fonction d'un module on peut procéder de deux façons :

``` {.python language="python" title="Première façon"}
#import du module de mathématique (création d'un point d'accès)
import math

#pour utiliser la fonction sqrt, on la préfixe du nom du module et d'un point
racine = math.sqrt(2)
```

``` {.python language="python" title="Deuxième façon"}
#import de la fonction sqrt du module math
from math import sqrt

racine = sqrt(2)

#Pour importer toutes les fonctions de math, ecrire
#from math import *  
```

Pour obtenir de l'aide sur le module math dans la console Python, il
faut d'abord l'importer avec `import math` puis taper `help(math)`, mais
le mieux est encore de consulter la documentation en ligne
<https://docs.python.org/3/>. Sans connexion internet, on peut lancer en
local le serveur web de documentation avec la commande
`python3 -m pydoc -b`.

 **Méthode**

-   Le module `random` rassemble diverses fonctions simulant le hasard :

      Fonction                  Spécification
      ------------------------- -------------------------------------------
      `random.randrange(a,b)`   renvoie un entier aléatoire dans \[a;b\[
      `random.randint(a,b)`     renvoie un entier aléatoire dans \[a;b\]
      `random.random()`         renvoie un décimal aléatoire dans \[0;1\[
      `random.uniform(a,b)`     renvoie un décimal aléatoire dans \[a;b\]

-   Le module `turtle` est une implémentation en `Python` du langage
    `Logo` créé dans les années 1970 pour l'enseignement de
    l'informatique à l'école. Il est disponible dans la distribution
    standard de `Python`.

    En déplaçant une pointe de stylo qui peut être matérialisée par une
    tortue, on peut tracer des figures géométriques dans un repère
    cartésien dont l'origine est au centre de la fenêtre et dont l'unité
    par défaut est le pixel. Lorsqu'on déplace le crayon, il laisse une
    trace s'il est baissé ou pas de trace s'il est levé.

    Nous utiliserons les fonctions suivantes de `turtle`.

      Fonction                     Spécification
      ---------------------------- ------------------------------------------------------------------
      `turtle.goto(x,y)`           déplace la tortue jusqu'au point de coordonnées (x, y)
      `turtle.penup()`             lever le crayon
      `turtle.pendown()`           baisser le crayon
      `turtle.setheading(angle)`   choisir l'angle d'orientation de la tortue en degrés
      `turtle.forward(n)`          avancer de `n` pixels selon l'orientation de la tortue
      `turtle.left(a)`             tourne à gauche de `a` degrés
      `turtle.right(a)`            tourne à droite de `a` degrés
      `turtle.color("red")`        choisir la couleur rouge (ou \"black\", \"green\", \"blue\" ...)

 **Exemple**

On donne ci-dessous un exemple d'utilisation du module `turtle`. On
utilise un `import` avec renommage du module.

/home/fjunier/Maths/python-logo.png, marge=4\]  **Programme**

``` {style="rond"}
import turtle as tt

def spirale(n):
    tt.penup()
    tt.goto(0,0)
    tt.pendown()
    c = 5
    for i in range(4):
        for j in range(4):
            tt.forward(c)
            c = 10 + c
            tt.left(90)

spirale(4)
tt.exitonclick()
```

![image](ressources/boucle1.png)

Erreurs
=======

 **Manuel :** *Erreurs*

-   Définitions page 37

-   Exercice 13 p. 42.

 **Définition**

On distingue plusieurs types d'**erreurs** ou **bugs** lors de
l'exécution d'un programme Python. Le **débogage** d'un programme
s'appuie d'abord sur la lecture attentive des messages d'erreurs de
l'interpréteur et sur un traçage des valeurs par exemple en insérant des
`print` ou en utilisant les outils de l'environnement de programmation
(points d'arrêt, débogueur).

Python caractérise chaque erreur par un **type**, voici les plus
courants :

-   une **erreur de syntaxe** se produit lorsque la syntaxe du langage
    Python n'est pas respectée :

    ``` {style="compil"}
    >>> 4 = a
      File "<stdin>", line 1
    SyntaxError: cannot assign to literal
    ```

-   une **erreur de définition** se produit lorsqu'on évalue une
    expression avec des valeurs de variables non définies :

    ``` {style="compil"}
    >>> 3 * a +1
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    NameError: name 'a' is not defined
    ```

-   une **erreur de type** se produit lorsqu'on évalue une expression
    avec des valeurs de variables non définies :

    ``` {style="compil"}
    >>> 2 + '2'
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: unsupported operand type(s) for +: 'int' and 'str'
    ```

-   une **erreur d'exécution** se produit lorsque l'interpréteur ne peut
    pas évaluer une expression ou exécuter une instruction :

    ``` {style="compil"}
    >>> def division(a, b):
    ...     return a / b
    ... 
    >>> x = 8
    >>> y = 0
    >>> z = division(x, y)
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
      File "<stdin>", line 2, in division
    ZeroDivisionError: division by zero
    TypeError: unsupported operand type(s) for +: 'int' and 'str'
    ```

-   une **erreur de logique** se produit lorsque le programme ne produit
    pas le résultat attendu ou ne se termine pas (boucle infinie) :

    ``` {style="compil"}
    >>> x = 1
    >>> while x > 0:
    ...     x = x +1      #boucle infinie, pour en sortir CTRL + C
    ^CTraceback (most recent call last):
      File "<stdin>", line 2, in <module>
    KeyboardInterrupt
    ```

![image](ressources/python.png)

[XKCD : 353](https://www.explainxkcd.com/wiki/index.php/353:_Python)
