---
layout: parc
title: Séances
---

??? done "Matériel et méthode de travail"

    * Matériel :
        * Un classeur avec des feuilles pour écrire et des pochettes transparentes pour ranger les cours
        * Mettre le site <https://frederic-junier.gitlab.io/parc-nsi-premiere/> dans les favoris de son navigateur, navigation responsive adaptée aux smartphone : toutes les ressources (cours, corrigés) sont publiés et disponibles sur ce site.des liens vers des QCM externes et des exercices pour travailler les automatismes.
        * Une clef USB de 8 Go minimum, cet [article](https://www.boulanger.com/c/cle-usb) n'est pas cher.
        * Le manuel Hachette NSI version papier fourni par la région de référence `978-2-01-786630-5`, accessible en ligne sur <https://mesmanuels.fr/acces-libre/3813624>

    * Méthode de travail :
        * D'une séance à l'autre : relire le cours, faire les exercices
        * Pendant la séance : alternance de temps d'activités et de synthèse, travail sur des projets    à rendre
        * Évaluations :
            * Rendu de mini-projet ou de projet plus conséquent (pendant les vacances) : travail en classe et à la maison en binôme, évaluations écrites ou orales
            * Formatives sous forme d'interrogations courtes (format QCM) ou d'exposés oral (histoire de l'informatique, synthèse de cours)
            * Sommatives sous forme de devoir d'une heure ou de TP noté
            * Autres (exposés, création d'un tuto video)

??? bug "Septembre"

    ??? done "Séance 1 : mercredi 4/09/2024"

        * 🎯 Chapitre 1: constructions de bases en langage Python

            1. Rejoindre le groupe __ParcPremiereNSI__ sur <http://www.france-ioi.org>  (se créer un compte d'abord) et commencer le chapitre 1 (affichage) du niveau 1.
            2. __Exercices :__  Démarrage en Python :  [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/aa2f-3818631)
            3. __Exercices :__  [TP 1](../chapitre1/TP1/1NSI-Chap1-Variables-TP1-.pdf) : instructions; expressions, variables, erreurs. Sur Capytale < <https://capytale2.ac-paris.fr/web/c/a5e6-644695/mcer>
            4. __Cours :__ Le point sur les constructions de base d'un langage de programmation [Section 1 : Bases d'un langage de programmation : instructions, littéraux, expressions](../chapitre1/cours/Chap1-Bases-Programmation-2021.pdf). Les thèmes suivants sont abordés :
    
                * ➡️ _Bases d'un langage de programmation : instructions, littéraux, expressions_, exemple d'un programme avec entrée/sortie (calcul d'âge à partir de la date de naissance)
                * ➡️ _Instructions conditionnelles_ :  Exercice 5 p. 42, QCM question 6 p. 39
                * ➡️ _Boucles bornées_ 
                * ➡️ _Boucles non bornées_ 

        * ✍️ Travail à faire 

            1. Rejoindre le groupe __ParcPremiereNSI__ sur <http://www.france-ioi.org>  (se créer un compte d'abord) et finir le chapitre 1 (affichage) du niveau 1.
            2. Finir le parcours "Répétitions d'instructions" sur France IOI <https://www.france-ioi.org/algo/chapter.php?idChapter=643>


        ![map](ressources/map.jpg){:align="center"}


    ??? done "Séance 2 : lundi 9/09/2024"

        🎯 Chapitre 1: constructions de bases en langage Python

        1. [France IOI](http://www.france-ioi.org)  :
            *  Niveau 1, chapitre 2 _Répétition d'instructions_, correction commentée par un élève : 
                 *  Correction de l'exercice [Sisyphe](http://www.france-ioi.org/algo/task.php?idChapter=643&idTask=1880), enchaîner deux répétitions
                 *  Correction de l'exercice  [Jeu de dames](http://www.france-ioi.org/algo/task.php?idChapter=643&idTask=1882), imbriquer deux répétitions
                 *  Correction de l'exercice [Mont Kailash](http://www.france-ioi.org/algo/task.php?idChapter=643&idTask=1883), imbriquer des répétitions
        3. __Cours :__ Le point sur les constructions de base d'un langage de programmation [Section 1 : Bases d'un langage de programmation : instructions, littéraux, expressions](../chapitre1/cours/Chap1-Bases-Programmation-2021.pdf). Les thèmes suivants sont abordés. Version pdf distribuée, ouvrir la version en [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/db3d-3851246)

            * ➡️ _Types de base des objets Python, Variable, Tableau d'état_
            * ➡️ _Instructions conditionnelles_ 
            * ➡️ _Boucles bornées_ 

        4. __Exercices d'entraînement :__   [Carnet Capytale France IOI Niveau 1](https://capytale2.ac-paris.fr/web/c/9cd0-1866888) => faire tous les exercices marqués d'un symbole ✅. Objectifs du jour :
   
             *  _Calculs et découverte des variables_ 
             *  _Lecture de l'entrée_ 
             *  _Tests et conditions_

   
        ✍️ Travail à faire 


        6. Finir les exercices France IOI marqués du symbole  ✅ des modules _Calculs et découverte des variables_ , _Lecture de l'entrée_  et _Tests et conditions_ du [Carnet Capytale France IOI Niveau 1](https://capytale2.ac-paris.fr/web/c/9cd0-1866888).


    ??? done "Séance 3 : mercredi 11/09/2024"

        🎯 Chapitre 1: constructions de bases en langage Python

        1. [France IOI](http://www.france-ioi.org)  :
            *  Niveau 1, chapitre 2 _Répétition d'instructions_, correction commentée par un élève : 
                 *  Correction de l'exercice [Sisyphe](http://www.france-ioi.org/algo/task.php?idChapter=643&idTask=1880), enchaîner deux répétitions
                 *  Correction de l'exercice  [Jeu de dames](http://www.france-ioi.org/algo/task.php?idChapter=643&idTask=1882), imbriquer deux répétitions
                 *  Correction de l'exercice [Mont Kailash](http://www.france-ioi.org/algo/task.php?idChapter=643&idTask=1883), imbriquer des répétitions
            *  Niveau 1, chapitre 3 _Calculs et découverte des variables_ , correction commentée par un élève : 
                 *  Correction de l'exercice [Une partie de cache cache](https://www.france-ioi.org/algo/task.php?idChapter=644&iOrder=11)
                 *  Correction de l'exercice [Décollage de fusée](https://www.france-ioi.org/algo/task.php?idChapter=644&idTask=1891)
                 *  Correction de l'exercice [Invasion de batraciens](https://www.france-ioi.org/algo/task.php?idChapter=644&iOrder=15)
        2. __Cours :__ Le point sur les constructions de base d'un langage de programmation [Section 1 : Bases d'un langage de programmation : instructions, littéraux, expressions](../chapitre1/cours/Chap1-Bases-Programmation-2021.pdf). Les thèmes suivants sont abordés. Version pdf distribuée, ouvrir la version en [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/db3d-3851246)

            * ➡️ _Boucles non bornées_ 

        3. __Exercices d'entraînement :__   [Carnet Capytale France IOI Niveau 1](https://capytale2.ac-paris.fr/web/c/9cd0-1866888) => faire tous les exercices marqués d'un symbole ✅. Objectifs du jour :
   
             *  _Lecture de l'entrée_  (3 exercices au moins)
             *  _Tests et conditions_  (3 exercices au moins)
             *  _Structures avancées_  (3 exercices au moins)

   
        ✍️ Travail à faire 


        1. Faire les exercices France IOI marqués du symbole  ✅  (au moins trois par module) des modules _Conditions avancées, opérateurs booléens_ et  _Répétitions conditionnées_  du [Carnet Capytale France IOI Niveau 1](https://capytale2.ac-paris.fr/web/c/9cd0-1866888).
        2. * Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs) : possibilité de choisir un des sujets proposés [ici](../Culture/expos%C3%A9s.md) ou de proposer un sujet à faire valider par l'enseignant.



    ??? done "Séance 4 : lundi 16/09/2024"

        🎯 Chapitre 1: constructions de bases en langage Python

        1. [France IOI](http://www.france-ioi.org)  :
            *  Niveau 1, chapitre 2 _Tests_, correction commentée par un élève : 
                 *  Correction de l'exercice [Tarifs dégressifs](https://www.france-ioi.org/algo/task.php?idChapter=646&idTask=1969), test avec un seul if
                 *  Correction de l'exercice  [Tarif du bateau](https://www.france-ioi.org/algo/task.php?idChapter=646&iOrder=6), test avec if et else
        
            *  Niveau 1, chapitre 3 _Structures avancées_ , correction commentée par un élève : 
                 *  Correction de l'exercice [Villes et villages](https://www.france-ioi.org/algo/task.php?idChapter=647&idTask=1939), test imbriqué dans une boucle
                 *  Correction de l'exercice [Distance la plus longue](https://www.france-ioi.org/algo/task.php?idChapter=647&iOrder=3), recherche de maximum
                 *  Correction de l'exercice [ Tarifs de l'auberge](https://www.france-ioi.org/algo/task.php?idChapter=647&idTask=1948), tests imbriqués => if elif else
        2. __Cours :__ Le point sur les constructions de base d'un langage de programmation [Section 1 : Bases d'un langage de programmation : instructions, littéraux, expressions](../chapitre1/cours/Chap1-Bases-Programmation-2021.pdf). Les thèmes suivants sont abordés. Version pdf distribuée, ouvrir la version en [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/db3d-3851246)

            * ➡️ _Boucles non bornées_ 
            *  ➡️ _Erreurs_

        3. __Exercices d'entraînement :__   [Carnet Capytale France IOI Niveau 1](https://capytale2.ac-paris.fr/web/c/9cd0-1866888) => faire tous les exercices marqués d'un symbole ✅. Objectifs du jour :
   
             *  _Boucles conditionnées_   :
                *  [Administration : comptes annuels](https://www.france-ioi.org/algo/task.php?idChapter=649&idTask=2057) => boucle non bornée avec condition d'arrêt sur test d'égalité
                *  [Construction de pyramide](https://www.france-ioi.org/algo/task.php?idChapter=649&iOrder=6) => boucle non bornée avec condition d'arrêt sur test d'inégalité

            * _Conditions avancées et opérateurs booléens_ :
                * [Maison de l'espion](https://www.france-ioi.org/algo/task.php?idChapter=648&idTask=1975) => condition avec and
                * [Nombre de jours dans le mois](https://www.france-ioi.org/algo/task.php?idChapter=648&iOrder=4)  => condition avec and et or
                * [Amitié entre gardes](https://www.france-ioi.org/algo/task.php?idChapter=648&iOrder=5) => condition avec or
        
        4. [Préparation du DS 1](https://nuage03.apps.education.fr/index.php/s/meS39fNdDkXHZi2)

   
        ✍️ Travail à faire 


        1. [Mercredi DS 1](https://nuage03.apps.education.fr/index.php/s/meS39fNdDkXHZi2)
        4. Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs) : possibilité de choisir un des sujets proposés [ici](../Culture/expos%C3%A9s.md) ou de proposer un sujet à faire valider par l'enseignant.



    ??? done "Séance 5 : mercredi 18/09/2024"

        🎯 Chapitre 1: constructions de bases en langage Python

        1.  💯 DS n° 1 : boucles bornées, calculs de variables ,tests
        2. __Cours :__ Le point sur les constructions de base d'un langage de programmation [Section 1 : Bases d'un langage de programmation : instructions, littéraux, expressions](../chapitre1/cours/Chap1-Bases-Programmation-2021.pdf). Les thèmes suivants sont abordés. Version pdf distribuée, ouvrir la version en [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/db3d-3851246)

            * ➡️ _Boucles non bornées / boucles conditionnées_ 
            * ➡️ _Fonctions_ 
            *  ➡️ _Erreurs_

        3. __Exercices d'entraînement France IOI :__   [Carnet Capytale France IOI Niveau 1](https://capytale2.ac-paris.fr/web/c/9cd0-1866888) => faire tous les exercices marqués d'un symbole ✅. Objectifs du jour :

             *  _Boucles conditionnées_   :
             *  [Administration : comptes annuels](https://www.france-ioi.org/algo/task.php?idChapter=649&idTask=2057) => boucle non bornée avec condition d'arrêt sur test d'égalité
             *  [Construction de pyramide](https://www.france-ioi.org/algo/task.php?idChapter=649&iOrder=6) => boucle non bornée avec condition d'arrêt sur test d'inégalité
        4. __Exercices de découverte des fonctions__ : [Carnet Capytale découverte des fonctions](https://capytale2.ac-paris.fr/web/c/a386-3953863)

            



        ✍️ Travail à faire 


        1. Sur France IOI traiter ces trois exercices sur _Conditions avancées et opérateurs booléens_ :
             * [Maison de l'espion](https://www.france-ioi.org/algo/task.php?idChapter=648&idTask=1975) => condition avec and
             * [Nombre de jours dans le mois](https://www.france-ioi.org/algo/task.php?idChapter=648&iOrder=4)  => condition avec and et or
             * [Amitié entre gardes](https://www.france-ioi.org/algo/task.php?idChapter=648&iOrder=5) => condition avec or
        
        2. Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs) : possibilité de choisir un des sujets proposés [ici](../Culture/expos%C3%A9s.md) ou de proposer un sujet à faire valider par l'enseignant.


    ??? done "Séance 6 : lundi 23/09/2024"

        🧑‍🏫 Séance

        1.  💯 Correction du DS n° 1, parcours différencié (30 minutes) :

            * Pour ceux qui ont eu moins de 14 :
                
                [TP2](../chapitre1/TP2/TP2.pdf) en s'aidant de la synthèse de cours. Faire les exercices dans ce carnet [Capytale](https://capytale2.ac-paris.fr/web/c/63ce-1776650)

                Ressources pour le [TP2](../chapitre1/TP2/TP2.pdf)  :
                Liens vers les corrigés :

                * [corrigé pdf](../chapitre1/TP2/correction/Correction_TP2.pdf)
                * [corrigé .py](https://gitlab.com/frederic-junier/parc-nsi/-/raw/master/docs/chapitre1/TP2/correction/Correction_TP2.py)
  
            * Les autres avancent sur le parcours général France IOI
  
        2.  Fonctions : 
            
            * correction de l'exercice 1 du [Carnet Capytale découverte des fonctions](https://capytale2.ac-paris.fr/web/c/a386-3953863)

        3. Découverte du module [tkinter](./tkinter.md) pour créer des interfaces graphiques.
           
            Plusieurs objectifs :

            * donner une boîte à outils pour de futurs projets
            * découvrir les fonctions dans un autre cadre 
            * aborder la notion de portée d'une variable (locale ou globale)
            
            [Quatre exercices](./tkinter.md)  à traiter dans l'ordre.

        ✍️ Travail à faire 


        1. Sur France IOI traiter ces trois exercices sur _Conditions avancées et opérateurs booléens_ :
             * [Maison de l'espion](https://www.france-ioi.org/algo/task.php?idChapter=648&idTask=1975) => condition avec and
             * [Nombre de jours dans le mois](https://www.france-ioi.org/algo/task.php?idChapter=648&iOrder=4)  => condition avec and et or
             * [Amitié entre gardes](https://www.france-ioi.org/algo/task.php?idChapter=648&iOrder=5) => condition avec or
        
        2. Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs) : possibilité de choisir un des sujets proposés [ici](../Culture/expos%C3%A9s.md) ou de proposer un sujet à faire valider par l'enseignant.


    ??? done "Séance 7 : mercredi 25/09/2024"

        🧑‍🏫 Séance

        1.  🏊 Exercices sur les boucles et les fonctions dans ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/fd1a-4019153)

           
  
        2.  __France IOI :__   [Carnet Capytale France IOI Niveau 1](https://capytale2.ac-paris.fr/web/c/9cd0-1866888) 
            
            Correction de ces trois exercices sur _Conditions avancées et opérateurs booléens_ :
             * [Maison de l'espion](https://www.france-ioi.org/algo/task.php?idChapter=648&idTask=1975) => condition avec and
             * [Nombre de jours dans le mois](https://www.france-ioi.org/algo/task.php?idChapter=648&iOrder=4)  => condition avec and et or
             * [Amitié entre gardes](https://www.france-ioi.org/algo/task.php?idChapter=648&iOrder=5) => condition avec or

        3. Découverte du module [tkinter](./tkinter.md) pour créer des interfaces graphiques.
           
            Plusieurs objectifs :

            * donner une boîte à outils pour de futurs projets
            * découvrir les fonctions dans un autre cadre 
            * aborder la notion de portée d'une variable (locale ou globale)
            
            [Quatre exercices](./tkinter.md)  à traiter dans l'ordre.

        4. 🎯  Chapitre 2 : fonctions, spécification et tests

            * [Cours](./chapitre3.md)
            * Vous faites les exercices dans le [carnet Capytale](https://capytale2.ac-paris.fr/web/c/23a7-84924/mcer). Aujourd'hui :
                * Définition d'une fonction :exercices 1, 2 et 3
     
        ✍️ Travail à faire 


        1. Dans le [carnet Capytale du chapitre 2 Fonctions](https://capytale2.ac-paris.fr/web/c/23a7-84924/mcer) avancer jusqu'à l'entrainement 2
        
        2. Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs) : possibilité de choisir un des sujets proposés [ici](../Culture/expos%C3%A9s.md) ou de proposer un sujet à faire valider par l'enseignant.


    ??? done "Séance 8 : lundi 30/09/2024"

        🧑‍🏫 Séance


        1. 🎯  Chapitre 2 : fonctions, spécification et tests

            * [Cours](./chapitre3.md)
            * Vous faites les exercices dans le [carnet Capytale](https://capytale2.ac-paris.fr/web/c/23a7-84924/mcer). Aujourd'hui :
                * Définition d'une fonction :exercices 1, 2, 3 et 4
                * Mise au point : spécification ,précondition et tests unitaires : exercice 5
  
  

        2. Découverte du module [tkinter](./tkinter.md) pour créer des interfaces graphiques.
           
            Plusieurs objectifs :

            * donner une boîte à outils pour de futurs projets
            * découvrir les fonctions dans un autre cadre 
            * aborder la notion de portée d'une variable (locale ou globale)
            
            [Quatre exercices](./tkinter.md)  à traiter dans l'ordre.

        
        3. Fonctions et Shadoks : [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/2e20-2104997)
   

        ![alt](https://nuage03.apps.education.fr/index.php/apps/files_sharing/publicpreview/4JMJkBopBHpxLd7?file=/&fileId=249657723&x=1920&y=1080&a=true&etag=66f9942b81157){:.center}

        _Source :_ Numération Shadok  <https://shadoks.fandom.com/fr/wiki/Syst%C3%A8me_de_num%C3%A9rotation_Shadok>

        ✍️ Travail à faire 


        1. Dans le [carnet Capytale du chapitre 2 Fonctions](https://capytale2.ac-paris.fr/web/c/23a7-84924/mcer)  faire les entraînements 1, 2 et 3
        2. DS sur les chapitres 1 (Programmation) et 2 (Fonctions) le lundi 7 Octobre        
        3. Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs) : possibilité de choisir un des sujets proposés [ici](../Culture/expos%C3%A9s.md) ou de proposer un sujet à faire valider par l'enseignant.

??? bug "Octobre"


    ??? done "Séance 9 : mercredi 02/10/2024"

        🧑‍🏫 Séance

        1. 🎯  Chapitre 2 : fonctions, spécification et tests

            * [Cours](./chapitre3.md)
            * Vous faites les exercices dans le [carnet Capytale](https://capytale2.ac-paris.fr/web/c/23a7-84924/mcer). Aujourd'hui :
                * Définition d'une fonction : exercice  4 et correction des entraînements 1 et 2
                * Mise au point : spécification, précondition et tests unitaires : exercice 5

        2.  🎯  Chapitre 3 : Opérateurs booléens et tables de vérité : [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/aa70-4126072) et [Cours](./chapitre1/cours/operateurs_booleens.md) et [Cours pdf](./chapitre1/cours/operateurs_booleens.pdf) 

        3. Fonctions et Shadoks : [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/2e20-2104997)

            ![alt](https://nuage03.apps.education.fr/index.php/apps/files_sharing/publicpreview/4JMJkBopBHpxLd7?file=/&fileId=249657723&x=1920&y=1080&a=true&etag=66f9942b81157){:.center}

            _Source :_ Numération Shadok  <https://shadoks.fandom.com/fr/wiki/Syst%C3%A8me_de_num%C3%A9rotation_Shadok>


        4. Découverte du module [tkinter](./tkinter.md) pour créer des interfaces graphiques.
           
            Plusieurs objectifs :

            * donner une boîte à outils pour de futurs projets
            * découvrir les fonctions dans un autre cadre 
            * aborder la notion de portée d'une variable (locale ou globale)
            
            [Quatre exercices](./tkinter.md)  à traiter dans l'ordre.

        
   
        
        ✍️ Travail à faire 


        6. Dans le [carnet Capytale du chapitre 2 Fonctions](https://capytale2.ac-paris.fr/web/c/23a7-84924/mcer)  faire les entraînements 1, 2 et 3
        7. DS sur les chapitres 1 (Programmation) et 2 (Fonctions) le lundi 7 Octobre        
        8. Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs) : possibilité de choisir un des sujets proposés [ici](../Culture/expos%C3%A9s.md) ou de proposer un sujet à faire valider par l'enseignant.



    ??? done "Séance 10 : lundi 07/10/2024"

        🧑‍🏫 Séance

        1.  💯 DS n° 2 : boucles bornées, calculs de variables ,tests, fonctions et opérateurs booléens
        2.  Découverte du module [tkinter](./tkinter.md) pour créer des interfaces graphiques.
           
            Plusieurs objectifs :

            * donner une boîte à outils pour de futurs projets
            * découvrir les fonctions dans un autre cadre 
            * aborder la notion de portée d'une variable (locale ou globale)
            
            [Quatre exercices](./tkinter.md)  à traiter dans l'ordre.

        3. [Présentation des projets](https://frederic-junier.org/NSI/premiere/tkinter#projets) :

              * [Tableau d'affectation des sujets](https://nuage03.apps.education.fr/index.php/s/bKaytcT27gxjDxZ)
              * Premier point étape : lundi 4 Novembre, rendre la [fiche de suivi](./Projets/Projets2025/tkinter/Fiche_Suivi_Projet_Tkinter_2024.pdf) complété => déposer la fiche dans cet [espace de dépôt](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=36421)
              * Rendu final et notation (total sur 10 points) : 
                  * Rendu final du code : première semaine de décembre
                  * Fiche point étape 1 : notation individuelle sur 2 points
                  * Code :  notation collective sur 5 points 
                  * Présentation individuelle de 3 minutes : sur 3 points

        
   
        
        ✍️ Travail à faire 

        4. Faire les exercices sur Fonctions et Shadoks : [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/2e20-2104997)
        5. Réfléchir au choix d'un sujet d'exposé à placer dans le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs) : possibilité de choisir un des sujets proposés [ici](../Culture/expos%C3%A9s.md) ou de proposer un sujet à faire valider par l'enseignant.


    ??? done "Séance 11 : mercredi 9/10/2024"

        🧑‍🏫 Séance

        
        1. 🗣️ Exposé oral
        2.  💯 Carnet Capytale : [Retour sur le DS 2](https://capytale2.ac-paris.fr/web/c/1627-4230872). 
        💡[Dépôt des DS de NSI](https://aura-69-metro2.elea.apps.education.fr/course/view.php?id=189&section=20)
        3. 🎯  Chapitre 2 : fonctions, spécification et tests => **uniquement pour ceux qui ont eu 13 ou plus au DS**

            * [Cours](./chapitre3.md)
            * [carnet Capytale](https://capytale2.ac-paris.fr/web/c/23a7-84924/mcer). Aujourd'hui :
                * Portée d'une variable : définition et exercices 6 et 7
        4. Fonctions et Shadoks correction : [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/2e20-2104997)
        5. 🎯 Chapitre 3 : HTML-CSS
            * [Synthèse de cours](../chapitre2/memo/MemoHTML-CSS-2020.pdf)  ➡️ pages 1 à 3
            * Télécharger le [le mini-site portable](https://aura-69-metro2.elea.apps.education.fr/pluginfile.php/10809/mod_resource/content/1/HTML-CSS.zip)
            * Activités proposées dans [le mini-site portable](https://aura-69-metro2.elea.apps.education.fr/pluginfile.php/10809/mod_resource/content/1/HTML-CSS.zip)  :
                * Dans `site/premiere_page_html/html_balises.html` faire les exercices 2, 3, 4, 5 et 6 : lire les consignes sur le mini-site et traiter les exercices dans Capytale :
        
                    * [exercice 2](https://capytale2.ac-paris.fr/web/c/2c4a-61119/mcer)
                    * [exercice 3](https://capytale2.ac-paris.fr/web/c/0f0c-61116/mcer)
                    * [exercice 4](https://capytale2.ac-paris.fr/web/c/6caa-61110/mcer)
        
        6. [Travail sur Projets](https://frederic-junier.org/NSI/premiere/tkinter#projets) :

              * [Tableau d'affectation des sujets](https://nuage03.apps.education.fr/index.php/s/bKaytcT27gxjDxZ)
              * Premier point étape : lundi 4 Novembre, rendre la [fiche de suivi](./Projets/Projets2025/tkinter/Fiche_Suivi_Projet_Tkinter_2024.pdf) complété => déposer la fiche dans cet [espace de dépôt](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=36421)
              * Rendu final et notation (total sur 10 points) : 
                  * Rendu final du code : première semaine de décembre
                  * Fiche point étape 1 : notation individuelle sur 2 points
                  * Code :  notation collective sur 5 points 
                  * Présentation individuelle de 3 minutes : sur 3 points

        
   
        
        ✍️ Travail à faire 

        7. Lire attentivement les codes de correction des quatre exercices d'[introduction à tkinter](https://frederic-junier.org/NSI/premiere/tkinter) et s'en inspirer pour travailler sur son projet
        8. Petite interrogation de rattrapage du DS2 : réviser la correction du DS2, voir le [Dépôt des DS de NSI](https://aura-69-metro2.elea.apps.education.fr/course/view.php?id=189&section=20) et le [Carnet retour sur le DS 2](https://capytale2.ac-paris.fr/web/c/1627-4230872). 
        9.  Apporter une clef USB
        10. Installer sur son PC personnel une distribution Python, par exemple [Edupyter](https://www.edupyter.net/)
        11. Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs) : possibilité de choisir un des sujets proposés [ici](../Culture/expos%C3%A9s.md) ou de proposer un sujet à faire valider par l'enseignant.


    ??? done "Séance 12 : lundi 14/10/2024"

        🧑‍🏫 Séance

               
        1.  💯 DS3 : bases de programmation. 💡[Dépôt des DS de NSI](https://aura-69-metro2.elea.apps.education.fr/course/view.php?id=189&section=20)
        2.  🗣️ Exposé oral        
        3. 🎯 Chapitre 3 : HTML-CSS
            * [Synthèse de cours](../chapitre2/memo/MemoHTML-CSS-2020.pdf)  ➡️ pages 1 à 3
            * Télécharger le [le mini-site portable](https://aura-69-metro2.elea.apps.education.fr/pluginfile.php/10809/mod_resource/content/1/HTML-CSS.zip)
            * Activités proposées dans [le mini-site portable](https://aura-69-metro2.elea.apps.education.fr/pluginfile.php/10809/mod_resource/content/1/HTML-CSS.zip)  :
                * Dans `site/premiere_page_html/html_balises.html` faire les exercices 2, 3, 4, 5 et 6 : lire les consignes sur le mini-site et traiter les exercices dans Capytale :
        
                    * [exercice 2](https://capytale2.ac-paris.fr/web/c/2c4a-61119/mcer)
                    * [exercice 3](https://capytale2.ac-paris.fr/web/c/0f0c-61116/mcer)
                    * [exercice 4](https://capytale2.ac-paris.fr/web/c/6caa-61110/mcer)
                    * [exercice 5](https://capytale2.ac-paris.fr/web/c/6226-61112/mcer)
                    * [exercice 6](https://capytale2.ac-paris.fr/web/c/29bf-61114/mcer)
                *  Dans `site/premiere_feuille_css/css_selecteurs.html` lire le cours sur les sélecteurs CSS par identifiant ou par classe, puis faire :
    
                    *  [exercice 2](https://capytale2.ac-paris.fr/web/code/9d3b-61121)
        
        4. [Travail sur Projets](https://frederic-junier.org/NSI/premiere/tkinter#projets) :

              * [Tableau d'affectation des sujets](https://nuage03.apps.education.fr/index.php/s/bKaytcT27gxjDxZ)
              * Premier point étape : lundi 4 Novembre, rendre la [fiche de suivi](./Projets/Projets2025/tkinter/Fiche_Suivi_Projet_Tkinter_2024.pdf) complété => déposer la fiche dans cet [espace de dépôt](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=36421)
              * Rendu final et notation (total sur 10 points) : 
                  * Rendu final du code : première semaine de décembre
                  * Fiche point étape 1 : notation individuelle sur 2 points
                  * Code :  notation collective sur 5 points 
                  * Présentation individuelle de 3 minutes : sur 3 points

        
   
        
        ✍️ Travail à faire 

        1. Lire attentivement les codes de correction des quatre exercices d'[introduction à tkinter](https://frederic-junier.org/NSI/premiere/tkinter) et s'en inspirer pour travailler sur son projet
        2. Répondre à ce [QCM dans le cours 1NSI sur Elea](https://aura-69-metro2.elea.apps.education.fr/mod/simplequiz/view.php?id=36986)
        3. Apporter une clef USB
        4.  Installer sur son PC personnel une distribution Python, par exemple [Edupyter](https://www.edupyter.net/)
        5.  Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs) : possibilité de choisir un des sujets proposés [ici](../Culture/expos%C3%A9s.md) ou de proposer un sujet à faire valider par l'enseignant.


    ??? done "Séance 13 : mercredi 16/10/2024"

        🧑‍🏫 Séance

               
        6.  💯 Retour du DS3. 💡[Dépôt des DS de NSI](https://aura-69-metro2.elea.apps.education.fr/course/view.php?id=189&section=20){:target="_blank"}    
        7.  🏊 Automatismes : exercices de programmation sur boucles et fonctions [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/d166-4333240)
        8. 🎯 Chapitre 3 : HTML-CSS
            * [Synthèse de cours](../chapitre2/memo/MemoHTML-CSS-2020.pdf)  ➡️ pages 1 à 3
            * Télécharger le [le mini-site portable](https://aura-69-metro2.elea.apps.education.fr/pluginfile.php/10809/mod_resource/content/1/HTML-CSS.zip){:target="_blank"}
            * Correction du [QCM dans le cours 1NSI sur Elea](https://aura-69-metro2.elea.apps.education.fr/mod/simplequiz/view.php?id=36986){:target="_blank"}
            * Activités proposées dans [le mini-site portable](https://aura-69-metro2.elea.apps.education.fr/pluginfile.php/10809/mod_resource/content/1/HTML-CSS.zip){:target="_blank"}  :
                * Dans `site/premiere_page_html/html_balises.html` faire les exercices 2, 3, 4, 5 et 6 : lire les consignes sur le mini-site et traiter les exercices dans Capytale :
        
                    * [exercice 2](https://capytale2.ac-paris.fr/web/c/2c4a-61119/mcer){:target="_blank"}
                    * [exercice 3](https://capytale2.ac-paris.fr/web/c/0f0c-61116/mcer){:target="_blank"}
                    * [exercice 4](https://capytale2.ac-paris.fr/web/c/6caa-61110/mcer){:target="_blank"}
                    * [exercice 5](https://capytale2.ac-paris.fr/web/c/6226-61112/mcer){:target="_blank"}
                    * [exercice 6](https://capytale2.ac-paris.fr/web/c/29bf-61114/mcer){:target="_blank"}
                *  Dans `site/premiere_feuille_css/css_selecteurs.html` lire le cours sur les sélecteurs CSS par identifiant ou par classe, puis faire :
    
                    *  [exercice 2](https://capytale2.ac-paris.fr/web/code/9d3b-61121){:target="_blank"}
        
        9. [Travail sur Projets](https://frederic-junier.org/NSI/premiere/tkinter#projets){:target="_blank"} :

              * [Tableau d'affectation des sujets](https://nuage03.apps.education.fr/index.php/s/bKaytcT27gxjDxZ){:target="_blank"}
              
              * Rendu final et notation (total sur 10 points) : 
                  * Rendu final du code : première semaine de décembre
                  * Fiche point étape 1 : notation individuelle sur 2 points
                  * Code :  notation collective sur 5 points 
                  * Présentation individuelle de 3 minutes : sur 3 points

        
   
        
        ✍️ Travail à faire 

        10. Lire attentivement les codes de correction des quatre exercices d'[introduction à tkinter](https://frederic-junier.org/NSI/premiere/tkinter){:target="_blank"} et s'en inspirer pour travailler sur son projet
        11. Premier point étape : lundi 4 Novembre, rendre la [fiche de suivi](./Projets/Projets2025/tkinter/Fiche_Suivi_Projet_Tkinter_2024.pdf) complété => déposer la fiche dans cet [espace de dépôt](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=36421){:target="_blank"}
        12. Apporter une clef USB
        13. Installer sur son PC personnel une distribution Python, par exemple [Edupyter](https://www.edupyter.net/){:target="_blank"}
        14. Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](../Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.


??? bug "Novembre"


    ??? done "Séance 14 : lundi 04/11/2024"

        * 🗣️  Exposé
        * 📓 Récupération de  la [fiche de suivi](./Projets/Projets2025/tkinter/Fiche_Suivi_Projet_Tkinter_2024.pdf) qui a été déposée dans cet [espace de dépôt](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=36421){:target="_blank"}
        * 🎯  Chapitre 4 : tableaux à une dimension
            * Exposé du [Cours](./chapitre6/Cours/tableaux-cours-git.md){:target="_blank"}  et traitement des exercices dans le  [Carnet Capytale avec exercices du cours](https://capytale2.ac-paris.fr/web/c/a3d0-4422844){:target="_blank"} : 1, 2, 3, 4
            * Exercices :  traiter les exercices du [carnet Capytale avec exercices d'entraînement sur les tableaux](https://capytale2.ac-paris.fr/web/c/c1d1-2193695){:target="_blank"}
        * [Travail sur Projets](https://frederic-junier.org/NSI/premiere/tkinter#projets){:target="_blank"} :
            * [fiche de suivi](./Projets/Projets2025/tkinter/Fiche_Suivi_Projet_Tkinter_2024.pdf){:target="_blank"} complété => déposer la fiche dans cet [espace de dépôt](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=36421){:target="_blank"}
        * Les élèves qui sont en avance traitent les exercices avec le tag boucle sur le site [/e-nsi.forge.aeif.fr/pratique](https://e-nsi.forge.aeif.fr/pratique/tags/#1-boucle). Il est conseillé de recopier ses solutions dans un carnet Capytale.
        * ✍️ Travail à faire  :
            * Exercices 1 à 4 du [carnet Capytale avec exercices d'entraînement sur les tableaux](https://capytale2.ac-paris.fr/web/c/c1d1-2193695){:target="_blank"}
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](../Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.



    ??? done "Séance 15 : mercredi 06/11/2024"

        * 🗣️  Exposé
        * 🎯  Chapitre 4 : tableaux à une dimension
            * Exercices :  correction des  exercices 1 à 4 du [carnet Capytale avec exercices d'entraînement sur les tableaux](https://capytale2.ac-paris.fr/web/c/c1d1-2193695){:target="_blank"}
            * Exposé du [Cours](./chapitre6/Cours/tableaux-cours-git.md){:target="_blank"}  et traitement des exercices dans le  [Carnet Capytale avec exercices du cours](https://capytale2.ac-paris.fr/web/c/a3d0-4422844){:target="_blank"} :  exercice 5
            * Exercices :  traiter les exercices 5 à 9  du [carnet Capytale avec exercices d'entraînement sur les tableaux](https://capytale2.ac-paris.fr/web/c/c1d1-2193695){:target="_blank"}
        * [Travail sur Projets](https://frederic-junier.org/NSI/premiere/tkinter#projets){:target="_blank"} :
            * [fiche de suivi](./Projets/Projets2025/tkinter/Fiche_Suivi_Projet_Tkinter_2024.pdf){:target="_blank"} complété => déposer la fiche dans cet [espace de dépôt](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=36421){:target="_blank"}
        * Les élèves qui sont en avance traitent les exercices avec le tag boucle sur le site [/e-nsi.forge.aeif.fr/pratique](https://e-nsi.forge.aeif.fr/pratique/tags/#1-boucle). Il est conseillé de recopier ses solutions dans un carnet Capytale.
        * ✍️ Travail à faire  :
            * Finir de traiter tous les exercices du [carnet Capytale avec exercices d'entraînement sur les tableaux](https://capytale2.ac-paris.fr/web/c/c1d1-2193695){:target="_blank"}
            * Interrogation écrite courte sur le chapitre Tableau à une dimension
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](../Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.


    ??? done "Séance 16 : mercredi 13/11/2024"

        🧑‍🏫 Séance

        * 💯 Retour du DS4. 💡[Dépôt des DS de NSI](https://aura-69-metro2.elea.apps.education.fr/course/view.php?id=189&section=20){:target="_blank"}                
        *  💯 DS 4 sur les tableaux
        *  🗣️  Exposé
        * 🎯  Chapitre 4 : tableaux à une dimension
            * Exposé du [Cours](./chapitre6/Cours/tableaux-cours-git.md){:target="_blank"}  et traitement des exercices dans le  [Carnet Capytale avec exercices du cours](https://capytale2.ac-paris.fr/web/c/a3d0-4422844){:target="_blank"} :
                * [Aliasing et copie de tableaux](http://127.0.0.1:8000/parc-nsi-premiere/chapitre6/Cours/tableaux-cours-git/#aliasing){:target="_blank"}  => [Exercice 6 question 1 uniquement](https://capytale2.ac-paris.fr/web/c/a3d0-4422844){:target="_blank"}
                * [Aliasing et passage de tableau en paramètre d'une fonction](http://127.0.0.1:8000/parc-nsi-premiere/chapitre6/Cours/tableaux-cours-git/#aliasing-et-passage-de-tableau-en-parametre-dune-fonction){:target="_blank"}  => [Exercice 7 question 1 uniquement](https://capytale2.ac-paris.fr/web/c/a3d0-4422844){:target="_blank"}
                * [Méthodes de tableau dynamique](http://127.0.0.1:8000/parc-nsi-premiere/chapitre6/Cours/tableaux-cours-git/#methodes-de-tableau-dynamique-en-python){:target="_blank"}  => [Exercice 8 ](https://capytale2.ac-paris.fr/web/c/a3d0-4422844){:target="_blank"}
            * Carnet d'exercices 1 :  traiter les exercices 5 à  7 du [carnet Capytale avec exercices d'entraînement sur les tableaux](https://capytale2.ac-paris.fr/web/c/c1d1-2193695){:target="_blank"}
            * Carnet d'exercices 2 : [deuxième carnet Capytale avec exercices d'entraînement sur les tableaux](https://capytale2.ac-paris.fr/web/c/e1c2-4547072){:target="_blank"}
        * [Travail sur Projets](https://frederic-junier.org/NSI/premiere/tkinter#projets){:target="_blank"} :

        * ✍️ Travail à faire  :
            * Traiter les cinq premiers exercices du [deuxième carnet Capytale avec exercices d'entraînement sur les tableaux](https://capytale2.ac-paris.fr/web/c/e1c2-4547072){:target="_blank"}
            * Avancer son projet
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](../Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.


    ??? done "Séance 17 : lundi 18/11/2024"
        🧑‍🏫 Séance

        * 💯 Retour du DS4. 💡[Dépôt des DS de NSI](https://aura-69-metro2.elea.apps.education.fr/course/view.php?id=189&section=20){:target="_blank"}    
        * 🏊  Entraînement : puzzle de Parsons, remettre le code dans l'ordre :
            *  [Recherche de maximum](https://www.codepuzzle.io/PZ72N)
            *  [Recherche linéaire d'un élément](https://www.codepuzzle.io/PTFZN)
        * 🗣️  Exposé 
        * 🎯  Chapitre 4 : tableaux à une dimension
            * Exposé du [Cours](./chapitre6/Cours/tableaux-cours-git.md){:target="_blank"}  et traitement des exercices dans le  [Carnet Capytale avec exercices du cours](https://capytale2.ac-paris.fr/web/c/a3d0-4422844){:target="_blank"} :
                * [Méthodes de tableau dynamique](./chapitre6/Cours/tableaux-cours-git.md){:target="_blank"}  => [Exercice 8 ](https://capytale2.ac-paris.fr/web/c/a3d0-4422844){:target="_blank"}
     
            *  Carnet d'exercices 2 : [deuxième carnet Capytale avec exercices d'entraînement sur les tableaux](https://capytale2.ac-paris.fr/web/c/e1c2-4547072){:target="_blank"} => Exercices 9, 10 et 11
        * 🎯 Chapitre 5 : tableaux à deux dimensions / Traitement d'images

            * [Cours / TP en version pdf](./chapitre7/NSI-Images-Tableaux2d--2023V1.pdf)  
            * [Cours / TP en version notebook sur Capytale (avec corrigés)](https://capytale2.ac-paris.fr/web/c/7ad3-163223)
        * [Travail sur Projets](https://frederic-junier.org/NSI/premiere/tkinter#projets){:target="_blank"}
        * ✍️ Travail à faire pour  :
            * Pour le 20/11 Faire les deux exercices sur les tableaux à deux dimensions de ce  [carnet Capytale](https://capytale2.ac-paris.fr/web/c/fe19-2326379)
            * Avancer son projet
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](./Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant. 


    ??? done "Séance 18 : mercredi 20/11/2024"
        🧑‍🏫 Séance

        *  🏊  Entraînement : exercice 1 de ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/218b-4632527) qui devra être terminé pour le 25/11
        * 🗣️  Exposé 
   
        
        * 🎯  Chapitre 4 : tableaux à une dimension
            *  Carnet d'exercices 2 : [deuxième carnet Capytale avec exercices d'entraînement sur les tableaux](https://capytale2.ac-paris.fr/web/c/e1c2-4547072){:target="_blank"} => Correction de exercices 9, 10 et 11
        * 🎯 Chapitre 5 : tableaux à deux dimensions / Traitement d'images

            * [Cours / TP en version pdf](./chapitre7/NSI-Images-Tableaux2d--2023V1.pdf)  
            * [Cours / TP en version notebook sur Capytale (avec corrigés)](https://capytale2.ac-paris.fr/web/c/7ad3-163223)
        * [Travail sur Projets](https://frederic-junier.org/NSI/premiere/tkinter#projets){:target="_blank"}
        * ✍️ Travail à faire pour  :
            * Pour le 25/11 :  faire tous les exercices de ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/218b-4632527)
            * Avancer son projet
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](./Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.



    ??? done "Séance 19 : lundi 25/11/2024"
        🧑‍🏫 Séance


        *  🏊  Entraînement : exercice 1 de ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/995f-4713185) qui devra être terminé pour le 27/11
        *  📓 Correction des exercices de ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/218b-4632527)      
        * 🎯 Chapitre 5 : tableaux à deux dimensions / Traitement d'images

            * [Cours / TP en version pdf](./chapitre7/NSI-Images-Tableaux2d--2023V1.pdf)  
            * [Cours / TP en version notebook sur Capytale (avec corrigés)](https://capytale2.ac-paris.fr/web/c/7ad3-163223) => à partir de l'exercice 3 (QCM) jusqu'à l'exercice 6
        * [Travail sur Projets](https://frederic-junier.org/NSI/premiere/tkinter#projets){:target="_blank"}
        * Pour ceux qui sont en avance : [Projet Stéganographie](https://capytale2.ac-paris.fr/web/c/69bf-1019976)
        * Pour les élèves volontaires qui sont motivés par la réalisation d'un projet ambitieux, je vous propose de participer au  concours [Trophées des NSI](https://trophees-nsi.fr). Le thème du concours 2025 est _Arts & Informatique_ (mais vous pouvez déposer un projet en dehors du thème).
        * ✍️ Travail à faire pour  :
            * Pour le 27/11 :  faire tous les exercices de ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/995f-4713185)
            * DS sur les tableaux à une ou deux dimensions le lundi 2/12
            * Avancer son projet
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](./Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.
  

        ??? info "Trophées NSI"
            
            ![alt](./assets/trophees_nsi.png){:.center}

            * [Comment participer  ?](https://trophees-nsi.fr/participation)
            * [Le réglement](https://trophees-nsi.fr/reglement)

            ![alt](./assets/trophees_nsi_calendrier.png){:.center}

            !!! example "Video du projet vainqueur en 2024"

                 <iframe title="Électron libre - Trophées NSI 2024" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/7a61f6fa-f240-472a-ba98-1907d22b59ff" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>


    ??? done "Séance 20 : mercredi 27/11/2024"
        🧑‍🏫 Séance

        *  📓 Correction des exercices de ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/995f-4713185)
        * 🎯 Chapitre 5 : tableaux à deux dimensions / Traitement d'images

            * [Cours / TP en version pdf](./chapitre7/NSI-Images-Tableaux2d--2023V1.pdf)  
            * [Cours / TP en version notebook sur Capytale (avec corrigés)](https://capytale2.ac-paris.fr/web/c/7ad3-163223) => à partir de l'exercice 3 (QCM) jusqu'à l'exercice 6
        *  🎯 Chapitre 6 : représentation des entiers

             * [Cours / TP en version pdf](../chapitre8/Chapitre6-ReprésentationEntiers-2021V1.pdf)
             * [Cours / TP en notebook Python sur Capytale](https://capytale2.ac-paris.fr/web/c/2076-172112)
             * [Corrigé du cours / TP en version pdf](../chapitre8/Cours_Representation_Entiers_Correction.pdf)
             * [Corrigé des exercices de calcul sans Python](https://frederic-junier.gitlab.io/parc-nsi/chapitre8/Chapitre6-Repr%C3%A9sentationEntiers-2021V1.pdf)
             * Objectif du jour : exercice1 (de la liste des chiffres en décimal au nombre), exercice 2 (liste des chiffres d'un décimal), exercice 3 (compter en binaire).
        * Pour ceux qui sont en avance : [Projet Stéganographie](https://capytale2.ac-paris.fr/web/c/69bf-1019976)
        * Pour les élèves volontaires qui sont motivés par la réalisation d'un projet ambitieux, je vous propose de participer au  concours [Trophées des NSI](https://trophees-nsi.fr). Le thème du concours 2025 est _Arts & Informatique_ (mais vous pouvez déposer un projet en dehors du thème).
        * ✍️ Travail à faire pour  :
            * DS sur les tableaux à une ou deux dimensions le lundi 2/12
            * Terminer son projet
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](./Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.
  

        ??? info "Trophées NSI"
            
            ![alt](./assets/trophees_nsi.png){:.center}

            * [Comment participer  ?](https://trophees-nsi.fr/participation)
            * [Le réglement](https://trophees-nsi.fr/reglement)

            ![alt](./assets/trophees_nsi_calendrier.png){:.center}

            !!! example "Video du projet vainqueur en 2024"

                 <iframe title="Électron libre - Trophées NSI 2024" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/7a61f6fa-f240-472a-ba98-1907d22b59ff" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>



??? bug "Décembre"


    ??? done "Séance 21 : lundi 2/12/2024"
        🧑‍🏫 Séance

        *  🎯 Chapitre 6 : représentation des entiers

             * [Cours / TP en version pdf](../chapitre8/Chapitre6-ReprésentationEntiers-2021V1.pdf)
             * [Cours / TP en notebook Python sur Capytale](https://capytale2.ac-paris.fr/web/c/2076-172112)
             * [Corrigé du cours / TP en version pdf](../chapitre8/Cours_Representation_Entiers_Correction.pdf)
             * [Corrigé des exercices de calcul sans Python](https://frederic-junier.gitlab.io/parc-nsi/chapitre8/Chapitre6-Repr%C3%A9sentationEntiers-2021V1.pdf)
             * Objectif du jour : exercice 1 (de la liste des chiffres en décimal au nombre), exercice 2 (liste des chiffres d'un décimal), exercice 3 (compter en binaire), exercice 4 (conversion gloutonne ou par divisions successives d'un décimal en binaire)
        * Pour ceux qui sont en avance : [Projet Stéganographie](https://capytale2.ac-paris.fr/web/c/69bf-1019976)
        * Pour les élèves volontaires qui sont motivés par la réalisation d'un projet ambitieux, je vous propose de participer au  concours [Trophées des NSI](https://trophees-nsi.fr). Le thème du concours 2025 est _Arts & Informatique_ (mais vous pouvez déposer un projet en dehors du thème).
        * ✍️ Travail à faire pour  mercredi :
            * Faire les exercices de ce carnet Capytale <https://capytale2.ac-paris.fr/web/c/a8a7-4814377>
            * Terminer son projet
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](./Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.
  

        ??? info "Trophées NSI"
            
            ![alt](./assets/trophees_nsi.png){:.center}

            * [Comment participer  ?](https://trophees-nsi.fr/participation)
            * [Le réglement](https://trophees-nsi.fr/reglement)

            ![alt](./assets/trophees_nsi_calendrier.png){:.center}

            !!! example "Video du projet vainqueur en 2024"

                 <iframe title="Électron libre - Trophées NSI 2024" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/7a61f6fa-f240-472a-ba98-1907d22b59ff" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>



    ??? done "Séance 22 : mercredi 4/12/2024"
        🧑‍🏫 Séance

        *  💯  DS 5 sur les tableaux
        * Finalisation du projet
        * Pour ceux qui sont en avance : [Projet Stéganographie](https://capytale2.ac-paris.fr/web/c/69bf-1019976)
        * Pour les élèves volontaires qui sont motivés par la réalisation d'un projet ambitieux, je vous propose de participer au  concours [Trophées des NSI](https://trophees-nsi.fr). Le thème du concours 2025 est _Arts & Informatique_ (mais vous pouvez déposer un projet en dehors du thème).
        * ✍️ Travail à faire pour  mercredi :
            * Déposer le code de son projet (un fichier python par groupe nommé 'Nom1_Nom2_Nom3_Projet.py') dans cet [espace sur Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=40736)
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](./Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.
  

        ??? info "Trophées NSI"
            
            ![alt](./assets/trophees_nsi.png){:.center}

            * [Comment participer  ?](https://trophees-nsi.fr/participation)
            * [Le réglement](https://trophees-nsi.fr/reglement)

            ![alt](./assets/trophees_nsi_calendrier.png){:.center}

            !!! example "Video du projet vainqueur en 2024"

                 <iframe title="Électron libre - Trophées NSI 2024" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/7a61f6fa-f240-472a-ba98-1907d22b59ff" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>


    ??? done "Séance 23 : lundi 9/12/2024"
        🧑‍🏫 Séance

        *  💯  Retour du DS 5 sur les tableaux
        * 🗣️  Exposé : Louis Pouzin
        *  🎯 Chapitre 6 : représentation des entiers

             * [Cours / TP en version pdf](../chapitre8/Chapitre6-ReprésentationEntiers-2021V1.pdf)
             * [Cours / TP en notebook Python sur Capytale](https://capytale2.ac-paris.fr/web/c/2076-172112)
             * [Corrigé du cours / TP en version pdf](../chapitre8/Cours_Representation_Entiers_Correction.pdf)
             * [Corrigé des exercices de calcul sans Python](https://frederic-junier.gitlab.io/parc-nsi/chapitre8/Chapitre6-Repr%C3%A9sentationEntiers-2021V1.pdf)
             * Objectif du jour : exercice 3 (compter en binaire), exercice 4 (conversion gloutonne ou par divisions successives d'un décimal en binaire), exercice 5 juste le 1) (addition binaire), exercice 6 (compter en base 16), exerices 7 et 8 (jusqu'au 4) => Représentation des entiers relatifs en complément à 2
             * Correction des exercices de ce carnet Capytale <https://capytale2.ac-paris.fr/web/c/a8a7-4814377>
        * 🎯 Chapitre 7 : recherche séquentielle et dichotomique
             * [Cours version pdf](../chapitre10/cours/NSI-Recherches-2021.pdf)
             * [Exercices du cours, carnet Notebook Capytale](https://capytale2.ac-paris.fr/web/c/8c8a-224776)
        * Pour ceux qui sont en avance : [Projet Stéganographie](https://capytale2.ac-paris.fr/web/c/69bf-1019976)
        * Pour les élèves volontaires qui sont motivés par la réalisation d'un projet ambitieux, je vous propose de participer au  concours [Trophées des NSI](https://trophees-nsi.fr). Le thème du concours 2025 est _Arts & Informatique_ (mais vous pouvez déposer un projet en dehors du thème).
        * ✍️ Travail à faire pour  mercredi :
            * Faire les exercices de ce  [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/9ca0-4914390)
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](./Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.
  

        ??? info "Trophées NSI"
            
            ![alt](./assets/trophees_nsi.png){:.center}

            * [Comment participer  ?](https://trophees-nsi.fr/participation)
            * [Le réglement](https://trophees-nsi.fr/reglement)

            ![alt](./assets/trophees_nsi_calendrier.png){:.center}

            !!! example "Video du projet vainqueur en 2024"

                 <iframe title="Électron libre - Trophées NSI 2024" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/7a61f6fa-f240-472a-ba98-1907d22b59ff" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>


    ??? done "Séance 24 : mercredi 11/12/2024"
        🧑‍🏫 Séance

        * 🗣️  Exposé : Les machines mécaniques
        *  🎯 Chapitre 6 : représentation des entiers

            * [Cours / TP en version pdf](../chapitre8/Chapitre6-ReprésentationEntiers-2021V1.pdf)
            * [Cours / TP en notebook Python sur Capytale](https://capytale2.ac-paris.fr/web/c/2076-172112)
            * [Corrigé du cours / TP en version pdf](../chapitre8/Cours_Representation_Entiers_Correction.pdf)
            * [Corrigé des exercices de calcul sans Python](https://frederic-junier.gitlab.io/parc-nsi/chapitre8/Chapitre6-Repr%C3%A9sentationEntiers-2021V1.pdf)
            * Objectif du jour : exercice 4 (Codes des conversions gloutonne ou par divisions successives d'un décimal en binaire), exercices 7 et 8 (jusqu'au 4) => Représentation des entiers relatifs en complément à 2
            * Correction des exercices de ce carnet Capytale <https://capytale2.ac-paris.fr/web/c/a8a7-4814377>
            * Correction des exercices de cet autre carnet Capytale <https://capytale2.ac-paris.fr/web/c/9ca0-4914390>
        * 🎯 Chapitre 7 : recherche séquentielle et dichotomique
            * [Cours version pdf](../chapitre10/cours/NSI-Recherches-2021.pdf)
            * [Exercices du cours, carnet Notebook Capytale](https://capytale2.ac-paris.fr/web/c/8c8a-224776)
        * Pour ceux qui sont en avance : [Carnet Capytale sur la recherche en table](https://capytale2.ac-paris.fr/web/c/33f5-4958951)
        * Pour les élèves volontaires qui sont motivés par la réalisation d'un projet ambitieux, je vous propose de participer au  concours [Trophées des NSI](https://trophees-nsi.fr). Le thème du concours 2025 est _Arts & Informatique_ (mais vous pouvez déposer un projet en dehors du thème).
        * ✍️ Travail à faire pour  la semaine prochaine :
            * Lundi : Interrogation sur la représentation des entiers : conversion de la base dix verds la base 2 ou 16 et réciproquement, algorithme glouton et des divisions en cascade
            * Mercredi : oral de présentation du projet : entre 3 et 5 minutes par élève. Une organisation possible :
                * un premier élève présente l'introduction et une partie du code 
                * un second élève présente l'organisation du travail dans le groupe et une partie du code.
                * un troisième  élève présente la conclusion et un autre partie du code
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](./Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.


        ??? info "Trophées NSI"
            
            ![alt](./assets/trophees_nsi.png){:.center}

            * [Comment participer  ?](https://trophees-nsi.fr/participation)
            * [Le réglement](https://trophees-nsi.fr/reglement)

            ![alt](./assets/trophees_nsi_calendrier.png){:.center}

            !!! example "Video du projet vainqueur en 2024"

                <iframe title="Électron libre - Trophées NSI 2024" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/7a61f6fa-f240-472a-ba98-1907d22b59ff" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>


    ??? done "Séance 25 : lundi 16/12/2024"
        🧑‍🏫 Séance

        *  🗣️ : Présentation orale individuelle des projets
        * Pour les élèves volontaires qui sont motivés par la réalisation d'un projet ambitieux, je vous propose de participer au  concours [Trophées des NSI](https://trophees-nsi.fr). Le thème du concours 2025 est _Arts & Informatique_ (mais vous pouvez déposer un projet en dehors du thème).
        * ✍️ Travail à faire pour  la semaine prochaine :
            * Lundi : Interrogation sur la représentation des entiers : conversion de la base dix verds la base 2 ou 16 et réciproquement, algorithme glouton et des divisions en cascade
            * Mercredi : oral de présentation du projet : entre 3 et 5 minutes par élève. Une organisation possible :
                * un premier élève présente l'introduction et une partie du code 
                * un second élève présente l'organisation du travail dans le groupe et une partie du code.
                * un troisième  élève présente la conclusion et un autre partie du code
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](./Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.


        ??? info "Trophées NSI"
            
            ![alt](./assets/trophees_nsi.png){:.center}

            * [Comment participer  ?](https://trophees-nsi.fr/participation)
            * [Le réglement](https://trophees-nsi.fr/reglement)

            ![alt](./assets/trophees_nsi_calendrier.png){:.center}

            !!! example "Video du projet vainqueur en 2024"

                <iframe title="Électron libre - Trophées NSI 2024" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/7a61f6fa-f240-472a-ba98-1907d22b59ff" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>




    ??? done "Séance 26 : mercredi 18/12/2024"
        🧑‍🏫 Séance

        * 💯 : interrogation écrite sur la représentation des entiers.
        * 🎯 Chapitre 7 : recherche séquentielle et dichotomique
            * [Cours version pdf](../chapitre10/cours/NSI-Recherches-2021.pdf)
            * [Exercices du cours, carnet Notebook Capytale](https://capytale2.ac-paris.fr/web/c/8c8a-224776)
        * Pour les élèves volontaires qui sont motivés par la réalisation d'un projet ambitieux, je vous propose de participer au  concours [Trophées des NSI](https://trophees-nsi.fr). Le thème du concours 2025 est _Arts & Informatique_ (mais vous pouvez déposer un projet en dehors du thème).
        * ✍️ Travail à faire pour  la prochaine séance :
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](./Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.


        ??? info "Trophées NSI"
            
            ![alt](./assets/trophees_nsi.png){:.center}

            * [Comment participer  ?](https://trophees-nsi.fr/participation)
            * [Le réglement](https://trophees-nsi.fr/reglement)

            ![alt](./assets/trophees_nsi_calendrier.png){:.center}

            !!! example "Video du projet vainqueur en 2024"

                <iframe title="Électron libre - Trophées NSI 2024" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/7a61f6fa-f240-472a-ba98-1907d22b59ff" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>


??? bug "Janvier"

    ??? done "Séance 27 : lundi 06/01/2025"
        
        !!! example "🧑‍🏫 Séance"

            * Retour de l'interrogation écrite sur la représentation des entiers.
            * 🎯 Chapitre 7 : recherche séquentielle et dichotomique
                * Traiter d'abord sur papier les questions de cet [exercice sur la trace d'une recherche dichotomique](./chapitre10/exercice/trace-dichotomie.pdf)
                * Traiter ensuite les exercices de ce [Carnet Capytale sur la recherche en table](https://capytale2.ac-paris.fr/web/c/33f5-4958951)
                * Lire  ensuite le Point de cours 3 sur la recherche dichotomique dans un tableau trié et traiter les exercices 4 et 5 du cours :
                    * [Cours version pdf](../chapitre10/cours/NSI-Recherches-2021.pdf)
                    * [Exercices du cours, carnet Notebook Capytale](https://capytale2.ac-paris.fr/web/c/8c8a-224776)


        !!! tip  "👾 Mini projet Pyxel"
            * [Présentation du  mini projet](./pyxel.md) 

        ??? tip "✍️ Travail à la maison"
            
            * Traiter les exercices de [ce carnet Capytale](https://capytale2.ac-paris.fr/web/c/c931-5142254)
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](./Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.

        ??? info "Trophées NSI"
            
            ![alt](./assets/trophees_nsi.png){:.center}

            Pour les élèves volontaires qui sont motivés par la réalisation d'un projet ambitieux, je vous propose de participer au  concours [Trophées des NSI](https://trophees-nsi.fr). Le thème du concours 2025 est _Arts & Informatique_ (mais vous pouvez déposer un projet en dehors du thème).

            * [Comment participer  ?](https://trophees-nsi.fr/participation)
            * [Le réglement](https://trophees-nsi.fr/reglement)

            ![alt](./assets/trophees_nsi_calendrier.png){:.center}

            !!! example "Video du projet vainqueur en 2024"

                <iframe title="Électron libre - Trophées NSI 2024" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/7a61f6fa-f240-472a-ba98-1907d22b59ff" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>

        
    ??? done "Séance 28 : mercredi 08/01/2025"
        
        !!! example "🧑‍🏫 Séance"

            *  🗣️ : exposé oral sur Georges Boole
            * 🎯 Chapitre 7 : recherche séquentielle et dichotomique
                * Correction des exercices de ce [Carnet Capytale sur la recherche en table](https://capytale2.ac-paris.fr/web/c/33f5-4958951)
                * Dans le cours faire l'exercice 4  et l'exercice 5 questions 1 ) à 4) uniquement :
                    * [Cours version pdf](../chapitre10/cours/NSI-Recherches-2021.pdf)
                    * [Exercices du cours, carnet Notebook Capytale](https://capytale2.ac-paris.fr/web/c/8c8a-224776)

            * 🎯 Chapitre 8 : système d'exploitation
                * [TP Terminus](./chapitre9/terminus/terminus.md)
                * [Cours](./chapitre9/cours/cours_systeme.md)
                
        !!! tip  "👾 Mini projet Pyxel"
            * [Présentation du  mini projet](./pyxel.md) 

        ??? tip "✍️ Travail à la maison"
            
            * Traiter les exercices de [ce carnet Capytale](https://capytale2.ac-paris.fr/web/c/c931-5142254)
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](./Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.

        ??? info "Trophées NSI"
            
            ![alt](./assets/trophees_nsi.png){:.center}

            Pour les élèves volontaires qui sont motivés par la réalisation d'un projet ambitieux, je vous propose de participer au  concours [Trophées des NSI](https://trophees-nsi.fr). Le thème du concours 2025 est _Arts & Informatique_ (mais vous pouvez déposer un projet en dehors du thème).

            * [Comment participer  ?](https://trophees-nsi.fr/participation)
            * [Le réglement](https://trophees-nsi.fr/reglement)

            ![alt](./assets/trophees_nsi_calendrier.png){:.center}

            !!! example "Video du projet vainqueur en 2024"

                <iframe title="Électron libre - Trophées NSI 2024" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/7a61f6fa-f240-472a-ba98-1907d22b59ff" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>


    ??? done "Séance 29 : lundi 13/01/2025"
        
        !!! example "🧑‍🏫 Séance"

            *  🗣️ : exposé oral sur le langage C
            * 🎯 Chapitre 7 : recherche séquentielle et dichotomique
                * Correction des exercices de ce [ce carnet Capytale](https://capytale2.ac-paris.fr/web/c/c931-5142254)
                * On complète l'exercice 5 avec une synthèse sur la comparaison des coûts d'une recherche en table et d'une recherche dichotomique d'une valeur qui ne se trouve pas dans un tableau trié.  

            * 🎯 Chapitre 8 : système d'exploitation
                * [TP Terminus](./chapitre9/terminus/terminus.md)
                * [Cours](./chapitre9/cours/cours_systeme.md)
                * Répondre aux questions qui sont sur la page [https://frederic-junier.org/PremiereNSI2023/rituels/20231219/rituel.html](https://frederic-junier.org/PremiereNSI2023/rituels/20231219/rituel.html) à l'aide du cours
                
        !!! tip  "👾 Mini projet Pyxel"
            * [Présentation du  mini projet](./pyxel.md) 
            * Travail sur le mini-projet dans pyxelstudio sur Capytale

        ??? tip "✍️ Travail à la maison"
            
            * Traiter les exercices de [ce carnet Capytale](https://capytale2.ac-paris.fr/web/c/7916-5243178)
            * Lundi 20/01 : DS sur les chapitre Recherche Séquentielle et Dichotomique et Système d'exploitation : apprendre par coeur l'algorithme de recherche dichtomique dans un tableau trié présenté dans le cours, savoir répondre aux questions de la page [https://frederic-junier.org/PremiereNSI2023/rituels/20231219/rituel.html](https://frederic-junier.org/PremiereNSI2023/rituels/20231219/rituel.html) sur les Systèmes d'exploitation et savoir refaire les exercices des carnets Capytale du [6/01](https://capytale2.ac-paris.fr/web/c/c931-5142254) et du [15/01](https://capytale2.ac-paris.fr/web/c/7916-5243178)
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](./Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.

        ??? info "Trophées NSI"
            
            ![alt](./assets/trophees_nsi.png){:.center}

            Pour les élèves volontaires qui sont motivés par la réalisation d'un projet ambitieux, je vous propose de participer au  concours [Trophées des NSI](https://trophees-nsi.fr). Le thème du concours 2025 est _Arts & Informatique_ (mais vous pouvez déposer un projet en dehors du thème).

            * [Comment participer  ?](https://trophees-nsi.fr/participation)
            * [Le réglement](https://trophees-nsi.fr/reglement)

            ![alt](./assets/trophees_nsi_calendrier.png){:.center}

            !!! example "Video du projet vainqueur en 2024"

                <iframe title="Électron libre - Trophées NSI 2024" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/7a61f6fa-f240-472a-ba98-1907d22b59ff" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>


    ??? done "Séance 30 : mercredi 15/01/2025"
        
        !!! example "🧑‍🏫 Séance"

            *  🗣️ : exposé oral 
            * 🎯  Révisions pour le DS de lundi prochain
                * Correction des exercices de ce [ce carnet Capytale](https://capytale2.ac-paris.fr/web/c/c931-5142254)
                * Correction des exercices de ce [cet autre carnet Capytale](https://capytale2.ac-paris.fr/web/c/7916-5243178)
             
            * 🎯 Chapitre 9 : Codages des caractères
                * [Cours version pdf](./chapitre12/cours/Cours-NSI-Codage-Caracteres-2025.pdf)
                *  [Carner Capytale avec les exercices du cours Codage des caractères](https://capytale2.ac-paris.fr/web/c/9aac-5273384)
                
        !!! tip  "👾 Mini projet Pyxel"
            * [Présentation du  mini projet](./pyxel.md) 
            * Travail sur le mini-projet dans pyxelstudio sur Capytale

        ??? tip "✍️ Travail à la maison"

            * Lundi 20/01 : DS sur les chapitre Recherche Séquentielle et Dichotomique et Système d'exploitation : apprendre par coeur l'algorithme de recherche dichtomique dans un tableau trié présenté dans le cours, savoir répondre aux questions de la page [https://frederic-junier.org/PremiereNSI2023/rituels/20231219/rituel.html](https://frederic-junier.org/PremiereNSI2023/rituels/20231219/rituel.html) sur les Systèmes d'exploitation et savoir refaire les exercices des carnets Capytale du [6/01](https://capytale2.ac-paris.fr/web/c/c931-5142254) et du [15/01](https://capytale2.ac-paris.fr/web/c/7916-5243178)
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](./Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.

        ??? info "Trophées NSI"
            
            ![alt](./assets/trophees_nsi.png){:.center}

            Pour les élèves volontaires qui sont motivés par la réalisation d'un projet ambitieux, je vous propose de participer au  concours [Trophées des NSI](https://trophees-nsi.fr). Le thème du concours 2025 est _Arts & Informatique_ (mais vous pouvez déposer un projet en dehors du thème).

            * [Comment participer  ?](https://trophees-nsi.fr/participation)
            * [Le réglement](https://trophees-nsi.fr/reglement)

            ![alt](./assets/trophees_nsi_calendrier.png){:.center}

            !!! example "Video du projet vainqueur en 2024"

                <iframe title="Électron libre - Trophées NSI 2024" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/7a61f6fa-f240-472a-ba98-1907d22b59ff" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>



    ??? done "Séance 31 : lundi 20/01/2025"
        
        !!! example "🧑‍🏫 Séance"

            * 💯 : DS sur la recherche en table (séquentielle et dichotomique) et les systèmes d'exploitation
            * 🎯 Chapitre 9 : Codages des caractères
                * [Cours version pdf](./chapitre12/cours/Cours-NSI-Codage-Caracteres-2025.pdf)
                *  [Carner Capytale avec les exercices du cours Codage des caractères](https://capytale2.ac-paris.fr/web/c/9aac-5273384)
                
        !!! tip  "👾 Mini projet Pyxel"
            * [Présentation du  mini projet](./pyxel.md) 
            * Travail sur le mini-projet dans pyxelstudio sur Capytale

        ??? tip "✍️ Travail à la maison"
            * Avancer le projet Pyxel
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](./Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.

        ??? info "Trophées NSI"
            
            ![alt](./assets/trophees_nsi.png){:.center}

            Pour les élèves volontaires qui sont motivés par la réalisation d'un projet ambitieux, je vous propose de participer au  concours [Trophées des NSI](https://trophees-nsi.fr). Le thème du concours 2025 est _Arts & Informatique_ (mais vous pouvez déposer un projet en dehors du thème).

            * [Comment participer  ?](https://trophees-nsi.fr/participation)
            * [Le réglement](https://trophees-nsi.fr/reglement)

            ![alt](./assets/trophees_nsi_calendrier.png){:.center}

            !!! example "Video du projet vainqueur en 2024"

                <iframe title="Électron libre - Trophées NSI 2024" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/7a61f6fa-f240-472a-ba98-1907d22b59ff" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>


    ??? done "Séance 32 : mercredi 22/01/2025"
        
        !!! example "🧑‍🏫 Séance"
            
            *  🗣️ : exposé oral 
            * 🎯 Chapitre 9 : Codages des caractères
                * [Cours version pdf](./chapitre12/cours/Cours-NSI-Codage-Caracteres-2025.pdf)
                *  [Carner Capytale avec les exercices du cours Codage des caractères](https://capytale2.ac-paris.fr/web/c/9aac-5273384) => correction des exercices 1, 3,  4, 5, et 6 sur la manipulation de chaînes de caractères et des QCM 11 et 12
            * 🎯 Chapitre 10 : Algorithmes de tri
                * [Cours/TP version pdf](../chapitre11/1NSI-Cours-Tris-2021V1.pdf)
                * [Cours/TP sur Capytale](https://capytale2.ac-paris.fr/web/c/967c-255137) => Exercices 1, 4 et 5, ne pas faire les exercices 1 et 2
                * Pour les plus avancés : exercices de ce carnet Capytale [https://capytale2.ac-paris.fr/web/c/3028-5378482](https://capytale2.ac-paris.fr/web/c/3028-5378482) 
                
        !!! tip  "👾 Mini projet Pyxel"
            * [Présentation du  mini projet](./pyxel.md) 
            * Travail sur le mini-projet dans pyxelstudio sur Capytale

        ??? tip "✍️ Travail à la maison"
            * Traiter les exercices de ce carnet Capytale [https://capytale2.ac-paris.fr/web/c/3028-5378482](https://capytale2.ac-paris.fr/web/c/3028-5378482) 
            * Avancer le projet Pyxel
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](./Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.

        ??? info "Trophées NSI"
            
            ![alt](./assets/trophees_nsi.png){:.center}

            Pour les élèves volontaires qui sont motivés par la réalisation d'un projet ambitieux, je vous propose de participer au  concours [Trophées des NSI](https://trophees-nsi.fr). Le thème du concours 2025 est _Arts & Informatique_ (mais vous pouvez déposer un projet en dehors du thème).

            * [Comment participer  ?](https://trophees-nsi.fr/participation)
            * [Le réglement](https://trophees-nsi.fr/reglement)

            ![alt](./assets/trophees_nsi_calendrier.png){:.center}

            !!! example "Video du projet vainqueur en 2024"

                <iframe title="Électron libre - Trophées NSI 2024" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/7a61f6fa-f240-472a-ba98-1907d22b59ff" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>


    ??? done "Séance 33 : lundi 27/01/2025"
        
        !!! example "🧑‍🏫 Séance"
            
        
            * 🎯 Chapitre 10 : Algorithmes de tri
                * [Cours/TP version pdf](../chapitre11/1NSI-Cours-Tris-2021V1.pdf)
                * [Cours/TP sur Capytale](https://capytale2.ac-paris.fr/web/c/967c-255137) :
                    * Déterminer si un tableau est dans l'ordre croissant : exercice 1
                    * Tri par sélection (algo et implémentation) : exercices 4 et 5 et correction des exercices 1 et 2 du carnet [https://capytale2.ac-paris.fr/web/c/3028-5378482](https://capytale2.ac-paris.fr/web/c/3028-5378482) 
                    
                    
              
                
        !!! tip  "👾 Mini projet Pyxel"
            * [Présentation du  mini projet](./pyxel.md) 
            * Travail sur le mini-projet dans pyxelstudio sur Capytale

        ??? tip "✍️ Travail à la maison"
            * Finir les exercices de ce carnet Capytale [https://capytale2.ac-paris.fr/web/c/d09c-5431880](https://capytale2.ac-paris.fr/web/c/d09c-5431880)
            * Avancer le projet Pyxel
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](./Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.

        ??? info "Trophées NSI"
            
            ![alt](./assets/trophees_nsi.png){:.center}

            Pour les élèves volontaires qui sont motivés par la réalisation d'un projet ambitieux, je vous propose de participer au  concours [Trophées des NSI](https://trophees-nsi.fr). Le thème du concours 2025 est _Arts & Informatique_ (mais vous pouvez déposer un projet en dehors du thème).

            * [Comment participer  ?](https://trophees-nsi.fr/participation)
            * [Le réglement](https://trophees-nsi.fr/reglement)

            ![alt](./assets/trophees_nsi_calendrier.png){:.center}

            !!! example "Video du projet vainqueur en 2024"

                <iframe title="Électron libre - Trophées NSI 2024" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/7a61f6fa-f240-472a-ba98-1907d22b59ff" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>



    ??? done "Séance 34 : mercredi 29/01/2025"
        
        !!! example "🧑‍🏫 Séance"
            
            *  🗣️ : exposé oral 
            *  🏊  Entraînement : 
                *  Tri par sélection faire ce défi Code-Puzzle [https://www.codepuzzle.io/DVW5A/@](https://www.codepuzzle.io/DVW5A/@) en saisissant le code individuel transmis par mail
                * Vérifier si un tableau est dans l'ordre croissant  faire ce défi Code-Puzzle [https://www.codepuzzle.io/DCDG3/@](https://www.codepuzzle.io/DCDG3/@) en saisissant le code individuel transmis par mail
            * 🎯 Chapitre 10 : Algorithmes de tri
                * [Cours/TP version pdf](../chapitre11/1NSI-Cours-Tris-2021V1.pdf)
                * [Cours/TP sur Capytale](https://capytale2.ac-paris.fr/web/c/967c-255137) :
                    * Tri par sélection : 
                        * (terminaison, correction, complexité) : exercices 6 et 8  du cours
                    * Tri par insertion :  
                        * exercice 9 du cours
                        * correction des exercices 1 et 2 de ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/d09c-5431880)
                        * exercice 10  du cours (complexité et comparaison avec le tri par insertion)
                        * Défis Code-puzzle (saisir le code individuel transmis par mail):
                            * Fonction d'insertion dans un tableau trié  [https://www.codepuzzle.io/DWPEL/@](https://www.codepuzzle.io/DWPEL/@)
                            * Tri par insertion [https://www.codepuzzle.io/DRG7N/@](https://www.codepuzzle.io/DRG7N/@)
                            * Tri par bulles [https://www.codepuzzle.io/DQXNP/@](https://www.codepuzzle.io/DQXNP/@)
              
                
        !!! tip  "👾 Mini projet Pyxel"
            * [Présentation du  mini projet](./pyxel.md) 
            * Travail sur le mini-projet dans pyxelstudio sur Capytale

        ??? tip "✍️ Travail à la maison"
            * Avancer le projet Pyxel
            * Exercices 1 et 2 de ce  [carnet Capytale](https://capytale2.ac-paris.fr/web/c/d09c-5431880) sur le tri par insertion
            * Faire les défis Code-puzzle (saisir le code individuel transmis par mail):
                * Fonction d'insertion dans un tableau trié  [https://www.codepuzzle.io/DWPEL/@](https://www.codepuzzle.io/DWPEL/@)
                * Tri par insertion [https://www.codepuzzle.io/DRG7N/@](https://www.codepuzzle.io/DRG7N/@)
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](./Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.

        ??? info "Trophées NSI"
            
            ![alt](./assets/trophees_nsi.png){:.center}

            Pour les élèves volontaires qui sont motivés par la réalisation d'un projet ambitieux, je vous propose de participer au  concours [Trophées des NSI](https://trophees-nsi.fr). Le thème du concours 2025 est _Arts & Informatique_ (mais vous pouvez déposer un projet en dehors du thème).

            * [Comment participer  ?](https://trophees-nsi.fr/participation)
            * [Le réglement](https://trophees-nsi.fr/reglement)

            ![alt](./assets/trophees_nsi_calendrier.png){:.center}

            !!! example "Video du projet vainqueur en 2024"

                <iframe title="Électron libre - Trophées NSI 2024" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/7a61f6fa-f240-472a-ba98-1907d22b59ff" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>

??? bug "Février"


    ??? done "Séance 35 : lundi 03/02/2025"
        
        !!! example "🧑‍🏫 Séance"
            
            *  🗣️ : exposé oral 
            * 📓   Correction d'exercices : 
                * Exercices 1 et 2 de ce  [carnet Capytale](https://capytale2.ac-paris.fr/web/c/d09c-5431880) sur le tri par insertion
                * Défis Code-puzzle (saisir le code individuel transmis par mail):
                    * Fonction d'insertion dans un tableau trié  [https://www.codepuzzle.io/DWPEL/@](https://www.codepuzzle.io/DWPEL/@)
                    * Tri par insertion [https://www.codepuzzle.io/DRG7N/@](https://www.codepuzzle.io/DRG7N/@)
            * 🎯 Chapitre 10 : Algorithmes de tri : aujoud'hui le tri par insertion
                * [Cours/TP version pdf](../chapitre11/1NSI-Cours-Tris-2021V1.pdf)
                * [Cours/TP sur Capytale](https://capytale2.ac-paris.fr/web/c/967c-255137) :
                    * Tri par insertion :  
                        * exercice 9 du cours (implémentation) et exercice 10 (complexité)
                        *  exercice 10 identifier les algorithmes de  tri par insertion ou par sélection dans n autre langage
                       
            * 🏊 Entrainement : exercices divers (compression RLE, fusion de deux listes triées ...) dans ce [carnet Cpaytale](https://capytale2.ac-paris.fr/web/c/e15e-5532768)
                
        !!! tip  "👾 Mini projet Pyxel"
            * [Présentation du  mini projet](./pyxel.md) 
            * Travail sur le mini-projet dans pyxelstudio sur Capytale

        ??? tip "✍️ Travail à la maison"
            * Avancer le projet Pyxel
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](./Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.

        ??? info "Trophées NSI"
            
            ![alt](./assets/trophees_nsi.png){:.center}

            Pour les élèves volontaires qui sont motivés par la réalisation d'un projet ambitieux, je vous propose de participer au  concours [Trophées des NSI](https://trophees-nsi.fr). Le thème du concours 2025 est _Arts & Informatique_ (mais vous pouvez déposer un projet en dehors du thème).

            * [Comment participer  ?](https://trophees-nsi.fr/participation)
            * [Le réglement](https://trophees-nsi.fr/reglement)

            ![alt](./assets/trophees_nsi_calendrier.png){:.center}

            !!! example "Video du projet vainqueur en 2024"

                <iframe title="Électron libre - Trophées NSI 2024" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/7a61f6fa-f240-472a-ba98-1907d22b59ff" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>



    ??? done "Séance 36 : mercredi 06/02/2025"
        
        !!! example "🧑‍🏫 Séance"
            
            *  🗣️ : exposé oral 
            * 🎯 Chapitre 11 : tuples 
                * [Cours](../chapitre15/Cours/puplets-cours-reduit-.pdf) 
                * TP : [énoncé](../chapitre15/TP/NSI-Puplets-TP-2020V1.pdf) et [carnet Capytale avec exercices et corrigés](https://capytale2.ac-paris.fr/web/c/8361-1269409)
                       
            * 🏊 Entrainement : exercices divers (compression RLE, fusion de deux listes triées ...) dans ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/e15e-5532768)
                
        !!! tip  "👾 Mini projet Pyxel"
            * [Présentation du  mini projet](./pyxel.md) 
            * Travail sur le mini-projet dans pyxelstudio sur Capytale

        ??? tip "✍️ Travail à la maison"
            * Avancer le projet Pyxel
            * Devoir sur les algorithmes de tri : mercredi 12/02
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](./Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.

        ??? info "Trophées NSI"
            
            ![alt](./assets/trophees_nsi.png){:.center}

            Pour les élèves volontaires qui sont motivés par la réalisation d'un projet ambitieux, je vous propose de participer au  concours [Trophées des NSI](https://trophees-nsi.fr). Le thème du concours 2025 est _Arts & Informatique_ (mais vous pouvez déposer un projet en dehors du thème).

            * [Comment participer  ?](https://trophees-nsi.fr/participation)
            * [Le réglement](https://trophees-nsi.fr/reglement)

            ![alt](./assets/trophees_nsi_calendrier.png){:.center}

            !!! example "Video du projet vainqueur en 2024"

                <iframe title="Électron libre - Trophées NSI 2024" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/7a61f6fa-f240-472a-ba98-1907d22b59ff" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>


    ??? done "Séance 37 : lundi 10/02/2025"
        
        !!! example "🧑‍🏫 Séance"
            
            *  🗣️ : exposé oral 
            * 🎯 Chapitre 11 : tuples 
                * [Cours](../chapitre15/Cours/puplets-cours-reduit-.pdf) 
                * TP : [énoncé](../chapitre15/TP/NSI-Puplets-TP-2020V1.pdf) et [carnet Capytale avec exercices et corrigés](https://capytale2.ac-paris.fr/web/c/8361-1269409)
                * [Carnet Capytale d'exercices supplémentaires sur les tuples](https://capytale2.ac-paris.fr/web/c/2952-5634348) 
                       
            * 🏊 Entrainement : exercices divers (compression RLE, fusion de deux listes triées ...) dans ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/e15e-5532768)
                
        !!! tip  "👾 Mini projet Pyxel"
            * [Présentation du  mini projet](./pyxel.md) 
            * Travail sur le mini-projet dans pyxelstudio sur Capytale

        ??? tip "✍️ Travail à la maison"
            * Avancer le projet Pyxel
            * Devoir sur les algorithmes de tri : mercredi 12/02
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](./Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.

        ??? info "Trophées NSI"
            
            ![alt](./assets/trophees_nsi.png){:.center}

            Pour les élèves volontaires qui sont motivés par la réalisation d'un projet ambitieux, je vous propose de participer au  concours [Trophées des NSI](https://trophees-nsi.fr). Le thème du concours 2025 est _Arts & Informatique_ (mais vous pouvez déposer un projet en dehors du thème).

            * [Comment participer  ?](https://trophees-nsi.fr/participation)
            * [Le réglement](https://trophees-nsi.fr/reglement)

            ![alt](./assets/trophees_nsi_calendrier.png){:.center}

            !!! example "Video du projet vainqueur en 2024"

                <iframe title="Électron libre - Trophées NSI 2024" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/7a61f6fa-f240-472a-ba98-1907d22b59ff" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>


    ??? done "Séance 38 : mercredi 12/02/2025"
        
        !!! example "🧑‍🏫 Séance"
            
            *  💯  DS 8 : algorithmes de tri
            *  🗣️ : exposé oral 
            * 🎯 Chapitre 12 : architecture de Von Neumann:
                * [Vidéo d'introduction : Space War](https://nuage03.apps.education.fr/index.php/s/MxsjqBJBxkAztr6)
                * [Synthèse de cours version markdown](./chapitre17/synthese_von_neumann_corrected.md)
                * [Synthèse de cours version pdf](./chapitre17/synthese_von_neumann_corrected.pdf)
                * [Cours version longue pdf](./chapitre17/NSI-ArchitectureVonNeumann-Cours2022.pdf)
                * __Premier temps :__ histoire de l'informatique et visionnage de quelques séquences de la video <https://www.lumni.fr/video/une-histoire-de-l-architecture-des-ordinateurs> avec application aux exercices 1 et 2
                * __Second temps (cours) :__ présentation de l'architecture de Von Neummann, de la hiérarchie des mémoires, des différents niveaux de langage entre l'homme et la machine : dans le cours version longue  faire l'exercice 3 mais pas les 4 et 5. ([exercice 5 pour les curieux](./chapitre17/ressources/exercice5.zip)) 
                       
                
        !!! tip  "👾 Mini projet Pyxel"
            * [Présentation du  mini projet](./pyxel.md) 
            * Travail sur le mini-projet dans pyxelstudio sur Capytale

        ??? tip "✍️ Travail à la maison"
            * Avancer le projet Pyxel
            * Devoir sur les algorithmes de tri : mercredi 12/02
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](./Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.

        ??? info "Trophées NSI"
            
            ![alt](./assets/trophees_nsi.png){:.center}

            Pour les élèves volontaires qui sont motivés par la réalisation d'un projet ambitieux, je vous propose de participer au  concours [Trophées des NSI](https://trophees-nsi.fr). Le thème du concours 2025 est _Arts & Informatique_ (mais vous pouvez déposer un projet en dehors du thème).

            * [Comment participer  ?](https://trophees-nsi.fr/participation)
            * [Le réglement](https://trophees-nsi.fr/reglement)

            ![alt](./assets/trophees_nsi_calendrier.png){:.center}

            !!! example "Video du projet vainqueur en 2024"

                <iframe title="Électron libre - Trophées NSI 2024" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/7a61f6fa-f240-472a-ba98-1907d22b59ff" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>


    ??? done "Séance 39 : lundi 17/02/2025"
        
        !!! example "🧑‍🏫 Séance"
            
            *  💯 Retour du DS 8 : algorithmes de tri
            * 🎯 Chapitre 12 : architecture de Von Neumann:
                * [Synthèse de cours version markdown](./chapitre17/synthese_von_neumann_corrected.md)
                * [Synthèse de cours version pdf](./chapitre17/synthese_von_neumann_corrected.pdf)
                * __Premier temps :__ histoire de l'informatique et visionnage de quelques séquences de la video <https://www.lumni.fr/video/une-histoire-de-l-architecture-des-ordinateurs> avec application aux exercices 1 et 2
                * __Deuxième temps (pratique) :__  Manipulation de l'[Emulateur Aqua](http://www.peterhigginson.co.uk/AQA/)
                    * [Fiche de TD avec les exercices](./chapitre17/TD-Assembleur-Aqua.pdf)
                    * Ressources : [Programmes en langage d'assemblage Aqua](./chapitre17/ressources/programmes_assembleur_eleves.zip)
                    * [Correction video de l'exercice 2](https://nuage03.apps.education.fr/index.php/s/PZmdbwns2wW2AJ8)
                    * [Correction video de l'exercice 5](https://nuage03.apps.education.fr/index.php/s/zwPMMFXXm6gzbCt)
                       
                
        !!! tip  "👾 Mini projet Pyxel"
            * [Présentation du  mini projet](./pyxel.md) 
            * Travail sur le mini-projet dans pyxelstudio sur Capytale

        ??? tip "✍️ Travail à la maison"
            * Avancer le projet Pyxel
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](./Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.

        ??? info "Trophées NSI"
            
            ![alt](./assets/trophees_nsi.png){:.center}

            Pour les élèves volontaires qui sont motivés par la réalisation d'un projet ambitieux, je vous propose de participer au  concours [Trophées des NSI](https://trophees-nsi.fr). Le thème du concours 2025 est _Arts & Informatique_ (mais vous pouvez déposer un projet en dehors du thème).

            * [Comment participer  ?](https://trophees-nsi.fr/participation)
            * [Le réglement](https://trophees-nsi.fr/reglement)

            ![alt](./assets/trophees_nsi_calendrier.png){:.center}

            !!! example "Video du projet vainqueur en 2024"

                <iframe title="Électron libre - Trophées NSI 2024" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/7a61f6fa-f240-472a-ba98-1907d22b59ff" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>



    ??? done "Séance 40 : mercredi 19/02/2025"
        
        !!! example "🧑‍🏫 Séance"
            
            * 🎯 Chapitre 13 : dictionnaires
                * [Cours 2025 version markdown](./chapitre18/Cours/cours_dictionnaires_2025.md)
                * [Cours 2025 version pdf](./chapitre18/Cours/cours_dictionnaires_2025.pdf)
                * [Carnet Capytale : exercices sur les dictionnaires 2025 série 1](https://capytale2.ac-paris.fr/web/c/c630-5700750) => correction des exercices 4, 5, 6 et 7
                * [QCM de révisions sur les dictionnaires](https://fjunier.forge.apps.education.fr/qcm-nsi-snt-maths/01_NSI_Premiere/Dictionnaires/Dictionnaires/)
                
                
        !!! tip  "👾 Mini projet Pyxel"
            * [Présentation du  mini projet](./pyxel.md) 
            * Travail sur le mini-projet dans pyxelstudio sur Capytale

        ??? tip "✍️ Travail à la maison"
            * Rendre le code du projet pyxel (partage sur Capytale) le lundi 17/03
            * Pour le lundi 10/03 : finir les six premiers exercices du  [Carnet Capytale : exercices sur les dictionnaires 2025 série 1](https://capytale2.ac-paris.fr/web/c/c630-5700750)
            * Pour le mercredi 12/03 : petite interrogation sur l'Architecture de Von Neumann et les Dictionnaires 
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](./Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.

        ??? info "Trophées NSI"
            
            ![alt](./assets/trophees_nsi.png){:.center}

            Pour les élèves volontaires qui sont motivés par la réalisation d'un projet ambitieux, je vous propose de participer au  concours [Trophées des NSI](https://trophees-nsi.fr). Le thème du concours 2025 est _Arts & Informatique_ (mais vous pouvez déposer un projet en dehors du thème).

            * [Comment participer  ?](https://trophees-nsi.fr/participation)
            * [Le réglement](https://trophees-nsi.fr/reglement)

            ![alt](./assets/trophees_nsi_calendrier.png){:.center}

            !!! example "Video du projet vainqueur en 2024"

                <iframe title="Électron libre - Trophées NSI 2024" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/7a61f6fa-f240-472a-ba98-1907d22b59ff" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>



??? bug "Mars"


    ??? done "Séance 41 : lundi 10/03/2025"
        
        !!! example "🧑‍🏫 Séance"
            *  🗣️ : exposé oral 
            * 🎯 Chapitre 13 : dictionnaires
                * [Cours 2025 version markdown](./chapitre18/Cours/cours_dictionnaires_2025.md)
                * [Cours 2025 version pdf](./chapitre18/Cours/cours_dictionnaires_2025.pdf)
                * [Carnet Capytale : exercices sur les dictionnaires 2025 série 1](https://capytale2.ac-paris.fr/web/c/c630-5700750)
                * [QCM sur les dictionnaires](https://fjunier.forge.apps.education.fr/qcm-nsi-snt-maths/01_NSI_Premiere/Dictionnaires/Dictionnaires/)
            * 🎯 Chapitre 14 Complexité :
                * [Cours version pdf](./chapitre14/cours_complexite_2025.pdf)
                * [Carnet Capytale avec corrigés des exercices](https://capytale2.ac-paris.fr/web/c/894d-5867775)
                
                
        !!! tip  "👾 Mini projet Pyxel"
            * [Présentation du  mini projet](./pyxel.md) 
            * Travail sur le mini-projet dans pyxelstudio sur Capytale

        ??? tip "✍️ Travail à la maison"
            * Rendre le code du projet pyxel (partage sur Capytale) le lundi 17/03
            * Pour le mercredi 12/03 :  interrogation sur l'Architecture de Von Neumann et les Dictionnaires 
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](./Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.

        ??? info "Trophées NSI"
            
            ![alt](./assets/trophees_nsi.png){:.center}

            Pour les élèves volontaires qui sont motivés par la réalisation d'un projet ambitieux, je vous propose de participer au  concours [Trophées des NSI](https://trophees-nsi.fr). Le thème du concours 2025 est _Arts & Informatique_ (mais vous pouvez déposer un projet en dehors du thème).

            * [Comment participer  ?](https://trophees-nsi.fr/participation)
            * [Le réglement](https://trophees-nsi.fr/reglement)

            ![alt](./assets/trophees_nsi_calendrier.png){:.center}

            !!! example "Video du projet vainqueur en 2024"

                <iframe title="Électron libre - Trophées NSI 2024" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/7a61f6fa-f240-472a-ba98-1907d22b59ff" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>




    ??? done "Séance 42 : mercredi 12/03/2025"
        
        !!! example "🧑‍🏫 Séance"
            *  💯 : DS 9 : architecture de Von Neummann et Dictionnaires
            *  🗣️ : exposé oral 
            * 🎯 Chapitre 14 Complexité :
                * [Cours version pdf](./chapitre14/cours_complexite_2025.pdf)
                * [Carnet Capytale avec corrigés des exercices](https://capytale2.ac-paris.fr/web/c/894d-5867775)
                
                
        !!! tip  "👾 Mini projet Pyxel"
            * [Présentation du  mini projet](./pyxel.md) 
            * Travail sur le mini-projet dans pyxelstudio sur Capytale

        ??? tip "✍️ Travail à la maison"
            * Rendre le code du projet pyxel (partage sur Capytale) le lundi 17/03
            * Préparation d'exposé, voir le [fichier des exposés oraux](https://nuage03.apps.education.fr/index.php/s/kNFN8w5D9czBQcs){:target="_blank"} : possibilité de choisir un des sujets proposés [ici](./Culture/expos%C3%A9s.md){:target="_blank"} ou de proposer un sujet à faire valider par l'enseignant.

        ??? info "Trophées NSI"
            
            ![alt](./assets/trophees_nsi.png){:.center}

            Pour les élèves volontaires qui sont motivés par la réalisation d'un projet ambitieux, je vous propose de participer au  concours [Trophées des NSI](https://trophees-nsi.fr). Le thème du concours 2025 est _Arts & Informatique_ (mais vous pouvez déposer un projet en dehors du thème).

            * [Comment participer  ?](https://trophees-nsi.fr/participation)
            * [Le réglement](https://trophees-nsi.fr/reglement)

            ![alt](./assets/trophees_nsi_calendrier.png){:.center}

            !!! example "Video du projet vainqueur en 2024"

                <iframe title="Électron libre - Trophées NSI 2024" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/7a61f6fa-f240-472a-ba98-1907d22b59ff" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>
