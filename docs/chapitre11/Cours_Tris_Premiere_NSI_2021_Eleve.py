#!/usr/bin/env python
# coding: utf-8


#%% Exercice 1


def tri_croissant(tab):
    """
    Détermine si le tableau d'entiers tab
    est trié dans l'ordre croissant
tab
    Parameters:
        tab : tableau d'entiers
        Précondition : len(tab) > 0

    Returns:  booléen
    """
    assert len(tab) > 0
    #à compléter
   

#Tests unitaires
def test_tri_croissant():
    """Tests unitaires pour tri_croissant"""
    assert tri_croissant([20])
    assert tri_croissant([1, 1])
    assert tri_croissant([-2, 0, 0, 1])
    assert not tri_croissant([2, 1, 3])
    assert not tri_croissant([2, 1])
    assert not tri_croissant([2, 2, 1])
    print("Tests unitaires réussis")

# décommenter pour exécuter les tests unitaires
#test_tri_croissant()


#%% Exercice 2

import random

def mystere(tab):
    n = len(tab)
    for i in range(n):
        r = random.randint(i, n)
        tmp = tab[r]
        tab[r] = tab[i]
        tab[i] = tmp



#%% Exercice 3



def compare_tab(tab1, tab2):
    """
    Détermine si les tableaux d'entiers tab1
    et tab2 triés et de même taille
    comportent les memes éléments
    
    Parameters: tab1, tab 2: tableaux d'entiers

    Returns: booléen
    """
    #à compléter
    
    
#jeu de tests unitaires pour une fonction de comparaison de tableaux
def test_comparaison_tableaux(fonc_compar):
    assert fonc_compar([1, 2, 3], [1, 2, 3])
    assert fonc_compar([1, 2, 2], [1, 2, 2])
    assert fonc_compar([10], [10])
    assert not fonc_compar([1, 2, 3], [1, 2, 4])
    assert not fonc_compar([10], [-4])
    assert not fonc_compar([1, 2, 3, 3], [1, 2, 3, 4])
    print("Tests unitaires réussis")
    
#application  des test unitaires à compare_tab (à décommenter)
#test_comparaison_tableaux(compare_tab)



#%% Exercice 5


def recherche_index_min(t, i):
    """
    Renvoie l'index de  la première occurence 
    de min(t[i:len(t)])
    
    Parameters
    ----------
    t : tableau d'entiers
    k : int
    Précondition :   0 <= a  < len(t)
    Returns:
    -------
    int
    """
    assert 0 <= i < len(t)
    #à compléter

    
def echange(t, a, b):
    """
    Echange t[a] et t[b]

    Parameters
    ----------
    t : tableau d'entiers
    a : int
    b :  int
    Précondition :  0 <= a < len(t) et 0 <= b < len(t)

    Returns
    -------
    None.

    """
    #à compléter

    
def tri_selection(t):
    """
    Trie en place par sélection un tableau d'entiers

    Parameters : 
    t : tableau d'entiers

    Returns :  None.
    """
    n = len(t)
    #boucle externe
    for i in range(0, n):
        #invariant : t[0:i] trié dans l'ordre croissant
        #boucle interne de recherche de l'index du minimum dans t[i:n]
        imin = recherche_index_min(t, i) 
        #t[imin] = min(t[i:n])
        #echange de t[i] et t[imin]
        echange(t, i, imin) 
        
        
import random

def test_tri(tri_en_place):    
    """
    Jeu de tests unitaires pour une fonction de tri en place
    par comparaison avec la fonction de bibliothèque sorted

    Parameters
    ----------
    tri_en_place : fonction de tri  en place
 
    Returns
    -------
    None.

    """
    taille = [10**k for k in range(3)]
    for n in taille:
        #test sur tableau constant
        for const in [-n, 0, n]:
            t = [const for _ in range(n)]
            t_tri = sorted(t)
            tri_en_place(t)
            assert t == t_tri        
        #tests sur tableaux déjà triés dans l'ordre croissant
        for debut in [-(n//2), 0, n//2]:
            t = [e for e in range(debut, debut + n)]
            t_tri = sorted(t)
            tri_en_place(t)
            assert t == t_tri
        #tests sur tableaux déjà triés dans l'ordre décroissant
        for debut in [-(n//2), 0, n//2]:
            t = [e for e in range(debut + n, debut - 1, -1)]
            t_tri = sorted(t)
            tri_en_place(t)
            assert t == t_tri
        #tests sur des tableaux aléatoires
        for _ in range(50):
            t = [random.randint(-n, n) for _ in range(n)]
            t_tri = sorted(t)
            tri_en_place(t)
            assert t == t_tri
    print(f"jeu de tests { test_tri.__name__} réussi pour {tri_en_place.__name__}")
    
    
#tests unitaires pour le tri par sélection (à décommenter)
#test_tri(tri_selection)  


def tri_selection2(t):
    """
    Trie en place par sélection un tableau d'entiers
    Sans utiliser de fonctions externes

    Parameters : 
    t : tableau d'entiers

    Returns :  None.
    """
    n = len(t)
    #boucle externe
    for i in range(0, n):
        #invariant : t[0:i] trié
        #à compléter

#tests unitaires pour le tri par sélection (à décommenter)
#test_tri(tri_selection2)  


#%% Exercice 8


import time, random

def temps_echantillon(tri_en_place, taille_tab, nb_essais):
    """
    Mesure le temps d'exécution de la fonction de tri tri_en_place 
    sur un échantillon de nb_essais tableaux d'entiers aléatoires
    de taille taille_tab (entiers entre -1000 et 1000)

    Parameters
    ----------
    tri_en_place : fonction de tri en place
    taille_tab : int, taille de tableaux
    nb_essais : int, taille d'échantillons

    Returns
    -------
    None.

    """
    total = 0
    for _ in range(nb_essais):
        tab_alea = [random.randint(-1000, 1000) for _ in range(taille_tab)]
        debut = time.perf_counter()
        tri_en_place(tab_alea)
        total = total + (time.perf_counter() - debut)
    return total

def test_doubling_ratio(tri_en_place, nb_essais):
    """
    Description à compléter

    Parameters
    ----------
    tri_en_place : fonction de tri en place d'un tableau
    nb_essais : int

    Returns
    -------
    None.

    """
    # dans le notebook on limite à 5 itérations
    n = 128
    for _ in range(5): 
          precedent = temps_echantillon(tri_en_place, n // 2,  nb_essais) 
          courant = temps_echantillon(tri_en_place, n,  nb_essais) 
          ratio = courant / precedent
          print(n, ratio)
          n = n * 2



#%% Exercice 9



def insertion(t, i):
    """
    Insère t[i] à sa place dans t[:i] trié dans l'ordre croissant

    Parameters
    ----------
    t : tableau d'entiers, précondition len(t) > i
    i : int
    Returns
    -------
    None.

    """
    assert len(t) > i #précondition
    #à compléter
    
    
def tri_insertion(t):
    """
    Trie en place par insertion un tableau d'entiers

    Parameters : 
    t : tableau d'entiers

    Returns :  None.
    """
    n = len(t)
    #boucle externe
    for i in range(1, n):
        #invariant de boucle : t[0:i] trié
        #boucle interne d'insertion de t[i] à sa place dans t[0:i]
        insertion(t, i) 
        
#tests unitaires pour le tri par insertion (à décommenter)
#test_tri(tri_insertion)  



def insertion2(t, i):
    """
    Insère t[i] à sa place dans t[:i] trié dans l'ordre croissant

    Parameters[
    ----------
    t : tableau d'entiers, précondition len(t) > i
    i : int
    Returns
    -------
    None.

    """
    assert len(t) > i #précondition    
    tmp = t[i]
    j = i
    # à compléter


def tri_insertion2(t):
    """
    Trie en place par insertion un tableau d'entiers

    Parameters : 
    t : tableau d'entiers

    Returns :  None.
    """
    n = len(t)
    #boucle externe
    for i in range(1, n):
        #invariant de boucle : t[0:i] trié
        #boucle interne d'insertion de t[i] à sa place dans t[0:i]
        #à compléter
        
#tests unitaires pour le tri par insertion (à décommenter)
#test_tri(tri_insertion2)  


#%% Exercice 10

#%% Exercice 11


#%% Exercice 12




def tri_bulle1(tab):
    n = len(tab)
    for i in range(n):
        for j in range(n - 1):
            if tab[j] > tab[j + 1]:
                echange(tab, j, j + 1)   

#tests unitaires de tri_bulle1 en place, à décommenter
test_tri(tri_bulle1)




def tri_bulle3(tab):   
    n, i, permutation  = len(tab), 0, True
    # à compléter
    
    

#%% Exercice 13





def tri_comptage(tab, binf, bsup):
    """
    Tri en place par comptage le tableau 
    tab tel que pour tout 0 <= k < len(tab)
    on a  binf <= tab[k] <= bsup
    
    Parameters
    ----------
    tab : tableau d'entiers
    binf, bsup : int et int
    Returns
    -------
    None.
    
    """
    histo = [0 for _ in range(bsup - binf + 1)]
    #on remplit l'histogramme
    #à compléter
    #on reconstitue le tableau
    #à compléter
    
            
def test_tri_comptage(tri_en_place):    
    """
    Jeu de tests unitaires pour une fonction de tri comptage
    de signature tri(tab, binf, bsup)
    Comparaison avec la fonction de tri de bibliothèque sorted

    Parameters
    ----------
    tri : fonction de tri comptage
    
    Returns
    -------
    None.

    """
    
    for n in [10**k for k in range(3)]:
        for binf, bsup in  [(-100, -1), (-100, 100), (0, 100)]:      
            #test sur tableau constant
            for const in [random.randint(binf, bsup) for _ in range(5)]:
                t = [const for _ in range(n)]
                t_tri = sorted(t)
                tri_en_place(t, binf, bsup)
                assert t == t_tri   
            # sur tableaux déjà triés dans l'ordre croissant
            t = [e for e in range(binf, bsup + 1)]
            t_tri = sorted(t)
            tri_en_place(t, binf, bsup)
            assert t == t_tri  
            #tests sur tableaux déjà triés dans l'ordre décroissant
            t = [e for e in range(bsup, binf - 1, -1)]
            t_tri = sorted(t)
            tri_en_place(t, binf, bsup)
            assert t == t_tri  
            #tests sur des tableaux aléatoires
            for _ in range(50):
                t = [random.randint(binf, bsup) for _ in range(n)]
                t_tri = sorted(t)
                tri_en_place(t, binf, bsup)
                assert t == t_tri  
    print(f"jeu de tests { test_tri_comptage.__name__} réussi pour {tri_en_place.__name__}")

# tests unitaires pour tri_comptage, à décommenter
#test_tri_comptage(tri_comptage)



#%%

def tri_comptage_mille(tab):
    """
    Pour mesurer le temps d'exécution avec test_doubling_ratio
    qui manipule des tableaux dont les valeurs sont entre -1000
    et 1000
    """
    return tri_comptage(tab, -1000, 1000)

