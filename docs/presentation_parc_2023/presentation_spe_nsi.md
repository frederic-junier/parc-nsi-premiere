---
presentation:
  width: 800
  height: 600
  enableSpeakerNotes: true
  # Display a presentation progress bar
  progress: true
  # Display the page number of the current slide
  slideNumber: true
  # Number of milliseconds between automatically proceeding to the
  # next slide, disabled when set to 0, this value can be overwritten
  # by using a data-autoslide attribute on your slides
  autoSlide: 15000
  # Stop auto-sliding after user input
  autoSlideStoppable: true
---
<!-- slide id="0" -->

# Spécialité NSI 

![alt](images/logo-parc.png)

<!-- slide id="01" -->

# NSI ?
 
La spécialité  Numérique et Sciences Informatiques  permet :

* l'appropriation des concepts et méthodes, scientifiques et techniques, qui fondent l'informatique
* de former les élèves à la pratique d'une démarche scientifique.

<!-- slide vertical=true id="20" -->

## 7 thèmes du programme

!["7 thèmes, source : Lycée Laennec - Pont L'Abbé"](images/7themes2.png "autre")

<!-- slide vertical=true id="21" -->

## Compétences du programme

* Analyser et modéliser un problème en termes de flux et de  traitement d'informations ;
* Décomposer un problème en sous-problèmes;
* Concevoir des solutions algorithmiques ;
* Traduire un algorithme dans un langage de programmation,

<!-- slide vertical=true id="22" -->

## Compétences transversales 


* faire preuve d'autonomie, d'initiative et de créativité
* présenter un problème ou sa solution
* coopérer au sein d'une équipe
* rechercher une information et partager des ressources
* faire un usage responsable et critique de l'information


<!-- slide id="40" -->

# Choisir NSI

<!-- slide vertical=true id="41" -->


## Pourquoi  NSI ?

La transformation numérique va impacter tous les domaines de la société et  engendrer un besoin immense de personnes ayant des compétences informatiques.

Choisir NSI, c'est choisir une orientation avec une compétence numérique forte 
pour l'appliquer dans un autre domaine (cinéma, histoire, services etc.) ou 
pour développer l'informatique de demain.

<!-- slide vertical=true id="42" -->

##  Avec quelles spécialités ?

NSI peut venir en   complément de  toute autre spécialité car les compétences informatiques sont un atout dans tous les domaines, des sciences exactes aux sciences sociales. 

<!-- slide vertical=true id="43" -->
## Avec quelles spécialités ?

Si on veut se préparer  à des études d'informatique, alors  il est  conseillé d'associer  NSI avec la spécialité Mathématiques en première et la spécialité Mathématiques ou  l'option Mathématiques complémentaires en terminale.

<!-- slide vertical=true id="44" -->
## Pour  les geeks  ?

NSI s'adresse à tous les profils, la connaissance technique n'est pas du tout un prérequis. Si vous avez une appétence pour le raisonnement et la résolution de problèmes, alors NSI est fait pour vous. En programmation on reprend tout depuis le début en langage Python et on s'intéresse plus à la logique qu'à la technicité. 

<!-- slide vertical=true id="45" -->
## Pour  les filles aussi ?

La première thèse soutenue en informatique en 1965 l'a été par une femme, Marthe
Keller ! 

La transformation numérique de la société change notre façon de vivre en profondeur. Pourquoi laisser cette
transformation aux mains des garçons uniquement ?

<!-- slide id="50" -->

# Les débouchés



<!-- slide vertical=true id="51" -->

## Un marché de l'emploi en plein expansion

NSI permet de s'orienter vers l'industrie du numérique et tous les  métiers d'avenir qui changent le monde dans tous les domaines : la médecine, la banque, le marketing, la sécurité des données, les transports, etc.  Le secteur du numérique emploie plus de 500.000 salariés, crée plus de 20% des emplois nets en France et il ya plus de 80.000 emplois vacants.

<!-- slide vertical=true id="52" -->

!["Les débouchés, source : Delphine Chavanon"](images/orientation_post_nsi_reduce.png "les débouchés")

Infographie :  _Delphine Chavanon_

<!-- slide vertical=true id="08" -->
## Découvrir les métiers du numérique

De nombreux sites existent :

* Métiers des mathématiques et de l'informatique : [Zoom ONISEP](https://www.onisep.fr/publications/Zoom/les-metiers-des-mathematiques-statistique-et-informatique)
* Métiers de la cybersécurité :[demainspecialistecyber](https://www.demainspecialistecyber.fr/)
* Métiers du numérique sur [Lumni](https://www.lumni.fr/programme/dans-mon-job-les-metiers-du-numerique)


<!-- slide id="60" -->


# Évaluation 


<!-- slide vertical=true id="61" -->

## Modalités

* Interrogations écrites classiques
* Compte rendus de projet : écrit, oral, capsule video
* TP notés

<!-- slide vertical=true id="62" -->

## Abandon de la spécialité en fin de première

Si l'élève abandonne la spécialité NSI en fin de première, sa moyenne annuelle est conservée pour le bac avec un coefficient 8.

<!-- slide vertical=true id="63" -->

## Épreuves terminales 


Si l'élève a conservé la spécialité NSI en terminale, (voir [eduscol](https://eduscol.education.fr/cid144156/nsi-bac-2021.html)) il passe deux épreuves terminales : une épreuve écrite de trois heures trente minutes et une épreuve sur machine d'une heure.

Le coefficient est de 16.


<!-- slide vertical=true id="63" -->


![alt](images/coef_bac_2024.jpeg)



<!-- slide  id="70" -->


# NSI au lycée du Parc ?


* Professeurs référents : M.Duclosson et M.Junier
* [contact mail](mailto:frederic.junier@le-parc.ent.auvergnerhonealpes.fr)
* Sites web :
    *  [Première NSI](https://frederic-junier.org/NSI/premiere/)
    *  [Terminale  NSI](https://fjunier.forge.aeif.fr/terminale_nsi/)
