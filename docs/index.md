---
title:  Accueil
layout: parc
---



!!! note "La spécialité NSI au lycée du Parc"
    Bienvenue sur le site de ressources de la classe de première NSI du [lycée du Parc à Lyon](https://lyceeduparc.fr/ldp/), l'enseignement est assuré par deux professeurs de mathématiques Pierre Duclosson et Frédéric Junier, titulaires du [DIU "Enseigner l'informatique au lycée"](https://diu-eil.univ-lyon1.fr/) et assurant également depuis plusieurs années des cours d'informatique pour tous en CPGE.

    ??? bug "Présentation de la spécialité NSI"
        Voici notre [présentation de la spécialité NSI](presentation_parc_2023/presentation_spe_nsi.html){:target="_blank"}.

        <iframe src="presentation_parc_2023/presentation_spe_nsi.html"  allow="autoplay; fullscreen" allowfullscreen="true" frameborder="0" width="700" height="600" style="margin: 0 auto;"></iframe>

        


        !!! example "It's a Unix system, I know this !"

            J'ai récemment revu le film [Jurassic Park](https://fr.wikipedia.org/wiki/Jurassic_Park) qui date de 1993 et j'ai été frappé par l'importance de l'informatique  utilisée par l'homme pour contrôler la nature (par le séquençage génétique ou le contrôle automatique des barrières des enclos et du parcours du parc).L'informaticien (caricature de geek) est une personne corrompue, qui va désactiver le système d'informations et mettre en danger les autres protagonistes par seul appât du gain. Face à  cette incarnation masculine éthiquement défaillante, va émerger dans l'extrait ci-dessous, une figure féminine, qui va utiliser ses compétences en informatique à des fins plus positives. 

            Dans cet extrait les héros du film (2 paléontologues et 2 enfants), poursuivis par deux vélociraptors, se sont réfugiés dans la salle de commande du parc où trône l'ordinateur central abandonné par l'informaticien corrompu. Pendant que les adultes s'efforcent de bloquer la porte, la jeune Alex, gràce à sa connaissance de l'organisation des fichiers sur un système [Unix](https://fr.wikipedia.org/wiki/Unix), parvient à  accéder au fichier permettant de verrouiller la porte. En première NSI, vous apprendrez l'organisation du système de  fichiers UNIX et bien d'autres choses qui  comme à Alex vous donneront le pouvoir d'être acteur du monde numérique qui nous entoure sans subir son emprise !

            <iframe src="https://ladigitale.dev/digiview/inc/video.php?videoId=dxIPcbmo1_U&vignette=https://i.ytimg.com/vi/dxIPcbmo1_U/hqdefault.jpg&debut=0&fin=85&largeur=16&hauteur=9" allow="autoplay; fullscreen" frameborder="0" width="700" height="394"></iframe>

            L'explorateur de fichiers en 3D présenté dans le film est un véritable logiciel [File System Navigator](https://www.siliconbunny.com/fsn-the-irix-3d-file-system-tool-from-jurassic-park/), développé  en 1992 par Silicon Graphics, entreprise pionnière de l'accélération 3D, sur des stations de travail  opérant le système d'exploitation  [IRIX](https://en.wikipedia.org/wiki/IRIX) basé sur [Unix](https://fr.wikipedia.org/wiki/Unix).

            ![alt](https://www.siliconbunny.com/wp-content/uploads/2008/12/3060c037-069f-4715-a01e-c30e53e505a2.jpg){:.center}

            > __Source :__ <https://www.siliconbunny.com/fsn-the-irix-3d-file-system-tool-from-jurassic-park/>

        !!! example "Métiers du numérique sur Lumni"

            ![alt](https://medias.lumni.fr/dv8EHaxKr_Z7nl6J6gqiDbLB-sM/236x162/filters:focal(296460x-92130:296459x0):quality(95):max_bytes(120000)/6717c80dddb4efe36200857a){:.center}

            [Dans mon job sur Lumni](https://www.lumni.fr/programme/dans-mon-job-les-metiers-du-numerique) : les métiers du numérique. De jeunes professionnels du numérique te disent tout sur leur métier ! Comment travaille un développeur mobile ? Quelle formation suivre pour devenir data scientist ? Quel salaire gagne un scrum master ou un product manager ? Toutes les réponses à tes questions sur les métiers du numérique.

            👉  <https://www.lumni.fr/programme/dans-mon-job-les-metiers-du-numerique>

    !!! note "Programme"

        * Le  [programme officiel](Programme/PPL18_Numerique-sciences-informatiques_SPE_1eGen_1025707.pdf)
        * [Carte mentale du programme](Programme/PremièreNSI.jpg)

    !!! tip "Progression 2024/2025"
        La progression 2024/2025 est [ici](./seances.md){:target="_blank"}.


    !!! tip "Manuel"
        Nous utilisons le manuel Hachette NSI version papier  de référence `978-2-01-786630-5`, accessible en ligne sur <https://mesmanuels.fr/acces-libre/3813624>.

    
    !!! example "Exemple de réalisation"
        L'image `gif` ci-dessous présente  différentes étapes du déroulement d'un algorithme de rotation d'images inspiré d'un travail présenté par Laurent Abbal du lycée français de Tokyo. Le  programme assez court peut être réalisé par un élève  de terminale (récursivité, approche _diviser pour régner_).

        L'image source représente l'oeuvre _Matsuri Yatai Dragon_ du peintre japonais [Hokusai](https://en.wikipedia.org/wiki/en:Hokusai). Elle est dans le domaine public et disponible sur [https://commons.wikimedia.org](https://commons.wikimedia.org/wiki/File:Hokusai_Dragon.jpg).

        ![Dragon](assets/rotation-dragon-2.gif "dragon-hokusai"){:.center}

<!--
??? Progression


    * [Chapitre 1 : Constructions de base d'un langage de programmation](chapitre1.md)

    * [Chapitre 2 : HTML/CSS](chapitre2.md)
    * [Chapitre 3 : fonctions, spécification et mise au point, portée d'une variable](chapitre3.md)
    * [Chapitre 4 : tableaux à une dimension](chapitre4.md)
    * [Chapitre 5 : tableaux à deux dimensions / Images](chapitre5.md)
    * [Chapitre 6 : représentation des entiers](chapitre8.md)
    * [Chapitre 7 : système d'exploitation et ligne de commandes](chapitre9.md)
    * [Chapitre 8 : recherche séquentielle ou dichotomique](chapitre10.md)
    * [Chapitre 9 : algorithmes de tri](chapitre11.md)
    * [Chapitre 10 : codage des caractères](chapitre12.md)
    * [Chapitre 11 : circuits logiques et fonctions booléennes](chapitre13.md)
    * [Chapitre 12 : complexité](chapitre14.md)
    * [Chapitre 13 : p-uplets](chapitre15.md)
    * [Chapitre 14 : architecture de Von Neumann](chapitre17.md)
    * [Chapitre 15 : les dictionnaires](chapitre18.md)
    * [Chapitre 16 : flottants](chapitre16.md)
    * [Chapitre 17: correction d'algorithmes](chapitre17.md)
    * [Chapitre 18 : traitement de données en table](chapitre19.md)
    * [Chapitre 19 : Algorithmes gloutons](chapitre24.md) 
    * [Chapitre 19 : Interaction Homme Machine sur le Web partie 1, protocole HTTP et formulaires](chapitre21.md)
    * [Chapitre 21 : Interactions dans le navigateur, Javascript](chapitre23.md)  
    * [Chapitre 22 : Réseau TCP/IP](chapitre25.md)  
    * [Chapitre 23 : Algorithme de classification des K plus proches voisins](chapitre22.md) 
 
-->
