---
title:  Chapitre 18,  dictionnaires
layout: parc
---




Cours de Frédéric Junier.

## Nouveau cours

* [Nouveau cours version markdown](chapitre18/Cours/cours_dictionnaires_2025.md)
* [Nouveau cours version pdf](chapitre18/Cours/cours_dictionnaires_2025.pdf)

## Ancien TP 

* [TP Scrabble (introduction ludique)](chapitre18/scrabble.md)
* [TP dictionnaires](chapitre18/TP/TP-Dictionnaires-1.pdf)

## Ancien cours 

* [Ancien cours version pdf](chapitre18/Cours/dictionnaires-cours-.pdf)
* [Ancien cours version markdown](chapitre18/Cours/dictionnaires-cours-git.md)
* [Exemples et exercices de l'ancien cours, carnet Capytale](https://capytale2.ac-paris.fr/web/c/8394-398180)

## Cartes mentales 

* [Carte mentale sur les dictionnaires au format HTML](./chapitre18/Cours/Cartes_mentales/carte_mentale_dictionnaires.html)

* [Carte mentale sur les dictionnaires au format pdf](./chapitre18/Cours/Cartes_mentales/carte_mentale_dictionnaires.pdf)
  
## Videos

??? video

    * Présentation par David Latouche

    <iframe width="948" height="533" src="https://www.youtube.com/embed/ON_Ac-MbHik" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    * [Video Lumni de Charles Poulmaire](https://www.lumni.fr/video/les-dictionnaires)