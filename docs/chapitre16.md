---
title:  Chapitre 14  flottants
layout: parc
---




Cours de Frédéric Junier.





## Cours 

* [Cours version pdf](chapitre16/1NSI-Cours-Flottants-2021V1.pdf)
* [Carnet Capytale  des exemples du cours, avec corrigés](https://capytale2.ac-paris.fr/web/c/78a9-386046)
* [Conversion d'un décimal en flottant sur 32 bits](chapitre16/ressources/conversion-decimal-formatiee754.pdf)



<video width="320" height="240" controls>
<source src="https://nuage03.apps.education.fr/index.php/s/BzssTryiYTd4Kzq/download"type="video/mp4">
</video> 
