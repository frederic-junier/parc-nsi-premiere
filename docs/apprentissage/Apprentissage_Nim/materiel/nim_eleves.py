import random

# Constantes
GAGNANT = 1
PERDANT = -1
MACHINE = 1
JOUEUR = 0


#%% Partie 2 : stratégie

def precalcul_valeur_position(pos_max):   
    """
    Prend en paramètre un nombre de positions maximum  de stylos pos_max
    Renvoie un dictionnaie associant à chaque position de stylo entre 0 et pos_max - 1
    son état : GAGNANT (1)  ou PERDANT   (-1)  
    """
    valeur_position = {pos_max-1: GAGNANT, pos_max-2: GAGNANT}    
    for p in range(pos_max - 3, -1 , -1):
        # à compléter
        ...
    return valeur_position


def test_precalcul_valeur_position():
    assert  precalcul_valeur_position(15) == {14: 1,
 13: 1,
 12: -1,
 11: 1,
 10: 1,
 9: -1,
 8: 1,
 7: 1,
 6: -1,
 5: 1,
 4: 1,
 3: -1,
 2: 1,
 1: 1,
 0: -1}
    
def deplacement(pos, valeur_position):
    """
    pos une position entre 0 et  pos_max = len(valeur_position) - 1
    valeur_position un dictionnaire renvoyé par precalcul_valeur_position(pos_max)
    Renvoie pour la position pos le nombre de stylos pris (1 ou 2)
    selon la valeur de valeur_position[pos]
    """
    # à compléter
    ...
    
    

#%% Partie 3 : apprentissage

def partie(machine, joueur_parfait):
    pos_max = len(joueur_parfait)
    if random.randint(0, 1) == 0:
        joueur = MACHINE
    else:
        joueur = JOUEUR
    choix_machine = dict()
    pos = 0
    # à compléter
    ...
    # RENFORCEMENT
    if joueur == MACHINE: # de tous les choix si victoire
        # on renforce les choix faits pour chaque position (récompense)
        for p in choix_machine:
            machine[p][choix_machine[p] - 1] += 1
    else:   # de tous les choix alternatifs sinon
        # on renforce les choix opposés pour chaque position (punition)
        for p in choix_machine:
            machine[p][3- choix_machine[p] - 1] += 1
    return machine
        
        
def apprentissage_renforcement(pos_max, n_exp):
    joueur_parfait = precalcul_valeur_position(pos_max)
    machine  = [[1, 1] for _ in range(pos_max)]
    for _ in range(n_exp):
        machine = partie(machine, joueur_parfait)
    return machine
        
