import random

# Constantes
GAGNANT = 1
PERDANT = -1
ORDI = 1
PARFAIT = 0


#%% Partie 2 : stratégie

def precalcul_valeur_position(pos_max):   
    """
    Prend en paramètre un nombre de positions maximum  de stylos pos_max
    Renvoie un liste telle que valeur_position[p] est la valeur GAGNANT ou PERDANT
    associée à la positinon p
    """
    valeur_position = []   
    for p in range(pos_max):
        # à compléter
        ...
    return valeur_position


def test_precalcul_valeur_position():
    assert  precalcul_valeur_position(15) == [-1, 1, 1, -1, 1, 1, -1, 1, 1]
    
    
def deplacement(pos):
    """
    pos une position entre 0 et  pos_max = len(valeur_position) - 1
    Renvoie pour la position pos le nombre de stylos pris (1 ou 2)
    """
    # à compléter
    ...
    
    

#%% Partie 3 : apprentissage

def partie(machine):
    pos_max = len(machine)
    if random.randint(0, 1) == 0:
        joueur = ORDI
    else:
        joueur = PARFAIT
    choix_machine = dict()
    pos = 0
    # à compléter
    ...
    # RENFORCEMENT
    if joueur == PARFAIT: # de tous les choix si victoire
        for p in choix_machine:
            machine[p][str(choix_machine[p])] += 1
    else:   # de tous les choix alternatifs sinon
        for p in choix_machine:
            machine[p][str(3 - choix_machine[p])] += 1
    return machine
        
        
def apprentissage_renforcement(pos_max, n_exp):
    machine  = [{'1': 1, '2': 1} for _ in range(pos_max)]
    for _ in range(n_exp):
        machine = partie(machine)
    return machine
        
#print(apprentissage_renforcement(9, 100000))
