import random

# Constantes
GAGNANT = 1
PERDANT = -1
ORDI = 1
PARFAIT = 0


#%% Partie 2 : stratégie

def precalcul_valeur_position(pos_max):   
    """
    Prend en paramètre un nombre de positions maximum  de stylos pos_max
    Renvoie un liste telle que valeur_position[p] est la valeur GAGNANT ou PERDANT
    associée à la positinon p
    """
    valeur_position = []   
    for p in range(pos_max):
        # à compléter
        ...
        # BEGIN CUT
        if p % 3 == 0:
            valeur_position.append(PERDANT)
        else:
            valeur_position.append(GAGNANT)
        # END CUT
    return valeur_position


def test_precalcul_valeur_position():
    assert  precalcul_valeur_position(15) == [-1, 1, 1, -1, 1, 1, -1, 1, 1]
    
    
def deplacement(pos):
    """
    pos une position entre 0 et  pos_max = len(valeur_position) - 1
    Renvoie pour la position pos le nombre de stylos pris (1 ou 2)
    """
    # à compléter
    ...
    # BEGIN CUT
    if pos % 3 == 0: # position perdante
        return random.randint(1, 2)
    else: # position gagnante
        return 3 - (pos % 3)
    # END CUT
    
    

#%% Partie 3 : apprentissage

def partie(machine):
    pos_max = len(machine)
    if random.randint(0, 1) == 0:
        joueur = ORDI
    else:
        joueur = PARFAIT
    choix_machine = dict()
    pos = 0
    # à compléter
    ...
    # BEGIN CUT
    while pos < pos_max:
        if joueur == ORDI: # si joueur ORDI
            nb1 = machine[pos]['1']
            nb2 = machine[pos]['2']
            alea = random.randint(1, nb1 + nb2)
            if alea <= nb1:
                dpos = 1
            else:
                dpos  = 2
            choix_machine[pos] = dpos
            pos = pos + dpos            
        else:
            pos = pos + deplacement(pos)       
        joueur = 1 - joueur 
    # END CUT
    # RENFORCEMENT
    if joueur == PARFAIT: # de tous les choix si victoire
        for p in choix_machine:
            machine[p][str(choix_machine[p])] += 1
    else:   # de tous les choix alternatifs sinon
        for p in choix_machine:
            machine[p][str(3 - choix_machine[p])] += 1
    return machine
        
        
def apprentissage_renforcement(pos_max, n_exp):
    machine  = [{'1': 1, '2': 1} for _ in range(pos_max)]
    for _ in range(n_exp):
        machine = partie(machine)
    return machine
        
#print(apprentissage_renforcement(9, 100000))
