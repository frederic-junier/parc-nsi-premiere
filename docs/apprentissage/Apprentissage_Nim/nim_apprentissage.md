---
title: TP Jeu de Nim et apprentissage machine par renforcement
subtitle: NSI
author: Lycée du Parc
numbersections: true
fontsize: 11pt
geometry:
  - top=20mm
  - left=20mm
  - right=20mm
  - heightrounded
---


Deux joueurs s'affrontent  dans  le jeu suivant qui appartient à la famille des [jeux de Nim](https://fr.wikipedia.org/wiki/Jeux_de_Nim) :

* on dispose 9 stylos disposés parallèlement comme ci-dessous :

![image](jeu_nim.png)

* à tour de rôle chaque joueur peut prendre 1 ou 2 stylos
* celui qui ne peut plus prendre de stylos a perdu


# Découvrir le jeu


Effectuer quelques parties avec son voisin.

# Établir une stratégie


1. Nommons Joueur A et Joueur B les deux joueurs. Déterminer le gagnant dans chaque cas s'il reste $n$  stylos  avec $1 \leqslant n \leqslant 9$  pour le joueur A. 


Pour la suite de l'exercice on fixe comme contrainte d'enlever les stylos dans un ordre  de parcours fixé : par exemple de haut en bas.


2. On repère chaque stylo par sa position dans l'ordre de parcours fixé   de $0$ pour le premier  à $9-1=8$ = `pos_max` pour le dernier.  Compléter la fonction `precalcul_valeur_position` qui prend en argument une valeur de `pos_max` (nombre de stylos - 1) et renvoie une liste  associant à chaque position  (index dans  la liste) entre $0$ et `pos_max - 1` son état :

* `GAGNANT` si le joueur peut remporter le jeu à partir de cette position s'il choisit bien le nombre de stylos à enlever :  enlever les derniers stylos ou amener son adversaire dans une position `PERDANT`
* `PERDANT` si le joueur perd nécessairement à partir de cette position si son adversaire joue bien c'est-à-dire qu'il amène forcément son adversaire dans une position gagnante quel que soit son choix de stylos à enlever.

~~~python
import random

GAGNANT = 1
PERDANT = -1
ORDI = 1
JOUEUR = 0

def precalcul_valeur_position(pos_max):   
    """
    Prend en paramètre un nombre de positions maximum  de stylos pos_max
    Renvoie un liste telle que valeur_position[p] est la valeur GAGNANT ou PERDANT
    associée à la positinon p
    """
    valeur_position = []   
    for p in range(pos_max):
        # à compléter
        ...
    return valeur_position
~~~

3. Compléter la fonction `deplacement` qui renvoie pour chaque position `pos` le déplacement entre 1 et 2 qu'il est préférable d'effectuer par rapport à l'état `GAGNANT` ou `PERDANT` de la position.


~~~python
def deplacement(pos):
    """
    pos une position entre 0 et  pos_max = len(valeur_position) - 1
    Renvoie pour la position pos le nombre de stylos pris (1 ou 2)
    """
    # à compléter
    ...
~~~


# La machine apprend la stratégie


Pour faire apprendre la stratégie précédente par la machine, on va procéder ainsi :

* au départ on associe à chaque position entre $0$ et `pos_max - 1` (0 et 8 dans le jeu initial) un dictionnaire `{'1': 1, '2': 1}`.  `machine[pos]['1']` représente un nombre de boules marquées '1' et `machine[pos]['2']`   boules marquées '2'. `machine` est donc une liste de dictionnaires.

~~~python
machine  = [[1, 1] for _ in range(pos_max)]
~~~

* Ensuite on fait disputer une partie avec  la machine contre le joueur parfait  :
    *  Au tour de la machine, si le prochain stylo est en position `pos`, la   machine choisit aléatoirement d'enlever 1 ou 2 stylos en tirant au hasard une boule dans une urne  contenant  `machine[pos]['1']` boules '1' et `machine[pos]['2']`  boules '2'. Si une boule '1' est tirée, elle choisit d'enlever 1 stylo, sinon 2
    *  Au tour du joueur parfait, il joue en prenant  `deplacement(pos)` stylos.
    *  En fin de partie, si le gagnant est la machine on augmente de 1 le nombre de boules du type choisi pour les différentes urnes où elle a pioché. C'est la *récompense*. Sinon on augmente de 1 le nombre de boules du type opposé  à celui  choisi  pour les différentes urnes où elle a pioché. C'est la *punition*.

* On répète un grand nombre de parties, on peut constater qu'à la fin la composition des urnes de `machine` détermine des choix cohérents avec ceux des valeurs `GAGNANT` ou `PERDANT` des différentes positions qui ont été déterminées dans la section précédente.


On donne un squelette de code à compléter :

~~~python
import random

def apprentissage_renforcement(pos_max, n_exp):
    machine  = [{'1': 1, '2': 1} for _ in range(pos_max)]
    for _ in range(n_exp):
        machine = partie(machine)
    return machine

def partie(machine):
    pos_max = len(machine)
    if random.randint(0, 1) == 0:
        joueur = ORDI
    else:
        joueur = PARFAIT
    choix_machine = dict()
    pos = 0
    # à compléter
    ...
    # RENFORCEMENT
    if joueur == PARFAIT: # de tous les choix si victoire
        for p in choix_machine:
            machine[p][str(choix_machine[p])] += 1
    else:   # de tous les choix alternatifs sinon
        for p in choix_machine:
            machine[p][str(3 - choix_machine[p])] += 1
    return machine
~~~