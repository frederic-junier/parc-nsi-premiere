def compare(chaine1, chaine2):
    """
    Compare deux chaînes de caractères et renvoie la plus petite
    dans l'ordre lexicographique

    Paramètres :
    chaine1 et chaine2 sont deux chaînes de caractères composés
    uniquement de caractères minuscules.

    Retour: la chaine la plus petite dans l'ordre lexicographique
    """
    n1 = len(chaine1)
    n2 = len(chaine2)
    if n1 >= n2:
        lmin = n2
        cmin = chaine2
    else:
        lmin = n1
        cmin = chaine1
    for k in range(lmin):
        if chaine1[k] < chaine2[k]:
            return chaine1
        elif chaine1[k] > chaine2[k]:
            return chaine2
    return cmin