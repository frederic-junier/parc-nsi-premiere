---
title: Thème Chaînes de caractères
---

{% include 'abbreviations.md' %}

!!! tip "Exercice"

    _Sujet inspiré de <https://prologin.org/train/2003/qualification/comparer_des_chaines>_

    Compléter la fonction `compare(chaine1, chaine2)` sans utiliser l'opérateur de comparaison directement avec les chaînes de caractère `chaine1` et `chaine2`, seule la comparaison de caractères est autorisée.

    ```python
    def compare(chaine1, chaine2):
        """
        Compare deux chaînes de caractères et renvoie la plus petite
        dans l'ordre lexicographique

        Paramètres :
        chaine1 et chaine2 sont deux chaînes de caractères composés
        uniquement de caractères minuscules.

        Retour: la chaine la plus petite dans l'ordre lexicographique
        """
        # à compléter

    # test 1
    assert compare('informatique', 'information') == 'information'

    # test 2
    assert compare('pyramide', 'python') == 'pyramide'

    # test 3
    assert compare('courge', 'courgette') == 'courge'
    ```

{{IDE("exo1_chaines")}}
