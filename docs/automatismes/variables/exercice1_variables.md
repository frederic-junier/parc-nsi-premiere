---
title: Thème Variables
---

{% include 'abbreviations.md' %}


!!! tip "Exercice"

    Compléter le programme ci-dessous pour qu'il échange  les valeurs des variables `a` et `b` (de type `int`). Les instructions ajoutées ne doivent 
    pas dépendre des valeurs d'initialisation des variables `a` et `b`.

    ~~~python
    a = 842
    b = 843
    #compléter

    ~~~


{{IDE("exo1_variables")}} 


[Correction](exo1_variables_corr.py)
