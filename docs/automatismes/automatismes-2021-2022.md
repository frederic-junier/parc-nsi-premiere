---
title:  Automatismes
layout: parc
---


[Fichier Capytale pour compléter les automatismes](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/150d-189184)


# Thème A : types de base


???+ {{ automatisme() }}
    === "Énoncé"
        Représenter en binaire le nombre d'écriture décimale 49.

    === "Solution"
        110001

??? {{ automatisme() }}
    === "Énoncé"
        Représenter en base dix, le nombre dont l'écriture en base deux est `1010110` puis le nombre dont l'écriture en base 16 est 'A4'.

    === "Solution"
        `1010110` s'écrit $64+16+4+2=86$ en base dix.

??? {{ automatisme() }}
    === "Énoncé"
        Déterminer le successeur  des entiers dont l'écriture en base deux est :

        * 111
        * 10011
        * 10111

    === "Solution"

        * 111 a pour successeur 1000
        * 10011 a pour successeur     10100
        * 10111 a pour successeur 11000


??? {{ automatisme() }}
    === "Énoncé"
        On considère les nombres dont l'écriture en base 16 (en hexadécimal) sont de la forme suivante : un 1 suivi de 0 en nombre quelconque, comme 1, 10, 100, 1000 etc ... Tous ces nombres sont exactement :

        1. les puissances de 2
        2. les puissances de 8
        3. les puissances de 10
        4. les puissances de 16

    === "Solution"
        Ce sont les puissances de 16.

??? {{ automatisme() }}

    === "Énoncé"
        Le résultat de l'addition en binaire $101001 + 101$ est égal au nombre binaire :

        1. `101102`
        2. `010101`
        3. `101110`
        4. `110000`

    === "Solution"
        Le résultat de l'addition en binaire $101001 + 101$ est égal à   $101110$

??? {{ automatisme() }}

    === "Énoncé"
        Quelle est la représentation hexadécimale de l'entier dont la représentation binaire s'écrit : 0100  1001 1101 0011 ?

        1. 18899
        2. 3D94
        3. 49D3
        4. 93A3
    
    === "Solution"
        On remplace chaque bloc de 4 bits par sa représentation sur un chiffre en base 16 et on obtient
        `49D3`

??? {{ automatisme() }}

    === "Énoncé"


        Le codage de César transforme un message en changeant chaque lettre en la décalant dans l'alphabet.

        Par exemple, avec un décalage de 3, le A se transforme en D, le B en E, ..., le X en A, le Y en B et le Z en C. Les autres caractères ('!','?'...) ne sont pas codés.

        La fonction `position_alphabet` ci-dessous prend en paramètre un caractère lettre et renvoie la position de lettre dans la chaîne de caractères ALPHABET s'il s'y trouve et -1 sinon.

        La fonction `cesar` prend en paramètre une chaîne de caractères message et un nombre entier `decalage` et renvoie le nouveau message codé avec le codage de César utilisant le décalage `decalage`.

        ```python
        ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

        def position_alphabet(lettre):
            return ALPHABET.find(lettre)

        def cesar(message, decalage):
            resultat = ''
            for ... in message :
                if lettre in ALPHABET :
                    indice = ( ... )%26
                    resultat = resultat + ALPHABET[indice]
                else:
                    resultat = ...
            return resultat

        def test_cesar():
            assert cesar('BONJOUR A TOUS. VIVE LA MATIERE NSI !',4) == 'FSRNSYV E XSYW. ZMZI PE QEXMIVI RWM !'
            assert cesar('GTSOTZW F YTZX. ANAJ QF RFYNJWJ SXN !',-5) == 'BONJOUR A TOUS. VIVE LA MATIERE NSI !'
        ```

    === "Solution"

        ```python
        ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

        def position_alphabet(lettre):
            return ALPHABET.find(lettre)

        def cesar(message, decalage):
            resultat = ''
            for lettre in message :
                if lettre in ALPHABET :
                    indice = (position_alphabet(lettre) + decalage)%26
                    resultat = resultat + ALPHABET[indice]
                else:
                    resultat = resultat + lettre
            return resultat

        def test_cesar():
            assert cesar('BONJOUR A TOUS. VIVE LA MATIERE NSI !',4) == 'FSRNSYV E XSYW. ZMZI PE QEXMIVI RWM !'
            assert cesar('GTSOTZW F YTZX. ANAJ QF RFYNJWJ SXN !',-5) == 'BONJOUR A TOUS. VIVE LA MATIERE NSI !'
        ```


??? {{ automatisme() }}

    === "Énoncé"

        On considère des mots à trous : ce sont des chaînes de caractères contenant uniquement
        des majuscules et des caractères `*`. Par exemple `INFO*MA*IQUE`, `***I***E**` et
        `*S*` sont des mots à trous.  
        Programmer une fonction correspond qui :

        - prend en paramètres deux chaînes de caractères `mot` et `mot_a_trous` où
        `mot_a_trous` est un mot à trous comme indiqué ci-dessus, 
        - renvoie :
            - `True` si on peut obtenir `mot` en remplaçant convenablement les caractères
        `'*'` de `mot_a_trous`.
            - `False` sinon.

        Exemple :

        ```python
        >>> correspond('INFORMATIQUE', 'INFO*MA*IQUE')
        True
        >>> correspond('AUTOMATIQUE', 'INFO*MA*IQUE')
        False
        ```

    === "Solution"

        ~~~python
        def correspond(mot, mot_a_trous):
            if len(mot) != len(mot_a_trous):
                return False
            for k in range(len(mot)):
                if (mot_a_trous[k] != '*') and (mot_a_trous[k] != mot[k]):
                    return False
            return True
        ~~~

        
??? {{ automatisme() }}

    === "Énoncé"
        Lorsque, en ligne de commande, on saisit la commande `rm *` ceci a pour effet :

        1. d'activer une télécommande
        2. d'accéder au répertoire parent du répertoire courant
        3. d'effacer tous les fichiers du répertoire courant et ses sous-répertoires
        4. d'effacer tous les fichiers du répertoire courant

    === "Solution"
        d'effacer tous les fichiers du répertoire courant


??? {{ automatisme() }}

    === "Énoncé"
        Soient a et b deux booléens. L'expression booléenne `not(a and b) or a` est équivalente à :

        * `False`
        * `True`
        * `not(b)`
        * `not(a) or not(b)`

    === "Solution"
        `True`

??? {{ automatisme() }}

    === "Énoncé"
        À quelle affectation sont équivalentes les instructions suivantes, où a, b sont des variables entières et c une variable booléenne ?
        
        ~~~python
        if a==b:
            c = True
        elif a > b+10:
            c = True
        else:
            c = False
        ~~~

        * Réponse A : `c = (a==b) or (a > b+10)`
        * Réponse B : `c = (a==b) and (a > b+10)`
        * Réponse C : `c = not(a==b)`
        * Réponse D : `c = not(a > b+10)`
        
    === "Solution"
        Réponse A 

??? {{ automatisme() }}

    === "Énoncé"
        On considère une formule booléenne formée des variables booléennes `a` et `b` dont voici la table de vérité. 


        | a     | b     | form  |
        |-------|-------|-------|
        | True  | True  | False |
        | False | True  | True  |
        | True  | False | True  |
        | False | False | False |


        Quelle est cette formule booléenne  ?

        * **Réponse A :**  `a and b`
        * **Réponse B :** `a or b`
        * **Réponse C :** `((not(a)) and b) or (a and  (not(b)))`
        * **Réponse D :** `(not(a)) or (not(b))`

    === "Solution"
    
        **Réponse C :** `((not(a)) and b) or (a and  (not(b)))`



# Thème B : types construits



???+ {{ automatisme() }}

    === "Énoncé"

        Programmer la fonction dont on donne la spécification (référence [Moodle](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=1340))

        ~~~python
        def au_moins_un_zero(t):
            """
            Paramètre : t un tableau de nombres (int ou float)
            Précondition : t non vide
            Valeur renvoyée : un booléen indiquant si t contient au moins un zéro
            """
        ~~~
    
    === "Solution"
    
        ~~~python
        def un_zero(tab):
            for e in tab:
                if e == 0:
                    return True
            return False
        ~~~


??? {{ automatisme() }}

    [Code Puzzle, fonction renvoyant le maximum d'un tableau](https://www.codepuzzle.io/p/Z72N)


??? {{ automatisme() }}

    [Code Puzzle, fonction testant l'appartenance d'un élément à un tableau](https://www.codepuzzle.io/p/TFZN)




??? {{ automatisme() }}

    [Code Puzzle, 0 <-> 1](https://www.codepuzzle.io/p/WBGP)


??? {{ automatisme() }}

    === "Énoncé"

        On travaille sur des tableaux à deux dimensions qui représentent des images binaires : un pixel a pour valeur un entier : 0 pour un pixel noir et 1 pour un pixel blanc.

        Compléter les fonctions ci-dessous en respectant leurs spécifications, les postconditions doivent être vérifiées.

        ~~~python
        def image_noire(largeur, hauteur):
            """
            Paramètre : 
                largeur et hauteur deux entiers non nuls
            Valeur renvoyée :
                un tableau à 2 dimensions représentant une image
                binaire de dimensions (largeur, hauteur)
                rempli de 0
            """
            # à compléter avec un tableau en compréhension
            
        def dimensions(tab):
            """
            Paramètre : 
                tab un tableau à deux dimensions d'entiers
                représentant une image binaire rectangulaire
            Valeur renvoyée :
                un tableau de deux entiers [largeur, hauteur]
                représentant les dimensions de l'image
            """
            # à compléter
            
        def nombre_blancs(tab):
            """
            Paramètre : 
                tab un tableau à deux dimensions d'entiers
                représentant une image binaire rectangulaire
            Valeur renvoyée :
                un entier représentant le nombre de pixels 
                blancs (valeur 1)
            """
            # à compléter

        # postconditions pour la fonction image_noire 
        assert image_noire(2,1) == [[0,0]]
        assert image_noire(1,2) == [[0], [0]]
        assert image_noire(3,2) == [[0,0,0], [0,0,0]]

        
        # postconditions pour la fonction dimensions 
        assert dimensions([[], []]) == [2,0]
        assert dimensions([[0,1,2], [3,4,5]]) == [2,3]

        # postconditions pour la fonction nombre_blancs
        assert nombre_blancs([[0,0], [0,0]]) == 0
        assert nombre_blancs([[0,1,1], [0,1,0]]) == 3
        assert nombre_blancs([[], []]) == 0
        ~~~

    === "Solution"

        ~~~python
        def image_noire(largeur, hauteur):
            """
            Paramètre : 
                largeur et hauteur deux entiers non nuls
            Valeur renvoyée :
                un tableau à 2 dimensions représentant une image
                binaire de dimensions (largeur, hauteur)
                rempli de 0
            """
            return [[0 for _ in range(largeur)] for _ in range(hauteur)]

        def dimensions(tab):
            """
            Paramètre : 
                tab un tableau à deux dimensions d'entiers
                représentant une image binaire rectangulaire
            Valeur renvoyée :
                un tableau de deux entiers [largeur, hauteur]
                représentant les dimensions de l'image
            """
            largeur = len(tab[0])
            hauteur = len(tab)
            return [largeur, hauteur]

        def nombre_blancs(tab):
            """
            Paramètre : 
                tab un tableau à deux dimensions d'entiers
                représentant une image binaire rectangulaire
            Valeur renvoyée :
                un entier représentant le nombre de pixels 
                blancs (valeur 1)
            """
            largeur, hauteur = dimensions(tab)
            c = 0
            for i in range(hauteur):
                for j in range(largeur):
                    if tab[i][j] == 1:
                        c = c + 1
            return c
            

        # postconditions pour la fonction image_noire 
        assert image_noire(2,1) == [[0,0]]
        assert image_noire(1,2) == [[0], [0]]
        assert image_noire(3,2) == [[0,0,0], [0,0,0]]


        # postconditions pour la fonction dimensions 
        assert dimensions([[], []]) == [0,2]
        assert dimensions([[0,1,2], [3,4,5]]) == [3,2]


        # postconditions pour la fonction nombre_blancs
        assert nombre_blancs([[0,0], [0,0]]) == 0
        assert nombre_blancs([[0,1,1], [0,1,0]]) == 3
        assert nombre_blancs([[], []]) == 0
        ~~~



# Thème C : traitement de données en tables

# Thème D : interactions entre l'homme et la machine sur le Web

# Thème E : architectures matérielles et systèmes d'exploitation

???+ {{ automatisme() }}

    === "Énoncé"
        Quelle commande permet de connaître le répertoire courant ?

        1. `cd` 
        2. `ls`
        3. `pwd`
        4. `chmod`

    === "Solution"
        `pwd`

??? {{ automatisme() }}

    === "Énoncé"
        Quelle commande permet de lister le contenu du  répertoire courant ?

        1. `cd` 
        2. `ls`
        3. `pwd`
        4. `chmod`

    === "Solution"
        `ls`


??? {{ automatisme() }}

    === "Énoncé"
        Lorsque, en ligne de commande, on saisit la commande `chmod u+rw a.txt` ceci a pour effet :

        1. de permettre au propriétaire du fichier de modifier le contenu de ce fichier
        2. d'interdire au propriétaire de modifier le contenu de ce fichier
        3. d'interdire à tous les autres utilisateurs de lire le fichier
        4. d'effacer le fichier

    === "Solution"
        `chmod u+rw a.txt`  a pour effet de permettre au propriétaire du fichier de modifier le contenu de ce fichier


??? {{ automatisme() }}

    === "Énoncé"
        Dans un terminal sous Linux, quelle commande faut-il écrire pour donner à tout le monde le droit d'écriture sur un fichier `information.py` ?

        1. `chmod o+w information.py`
        2. `chmod a+w information.py`
        3. `chmod o+x information.py`
        4. `chmod a+x information.py` 

    === "Solution"
        `chmod a+w information.py`





??? {{ automatisme() }}

    === "Énoncé"
        Pour renommer un fichier`text1.txt` en `text1.old` dans un même répertoire, quelle commande faut-il  utiliser ?

        1. `mv text1.txt ../text1.old`
        2. `mv text1.txt text1.old`
        3. `cp text1.txt text1.old`
        4. `lns text1.txt text1.old`
    
    === "Solution"
        `mv text1.txt text1.old`
        

??? {{ automatisme() }}

    === "Énoncé"
        Sous UNIX, que va réaliser la ligne de commande `cat file.txt` ?

        1. rien du tout
        2. l'affichage du contenu du fichier `file.txt` dans la console
        3. la création d'un fichier `file.txt`
        4. la suppression du fichier `file.txt`
    
    === "Solution"
        l'affichage du contenu du fichier `file.txt` dans la console

??? {{ automatisme() }}

    === "Énoncé"
        Sous UNIX, que va réaliser la ligne de commande `cat file.txt` ?

        1. rien du tout
        2. l'affichage du contenu du fichier `file.txt` dans la console
        3. la création d'un fichier `file.txt`
        4. la suppression du fichier `file.txt`
    
    === "Solution"
        l'affichage du contenu du fichier `file.txt` dans la console


??? {{ automatisme() }}

    === "Énoncé"
        Lorsque, en ligne de commande, on saisit la commande `rm *` ceci a pour effet :

        1. d'activer une télécommande
        2. d'accéder au répertoire parent du répertoire courant
        3. d'effacer tous les fichiers du répertoire courant et ses sous-répertoires
        4. d'effacer tous les fichiers du répertoire courant

    === "Solution"
        d'effacer tous les fichiers du répertoire courant

??? {{ automatisme() }}

    === "Énoncé"
        On a exécuté la commande `ls -l` et obtenu l’affichage suivant :

        ~~~
        total 0
        -rw-rw-rw- 1 etudiant etudiant 15 Jul 2 13:29 exercice
        drwxrwxrwx 1 etudiant etudiant 512 Jul 2 13:37 seances
        ~~~    
    
        Que permettent d’affirmer les informations obtenues ?

        1.  exercice et seances sont deux fichiers
        2.  exercice est un fichier et seances est un répertoire
        3.  exercice et seances sont deux répertoires
        4. exercice est un répertoire et seances est un fichier

    === "Solution"
        exercice est un fichier et seances est un répertoire


??? {{ automatisme() }}

    === "Énoncé"
        Sachant que le répertoire courant contient les fichiers `fich.txt`, `mafich.txt` et `programme.py`, quel est
        le résultat de la commande `ls fich*` dans un shell Linux ?
        
        1. `fich.txt mafich.txt`
        2. `mafich.txt`
        3. `fich.txt`
        4. `programme.py`

    === "Solution"
        `fich.txt`

??? {{ automatisme() }}

    === "Énoncé"
        Dans la console Linux, quelle commande faut-il exécuter pour obtenir la documentation sur la commande `pwd` ?

        1. `man pwd`
        2. `cd pwd`
        3. `mkdir pwd`
        4. `ls pwd`

    === "Solution"
        `man pwd`   

??? {{ automatisme() }}

    === "Énoncé"
        Dans un shell sous Linux, Alice utilise la commande `pwd`.

        Cette commande :

        1. liste les fichiers du répertoire courant
        2. liste les répertoires du répertoire courant
        3. affiche le chemin du répertoire courant
        4. affiche les permissions relatives au répertoire courant

    === "Solution"
        affiche le chemin du répertoire courant  


??? {{ automatisme() }}

    === "Énoncé"
        Sous Linux, la console indique que l'utilisateur se trouve dans le dossier `/var/lib`. Quelle commande doit-il exécuter pour revenir dans son dossier personnel `/home/martin` ?

        1. `cd ~`
        2. `cd /home`
        3. `dir`
        4. `dir /home/martin`

    === "Solution"
        `cd ~`

??? {{ automatisme() }}

    === "Énoncé"
        Le shell Linux renvoie ce résultat à la commande `ls -al` :

        ~~~
        lrwxr--r-- 2 toto toto  807 juin 26 14:06 eclipse
        drwxr-xr-x 2 toto toto  4096 juin 26 15:00 Doc_1
        -rw-r-xr-x 2 toto toto  4096 juin 26 14:06 QCM
        -rwxr-xr-x 2 toto toto  4096 juin 26 14:06 Doc_Travail
        ~~~
        
        Quel est le nom du fichier du répertoire courant, de taille 4096 octets, exécutable par son propriétaire ?

        1. eclipse
        2. Doc_1
        3. QCM
        4. Doc_Travail


    === "Solution"
        Doc_Travail



# Thème F : langages et programmation


???+ {{ automatisme() }} 
    
    [Code Puzzle : recherche dichotomique dans un tableau décroissant](https://capytale2.ac-paris.fr/web/c/bbb9-1163740)


??? {{ automatisme() }} 

    === "Énoncé"
        Pour déterminer la liste des chiffres en base dix d'un entier naturel, un élève a écrit la fonction ci-dessous :

        ~~~python
        def liste_chiffres(n):
            L = [n % 10]
            while n > 0:
                n = n // 10
                L.insert(0, n % 10)
            return L
        ~~~

        Malheureusement sa fonction ne retourne pas le résultat attendu pour l'entier 730 :

        ~~~
        >>> liste_chiffres(730)
        [0, 7, 3, 0]
        ~~~

        Proposer une version corrigée de la fonction `liste_chiffres`.

    === "Solution"

        ~~~python
        def liste_chiffres(n):
            L = [n % 10]
            while n >= 10:
                n = n // 10
                L.insert(0, n % 10)
            return L
        ~~~


# Thème G : algorithmique



??? {{ automatisme() }} 

    === "Énoncé"

        Compléter les fonctions Python ci-dessous pour que  les spécifications et les tests unitaires soient satisfaites.

        ~~~python
        def est_decroissant(t):
            """
            Paramètre : 
                t un tableau de nombres
                précondition len(t) > 0 
            Valeur renvoyée : 
                booléen indiquant si t dans l'ordre décroissant
            """
            "à compléter"


        def test_est_decroissant():
            assert not est_decroissant([k ** 2 for k in range(10)])
            assert est_decroissant([1])
            assert est_decroissant(list(range(10, -1, -1)))
            print("Tests unitaires réussis ")


        def recherche_dicho_decroissant(x, t):
            """
            Paramètre : 
                t un tableau de nombres trié dans l'ordre décroissant
                x un nombre 
            Valeur renvoyée : 
                True si x dans t
                False si x pas dans t
            """
            a = 0
            b = len(t) - 1
            while a <= b:
                m = (a + b) // 2
                "à compléter"
            return None

        def test_recherche_dicho_decroissant():
            t1 = list(range(10, -1, -1))
            assert recherche_dicho_decroissant(8, t1)
            assert recherche_dicho_decroissant(10, t1)
            assert recherche_dicho_decroissant(0, t1)
            assert not recherche_dicho_decroissant(4.5, t1)
            print("Tests unitaires réussis ")
        ~~~

    === "Solution"


        ~~~python
        def est_decroissant(t):
            """
            Paramètre : 
                t un tableau de nombres 
                précondition len(t) > 0
            Valeur renvoyée : 
                booléen indiquant si t dans l'ordre décroissant
            """
            assert len(t) > 0, "t doit être non vide"
            for i in range(1, len(t)):
                if t[i-1] < t[i]:
                    return False
            return True

        def test_est_decroissant():
            assert not est_decroissant([k ** 2 for k in range(10)])
            assert est_decroissant([1])
            assert est_decroissant(list(range(10, -1, -1)))
            print("Tests unitaires réussis ")


        def recherche_dicho_decroissant(x, t):
            """
            Paramètre : 
                t un tableau de nombres trié dans l'ordre décroissant
                x un nombre 
            Valeur renvoyée : 
                True si x dans t
                False si x pas dans t
            """
            a = 0
            b = len(t) - 1
            while a <= b:
                m = (a + b) // 2
                if t[m] > x:
                    a = m + 1
                elif t[m] < x:
                    b = m - 1
                else:
                    return True
            return False

        def test_recherche_dicho_decroissant():
            t1 = list(range(10, -1, -1))
            assert recherche_dicho_decroissant(8, t1)
            assert recherche_dicho_decroissant(10, t1)
            assert recherche_dicho_decroissant(0, t1)
            assert not recherche_dicho_decroissant(4.5, t1)
            print("Tests unitaires réussis ")
        ~~~



??? {{ automatisme() }} 

    === "Énoncé"

        Compléter la fonction Python ci-dessous pour que  la spécification et les tests unitaires soient satisfaits.

        ~~~python
        def index_premiere_occurence_dicho(x, t):
            """
            Paramètre : 
                t un tableau de nombres trié dans l'ordre croissant
                x un nombre 
            Valeur renvoyée : 
                l'index de la première de x dans t si x est dans t
                -1 sinon
            """
            a = 0
            b = len(t) - 1
            while a <= b:
                m = (a + b) // 2
                if t[m] < x:
                    "à compléter"
                elif t[m] > x:
                    "à compléter"
                else:
                    "à compléter"
                    return "à compléter"
            return -1
        
        def tests_unitaires():
            """Tests unitaires pour index_premiere_occurrence_dicho"""
            assert index_premiere_occurence_dicho(10, [10, 10, 11, 12, 13]) == 0
            assert index_premiere_occurence_dicho(10, [9, 10, 11, 12, 13]) == 1
            assert index_premiere_occurence_dicho(10, [9, 9, 11, 12, 13]) == -1
            assert index_premiere_occurence_dicho(10, [7, 8, 9, 10]) == 3 
            assert index_premiere_occurence_dicho(10, [7, 10, 10,  10, 10]) == 1 
            assert index_premiere_occurence_dicho(10, []) == -1
            print('tests réussis')
        ~~~

    === "Solution"

        ~~~python
        def index_premiere_occurrence_dicho(x, t):
            """
            Paramètre : 
                t un tableau de nombres trié dans l'ordre croissant
                x un nombre 
            Valeur renvoyée : 
                l'index de la première occurrence de x dans t si x est dans t
                -1 sinon
            """
            a = 0
            b = len(t) - 1
            while a <= b:
                m = (a + b) // 2
                if t[m] < x:
                    a = m + 1
                elif t[m] > x:
                    b = m - 1
                else:
                    while m >= 0 and t[m] == x:
                        m = m - 1
                    return m + 1
            return -1
        
        def tests_unitaires():
            """Tests unitaires pour index_premiere_occurrence_dicho"""
            assert index_premiere_occurence_dicho(10, [10, 10, 11, 12, 13]) == 0
            assert index_premiere_occurence_dicho(10, [9, 10, 11, 12, 13]) == 1
            assert index_premiere_occurence_dicho(10, [9, 9, 11, 12, 13]) == -1
            assert index_premiere_occurence_dicho(10, [7, 8, 9, 10]) == 3 
            assert index_premiere_occurence_dicho(10, [7, 10, 10,  10, 10]) == 1 
            assert index_premiere_occurence_dicho(10, []) == -1
            print('tests réussis')
        ~~~


??? {{ automatisme() }} 

    === "Énoncé 1"

        Compléter la fonction Python ci-dessous pour que  la spécification et les tests unitaires soient satisfaits.

        ~~~python
        def insertion(t, x):
            """
            Paramètre : 
                t un tableau de nombres trié dans l'ordre croissant
                x un nombre 
            Valeur renvoyée : 
                nouveau tableau avec x inséré à sa place dans t
            """
            t2 = t + [x]
            i = len(t2) - 2
            # à compléter
            return t2
        
        def tests_insertion():
            """Tests unitaires pour insertion"""
            assert insertion([], 10]) == [10]
            assert insertion([8], 10]) == [8, 10]
            assert insertion([8], 6]) == [6, 8]
            assert insertion([5, 8], 6]) == [5, 6, 8]
            assert insertion([5, 8], 8]) == [5, 8, 8]
            assert insertion([5, 8], 4]) == [4, 5, 8]
            print('tests réussis')
        ~~~


    === "Solution 1"

        ~~~python
        def insertion(t, x):
            """
            Paramètre : 
                t un tableau de nombres trié dans l'ordre croissant
                x un nombre 
            Valeur renvoyée : 
                nouveau tableau avec x inséré à sa place dans t
            """
            t2 = t + [x]
            i = len(t2) - 2
            while i >= 0 and t2[i] > x:
                t2[i + 1] = t2[i]
                i = i - 1
            t2[i + 1] = x
            return t2
                
        def test_insertion():
            """Tests unitaires pour insertion"""
            assert insertion([], 10) == [10]
            assert insertion([8], 10) == [8, 10]
            assert insertion([8], 6) == [6, 8]
            assert insertion([5, 8], 6) == [5, 6, 8]
            assert insertion([5, 8], 8) == [5, 8, 8]
            assert insertion([5, 8], 4) == [4, 5, 8]
            print('tests réussis')
        ~~~

    === "Énoncé 2"

        Compléter la fonction Python ci-dessous pour que  la spécification et les tests unitaires soient satisfaits.

        ~~~python
        def tri_insertion(t):
            """
            Paramètre : 
                t un tableau de nombres
            Valeur renvoyée : 
                nouveau tableau trié dans l'ordre croissant
            """
            if len(t) == 0:
                return []
            tc = [t[0]]
            for i in range(1, len(t)):                
                "à compléter"
            return tc
                
        def test_tri_insertion():
            """Tests unitaires pour insertion"""
            assert tri_insertion([]) == []
            assert tri_insertion([8, 10]) == [8, 10]
            assert tri_insertion([8, 8]) == [8, 8]
            assert tri_insertion([8, 6, 4]) == [4, 6, 8]
            assert tri_insertion([6, 4, 8]) == [4, 6, 8]
            print('tests réussis')
        ~~~


    === "Solution 2"

        ~~~python
        def tri_insertion(t):
            """
            Paramètre : 
                t un tableau de nombres 
            Valeur renvoyée : 
                nouveau tableau trié dans l'ordre croissant
            """
            tc = [t[0]]
            for i in range(1, len(t)):                
                tc = insertion(tc, t[i])
            return tc
                
        def test_tri_insertion():
            """Tests unitaires pour insertion"""
            assert tri_insertion([]) == []
            assert tri_insertion([8, 10]) == [8, 10]
            assert tri_insertion([8, 8]) == [8, 8]
            assert tri_insertion([8, 6, 4]) == [4, 6, 8]
            assert tri_insertion([6, 4, 8]) == [4, 6, 8]
            print('tests réussis')
        ~~~

??? {{ automatisme() }}

    === "Énoncé"

        La fonction fusion prend deux tableaux `t1` et `t2`  d'entiers triées par ordre croissant et les  fusionne en une liste triée L12 qu'elle renvoie. 

        Le code Python, à compléter, de la fonction est  :

        ~~~python
        def fusion(t1,t2):
            n1 = len(t1)
            n2 = len(L2)
            t12 = [0] * (n1 + n2)
            i1 = 0
            i2 = 0
            i = 0
            while i1 < n1 and ... :
                if t1[i1] < t2[i2]:
                    t12[i] = ...
                    i1 = ...
                else:
                    t12[i] = t2[i2]
                    i2 = ...
                i += 1
            while i1 < n1:
                t12[i] = ...
                i1 = i1 + 1
                i = ...
            while i2 < n2:
                t12[i] = ...
                i2 = i2 + 1
                i = ...
            return t12

        def test_fusion():
            assert fusion([1,6,10],[0,7,8,9]) == [0, 1, 6, 7, 8, 9, 10]
        ~~~

    === "Solution"

        ~~~python
        def fusion(t1,t2):
            n1 = len(t1)
            n2 = len(L2)
            t12 = [0] * (n1 + n2)
            i1 = 0
            i2 = 0
            i = 0
            while i1 < n1 and i2 < n2:
                if t1[i1] < t2[i2]:
                    t12[i] = t1[i]
                    i1 = i1 + 1
                else:
                    t12[i] = t2[i2]
                    i2 = i2 + 1
                i += 1
            while i1 < n1:
                t12[i] = t1[i]
                i1 = i1 + 1
                i = i + 1
            while i2 < n2:
                t12[i] = t2[i]
                i2 = i2 + 1
                i = i + 1 
            return t12

        def test_fusion():
            assert fusion([1,6,10],[0,7,8,9]) == [0, 1, 6, 7, 8, 9, 10]
        ~~~

