---
title: Thème Tests
---

{% include 'abbreviations.md' %}


!!! tip "Exercice"

    Compléter le programme Python ci-dessous pour qu'il affiche `"valeur cohérente"` si la variable `note` contient une valeur entre 0 et 20 et `"valeur incohérente"` sinon. 
    


{{IDE("exo1_tests")}} 



[Correction](corr_exo1_tests.py)
