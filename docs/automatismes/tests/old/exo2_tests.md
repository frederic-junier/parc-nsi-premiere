---
title: Thème Tests
---

{% include 'abbreviations.md' %}


!!! tip "Exercice"

    Compléter le programme Python ci-dessous pour qu'il affiche :
        * `"recalé"` si  0 $\leqslant$ `note` $<$ 8
        * `"second groupe"` si  8 $\leqslant$ `note` $<$ 10
        * `"reçu"` si 10 $\leqslant$ `note` $<$ 12
        * `"assez bien"` si 12 $\leqslant$ `note` $<$ 14
        * `"bien"` si 14 $\leqslant$ `note` $<$ 16
        * `"très bien"` si 16 $\leqslant$ `note` $\leqslant$ 20
        * `"valeur incohérente"` sinon
    


{{IDE("exo2/exo2_tests")}} 



[Correction](scripts/exo2/corr_exo2_tests.py)