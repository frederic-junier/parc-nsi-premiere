---
title: Thème Boucle bornée (for)
---

{% include 'abbreviations.md' %}


!!! tip "Exercice"

    Écrire un programme Python qui affiche "`au moins deux égales`" si au moins deux valeurs des variables `a`, `b` et `c` sont  égales et  "`toutes différentes`" sinon.
    


{{IDE("exo4_tests")}} 

[Correction](exo4_tests_corr.py)
