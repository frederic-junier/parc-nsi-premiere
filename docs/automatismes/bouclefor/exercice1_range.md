---
title: Thème Boucle bornée (for)
---

{% include 'abbreviations.md' %}


!!! tip "Exercice"

    Écrire un programme Python de deux lignes de code au plus, qui affiche tous les entiers entre 1 et 10 inclus dans l'ordre croissant (un par ligne).
    


{{IDE("../exo1_range")}} 


[Correction](exo1_range_corr.py)
