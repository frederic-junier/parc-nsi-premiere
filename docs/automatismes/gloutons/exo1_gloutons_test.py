# Tests de base


def test_mise_en_boite():
    restes1 = [832, 342, 500]
    boites1 = [750, 250, 500]
    assert mise_en_boite(restes1, boites1) == 2
    restes2 = [487, 601, 584, 819, 601]
    boites2 = [750, 500, 700, 65, 700]
    assert mise_en_boite(restes2, boites2) == 4
    restes3 = [500, 700, 600, 750, 800, 600]
    boites3 = [400, 750, 750, 700, 700, 600]
    assert mise_en_boite(restes3, boites3) == 5


test_mise_en_boite()
