def mise_en_boite(restes, boites):
    """
    Renvoie le nombre maximal de boites pour placer chaque reste
    d'aliment   dans une boite qui peut le contenir

    Paramètres :
    restes : liste de volume d'aliments (entiers positifs)
    boites : liste de volume de boite (entiers positifs)

    Retour: un entier
    """
    # à compléter
    # BEGIN CUT
    # on trie d'abord les listes restes et boites par ordre croissant
    restes_tri = sorted(restes)
    boites_tri = sorted(boites)
    ir = 0
    for b in boites_tri:
        # Choix glouton : on choisit la boite b si elle peut contenir
        # le plus petit aliment restant ( restes_tri[ir] car restes_tri dans l'ordre croissant)
        # choix jamais remis en cause par la suite : si la boite ne peut pas contenir
        # le plus petit aliment, elle ne pourra pas contenir les autres
        if restes_tri[ir] <= b:
            ir = ir + 1
    return ir
    # END CUT


def mise_en_boite2(restes, boites):
    """
    Renvoie le nombre maximal de boites pour placer chaque reste
    d'aliment   dans une boite qui peut le contenir

    Paramètres :
    restes : liste de volume d'aliments (entiers positifs)
    boites : liste de volume de boite (entiers positifs)

    Retour: un entier
    """
    # à compléter
    # BEGIN CUT
    # on trie d'abord les listes restes et boites par ordre décroissant
    restes_tri = sorted(restes, reverse=True)
    boites_tri = sorted(boites, reverse=True)
    ib = 0
    for r in restes_tri:
        # Choix glouton : on choisit l'aliment r s'il peut être contenu
        # dans la plus grande boite restante ( boites_tri[ib] car boites_tri dans l'ordre décroissant)
        # choix jamais remis en cause par la suite : si l'aliment est trop grand pour la plus grande boite
        # il le sera pour les autres
        if boites_tri[ib] >= r:
            ib = ib + 1
    return ib
    # END CUT
