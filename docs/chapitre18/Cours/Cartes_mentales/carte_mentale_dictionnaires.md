# Carte Mentale: Les Dictionnaires en Python

## Définition
- Structure de données
- Paires clé-valeur
- Dynamique et modifiable

## Création d'un Dictionnaire
- Dictionnaire vide : `mon_dico = {}`
- Dictionnaire avec des données initiales : `mon_dico = {'cle1': 'valeur1', 'cle2': 'valeur2'}`

## Accès aux Éléments
- Accéder à une valeur : `mon_dico['cle1']`
- Utiliser `get` : `mon_dico.get('cle1')`

## Modification et Ajout
- Ajouter une paire clé-valeur : `mon_dico['cle3'] = 'valeur3'`
- Modifier une valeur : `mon_dico['cle1'] = 'nouvelle valeur'`

## Itération
### Par Clé
- Boucle for : `for cle in mon_dico:`
### Par Valeur
- Itérer sur les valeurs : `for valeur in mon_dico.values():`
### Par Clé et Valeur
- Itérer sur les paires clé-valeur : `for cle, valeur in mon_dico.items():`

## Suppression d'Éléments
- Supprimer une paire clé-valeur : `del mon_dico['cle1']`
- Utiliser `pop` : `mon_dico.pop('cle2')`

## Méthodes Utiles
- `keys()` : Retourne les clés
- `values()` : Retourne les valeurs
- `items()` : Retourne les paires clé-valeur
- `clear()` : Vide le dictionnaire
- `copy()` : Copie le dictionnaire


