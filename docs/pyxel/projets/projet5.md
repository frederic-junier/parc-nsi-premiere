---
author: F.Junier
title: Projet 5
---

!!! example "Projet 5"

    !!! warning "Niveau de difficulté"
        * Moyen, long, pas de squelette de code.
        * Plus de 100 lignes de code.
        * Boucles, tests, listes, dictionnaires, gérer des événements avec les fonctions `update` et `draw`. 

    !!! tip "Cahier des charges"

        === "Objectif"
            
            Compléter le  squelette de code fourni sur le jour [8](https://24jdpp.forge.apps.education.fr/jour.html?j=8){:target="_blank"}  du [calendrier 24 jours de pyxel de Laurent Abbal](https://24jdpp.forge.apps.education.fr/) en respectant les contraintes de taille, pour faire pleuvoir des flocons sur le sapin, le recouvrir progressivement de neige ainsi que le sol. Enfin faire apparaitre le soleil qui fait fondre la neige lorsque le tronc est complètement recouvert.
            
            Il est interdit d'utiliser le mot clef `global`, on encapsulera les variables globales dans des dictionnaires (un pour le corps, un autre pour la tête etc ...).

           

  
        === "Prérequis"
            * Traiter les défis des jours  [3](https://24jdpp.forge.apps.education.fr/jour.html?j=3){:target="_blank"} (dessiner des disques),  [4](https://24jdpp.forge.apps.education.fr/jour.html?j=4){:target="_blank"} (gestion de la vitesse d'affichage avec `pyxel.frame_count`),  [6](https://24jdpp.forge.apps.education.fr/jour.html?j=6){:target="_blank"},[7](https://24jdpp.forge.apps.education.fr/jour.html?j=6){:target="_blank"}  et  [8](https://24jdpp.forge.apps.education.fr/jour.html?j=8){:target="_blank"}   du [calendrier 24 jours de pyxel](https://24jdpp.forge.apps.education.fr/).
  
        === "Squelette de code"

            Aucun squelette de code n'est fourni.


    !!! danger "Résultat attendu"

        Cliquer sur l'image pour tester l'application.

        [![alt](./assets/sapin_neige.png)](./assets/d8.html){:target="_blank"} 

