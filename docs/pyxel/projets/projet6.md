---
author: F.Junier
title: Projet 6
---

!!! example "Projet 6"

    !!! warning "Niveau de difficulté"
        * Moyen, long, squelette de code assez détaillé
        * Plus de 100 lignes de code.
        * Boucles, tests, listes, dictionnaires, gérer des événements avec les fonctions `update` et `draw`. 

    !!! tip "Cahier des charges"

        === "Objectif"
            
            * _Objectif niveau 1 :_ compléter le squelette de code fourni sur le jour [9](https://24jdpp.forge.apps.education.fr/jour.html?j=9){:target="_blank"}  du [calendrier 24 jours de pyxel de Laurent Abbal](https://24jdpp.forge.apps.education.fr/)  pour déplacer le chasse neige dans l'environnement sans qu'il puisse traverser la végétation ou la neige

            * _Objectif niveau 2 :_ on complète le code du niveau 1 :
                * le chasse neige ne peut pousser qu'un bloc de néige à la fois
                * le chasse neige doit pousser la neige sur les emplacements marqués par un caractère `o` dans la carte du niveau
                * le jeu est chronométré
                * un message de félicitations s'affiche à la fin
                *  l'aspect du chasse neige change selon son orientation
                
            
            Il est interdit d'utiliser le mot clef `global`, on encapsulera les variables globales dans des dictionnaires (un pour le corps, un autre pour la tête etc ...).

           

  
        === "Prérequis"
            * Traiter les défis des jours  [3](https://24jdpp.forge.apps.education.fr/jour.html?j=3){:target="_blank"} (dessiner des disques),  [4](https://24jdpp.forge.apps.education.fr/jour.html?j=4){:target="_blank"} (gestion de la vitesse d'affichage avec `pyxel.frame_count`), [5](https://24jdpp.forge.apps.education.fr/jour.html?j=5){:target="_blank"} (gestion des événements pour déplacer un objet)  [9](https://24jdpp.forge.apps.education.fr/jour.html?j=9){:target="_blank"} et [10](https://24jdpp.forge.apps.education.fr/jour.html?j=10){:target="_blank"}  du [calendrier 24 jours de pyxel](https://24jdpp.forge.apps.education.fr/).
  
        === "Squelette de code"

            Le squelette de code ci-dessous reprend celui fourni sur le jour [9](https://24jdpp.forge.apps.education.fr/jour.html?j=9){:target="_blank"}  du [calendrier 24 jours de pyxel](https://24jdpp.forge.apps.education.fr/). 

            ~~~python
            import pyxel

            pyxel.init(128, 128, title="Chasse-neige")

            # Variables globales dans un dictionnaire
            v = {
                "carte_terrain": [
                    "........+++.....",
                    "........+++.....",
                    "........+++.....",
                    "........+++++++.",
                    "........+++++++.",
                    "........+++++++.",
                    "............+++.",
                    "............+++.",
                    "............+++.",
                    "............+++.",
                    "............+++.",
                    "............+++.",
                    "............+++.",
                    "............+++.",
                    "............+++.",
                    "............+++.",
                ],
                "carte_niveau": [
                    "___________ooooo",
                    "_________**ooooo",
                    "__________*ooooo",
                    "_______****____o",
                    "___*______*____o",
                    "_______________o",
                    "___@___***_____o",
                    "_____________**o",
                    "___________***_o",
                    "_______*v___*__o",
                    "________**_____o",
                    "____*___*______o",
                    "___*v__________o",
                    "____*__*_______o",
                    "____*__*v______o",
                    "_______________o"
                ] ,
                "couleurs_terrain": {
                    ".": 4,  # sol : marron
                    "+": 0,  # route : noir
                },
                "positions_neige": [],
                "positions_vegetation": [],
                "position_chasse_neige": [0, 0],
            }


            def initialiser():
                """
                Initialisation des positions.
                """
                for num_ligne in range(len(v["carte_niveau"])):
                    for num_colonne in range(len(v["carte_niveau"][num_ligne])):
                        objet = v["carte_niveau"][num_ligne][num_colonne]
                        if objet == "@":
                            v["position_chasse_neige"] = [num_colonne, num_ligne]
                        elif objet == "*":
                            v["positions_neige"].append([num_colonne, num_ligne])
                        elif objet == "v":
                            v["positions_vegetation"].append([num_colonne, num_ligne])


            def case(x, y, couleur):
                """
                Cete fonction permet de créer des carrés de 8x8 pixels en fonction
                des positions des cases de l'espace de jeu.
                L'espace de jeu est découpé en 16x16 cases de 8x8 pixels.
                """
                pyxel.rect(x * 8, y * 8, 8, 8, couleur)


            def deplacements():
                """
                Modification des coordonnées en fonctions des touches utilisées.
                """
                dx = 0
                dy = 0

                if pyxel.btnp(pyxel.KEY_LEFT):
                    dx = -1
                if pyxel.btnp(pyxel.KEY_RIGHT):
                    dx = 1
                if pyxel.btnp(pyxel.KEY_UP):
                    dy = -1
                if pyxel.btnp(pyxel.KEY_DOWN):
                    dy = 1

                nouvelles_positions(dx, dy)


            def nouvelles_positions(dx, dy):
                """
                Modification des positions du chasse-neige et gestion des obstacles.
                Le chasse-neige ne doit pas sortir de la fenêtre et il ne doit
                pas pouvoir passer sur des obstacles (végétation ou neige).
                """
                nx = v["position_chasse_neige"][0] + dx
                ny = v["position_chasse_neige"][1] + dy

                # à compléter


            def dessin_terrain():
                """
                Dessine le terrain (sol et route).
                """
                for num_ligne in range(len(v["carte_terrain"])):
                    for num_colonne in range(len(v["carte_terrain"][num_ligne])):
                        # à compléter


            def dessin_neige():
                """
                Dessine la neige.
                Couleur: 7
                """
                # à compléter


            def dessin_vegetation():
                """
                Dessine la végétation.
                Couleur: 3
                """
                # à compléter


            def dessin_chasse_neige():
                """
                Dessine le chasse-neige.
                Couleur: 8
                """
                # à compléter
                
                
            # Initialisation
            initialiser()   

            # =========================================================
            # == UPDATE
            # =========================================================   
            def update():
                """Mise à jour des variables (30 fois par seconde)."""
                deplacements()

            # =========================================================
            # == DRAW
            # =========================================================
            def draw():
                """Dessin des objets (30 fois par seconde)."""
                
                pyxel.cls(0)
                
                # Terrain
                dessin_terrain()
                
                # Neige
                dessin_neige()
                
                # Végétation  
                dessin_vegetation()
                
                # Chasse-neige
                dessin_chasse_neige()


            pyxel.run(update, draw)
            ~~~


    !!! danger "Résultat attendu"

        Cliquer sur l'image pour tester l'application.

        [![alt](./assets/chasse_neige.png)](./assets/d10.html){:target="_blank"} 

