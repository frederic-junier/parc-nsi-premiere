---
author: F.Junier
title: Projet 1
---

!!! example "Projet 1"

    !!! warning "Niveau de difficulté"
        * Facile.
        * 50 lignes de code.
        * Boucles, tests.

    !!! tip "Cahier des charges"

        === "Objectif"
            * Dessiner à l'aide des caractères textuels `*`  (épines) et `<` et `>` (extrémité d'un niveau de feuillage), un sapin avec 14 niveaux de branches, un tronc de largeur 4 pixels et de hauteur 20 pixels.
            * Sur chaque niveau de branches les décorations sont disposées aléatoirement et couvrent environ la moitié des emplacements des caractères `*` représentant les épines du sapin : [défi du jour 2](https://24jdpp.forge.apps.education.fr/jour.html?j=2){:target="_blank"}.
            * Faire clignoter les décorations du sapin : [défi du jour 4](https://24jdpp.forge.apps.education.fr/jour.html?j=4){:target="_blank"}.
  
        === "Prérequis"
            * Traiter les défis des jours [2](https://24jdpp.forge.apps.education.fr/jour.html?j=2){:target="_blank"} et [4](https://24jdpp.forge.apps.education.fr/jour.html?j=4){:target="_blank"} du [calendrier 24 jours de pyxel](https://24jdpp.forge.apps.education.fr/).
            * 
        === "Squelette de code"

            Ci-dessous le code fourni dans le jour [2](https://24jdpp.forge.apps.education.fr/jour.html?j=2){:target="_blank"} du calendrier pyxel de Laurent Abbal. Il faut l'adapter  et l'enrichir pour satisfaire le cahier des charges.

            ~~~python3
            import pyxel

            pyxel.init(128, 128, title="Sapin décoré")

            pyxel.cls(0)

            # Coordonnées de base pour centrer le sapin
            base_x = 40
            base_y = 40

            # Étoile au sommet
            pyxel.text(base_x+20, base_y, "*", 10)

            # Feuillage
            pyxel.text(base_x+16, base_y+5, "<*>", 11)
            pyxel.text(base_x+12, base_y+10,"<***>", 11)
            pyxel.text(base_x+8, base_y+15,"<*****>",11)
            pyxel.text(base_x+4, base_y+20,"<* *****>",11)
            pyxel.text(base_x, base_y+25,"<*********>",11)
            pyxel.text(base_x, base_y+30,"<*********>",11)

            # Décoration
            pyxel.text(base_x+24, base_y+15,"*",8)
            pyxel.text(base_x+12, base_y+20,"o",5)
            pyxel.text(base_x+20, base_y+25,"*",2)

            # Tronc
            pyxel.text(base_x+16, base_y+35, "|", 4)
            pyxel.text(base_x+17, base_y+35, "|", 4)
            pyxel.text(base_x+18, base_y+35, "|", 4)
            pyxel.text(base_x+19, base_y+35, "|", 4)
            pyxel.text(base_x+20, base_y+35, "|", 4)
            pyxel.text(base_x+21, base_y+35, "|", 4)
            pyxel.text(base_x+22, base_y+35, "|", 4)
            pyxel.text(base_x+23, base_y+35, "|", 4)
            pyxel.text(base_x+24, base_y+35, "|", 4)

            pyxel.show()
            ~~~


    !!! danger "Résultat attendu"

        Cliquer sur l'image pour tester l'application.

        [![alt](./assets/sapin.png)](./assets/d4.html){:target="_blank"} 

