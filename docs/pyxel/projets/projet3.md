---
author: F.Junier
title: Projet 3
---

!!! example "Projet 3"

    !!! warning "Niveau de difficulté"
        * Facile
        * 50 lignes de code.
        * Boucles, tests, dictionnaires, gérer des événements avec les fonctions `update` et `draw`. 

    !!! tip "Cahier des charges"

        === "Objectif"
            
            Compléter le  squelette de code fourni sur le jour [5](https://24jdpp.forge.apps.education.fr/jour.html?j=5){:target="_blank"}  du [calendrier 24 jours de pyxel de Laurent Abbal](https://24jdpp.forge.apps.education.fr/) pour que :

            * on puisse sélectionner avec la souris un disque à déplacer parmi  quatre disques représentant : le corps, la tête, un premier oeil, un second oeil du bonhomme de neige ; 
            * on puisse déplacer le disque sélectionné avec les flèches du pavé directionnel pour reconstituer le  bonhomme de neige ;
            
            Il est interdit d'utiliser le mot clef `global`, on encapsulera les variables globales dans des dictionnaires (un pour le corps, un autre pour la tête etc ...).

           

  
        === "Prérequis"
            * Traiter les défis des jours  [3](https://24jdpp.forge.apps.education.fr/jour.html?j=3){:target="_blank"} et  [5](https://24jdpp.forge.apps.education.fr/jour.html?j=5){:target="_blank"}  du [calendrier 24 jours de pyxel](https://24jdpp.forge.apps.education.fr/).
  
        === "Squelette de code"

            Le squelette de code ci-dessous fourni sur le jour [5](https://24jdpp.forge.apps.education.fr/jour.html?j=5){:target="_blank"}  du [calendrier 24 jours de pyxel](https://24jdpp.forge.apps.education.fr/) permet de gérer les déplacements d'un carré avec la souris et d'un autre carré avec les flèches du pavé directionnel du clavier.

            ~~~python3
            import pyxel

            pyxel.init(128, 128, title="Détection des Entrées")

            # État des variables du jeu encapsulé dans un dictionnaire
            # Le nom du dictionnaire est volontairement court afin de ne pas trop alourdir
            # l'écriture lors de l'utilisation des variables
            v = {
                "x": 80, # Position initiale en x
                "y": 60  # Position initiale en y
            }

            def update():
                """
                Fonction appelée à chaque frame pour mettre à jour la logique du programme.
                Ici, elle gère le déplacement du rectangle en fonction des touches pressées.
                """

                # Vérifie si la touche gauche est pressée et déplace le rectangle vers la gauche
                if pyxel.btn(pyxel.KEY_LEFT):  
                    v['x'] -= 2  # Déplace le rectangle de 2 pixels vers la gauche

                # Vérifie si la touche droite est pressée et déplace le rectangle vers la droite
                if pyxel.btn(pyxel.KEY_RIGHT):
                    v['x'] += 2  # Déplace le rectangle de 2 pixels vers la droite

                # Vérifie si la touche haut est pressée et déplace le rectangle vers le haut
                if pyxel.btn(pyxel.KEY_UP):
                    v['y'] -= 2  # Déplace le rectangle de 2 pixels vers le haut

                # Vérifie si la touche bas est pressée et déplace le rectangle vers le bas
                if pyxel.btn(pyxel.KEY_DOWN):
                    v['y'] += 2  # Déplace le rectangle de 2 pixels vers le bas

            def draw():
                """
                Fonction appelée à chaque frame pour dessiner les éléments à l'écran.
                Elle efface l'écran, puis dessine deux rectangles :
                - Un rectangle contrôlé par les touches du clavier
                - Un rectangle qui suit la position de la souris
                """
                pyxel.cls(0)  # Efface l'écran en le remplissant de noir (couleur 0)

                # Dessine le premier rectangle à la position (x, y) avec une taille de 8x8 pixels et la couleur 9
                pyxel.rect(v['x'], v['y'], 8, 8, 9)

                # Dessine un deuxième rectangle qui suit la position actuelle de la souris
                # `pyxel.mouse_x` et `pyxel.mouse_y` donnent les coordonnées actuelles de la souris
                pyxel.rect(pyxel.mouse_x, pyxel.mouse_y, 8, 8, 10)  # Rectangle de couleur 10

            # Démarre la boucle principale de Pyxel
            # - `update` : gère la logique
            # - `draw` : gère l'affichage
            pyxel.run(update, draw)
            ~~~


    !!! danger "Résultat attendu"

        Cliquer sur l'image pour tester l'application.

        [![alt](./assets/bonhomme.png)](./assets/d5.html){:target="_blank"} 

