---
author: F.Junier
title: Projet 4
---

!!! example "Projet 4"

    !!! warning "Niveau de difficulté"
        * Moyen
        * 80 lignes de code.
        * Boucles, tests, listes, dictionnaires, gérer des événements avec les fonctions `update` et `draw`. 

    !!! tip "Cahier des charges"

        === "Objectif"
            
            Compléter le  squelette de code fourni sur le jour [6](https://24jdpp.forge.apps.education.fr/jour.html?j=6){:target="_blank"}  du [calendrier 24 jours de pyxel de Laurent Abbal](https://24jdpp.forge.apps.education.fr/) pour faire pleuvoir aléatoirement des flocons, des boules de Noel ou des sucres d'orge. On fera apparaitre un nouvel objet toutes les 15 frames (soit deux objets par seconde) en choisissant aléatoirement parmi les trois types : flocon, boule ou sucre d'orge. 

            
            Il est interdit d'utiliser le mot clef `global`, on encapsulera les variables globales dans des dictionnaires (un pour le corps, un autre pour la tête etc ...).

           

  
        === "Prérequis"
            * Traiter les défis des jours  [3](https://24jdpp.forge.apps.education.fr/jour.html?j=3){:target="_blank"} (dessiner des disques),  [4](https://24jdpp.forge.apps.education.fr/jour.html?j=4){:target="_blank"} (gestion de la vitesse d'affichage avec `pyxel.frame_count`) et  [6](https://24jdpp.forge.apps.education.fr/jour.html?j=6){:target="_blank"}  du [calendrier 24 jours de pyxel](https://24jdpp.forge.apps.education.fr/).
  
        === "Squelette de code"

            Le squelette de code ci-dessous reprend celui fourni sur le jour [6](https://24jdpp.forge.apps.education.fr/jour.html?j=6){:target="_blank"}  du [calendrier 24 jours de pyxel](https://24jdpp.forge.apps.education.fr/) sans paramètre pour les fonctions `flocon_creation` et `flocon_deplacement`.

            ~~~python3
            # on rajoute la bibliothèque `random`
            import pyxel, random

            pyxel.init(128, 128, title="Il neige!")

            # initialisation
            v = {
                'flocons' : []
            }

            def flocon_creation():
                """création aléatoire des flocons"""

                # Un flocon toutes les 15 frames (~2 flocons par seconde si Pyxel tourne à 30 FPS)
                if (pyxel.frame_count % 15 == 0):
                    v['flocons'].append([..., 0])



            def flocon_deplacement():
                """déplacement des flocons vers le bas et suppression s'ils sortent de la fenêtre"""

                for flocon in v['flocons']:
                    ...
                    if  flocon[1]>128:
                        v['flocons'].remove(flocon)


            # =========================================================
            # == UPDATE
            # =========================================================
            def update():
                """mise à jour des variables (30 fois par seconde)"""

                # création des flocons
                flocon_creation()

                # mise à jour des positions des flocons
                flocon_deplacement()


            # =========================================================
            # == DRAW
            # =========================================================
            def draw():
                """création des objets (30 fois par seconde)"""

                # nouvelle fenêtre bleue
                pyxel.cls(5)

                # affichage des flocons
                for flocon in v['flocons']:
                    ...

            pyxel.run(update, draw)
            ~~~


    !!! danger "Résultat attendu"

        Cliquer sur l'image pour tester l'application.

        [![alt](./assets/neige.png)](./assets/d6.html){:target="_blank"} 

