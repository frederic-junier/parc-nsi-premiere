---
author: F.Junier
title: Projet 7
---

!!! example "Projet 7"

    !!! warning "Niveau de difficulté"
        * Moyen, long,  squelette de code assez détaillé
        * Plus de 100 lignes de code.
        * Boucles, tests, listes, dictionnaires, gérer des événements avec les fonctions `update` et `draw`. 

    !!! tip "Cahier des charges"

        === "Objectif"
            
            * Réaliser un jeu de type [Snake]() en respectant le cahier des charges décrit dans  le jour [11](https://24jdpp.forge.apps.education.fr/jour.html?j=11){:target="_blank"}  du [calendrier 24 jours de pyxel de Laurent Abbal](https://24jdpp.forge.apps.education.fr/). Un traîneau  doit récolter les 20 sacs de sels disséminés aléatoirement dans une grille de $16 \times 16$ cases de $8$ pixels de côté. La partie est perdue si l'attelage sort de la grille ou se croise.
            *  Quatre  états de jeu sont possibles : 
                * trois états où le jeu est figé avec un message approprié : `PAUSE`, `GAGNE`, `PERDU`. L'appui sur la touche `P`permet de lancer une nouvelle partie
                * un état `JOUE` lorsqu'une partie est en cours

          
            Il est interdit d'utiliser le mot clef `global`, on encapsulera les variables globales dans des dictionnaires (un pour le corps, un autre pour la tête etc ...).

           

  
        === "Prérequis"
            * Traiter les défis des jours  [3](https://24jdpp.forge.apps.education.fr/jour.html?j=3){:target="_blank"} (dessiner des disques),  [4](https://24jdpp.forge.apps.education.fr/jour.html?j=4){:target="_blank"} (gestion de la vitesse d'affichage avec `pyxel.frame_count`), [5](https://24jdpp.forge.apps.education.fr/jour.html?j=5){:target="_blank"} (gestion des événements  pour déplacer un objet) et [11](https://24jdpp.forge.apps.education.fr/jour.html?j=11){:target="_blank"}  du [calendrier 24 jours de pyxel](https://24jdpp.forge.apps.education.fr/).
  
        === "Squelette de code"

            Le squelette de code ci-dessous reprend celui fourni sur le jour [11](https://24jdpp.forge.apps.education.fr/jour.html?j=11){:target="_blank"}  du [calendrier 24 jours de pyxel](https://24jdpp.forge.apps.education.fr/). 

            ~~~python
            import pyxel, random

            pyxel.init(128, 128, title="Collecte de sacs de sel")

            # Variables globales dans un dictionnaire
            v = {
                'positions_attelage': [],
                'direction': (),
                'sacs': []  
            }


            def initialiser():
                """
                Initialisation des positions.
                """
                
                # Positions des parties de l'attelage. La position de la motoneige est en tête de liste.
                # La position initiale est en haut à gauche de la fenêtre
                v['positions_attelage'] = [(0, 0)]
                
                # Direction initiale (vers la droite)
                v['direction'] = (1, 0)
                
                # Positions des 20 sacs de sel (placés au hasard sauf dans la case (0, 0)) 
                for _ in range(20):
                    # à compléter


            def deplacement():
                """
                - On définit la nouvelle position de la motoneige qui est la case suivante selon le sens du mouvement
                - On évite une collision avec les traîneaux et on reste dans la fenêtre
                - On ajoute la nouvelle position de la motoneige (l'ancienne position sera considérée comme celle du premier traîneau)
                - Si la motoneige arrive sur la position d'un sac: on retire le sac de la liste et on ne retire pas la première position
                (dernier traîneau) de `positions_attelage`, ce qui revient à ajouter un élément
                - Sinon, on retire la première position de la liste `positions_attelage` (dernier traîneau)
                """
                # Déplacement toutes les 5 'frames'
                if pyxel.frame_count % 5 == 0:
                    # à compléter


            def changement_direction():
                """
                Modification du mouvement en fonction des touches utilisées.
                """
                if pyxel.btn(pyxel.KEY_UP):
                    v['direction'] = (0, -1)
                if pyxel.btn(pyxel.KEY_DOWN):
                    v['direction'] = (0, 1)
                if pyxel.btn(pyxel.KEY_LEFT):
                    v['direction'] = (-1, 0)
                if pyxel.btn(pyxel.KEY_RIGHT):
                    v['direction'] = (1, 0)


            def dessin_sacs():
                """
                Dessine les sacs de sel.
                Couleur: 9
                """
                for sac in v['sacs']:
                    pyxel.rect(sac[0] * 8, sac[1] * 8, 8, 8, 9)


            def dessin_attelage():
                """
                Dessine l'attelage (motoneige + traîneaux).
                Couleur motoneige: 5
                Couleur traîneaux: 13
                """
                # à compléter


            # Initialisation
            initialiser()   


            # =========================================================
            # == UPDATE
            # =========================================================  
            def update():
                """Mise à jour des variables (30 fois par seconde)."""
                
                # Direction
                changement_direction()
                
                # Déplacement
                deplacement()


            # =========================================================
            # == DRAW
            # =========================================================
            def draw():
                """Dessin des objets (30 fois par seconde)."""
                
                pyxel.cls(7)
                
                # Sacs de sel
                dessin_sacs()
                
                # Attelage
                dessin_attelage()


            pyxel.run(update, draw)
            ~~~


    !!! danger "Résultat attendu"

        Cliquer sur l'image pour tester l'application.

        [![alt](./assets/snake.png)](./assets/d11.html){:target="_blank"} 

