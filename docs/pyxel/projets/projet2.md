---
author: F.Junier
title: Projet 2
---

!!! example "Projet 2"

    !!! warning "Niveau de difficulté"
        * Moyen
        * 50 lignes de code.
        * Boucles, tests, listes

    !!! tip "Cahier des charges"

        === "Objectif"
            * Réaliser générateur de tableau  dans le style de [Mondrian](https://fr.wikipedia.org/wiki/Piet_Mondrian){:target="_blank"}  constitué de rectangles horizontaux ou verticaux, de bordure noire et remplis avec l'une des couleurs de sa palette `[0, 5, 7, 8 , 10]` (avec les codes couleur de [pyxel](https://fr.wikipedia.org/wiki/Piet_Mondrian)) soit `s[noir, bleu, blanc, rouge, jaune]`.

  
        === "Prérequis"
            * Traiter les défis des jours [2](https://24jdpp.forge.apps.education.fr/jour.html?j=2){:target="_blank"} et [3](https://24jdpp.forge.apps.education.fr/jour.html?j=3){:target="_blank"} du [calendrier 24 jours de pyxel](https://24jdpp.forge.apps.education.fr/).
            * 
        === "Squelette de code"

            Le squelette de code ci-dessous permet de générer aléatoirement un tableau dans le style de Mondrian avec deux rectangles verticaux ou horiziontaux. Il faut le modifier pour avoir la possibilité de générer avec un nombre arbitraire de rectangles. Voir aussi le code fourni dans le jour [3](https://24jdpp.forge.apps.education.fr/jour.html?j=3){:target="_blank"} du calendrier pyxel de Laurent Abbal pour la syntaxe du dessin de formes géométriques.

            ~~~python3
            import pyxel
            import random


            pyxel.init(128, 128, title="Mondrian")

            pyxel.cls(0)

            palette = [0, 5, 7, 8 , 10]
            de = random.randint(1, 2)
            if de == 1: # découpage vertical
                ww = random.randint(2, 126)
                pyxel.rect(0, 0, ww, 128, palette[random.randint(0, len(palette) - 1)])
                pyxel.rectb(0, 0, ww, 128, 0)
                pyxel.rect(ww, 0, 128 - ww, 128, palette[random.randint(0, len(palette) - 1)])
                pyxel.rectb(ww, 0, 128 - ww, 128, 0)
            else: # découpage horizontal
                hh = random.randint(2, 126)
                pyxel.rect(0, 0, 128, hh, palette[random.randint(0, len(palette) - 1)])
                pyxel.rectb(0, 0, 128, hh, 0)
                pyxel.rect(0, hh, 128,128 -  hh, palette[random.randint(0, len(palette) - 1)])
                pyxel.rectb(0, hh, 128, 128 - hh, 0)
                    
                
            pyxel.show()
            ~~~


    !!! danger "Résultat attendu"

        Cliquer sur l'image pour tester l'application.

        [![alt](./assets/mondrian.png)](./assets/d3.html){:target="_blank"} 

