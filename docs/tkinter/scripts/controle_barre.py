from tkinter import *   # import du module tkinter

### Constantes (valeurs non modifiables à l'execution)
LARG_BARRE = 60  # largeur de la barre en pixels
HAUT_BARRE = 20  # hauteur de la barre en pixels
DX = 10          # déplacement horizontal en pixels pour un appui sur touche
DY = 10          # déplacement vertical en pixels pour un appui sur touche
HAUT_FENETRE = 400
LARG_FENETRE = 400

### Variables globales (valeurs modifiables à l'execution)
x_barre = 160   # abscisse de la barre
y_barre = 180   # ordonnée de la barre
    

### Modele

def deplacement(event):
      """Fonction gestionnaire de l'événement
      appui sur une touche"""
      global x_barre, y_barre   # variables globales
      if event.keysym == 'Up':
            y_barre = y_barre - DY
      elif event.keysym == 'Down':
            y_barre = y_barre + DY
      # à compléter  avec les déplacements droite / gauche  
      var_locale = "Je suis une variable locale"   # ne sert a rien      
      # on modifie les coordonnées de la barre dans le widget canevas
      can.coords(barre, x_barre, y_barre, x_barre + LARG_BARRE, y_barre + HAUT_BARRE)
      # on met à jour l'affichage de la fenetre
      fen.update()
      
### Vue : interface graphique

# Fenetre racine
fen = Tk()
fen.title("Controle Barre")

# Canevas d'affichage de la barre
can = Canvas(fen, background="#000000", width=LARG_FENETRE, height=HAUT_FENETRE)
can.pack()
# Barre rectangulaire dessinée dans le canevas
barre = can.create_rectangle(x_barre, y_barre, x_barre + LARG_BARRE, y_barre + HAUT_BARRE, fill="white")
         

### Controleur

# Si l'événement "Appui sur une touche" est intercepte, on appelle la fonction deplacement
fen.bind("<KeyPress>",  deplacement)
            
        
### Boucle infinie , réceptionnaire d'evenement
fen.mainloop()
