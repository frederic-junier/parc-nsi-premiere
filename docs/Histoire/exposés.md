---
title:  Sujets d'exposés sur l'histoire de l'informatique
layout: parc
---
# Thème 1 : Représentation des données : types et valeurs de base

|Numéro|Sujet|Personne(s) à citer|Ressources|
|:---:|:---:|:---:|:---:|
|1|Georges Boole : sa logique mathématique première pierre du langage des ordinateurs|Georges Boole|[Blog binaire : qui a inventé la logique avec des 0 et des 1](https://www.lemonde.fr/blog/binaire/2017/10/04/qui-a-invente-la-logique-avec-des-0-et-des-1/)|
|2|Compression des données : tout fichier est-il compressible ?|Shannon|[article interstice compression](https://interstices.info/idee-recue-tout-est-compressible/) et [article interstice théorie de l'information](https://interstices.info/theories-et-theorie-de-linformation/)|
|3|Compression de données sans perte : l'algorithme de Huffman|Huffman||

# Thème 2 : Représentation des données : types construits

# Thème 3 : Traitement de données en tables

# Thème 4 : Interactions entre l'homme et la machine sur le Web

# Thème 5 : Architectures matérielles et systèmes d'exploitation

# Thème 6 : Langage et programmation

# Thème 7 : Algorithmique