---
title:  Chapitre 9  système d'exploitation et ligne de commandes
layout: parc
---


Cours de Frédéric Junier.


## Cours 

* [Cours version pdf](./chapitre9/cours/cours_systeme.pdf)
* [Cours version markdown](./chapitre9/cours/cours_systeme.md)



## TP Terminus

* [Enoncé (version originale de Charles Poulmaire)](./chapitre9/terminus/terminus.md)
* [Corrigé du jeu Terminus](./chapitre9/terminus/corrige/terminus.html)

  
## Memento shell

* [Memento shell version pdf](./chapitre9/memento-shell/memento-shell-.pdf)
* [Memento shell version markdown](./chapitre9/memento-shell/memento-shell-git.md)
* [Matériel  pour les exemples du memento](./chapitre9/memento-shell/sandbox.zip)


??? video  "Démonstration Memento Shell"
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="memento_shell" src="https://tube.ac-lyon.fr/videos/embed/8d78876c-204e-45a2-bb53-1c9d277b512d" frameborder="0" allowfullscreen></iframe>

## TP 2

* [Enoncé TP2 version pdf](./chapitre9/TP2/NSI-TP2-systeme-.pdf)
* [Enoncé TP2 version markdown](./chapitre9/TP2/NSI-TP2-systeme-git.md)
* [tour-du-monde-80-jours.zip](./chapitre9/TP2/ressources/tour-du-monde-80-jours.zip)
* [contacts-1000.csv](./chapitre9/TP2/ressources/contacts-1000.csv)
* [corrigé exo tour du monde](./chapitre9/TP2/ressources/corrige_exo_tour_du_monde.pdf)
* [corrigé exo carnet](./chapitre9/TP2/ressources/corrige_exo_carnet.md)


![Comment choisir son OS ?](chapitre9/cours-systeme/choix-os.jpg)


## Tutoriels vidéo 

* [Comprendre ce qu'est un système d'exploitation](https://youtu.be/SpCP2oaCx8A)
* [Présentation par Pierre Marquestaut](https://peertube.lyceeconnecte.fr/videos/watch/f5663ffb-5605-46f8-85af-b058852d9f44)